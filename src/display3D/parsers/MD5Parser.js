function MD5Parser(){
    this.utils = new OMEGA.Utils.GetInstance();

    this.parseFile = function( file_path ){
        this.utils.LoadFile( file_path, this.onFileLoaded );
    };

    this.onFileLoaded = function( data ){
        var file_data    = data.currentTarget.responseText;
        var stringParser = new OMEGA.Utils.StringParser();
        var lines        = file_data.split(LINE_FEED);
        lines.push(null);

        var index = 0;
        while((line= lines[index++]) != null) {
            stringParser.Init(line);

            var firstWord = stringParser.GetFirstWord();
            switch( firstWord ){
                case 'MD5Version':
                        this.MD5version = stringParser.words[1];
                    break;
                case 'commandline':
                        this.commandLine = stringParser.words[1];
                    break;



            }
            if(firstWord == 'MD5Version') console.log( line );
        }
    };



    this.MD5Version  = -1;
    this.commandLine = "";
    this.numJoints   = 0;
    this.numMeshes   = 0;

    var LINE_FEED = String.fromCharCode(10);
    var SPACE     = String.fromCharCode(32);

}
OMEGA.Omega3D.parsers = OMEGA.Omega3D.parsers || {};
OMEGA.Omega3D.parsers.MD5Parser = MD5Parser;
