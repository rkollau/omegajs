function FreeCamera(){
    Camera.apply(this);

    this.speed = 0.1;
    this.rotationSpeed = 0.05;
    this.drag  = 0.1;
    this.mass  = 1;
    this.force = 0;
    this.pos = new Vector4(0,0,0);
    this.vel = new Vector4(0,0,0);
    this.rot = new Vector4(0,0,0);
    this.direction = new Vector4(0, 0, 1);
    this.look      = new Vector4(0, 0, 0);
    this.right     = new Vector4( 1, 0, 0);
    this.up        = new Vector4( 0, 0, 0);


    this.enabled = true;
    this.key_listener = null;
    this.mouse_listener = null;

    this.dirX, this.dirY, this.dirZ;

    this.SetPosition = function( x, y, z ){
        this.pos.x = x;
        this.pos.y = y;
        this.pos.z = z;
    };


    this.SetKeyListener      = function(l){ this.key_listener = l; Omega3D.AddKeyListener(l);};
    this.SetMouseListener = function(l){ this.mouse_listener = l; Omega3D.AddMouseListener(l);};
    this.RemoveListener = function(){ Omega3D.RemoveKeyListener(this.listener);};
    this.GetListener = function(){ return this.listener;};
    this.SetSpeed = function(value){ this.speed = value;};
    this.update = function(){
        if(!this.enabled )return;

        this.HandleListeners();


        this.direction.x = Math.cos( this.rot.x ) * Math.sin( this.rot.y );
        this.direction.y = Math.sin( this.rot.x );
        this.direction.z = Math.cos( this.rot.x ) * Math.cos( this.rot.y );

        var halfPI = Math.PI / 2.0;
        this.right     = new Vector4( Math.sin( this.rot.y + halfPI ),0, Math.cos( this.rot.y+halfPI ));



        this.up   = Vector4.Cross( this.right, this.direction );
        this.look = Vector4.Add( this.pos, this.direction );


        this.LookAt(this.look.x, this.look.y, this.look.z, Vector4.ToArray( this.pos ) );

        this.force = 0;
    };




    this.HandleListeners = function(){
        var range = 1.0;

        //keys.
        if(this.key_listener != null ){
            if ( this.key_listener.currentlyPressedKeys[87]) {
                this.force = this.speed;
                this.pos.add( Vector4.MultiplyByValue(this.direction, this.force ) );
               // console.log( "87 : W");
            }

            if ( this.key_listener.currentlyPressedKeys[83]) {
                this.force = -this.speed;
                this.pos.add( Vector4.MultiplyByValue(this.direction, this.force ) );
               // console.log( "83 : S");
            }

            if( this.key_listener.currentlyPressedKeys[65] )
            {
                this.force = -this.speed;
                this.pos.add( Vector4.MultiplyByValue(this.right, this.force ) );
               // console.log( "65: A");
            }
            if( this.key_listener.currentlyPressedKeys[68])
            {
                this.force = this.speed;
                this.pos.add( Vector4.MultiplyByValue(this.right, this.force ) );
               // console.log( "68: D")
            }

            if (this.key_listener.currentlyPressedKeys[39] || this.key_listener.currentlyPressedKeys[102]) {

                this.force = this.speed;
                this.pos.add( Vector4.MultiplyByValue(this.right, this.force ) );

                //console.log( "39, 102 : -> ");

            }
            if (this.key_listener.currentlyPressedKeys[37]  || this.key_listener.currentlyPressedKeys[100]) {

                this.force = -this.speed;
                this.pos.add( Vector4.MultiplyByValue(this.right, this.force ) );
                //console.log( "37,  100 : <- ");
            }

            if (this.key_listener.currentlyPressedKeys[38] ||this.key_listener.currentlyPressedKeys[104]) {

                this.force = this.speed;
                this.pos.add( Vector4.MultiplyByValue(this.direction, this.force ) );
                //console.log( "38, 104 : UP");
            }
            if (this.key_listener.currentlyPressedKeys[40] ||this.key_listener.currentlyPressedKeys[98]) {

                this.force = -this.speed;
                this.pos.add( Vector4.MultiplyByValue(this.direction, this.force ) );
                // console.log( "40, 98 : DOWN");
            }
        }

       //mouse.
        if(this.mouse_listener != null ){

            if(Math.abs((this.mouse_listener.mouse.x - ( this.mouse_listener.targetSize.x / 2.0 ))) > 25.0 ) {
                this.rot.y += (this.mouse_listener.mouse.x - ( this.mouse_listener.targetSize.x / 2.0 )) / ( this.mouse_listener.targetSize.x * 15.0);
            }
            if(this.rot.x <= range && this.rot.x >= -range ){
                if(Math.abs((this.mouse_listener.mouse.y - ( this.mouse_listener.targetSize.y / 2.0 ))) > 25.0 ){
                    this.rot.x += (this.mouse_listener.mouse.y - ( this.mouse_listener.targetSize.y / 2.0 )) / ( this.mouse_listener.targetSize.y *20.0);
                }
            }

            else if(this.rot.x > range ) this.rot.x = range;
            else if(this.rot.x < -range ) this.rot.x = -range;
        }
    };

    this.Enable = function(){
        if(this.enabled) return;
        if(this.listener) this.listener.Enable();
        this.enabled = true;
    };
    this.Disable = function(){
        if(!this.enabled)return;
        if(this.listener) this.listener.Disable();
        this.enabled = false;

    };

    this.Enable();






};
FreeCamera.prototype = new Camera();
OMEGA.Omega3D.cameras = OMEGA.Omega3D.cameras || {};
OMEGA.Omega3D.cameras.FreeCamera = FreeCamera;



