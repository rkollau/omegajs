function Camera( projection_type ){
    Object3D.apply(this);
    this.type = projection_type || OMEGA.Omega3D.cameras.Camera.TYPE.PROJECTION;
    this.projectionMatrix = mat4.create();
    this.laMatrix = mat4.create();

    this.fov = 45;
    this.near = 0.1;
    this.far  = 1000.0;
    this.ratio = "DEFAULT";

    this.x = this.y = this.z = 0;
    this.lookX = this.lookY = this.lookZ = 0;

    this.GetProjectionMatrix = function(){
        if(this.type == OMEGA.Omega3D.cameras.Camera.TYPE.PROJECTION)
            mat4.perspective(this.projectionMatrix,this.fov, this.ratio == "DEFAULT" ? OMEGA.Omega3D.GL.viewPortWidth / OMEGA.Omega3D.GL.viewPortHeight : this.ratio,  this.near, this.far );
        else if(this.type == OMEGA.Omega3D.cameras.Camera.TYPE.ORTHO)
            mat4.ortho(this.projectionMatrix,this.position[2]/2.0, -this.position[2]/2.0, -this.position[2]/2.0, this.position[2]/2.0 ,this.near, this.far );

        return this.projectionMatrix;
    };
    this.GetMatrix    = function(){
        mat4.identity(this.modelView);
        mat4.multiply(this.modelView, this.rMatrix, this.tMatrix);
        mat4.multiply(this.modelView, this.modelView, this.sMatrix);
        mat4.multiply(this.modelView, this.modelView, this.laMatrix);
        return this.modelView;
    };


    this.GetInverseMatrix = function(){
        var invViewMatrix = mat4.create();
        mat4.invert(invViewMatrix, this.GetMatrix() );
        //mat4.transpose(invViewMatrix, invViewMatrix);
        return invViewMatrix;
    };

    this.update = function(){ this.LookAt(this.lookX, this.lookY, this.lookZ, [this.x, this.y, this.z]); };
    this.LookAt = function( x, y, z, position ){
        this.lookX =x; this.lookY =y;this.lookZ =z;
        this.x = position[0]; this.y = position[1]; this.z = position[2];
        this.position = [this.x, this.y, this.z];
        mat4.identity(this.laMatrix);
        mat4.lookAt(this.laMatrix,position || this.position,[this.lookX,this.lookY,this.lookZ],  [0,-1,0]);
    };
    this.LookAtWithUp = function( x, y, z, position, up ){
        this.lookX =x; this.lookY =y;this.lookZ =z;
        this.x = position[0]; this.y = position[1]; this.z = position[2];
        mat4.identity(this.laMatrix);
        mat4.lookAt(this.laMatrix,position || this.position,[this.lookX,this.lookY,this.lookZ], up );
    };

};
Camera.prototype = new Object3D();
OMEGA.Omega3D.cameras = OMEGA.Omega3D.cameras || {};
OMEGA.Omega3D.cameras.Camera = Camera;
OMEGA.Omega3D.cameras.Camera.TYPE = OMEGA.Omega3D.cameras.Camera.TYPE || {};
OMEGA.Omega3D.cameras.Camera.TYPE.PEREPCTVE = 0;
OMEGA.Omega3D.cameras.Camera.TYPE.ORTHO = 1;


