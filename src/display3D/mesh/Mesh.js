function Mesh( geom ){
    this.combinedBuffer;
    this.vertexBuffer;
    this.normalBuffer;
    this.indexBuffer;
    this.uvBuffer;
    this.tangentBuffer;
    this.bitangentBuffer;
    this.colorPickingBuffer;

    this.combined = new Array();
    this.geometry = geom;
    this.gl = OMEGA.Omega3D.GL;
    this.GetCombinedBuffer = function(){ return this.combinedBuffer;};
    this.GetVertexBuffer = function(){ return this.vertexBuffer;};
    this.GetUVBuffer     = function(){ return this.uvBuffer;    };
    this.GetIndexBuffer  = function(){ return this.indexBuffer; };
    this.GetNormalBuffer = function(){ return this.normalBuffer;};
    this.GetTangentBuffer = function(){ return this.tangentBuffer;};
    this.GetBitangentBuffer = function(){ return this.bitangentBuffer;};
    this.GetColorPickingBuffer = function(){ return this.colorPickingBuffer;};
    this.GetIndexCount   = function(){ return this.geometry.GetIndexes() / 3;};
    this.GetGeometry     = function(){ return this.geometry; };

    this.ApplyGeometry   = function(geom){
        this.geometry = geom;
        if(this.geometry.isDirty)  this.CreateBuffers();
    };


    this.CreateBuffers = function(){
        this.combined  = new Array();
        var vertices   = this.geometry.GetVertices();
        var normals    = this.geometry.GetNormals();
        var uvs        = this.geometry.GetUVS();
        var colors     = this.geometry.GetColors();
        var tangents   = this.geometry.tangents;
        var bitangents = this.geometry.bitangents;

        var uvCounter = 0; //because uv = 2 component vector.
        for( var i = 0; i < vertices.length; i+=3){
            this.combined.push( vertices[i], vertices[i+1], vertices[i+2] );                          // 0, 1, 2
            if(normals   !=null) this.combined.push( normals[i] , normals[i+1] , normals[i+2] );      // 3, 4, 5
            if(uvs       !=null) this.combined.push( uvs[uvCounter], uvs[uvCounter+1] );              // 6, 7
            if(tangents  !=null) this.combined.push( tangents[i],tangents[i+1],tangents[i+2] );       // 8, 9, 10
            if(bitangents!=null) this.combined.push( bitangents[i],bitangents[i+1],bitangents[i+2] ); // 11, 12, 13
            if(colors    !=null) this.combined.push( colors[i], colors[i+1], colors[i+2] );           // 14, 15, 16

            uvCounter+=2;
            if( uvCounter >= vertices.length ) uvCounter = 0;
        }




        this.combinedBuffer = null;
        this.combinedBuffer = this.gl.createBuffer();
        this.gl.bindBuffer( this.gl.ARRAY_BUFFER, this.combinedBuffer );
        this.gl.bufferData( this.gl.ARRAY_BUFFER, new Float32Array( this.combined ), this.gl.STATIC_DRAW );



        /*INDEX BUFFER*/
        if( this.geometry.GetIndexes().length > 0) {
            if(!this.indexBuffer) this.indexBuffer = this.gl.createBuffer();
            this.gl.bindBuffer(this.gl.ELEMENT_ARRAY_BUFFER, this.indexBuffer);
            this.gl.bufferData(this.gl.ELEMENT_ARRAY_BUFFER, new Uint16Array(this.geometry.GetIndexes()), this.gl.STATIC_DRAW);
            this.indexBuffer.itemSize = 1;
            this.indexBuffer.numItems = this.geometry.GetIndexes().length;
        }
        this.geometry.isDirty = false;

    };
    this.CreateColorPickingBuffer = function( objectID ){
        if( parseFloat(objectID) > -1) {
            var colorData = new Array();
            var step = 1.0 / 255.0;
            var fID = parseFloat(objectID);
            var size = 255.0, r255 = 0, g255 = 0, b255 = 0;
            r255 = fID <= size ? fID : size;
            if(fID > (size * 1.0 )) g255 = fID - r255;        // bigger than: 255
            if(fID > (size * 2.0 )) b255 = fID - (r255+g255); // bigger than: 255 + 255


            for(var i = 0; i < this.geometry.GetVertices().length; i++){
                colorData.push(step * r255,step * g255, step * b255 );
            }
            if(!this.colorPickingBuffer) this.colorPickingBuffer = this.gl.createBuffer();
            this.gl.bindBuffer(this.gl.ARRAY_BUFFER, this.colorPickingBuffer);
            this.gl.bufferData(this.gl.ARRAY_BUFFER, new Float32Array(colorData), this.gl.STATIC_DRAW);
            this.colorPickingBuffer.itemSize = 3;
            this.colorPickingBuffer.numItems = colorData.length / this.colorPickingBuffer.itemSize;
        }
    };



    this.SetVertices = function( vertices ){
        var count = 0;
        for( var i = 0; i < this.combined.length; i+=17){
            this.combined[i] = vertices[count];
            this.combined[i+1] = vertices[count+1];
            this.combined[i+2] = vertices[count+2];
            count+=3;
        }
        this.gl.bindBuffer( this.gl.ARRAY_BUFFER, this.combinedBuffer );
        this.gl.bufferData( this.gl.ARRAY_BUFFER, new Float32Array( this.combined ), this.gl.STATIC_DRAW );
        this.gl.bindBuffer( this.gl.ARRAY_BUFFER, null );
    };
    this.SetColor = function( color ){
        for( var i = 0; i < this.combined.length; i+=17){
            this.combined[i+14] = color[0];
            this.combined[i+15] = color[1];
            this.combined[i+16] = color[2];
        }
        this.gl.bindBuffer( this.gl.ARRAY_BUFFER, this.combinedBuffer );
        this.gl.bufferData( this.gl.ARRAY_BUFFER, new Float32Array( this.combined ), this.gl.STATIC_DRAW );
        this.gl.bindBuffer( this.gl.ARRAY_BUFFER, null );
    };

    this.Update = function(){
        if(this.geometry.isDirty){
            this.CreateBuffers();
        }
    }
};
OMEGA.Omega3D.Mesh = Mesh;

