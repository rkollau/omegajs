function SquareGeometry(width, height,segW, segH){
    Geometry.apply( this );
    var halfScaleW = width || 1.0 ;
    var halfScaleH = height || 1.0 ;
    var segW = segW || 150;
    var segH = segH || 150;
    var stepW = width*2 / (segW-1);
    var stepH = height*2 / (segH-1)  ;

    var temp_verts, temp_uvs, temp_normals, temp_tan, temp_bitan;
    temp_verts = new Array();temp_uvs = new Array();temp_normals = new Array();
    temp_tan = new Array();temp_bitan = new Array();
    for( var i = 0; i < segH; i++){
        for( var j = 0; j < segW; j++){
            var xpos =  halfScaleW - (j * stepW);
            var ypos = halfScaleH - (i * stepH);
            var zpos =  0.0;

            var vec    = new Vector4( xpos, ypos, zpos );
            var uv     = new Vector2(1.0-j / (segW - 1),1.0- i / (segH - 1) );
            var normal = new Vector4( 0,0,-1 );

            temp_verts.push( vec );
            temp_normals.push( normal );
            temp_uvs.push( uv );

            this.colors.push( 1.0 );
            this.colors.push( 1.0 );
            this.colors.push( 1.0 );
        }
    }

    /*INDEXES.*/
    OMEGA.Omega3D.VBOUtil.ComputeIndices( segW, segH, this.indexes );

    /*TANGENTS, BITANGENTS */
    OMEGA.Omega3D.VBOUtil.ComputeTangentBasis( temp_verts, temp_uvs,  this.indexes, //in
                                               temp_tan, temp_bitan);               //out



    OMEGA.Omega3D.VBOUtil.IndexTB( temp_verts, temp_uvs, temp_normals, temp_tan, temp_bitan,                                      //in
                                   this.vertices_raw, this.uvs_raw, this.normals_raw,this.tangents_raw, this.bitangents_raw  );   //out


    OMEGA.Omega3D.VBOUtil.FlattenMeshDataTB( temp_verts, temp_uvs, temp_normals, temp_tan,temp_bitan,                //in
                                             this.vertices,this.uvs, this.normals, this.tangents, this.bitangents ); //out


    /*INDEXES.*/
    OMEGA.Omega3D.VBOUtil.ComputeIndices( segW, segH, this.indexes );

    /* FACES */
    Geometry.ComputeFaces( this.vertices, this.indexes, this.faces );

   // OMEGA.Omega3D.Log("GEOMETRY : square created");
};
SquareGeometry.prototype = new Geometry();
OMEGA.Omega3D.Geometry.SquareGeometry = SquareGeometry;

