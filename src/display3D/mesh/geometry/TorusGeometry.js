function TorusGeometry( radius, tube_radius, segW, segH ){
    Geometry.apply( this );

    var segW     = segW || 40;
    var segH     = segH || 50;
    var t_radius  = radius || this.scale;
    var c_radius  = tube_radius || t_radius/2;
    var xPos, yPos, zPos, xNorm, yNorm, zNorm;
    var outerAngle = 0;
    var innerAngle = 0;

    var temp_verts, temp_uvs, temp_normals, temp_tan, temp_bitan;
    temp_verts = new Array();temp_uvs = new Array();temp_normals = new Array();
    temp_tan = new Array();temp_bitan = new Array();

    for (var i = 0; i < segW; i++) {
        outerAngle = (((Math.PI *2) / (segW-1)) * i);
        for (var j = 0; j < segH; j++) {
            innerAngle =  (((Math.PI *2) / (segH-1)) * j);

            xPos = (Math.cos(outerAngle) * (Math.cos(innerAngle) * c_radius - t_radius));
            yPos = (Math.sin(outerAngle) * (Math.cos(innerAngle) * c_radius- t_radius));
            zPos = (Math.sin(innerAngle) *  c_radius);



            xNorm = (Math.cos(outerAngle ) * Math.cos(innerAngle ));
            yNorm = (Math.sin(outerAngle ) * Math.cos(innerAngle));
            zNorm = (Math.sin(innerAngle  ));

            var vec    = new Vector4( -xPos, -yPos, -zPos );
            var uv     = new Vector2(j / (segH - 1), i / (segW - 1) );
            var normal = new Vector4( xNorm,yNorm,zNorm);
            temp_verts.push( vec );
            temp_normals.push( normal );
            temp_uvs.push( uv );

            this.colors.push( 1.0 );
            this.colors.push( 1.0 );
            this.colors.push( 1.0 );
        }
    }

    /*INDEXES.*/
    OMEGA.Omega3D.VBOUtil.ComputeIndices( segH, segW, this.indexes );

    /*TANGENTS, BITANGENTS */
    OMEGA.Omega3D.VBOUtil.ComputeTangentBasis( temp_verts, temp_uvs, this.indexes,  //in
                                                 temp_tan, temp_bitan);             //out

   // console.log( temp_bitan );

    OMEGA.Omega3D.VBOUtil.IndexTB( temp_verts, temp_uvs, temp_normals, temp_tan, temp_bitan,                                      //in
                                   this.vertices_raw, this.uvs_raw, this.normals_raw,this.tangents_raw, this.bitangents_raw  );   //out


    OMEGA.Omega3D.VBOUtil.FlattenMeshDataTB( temp_verts, temp_uvs, temp_normals, temp_tan,temp_bitan,                //in
                                             this.vertices,this.uvs, this.normals, this.tangents, this.bitangents ); //out



    OMEGA.Omega3D.VBOUtil.ComputeIndices( segH, segW, this.indexes );

    /* FACES */
    Geometry.ComputeFaces( this.vertices, this.indexes, this.faces );

    //OMEGA.Omega3D.Log("GEOMETRY : torus created");
};
TorusGeometry.prototype = new Geometry();
OMEGA.Omega3D.Geometry.TorusGeometry = TorusGeometry;