function CubeGeometry(width, height, depth){
    Geometry.apply( this );
    var halfScaleW = width/2  || this.scale;
    var halfScaleH = height/2 || halfScaleW || this.scale;
    var halfScaleD = depth/2  || halfScaleW || this.scale;

    var temp_verts, temp_uvs, temp_normals, temp_tan, temp_bitan;
    temp_verts = new Array();temp_uvs = new Array();temp_normals = new Array();
    temp_tan = new Array();temp_bitan = new Array();

    temp_verts = [
        /* Front face*/
            new Vector4(-1.0*halfScaleW, -1.0*halfScaleH,  -1.0*halfScaleD),
            new Vector4(1.0*halfScaleW, -1.0*halfScaleH,  -1.0*halfScaleD),
            new Vector4(1.0*halfScaleW,  1.0*halfScaleH,  -1.0*halfScaleD),
            new Vector4(-1.0*halfScaleW,  1.0*halfScaleH, - 1.0*halfScaleD),

        /* Back face*/
            new Vector4(-1.0*halfScaleW, -1.0*halfScaleH, 1.0*halfScaleD),
            new Vector4(-1.0*halfScaleW,  1.0*halfScaleH, 1.0*halfScaleD),
            new Vector4(1.0*halfScaleW,  1.0*halfScaleH, 1.0*halfScaleD),
            new Vector4(1.0*halfScaleW, -1.0*halfScaleH, 1.0*halfScaleD),

        /* Top face*/
            new Vector4(-1.0*halfScaleW,  -1.0*halfScaleH, -1.0*halfScaleD),
            new Vector4(-1.0*halfScaleW,  -1.0*halfScaleH,  1.0*halfScaleD),
            new Vector4(1.0*halfScaleW, - 1.0*halfScaleH,  1.0*halfScaleD),
            new Vector4(1.0*halfScaleW,  -1.0*halfScaleH, -1.0*halfScaleD),

        /* Bottom face*/
            new Vector4(-1.0*halfScaleW, 1.0*halfScaleH, -1.0*halfScaleD),
            new Vector4(1.0*halfScaleW, 1.0*halfScaleH, -1.0*halfScaleD),
            new Vector4(1.0*halfScaleW, 1.0*halfScaleH,  1.0*halfScaleD),
            new Vector4(-1.0*halfScaleW, 1.0*halfScaleH,  1.0*halfScaleD),

        /* Right face*/
            new Vector4(-1.0*halfScaleW, -1.0*halfScaleH, -1.0*halfScaleD),
            new Vector4(-1.0*halfScaleW,  1.0*halfScaleH, -1.0*halfScaleD),
            new Vector4(-1.0*halfScaleW,  1.0*halfScaleH,  1.0*halfScaleD),
            new Vector4(-1.0*halfScaleW, -1.0*halfScaleH,  1.0*halfScaleD),

        /* Left face*/
            new Vector4(1.0*halfScaleW, -1.0*halfScaleH, -1.0*halfScaleD),
            new Vector4(1.0*halfScaleW, -1.0*halfScaleH,  1.0*halfScaleD),
            new Vector4(1.0*halfScaleW,  1.0*halfScaleH,  1.0*halfScaleD),
            new Vector4(1.0*halfScaleW,  1.0*halfScaleH, -1.0*halfScaleD)
    ];



    temp_normals = [
        new Vector4(0, 0, 1),
        new Vector4(0, 0, 1),
        new Vector4(0, 0, 1),
        new Vector4(0, 0, 1),

        new Vector4(0, 0, -1),
        new Vector4(0, 0, -1),
        new Vector4(0, 0, -1),
        new Vector4(0, 0, -1),

        new Vector4(0, 1,  0),
        new Vector4(0, 1,  0),
        new Vector4(0, 1,  0),
        new Vector4(0, 1,  0),

        new Vector4(0, -1,  0),
        new Vector4(0, -1,  0),
        new Vector4(0, -1,  0),
        new Vector4(0, -1,  0),

        new Vector4(-1,0,  0),
        new Vector4(-1,0,  0),
        new Vector4(-1,0,  0),
        new Vector4(-1,0,  0),

        new Vector4(1,0,  0),
        new Vector4(1,0,  0),
        new Vector4(1,0,  0),
        new Vector4(1,0, 0)
    ];


    this.indexes = [
        0, 1, 2,      0, 2, 3,    /* Front face */
        4, 5, 6,      4, 6, 7,    /* Back face */
        8, 9, 10,     8, 10, 11,  /* Top face */
        12, 13, 14,   12, 14, 15, /* Bottom face */
        16, 17, 18,   16, 18, 19, /* Right face */
        20, 21, 22,   20, 22, 23  /* Left face */
    ];

    temp_uvs = [
        /* Front face */
        new Vector2(1.0, 0.0),
        new Vector2(0.0, 0.0),
        new Vector2(0.0, 1.0),
        new Vector2(1.0, 1.0),

        /* Back face */
        new Vector2(0.0, 0.0),                 //   0.0 -- 1.0
        new Vector2(0.0, 1.0),                 //    |  \    |
        new Vector2(1.0, 1.0),                 //    |    \  |
        new Vector2(1.0, 0.0),                 //   0.1 --- 1.1

        /* Bottom face */
        new Vector2(1.0, 1.0),
        new Vector2(1.0, 0.0),
        new Vector2(0.0, 0.0),
        new Vector2(0.0, 1.0),

        /* Top face */
        new Vector2(0.0, 1.0),
        new Vector2(1.0, 1.0),
        new Vector2(1.0, 0.0),
        new Vector2(0.0, 0.0),

        /* Right face */
        new Vector2(0.0, 0.0),
        new Vector2(0.0, 1.0),
        new Vector2(1.0, 1.0),
        new Vector2(1.0, 0.0),

        /* Left face */
        new Vector2(1.0, 0.0),
        new Vector2(0.0, 0.0),
        new Vector2(0.0, 1.0),
        new Vector2(1.0, 1.0)
    ];

    for( var i = 0; i < this.vertices.length; i++){
        this.colors.push( 1.0 );
    }


    /*TANGENTS, BITANGENTS */
    OMEGA.Omega3D.VBOUtil.ComputeTangentBasis( temp_verts, temp_uvs, this.indexes,  //in
                                               temp_tan, temp_bitan);               //out

    OMEGA.Omega3D.VBOUtil.IndexTB( temp_verts, temp_uvs, temp_normals, temp_tan, temp_bitan,                                      //in
        this.vertices_raw, this.uvs_raw, this.normals_raw,this.tangents_raw, this.bitangents_raw  );   //out

    OMEGA.Omega3D.VBOUtil.FlattenMeshDataTB( temp_verts, temp_uvs, temp_normals, temp_tan,temp_bitan,                //in
        this.vertices,this.uvs, this.normals, this.tangents, this.bitangents ); //out

    /*INDEXES.*/
    //OMEGA.Omega3D.VBOUtil.ComputeIndices( 6, 6, this.indexes );


    /* FACES */
    Geometry.ComputeFaces( this.vertices, this.indexes, this.faces );


    //OMEGA.Omega3D.Log("GEOMETRY : cube created");

};
CubeGeometry.prototype = new Geometry();
OMEGA.Omega3D.Geometry.CubeGeometry = CubeGeometry;

