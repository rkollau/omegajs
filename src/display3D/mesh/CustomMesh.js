function CustomMesh( geom ){
    Mesh.apply(this, [geom]);

    this.AddSegment = function(vertex_a, vertex_b  ) {
        var normal = new Vector4(0, 0, 1);
        var uv = new Vector2(0, 0);
        var tangent = new Vector4(0, 0, 0);
        var bitangent = new Vector4(0, 0, 0);
        var baricentric = new Vector4(0, 1, 0);

        this.combined.push(  vertex_a.x, vertex_a.y, vertex_a.z,
                        normal.x, normal.y, normal.z,
                        uv.x, uv.y,
                        tangent.x, tangent.y, tangent.z,
                        bitangent.x, bitangent.y, bitangent.z,
                        baricentric.x, baricentric.y, baricentric.z);

        this.combined.push(  vertex_b.x, vertex_b.y, vertex_b.z,
                        normal.x, normal.y, normal.z,
                        uv.x, uv.y,
                        tangent.x, tangent.y, tangent.z,
                        bitangent.x, bitangent.y, bitangent.z,
                        baricentric.x, baricentric.y, baricentric.z);

        this.geometry.GetVertices().push( vertex_a.x, vertex_a.y, vertex_a.z );
        this.geometry.GetVertices().push( vertex_b.x, vertex_b.y, vertex_b.z );

        this.combinedBuffer = null;
        this.combinedBuffer = this.gl.createBuffer();
        this.gl.bindBuffer(this.gl.ARRAY_BUFFER, this.combinedBuffer);
        this.gl.bufferData(this.gl.ARRAY_BUFFER, new Float32Array(this.combined), this.gl.STATIC_DRAW);
    }

};
CustomMesh.prototype = new Mesh();
OMEGA.Omega3D.CustomMesh = CustomMesh;
