OMEGA.Omega3D.MathUtils = {};
OMEGA.Omega3D.MathUtils.ProjectToScreen = function( object, camera ){
   var mat = mat4.create();
   mat4.multiply( mat, camera.GetMatrix()   , object.GetMatrix() );
   mat4.multiply( mat, camera.GetProjectionMatrix(), mat );
   
   //a00 = a[0]; a01 = a[1]; a02 = a[2]; a03 = a[3];
   //a10 = a[4]; a11 = a[5]; a12 = a[6]; a13 = a[7];
   //a20 = a[8]; a21 = a[9]; a22 = a[10]; a23 = a[11];
   //a30 = a[12]; a21 = a[13]; a22 = a[14]; a23 = a[15];
   
   var c = mat[15]; // mat[3][3] -> last value.
   var translatedPosition = new Vector4( mat[12] / c, mat[13] / c, mat[14] / c );
    
    //multiply scalar.
   translatedPosition.multiplyScalar( 0.5 );
    
    //add scalar.
    translatedPosition.addScalar( 0.5 );
 
    //return screen pos.
    return translatedPosition;
};

