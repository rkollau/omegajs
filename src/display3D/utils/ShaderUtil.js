OMEGA.Omega3D.ShaderUtil = OMEGA.Omega3D.ShaderUtil || {};
OMEGA.Omega3D.ShaderUtil.InjectGLSLForLights02 = function(vs, fs, uniforms, scene ){
    var lights = scene.getLights();
    if(lights==null) return { vs: vs, fs:fs, uniforms:uniforms};
    var vs_lightSource = "";
    var fs_lightSource = "";



    if(lights.length == 0) return { vs: vs, fs:fs, uniforms:uniforms};
    for( var index in lights ){

        //get light vertex and fragment shader components.
        vs_lightSource += lights[index].vs;
        fs_lightSource += lights[index].fs;

        //add light uniforms to standard uniforms.
        for( var key in lights[index].uniforms ){
            uniforms[key] =  lights[index].uniforms[key];
        }
    }

    //first we inject uniforms and methods at start of page.
    var start = vs.indexOf("void main");
    start = start < 0 ? 0 : start;
    vs = vs.splice(start, 0, vs_lightSource );//vs_lightSource + vs;

    //second we modify the main function so that our light calculations are called.
    var header = vs.match(/(void main\(\)|void main\(void\))(\{|\s\{)?/g);
    var modHeader = header;
    modHeader = modHeader + "vTNormal    = vec3( uModelMatrix * vec4(aVertexNormal, 0.0) );";
    modHeader = modHeader + "vViewMatrix = uViewMatrix;";



    var tick = true;
    for(var index in lights){
        if(lights[index].type != OMEGA.Omega3D.DIRECTIONAL_LIGHT ) {
            if(tick){
                modHeader = modHeader + "vMVertexPos = uModelMatrix * vec4(aVertexPos.xyz, 1.0);";
                tick = false;
            }//modHeader = modHeader + "vLightWeight += light" + index + "(injctd_tNormal);";
        }else{// modHeader = modHeader + "vLightWeight += light" + index + "(injctd_tNormal);";
         }
    }
    vs = vs.replace(/(void main\(\)|void main\(void\))(\{|\s\{)?/g, modHeader);

    //third we inject the fragment source.
    fs = fs.splice(fs.indexOf("void main"),0, fs_lightSource);//fs_lightSource + fs;

    //adjust main for light methods.
    header = fs.match(/(void main\(\)|void main\(void\))(\{|\s\{)?/g);
    modHeader = header;
    modHeader = modHeader + "vec3 vLightWeight = vec3(0, 0, 0);";
    //for(var index in lights) {
    //    modHeader = modHeader + "vLightWeight += light" + index + "( );";
    //}
    fs = fs.replace(/(void main\(\)|void main\(void\))(\{|\s\{)?/g, modHeader);


    //fourth modify the gl fragColor.sd
    //TODO: find smarter way to do this instead of forcing gl_FragColor = vec4(color.rgb, color.a); template.
    var inj = "";
    for(var index in lights) {
        inj = inj + "vLightWeight += light" + index + "( color.rgb);";
    }
    inj = inj + "color = vec4( vLightWeight.rgb, color.a);";
    fs = fs.splice(fs.indexOf("gl_FragColor"),0, inj);//fs_lightSource + fs;

    //var footer = fs.match(/gl_FragColor.+;/g);
    //var modFooter =footer;
    //for(var index in lights) {
    //    modFooter = modFooter + "vLightWeight += light" + index + "( color.rgb);";
    //}
    //modFooter = modFooter + "color = vec4( vLightWeight.rgb, color.a);"+
    //                        "gl_FragColor = color;";
    //fs = fs.replace(/gl_FragColor.+;/g, modFooter );
    //console.log( fs );
    //OMEGA.Omega3D.Log(" >> ShaderUtil: Injected " + lights.length + " lights into Shader.");
    return { vs: vs, fs:fs, uniforms:uniforms};
};
OMEGA.Omega3D.ShaderUtil.InjectGLSLForLights = function(vs, fs, uniforms, scene ){

    var lights = scene.getLights();
    if(lights==null || lights.length == 0) return { vs: vs, fs:fs, uniforms:uniforms};

    var lightsArray = "uniform Light lights["+lights.length+"];";
    var varying =[
        "varying vec3 vTNormal;",
        "varying vec4 vMVertexPos;",
        "varying mat4 vViewMatrix;"
    ].join("\n");

    var vs_lightSource = varying;
    var fs_lightSourceStart  = OMEGA.Omega3D.Shaders.Components.Struct_Light() + "\n" + lightsArray + "\n" +varying +"\n";
    var fs_lightSourceMiddle = OMEGA.Omega3D.Shaders.Components.CalcLightAddition()+"\n";


    for( var index in lights ){
        uniforms["lights["+index+"].mat"]  = 0;
        uniforms["lights["+index+"].position"]  = 0;
        uniforms["lights["+index+"].direction"] = 0;
        uniforms["lights["+index+"].ambient"]   = 0;
        uniforms["lights["+index+"].diffuse"]   = 0;
        uniforms["lights["+index+"].specular"]  = 0;
        uniforms["lights["+index+"].radius"]    = 0;
        uniforms["lights["+index+"].cutOff"]    = 0;
        uniforms["lights["+index+"].type"]      = 0;
        uniforms["lights["+index+"].enabled"]   = 0;
    }


    //first we inject uniforms and methods at start of page.
    var start = vs.indexOf("void main");
    start = start < 0 ? 0 : start;
    vs = vs.splice(start, 0, vs_lightSource );//vs_lightSource + vs;

    //second we modify the main function so that our light calculations are called.
    var header = vs.match(/(void main\(\)|void main\(void\))(\{|\s\{)?/g);
    var modHeader = header;
    modHeader = modHeader + "vTNormal    = vec3( uModelMatrix * vec4(aVertexNormal, 0.0) );";
    modHeader = modHeader + "vViewMatrix = uViewMatrix;";

    if(lights[index].type != OMEGA.Omega3D.DIRECTIONAL_LIGHT )
        modHeader = modHeader + "vMVertexPos = uModelMatrix * vec4(aVertexPos.xyz, 1.0);";

    vs = vs.replace(/(void main\(\)|void main\(void\))(\{|\s\{)?/g, modHeader);


    //third we inject the fragment source.
    fs = fs.splice(fs.indexOf(";") +1,0, "\n"+fs_lightSourceStart);//fs_lightSource + fs;
    fs = fs.splice(fs.indexOf("void main"),0, fs_lightSourceMiddle);//fs_lightSource + fs;

    //adjust main for light methods.
    header = fs.match(/(void main\(\)|void main\(void\))(\{|\s\{)?/g);
    modHeader = header;
    modHeader = modHeader + "vec3 vLightWeight = vec3(0, 0, 0);";
    fs = fs.replace(/(void main\(\)|void main\(void\))(\{|\s\{)?/g, modHeader);


    //fourth modify the gl fragColor.sd
    //TODO: find smarter way to do this instead of forcing gl_FragColor = vec4(color.rgb, color.a); template.
    var inj = "";
    for(var index in lights) {
        inj = inj + "vLightWeight += CalcLightAddition( color.rgb, lights[" + index + "], vec3(vViewMatrix[3].xyz) );";
    }
    inj = inj + "\ncolor = vec4( min(vLightWeight.rgb, 1.0), color.a);\n";
    fs = fs.splice(fs.indexOf("gl_FragColor"),0, inj);//fs_lightSource + fs;


    return { vs: vs, fs:fs, uniforms:uniforms};
};
OMEGA.Omega3D.ShaderUtil.InjectGLSLForShadows = function(vs, fs, uniforms, scene ){

    var lights = scene.getLights();
    if(lights==null || lights.length == 0) return { vs: vs, fs:fs, uniforms:uniforms};

    var lightsArray = "uniform Light lights["+lights.length+"];";
    var varying =[
        "varying vec4 vertexPosLight;",
    ].join("\n");

    var vs_lightSource = varying;
    var fs_lightSource =  varying + OMEGA.Omega3D.Shaders.Components.CalcLightAddition();



    //first we inject uniforms and methods at start of page.
    var start = vs.indexOf("void main");
    start = start < 0 ? 0 : start;
    vs = vs.splice(start, 0, vs_lightSource );//vs_lightSource + vs;

    //second we modify the main function so that our light calculations are called.
    var header = vs.match(/(void main\(\)|void main\(void\))(\{|\s\{)?/g);
    var modHeader = header;
    modHeader = modHeader + "vTNormal    = vec3( uModelMatrix * vec4(aVertexNormal, 0.0) );";
    modHeader = modHeader + "vViewMatrix = uViewMatrix;";

    if(lights[index].type != OMEGA.Omega3D.DIRECTIONAL_LIGHT )
        modHeader = modHeader + "vMVertexPos = uModelMatrix * vec4(aVertexPos.xyz, 1.0);";

    vs = vs.replace(/(void main\(\)|void main\(void\))(\{|\s\{)?/g, modHeader);


    //third we inject the fragment source.
    fs = fs.splice(fs.indexOf("void main"),0, fs_lightSource);//fs_lightSource + fs;

    //adjust main for light methods.
    header = fs.match(/(void main\(\)|void main\(void\))(\{|\s\{)?/g);
    modHeader = header;
    modHeader = modHeader + "vec3 vLightWeight = vec3(0, 0, 0);";
    fs = fs.replace(/(void main\(\)|void main\(void\))(\{|\s\{)?/g, modHeader);


    //fourth modify the gl fragColor.sd
    //TODO: find smarter way to do this instead of forcing gl_FragColor = vec4(color.rgb, color.a); template.
    var inj = "";
    for(var index in lights) {
        inj = inj + "vLightWeight += CalcLightAddition( color.rgb, lights[" + index + "], vec3(vViewMatrix[3].xyz) );";
    }
    inj = inj + "color = vec4( min(vLightWeight.rgb, 1.0), color.a);";
    fs = fs.splice(fs.indexOf("gl_FragColor"),0, inj);//fs_lightSource + fs;


    return { vs: vs, fs:fs, uniforms:uniforms};
};
OMEGA.Omega3D.ShaderUtil.InjectGLSLForFog = function(vs, fs, uniforms, scene ){

    var fog_uniforms = "uniform vec3 uFogColor;"+
                       "uniform vec2 uFogDist;";

    uniforms["uFogColor"] = { id:"uFogColor", type: "vec3", glsl: "uniform vec3 uFogColor;", value: [scene.getColor().r, scene.getColor().g, scene.getColor().b ] };
    uniforms["uFogDist" ] = { id:"uFogDist" , type: "vec2", glsl: "uniform vec3 uFogDist;" , value: [scene.getColor().r, scene.getColor().g, scene.getColor().b ] };

    var fog_varying ="varying vec3 vColor;"+
                      "varying float vDist;";

    //first we inject uniforms and methods at start of page.
    var start = vs.indexOf("void main");
    start = start < 0 ? 0 : start;
    vs = vs.splice(start, 0, fog_varying );//vs_lightSource + vs;

    //second we modify the main function so that our light calculations are called.
    var header = vs.match(/(void main\(\)|void main\(void\))(\{|\s\{)?/g);
    var modHeader = header;
    modHeader = modHeader + "vDist  = distance(uModelMatrix*vec4(aVertexPos, 1.0), vec4(0, -0.5, -1, 1.0));";
    vs = vs.replace(/(void main\(\)|void main\(void\))(\{|\s\{)?/g, modHeader);


    //third we inject the fragment source.
    fs = fs.splice(fs.indexOf("void main"),0, fog_uniforms + fog_varying);//fs_lightSource + fs;

    //adjust main for light methods.
    //header = fs.match(/(void main\(\)|void main\(void\))(\{|\s\{)?/g);
    //modHeader = header;
    //modHeader = modHeader + "vec3 fogColor = vec3("+scene.getColor().r+","+scene.getColor().g+","+scene.getColor().b+");";
    //modHeader = modHeader + "vec2 fogDist  = vec2("+scene.fogStart+","+scene.fogEnd+");";
    //modHeader = modHeader + "float fogFactor = clamp((uFogDist.y - vDist)/(uFogDist.y - uFogDist.x), 0.0, 1.0);";
    //fs = fs.replace(/(void main\(\)|void main\(void\))(\{|\s\{)?/g, modHeader);

    var inj = "";
    var id = "void main(void){";
    inj = inj + "vec3 fogColor = vec3("+scene.getColor().r+","+scene.getColor().g+","+scene.getColor().b+");";
    inj = inj + "vec2 fogDist  = vec2("+scene.fogStart+","+scene.fogEnd+");";
    inj = inj + "float fogFactor = clamp((uFogDist.y - vDist)/(uFogDist.y - uFogDist.x), 0.0, 1.0);";
    fs = fs.splice(fs.indexOf("void main")  + id.length,0, inj);//fs_lightSource + fs;

    //fourth modify the gl fragColor.sd
    //TODO: find smarter way to do this instead of forcing gl_FragColor = vec4(color.rgb, color.a); template.
    inj = "color = vec4(mix(uFogColor, vec3(color), fogFactor), color.a);";
    fs = fs.splice(fs.indexOf("gl_FragColor"),0, inj);

    //var footer = fs.match(/gl_FragColor.+;/g);
    //var modFooter = "color = vec4(mix(uFogColor, vec3(color), fogFactor), color.a);"+
    //                "gl_FragColor = color;";
    //fs = fs.replace(/gl_FragColor.+;/g, modFooter );

    //console.log( fs );
    return { vs: vs, fs:fs, uniforms:uniforms};
};


