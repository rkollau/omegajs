function ShadowMapPass( renderer,scene, camera, texture ){
    RenderPass.apply(this, [renderer, scene, camera]);
    this.type    = this.scene.shadowType | OMEGA.Omega3D.ShadowMapPass.PCF;
    this.texture = texture;

    if(this.type == OMEGA.Omega3D.ShadowType.VSM){
        var sizeH = this.texture.GetFrameBuffer().width;
        var sizeV = this.texture.GetFrameBuffer().height;

        this.tex_temp  = new Omega3D.FrameBufferTexture(sizeH, sizeV, 1);
        this.sha_blurH = new Omega3D.Shaders.DirectionalBlur("1", sizeH);
        this.sha_blurV = new Omega3D.Shaders.DirectionalBlur("0", sizeV);
    }

    this.render = function(){
        this.cam.update();

        this.gl.frontFace(this.gl.CW);
        this.gl.enable(this.gl.CULL_FACE);
        this.gl.cullFace(this.gl.FRONT);
        this.gl.bindFramebuffer(this.gl.FRAMEBUFFER, this.texture.GetFrameBuffer());


        if(this.type == OMEGA.Omega3D.ShadowType.PCF )
        {
            this.gl.colorMask(false, false, false, false);
        }


        this.gl.enable( this.gl.DEPTH_TEST );


        this.renderer.viewPort( this.scene.shadowScene, 0, 0, this.texture.GetFrameBuffer().width ,this.texture.GetFrameBuffer().height );
        this.renderer.renderScene( this.scene.shadowScene, this.cam );


        this.gl.disable( this.gl.DEPTH_TEST );
        this.gl.cullFace(this.gl.BACK);
        this.gl.disable(this.gl.CULL_FACE);

        this.gl.bindFramebuffer(this.gl.FRAMEBUFFER, null);



        if(this.type == OMEGA.Omega3D.ShadowType.PCF )
        {
            this.gl.colorMask(true, true, true, true);
        }

        if(this.type == OMEGA.Omega3D.ShadowType.VSM )
        {
            this.renderer.renderFilter( this.sha_blurH, this.texture, this.tex_temp);
            this.renderer.renderFilter( this.sha_blurV, this.tex_temp, this.texture);
        }

    };
};
ShadowMapPass.prototype = new RenderPass();
OMEGA.Omega3D.ShadowMapPass = ShadowMapPass;


