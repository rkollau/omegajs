function RenderPass(renderer, scene, camera, alphaBlend ){
   Pass.apply(this);
   //if(!renderer||!scene||!camera){
   //    //if(!renderer) OMEGA.Omega3D.Log(" *** RENDERPASS WARNING: plz make sure you passed a renderer to the renderpass!");
   //   // if(!scene)    OMEGA.Omega3D.Log(" *** RENDERPASS WARNING: plz make sure you passed a scene to the renderpass!");
   //   // if(!camera)   OMEGA.Omega3D.Log(" *** RENDERPASS WARNING: plz make sure you passed a camera to the renderpass!");
   //    return;
   //}
   this.renderer = renderer;
   this.scene = scene;
   this.cam   = camera;
   this.gl    = scene!=null ? scene.getGL() : null;
   this.color = scene!=null ? scene.getColor():null;

   this.AlphaBlend = alphaBlend | false;

   this.render = function(){
       if(this.cam) this.cam.update();
       if(this.scene) this.scene.update();

       if(this.AlphaBlend){
           this.gl.disable( this.gl.DEPTH_TEST );
           this.gl.enable(this.gl.BLEND);
          // this.gl.blendFunc( this.gl.SRC_ALPHA,  this.gl.ONE_MINUS_SRC_ALPHA);
           this.gl.blendFunc( this.gl.ONE_MINUS_SRC_COLOR,  this.gl.SRC_COLOR);
       }else{
           this.gl.enable( this.gl.DEPTH_TEST );
       }
       this.gl.frontFace(this.gl.CW);

      // this.gl.enable(this.gl.CULL_FACE);
     //  this.gl.cullFace(this.gl.BACK);

       this.renderer.viewPort(this.scene);
       this.renderer.renderScene( this.scene, this.cam );

       this.gl.disable(this.gl.CULL_FACE);

       if(this.AlphaBlend){
           this.gl.disable(this.gl.BLEND);
           this.gl.enable( this.gl.DEPTH_TEST );
       }else{
           this.gl.disable( this.gl.DEPTH_TEST );
       }
      //
   };
};
RenderPass.prototype = new Pass();
OMEGA.Omega3D.RenderPass = RenderPass;