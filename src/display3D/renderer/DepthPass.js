function DepthPass(renderer,scene, camera, texture){
    RenderPass.apply(this, [renderer, scene, camera]);
    this.texture = texture;
    this.fogSave = false;
    this.render = function(){
        if(this.cam) this.cam.update();
        if(this.scene) {
            this.scene.update();
            this.fogSave = this.scene.hasFog;
            this.scene.hasFog = false;
        }


        this.gl.frontFace(this.gl.CW);
        this.gl.enable(this.gl.CULL_FACE);
        this.gl.cullFace(this.gl.FRONT);
        this.gl.bindFramebuffer(this.gl.FRAMEBUFFER, this.texture.GetFrameBuffer());


        this.gl.enable( this.gl.DEPTH_TEST );
        this.renderer.viewPort( this.scene, 0, 0, this.texture.GetFrameBuffer().width ,this.texture.GetFrameBuffer().height );
        this.renderer.renderScene( this.scene, this.cam );
        this.gl.disable( this.gl.DEPTH_TEST );


        this.gl.cullFace(this.gl.BACK);
        this.gl.disable(this.gl.CULL_FACE);
        this.gl.bindFramebuffer(this.gl.FRAMEBUFFER, null);

        if(this.scene) {
            this.scene.hasFog = this.fogSave;
        }
    };
};
DepthPass.prototype = new RenderPass();
OMEGA.Omega3D.DepthPass = DepthPass;