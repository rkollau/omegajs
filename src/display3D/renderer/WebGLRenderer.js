function WebGLRenderer( debugRender ){

    var projectionMatrix = null;
    var modelViewMatrix  = null;

    var rp_head = null;var rp_tail = null;
    var renderChain = null;
    this.passes    = new Array();
    this.materials = null;
    this.objects   = null;
    this.list      = null;
    var debug      = debugRender || false;

    this.autoClear = true;
    this.CLEAR_COLOR_BIT = true;
    this.CLEAR_DEPTH_BIT = true;
    this.CLEAR_STENCIL_BIT = true;


    __construct = function(){
        projectionMatrix = mat4.create();
        this.materials = new Array();
        this.objects   = new Array();
        this.list      = new Array();

        window.requestAnimFrame = (function(){
            return  (window.requestAnimationFrame       ||
                window.webkitRequestAnimationFrame ||
                window.mozRequestAnimationFrame    ||
                window.oRequestAnimationFrame      ||
                window.msRequestAnimationFrame     ||
                function(  callback ){
                    window.setTimeout(callback, 1000 / 60);
                });
        })();
    }();

    this.clear = function(color){
        var flagC = this.CLEAR_COLOR_BIT   ? OMEGA.Omega3D.GL.COLOR_BUFFER_BIT   : 0;
        var flagD = this.CLEAR_DEPTH_BIT   ? OMEGA.Omega3D.GL.DEPTH_BUFFER_BIT   : 0;
        var flagS = this.CLEAR_STENCIL_BIT ? OMEGA.Omega3D.GL.STENCIL_BUFFER_BIT : 0;
        var color = color || {r:0,g:0,b:0, a:1.0};
        OMEGA.Omega3D.GL.clearColor( color.r, color.g, color.b, color.a );
        OMEGA.Omega3D.GL.clear(flagC | flagD | flagS );
    };

    this.viewPort = function(scene, x, y, w, h ){
        var gl = scene.getGL();
        gl.viewport( x || 0, y || 0, w || gl.viewPortWidth,h || gl.viewPortHeight);
    };

    this.SetStencilParams = function(scene, stencilFuncParams, stencilOpParams, stencilMask, depthMask){
        var gl = scene.getGL();
        if(stencilFuncParams) gl.stencilFunc(stencilFuncParams[0], stencilFuncParams[1], stencilFuncParams[2]); // Set any stencil to 1
        if(stencilOpParams) gl.stencilOp(stencilOpParams[0], stencilOpParams[1], stencilOpParams[2]);
        gl.stencilMask(stencilMask); // Write to stencil buffer
        gl.depthMask(depthMask); // Don't write to depth buffer
        //gl.clear(gl.STENCIL_BUFFER_BIT); // Clear stencil buffer (0 by default)
    };




    this.addRenderPass = function( pass ){
        this.passes.push( pass );
        if(!rp_head){
            rp_head = pass;
            rp_tail = rp_head;
        }else{
            pass.prev = rp_tail;
            rp_tail.next = pass;
            rp_tail = pass;
        }
    };
    this.SetRenderChain = function( chain ){
        renderChain = chain;
    }


    this.render = function(){
        if(OMEGA.Omega3D.AVAILABLE && renderChain!= undefined )renderChain.Render();
    };

    this.renderSceneToStencil = function(scene, camera){
        camera.update();
        this.materials = scene.materials;
        this.objects   = scene.objects;
        this.list      = scene.list;


        var gl = scene.getGL();
        gl.enable( gl.STENCIL_TEST );

        var color = scene.getColor();
        gl.disable( gl.STENCIL_TEST );
    };


    var filterScene = new Omega3D.Scene();
    var filterMesh  = new Omega3D.Mesh( new Omega3D.Geometry.SquareGeometry(1, 1));
    var filterMat   = new Omega3D.Material(null, []);
    var filter_cam  = new Omega3D.cameras.Camera(  );
    filter_cam.LookAt( 0, 0, 0, [0, 0,-1]);

    this.filterQuad  = new Omega3D.Object3D( filterMesh, filterMat );
    filterScene.addChild( this.filterQuad );
    this.renderFilter = function( shader, tex_in, tex_out){
        filterScene.update();
        filter_cam.ratio = Omega3D.GL.viewPortWidth / OMEGA.Omega3D.GL.viewPortHeight;
        
        this.filterQuad.GetMaterial().SetShader( shader );
        this.filterQuad.GetMaterial().SetTextures( [tex_in] );
        filterScene.getGL().bindFramebuffer(filterScene.getGL().FRAMEBUFFER, tex_out.GetFrameBuffer());
        this.viewPort( filterScene, 0, 0, tex_out.GetFrameBuffer().width, tex_out.GetFrameBuffer().height);
        this.renderScene( filterScene,filter_cam );
        filterScene.getGL().bindFramebuffer(filterScene.getGL().FRAMEBUFFER, null);
    };


    //var shader_renderToScreen = new Omega3D.Shaders.RenderToScreen();
    var shader_renderToScreen = new Omega3D.Shaders.PostProcessing( true,null);
    this.renderToScreen = function( cam,  tex_in ){
        filterScene.update();
        this.filterQuad.GetMaterial().SetShader( shader_renderToScreen );
        this.filterQuad.GetMaterial().SetTextures( [tex_in] );

        this.viewPort( filterScene, 0, 0, filterScene.getGL().drawingBufferWidth, filterScene.getGL().drawingBufferHeight, tex_in.GetFrameBuffer().height);
        this.renderScene( filterScene,cam );
    };


    this.renderScene = function( scene, camera ){
        if(debug) OMEGA.Omega3D.Log("Renderer.Render START");
        var gl = scene.getGL();

        //clear scene.
        this.clear(scene.getColor());

        //render objects.
        var subject = null, program = null;
        for(var i = 0; i < scene.children.length; i++){
            subject = scene.children[i];
            if(!subject.mayRender) continue;

            //update the subject.
            subject.Update(scene.getGL(), camera );

            //render object.
            this.RenderObject(scene, camera, subject);

            subject.LateUpdate(scene.getGL(), camera );

        }

        //render lights.
        for(var i = 0; i < scene.getLights().length; i++){
            subject = scene.getLights()[i];
            if(!subject.mayRender) continue;

            //update the subject.
            subject.Update(scene.getGL(), camera );

            //render object.
            this.RenderObject(scene, camera, subject);

            subject.LateUpdate(scene.getGL(), camera );
        }

        if(debug) OMEGA.Omega3D.Log("Renderer.Render END");
    };

    this.RenderObject = function( scene, camera, subject  ){
        var gl = scene.getGL();
        if(subject.material!= null ){

            //Enable the material.
            //console.log("here : " + subject.material.hasAtlas );
            if(subject.material.hasAtlas) {
                subject.material.atlas.Update(scene);
            }
            subject.material.Enable();


            //-----------------------------------------------------------------
            //Update the material.
            // -----------------------------------------------------------------
            program = subject.material.GetShader().GetProgram();

            //Update textures.
            if( subject.material.GetTextures() != null ) {
                if(subject.material.hasAtlas){
                    if(subject.GetMesh().GetGeometry() != null ){
                        subject.GetMesh().GetGeometry().UpdateAtlasUV( subject.material.GetCurrentAtlasFrame(), 10, 10);
                    }
                }

                var t = subject.material.GetTextures();
                var l = t.length;
                for (var j = 0; j < l; j++) if(t[j].needsUpdate)t[j].Update();
            }

            //Update mesh if needed.
            if(subject.GetMesh() != null ) subject.GetMesh().Update();



            //Set projection + view matrix.
            if(program.uProjectionMatrix && camera != null ) gl.uniformMatrix4fv( program.uProjectionMatrix, false, camera.GetProjectionMatrix() );
            if(program.uViewMatrix       && camera != null ) gl.uniformMatrix4fv( program.uViewMatrix   , false,  camera.GetMatrix() );
            if(program.uInvViewMatrix    && camera != null ) gl.uniformMatrix4fv( program.uInvViewMatrix, false, camera.GetInverseMatrix() );
            if(program.uCamPos           && camera != null ) gl.uniform3fv( program.uCamPos             , Vector4.ToArray3( camera.GetPosition() ) );
            
//            if( program.uInvViewMatrix  ){
//                console.log( "yeah" );
//            }


            //Set standard uniforms in material.
            for(var key in subject.material.uniforms){
                gl.uniform3fv( program[key], subject.material.uniforms[key].value.apply(subject.material, null));
            }

            //Set custom uniforms in material.
            for(var key in subject.material.custom_uniforms){
                if(program[key] == undefined) continue;
                var u = subject.material.custom_uniforms[key];

                if(u.type == "int" ) gl.uniform1i(program[key], u.value );
                else if(u.type == "float" ) gl.uniform1f(program[key], u.value );
                else if(u.type == "mat4" ) gl.uniformMatrix4fv(program[key],false, u.value );
                else if(u.type == "mat3" ) gl.uniformMatrix3fv(program[key],false, u.value );
                else if(u.type == "vec4" ) gl.uniform4fv(program[key], u.value );
                else if(u.type == "vec3" )gl.uniform3fv(program[key], u.value );
                else if(u.type == "vec2" ) gl.uniform2fv(program[key], u.value );
            }

            //custom attributes in material.
            for( var key in subject.material.custom_attribs){
                gl.enableVertexAttribArray(program[key]);
                gl.bindBuffer( gl.ARRAY_BUFFER, subject.material.custom_attribs[key].value);
                gl.vertexAttribPointer( program[key],subject.material.custom_attribs[key].value.itemSize, gl.FLOAT, false, 0, 0 );
            };


            //Render lights.
            if(scene.getLights().length > 0 && scene.lightsON == true && program != null ){

                var lights = scene.getLights();
                for( var index in lights){
                     var light = lights[index];

                    var mat = mat4.create();
                    var mat2 = mat4.create();

                        mat4.multiply(mat, light.camera.GetProjectionMatrix(), light.camera.GetMatrix()  );
                        mat4.multiply(mat2, mat, subject.GetMatrix()  );
                        gl.uniformMatrix4fv( program[ "lights["+index+"].mat"] ,false, light.camera.GetMatrix() );
                        gl.uniform3fv( program[ "lights["+index+"].position"] , Vector4.ToArray3( light.GetPosition() ) );
                        gl.uniform3fv( program[ "lights["+index+"].direction"], light.GetDirection() );
                        gl.uniform3fv( program[ "lights["+index+"].ambient"]  , light.GetAmbient() );
                        gl.uniform3fv( program[ "lights["+index+"].diffuse"]  , light.GetDiffuse() );
                        gl.uniform3fv( program[ "lights["+index+"].specular"] , light.GetSpecular() );
                        gl.uniform1f( program[ "lights["+index+"].radius"]    , light.GetRadius() );
                        gl.uniform1f( program[ "lights["+index+"].cutOff"]    , light.GetCutOff() );
                        gl.uniform1i( program[ "lights["+index+"].type"]      , light.GetType() );
                        gl.uniform1i( program[ "lights["+index+"].enabled"]   , light.IsOn() );
                    }


                    //for(var key in lights[index].uniforms){
                    //    if(lights[index].uniforms[key].type == "vec3"       ) gl.uniform3fv( program[key], lights[index].uniforms[key].value.apply( lights[index], null));
                    //    else if(lights[index].uniforms[key].type == "float" ) gl.uniform1f ( program[key], lights[index].uniforms[key].value.apply( lights[index], null));
                    //}
                //}
            }

            //Render fog.
            if(scene.hasFog){
                gl.uniform3fv(program["uFogColor"], [scene.getColor().r, scene.getColor().g, scene.getColor().b ] );
                gl.uniform2fv(program["uFogDist" ], [scene.fogStart, scene.fogEnd] );
            }


            //-----------------------------------------------------------------
            // Render the Object.
            // -----------------------------------------------------------------

            // model matrix
            gl.uniformMatrix4fv( program.uModelMatrix , false, subject.GetMatrix() );

            // normal matrix
            var normalMatrix4 = mat4.create();
            var normalMatrix3 = mat3.create();
            if(camera != null){
                mat4.multiply(normalMatrix4, camera.GetMatrix(), subject.GetMatrix() );
                mat3.fromMat4(normalMatrix3,normalMatrix4 );
                mat3.invert(normalMatrix3,normalMatrix3 );
                mat3.transpose(normalMatrix3,normalMatrix3);
            }
            gl.uniformMatrix3fv( program.uNormalMatrix, false, normalMatrix3 );





            //Bind combined buffer -> [ vertex, normal, uv, tangent, bitangent, color ] = (3 + 3 + 2 + 3 + 3 + 3) * 4 bytes = 68 bytes
            var totalBytes = 68;
            if(subject.GetMesh() != null ) {
                gl.bindBuffer(gl.ARRAY_BUFFER, subject.GetMesh().GetCombinedBuffer());

                // vertices
                if(program.aVertexPos !=-1 ) {
                    gl.enableVertexAttribArray(program.aVertexPos);
                    gl.vertexAttribPointer(program.aVertexPos, 3, gl.FLOAT, false, totalBytes, 0);
                }

                // aVertexNormals
                if(program.aVertexNormal!=-1 ) {
                    gl.enableVertexAttribArray(program.aVertexNormal);
                    gl.vertexAttribPointer(program.aVertexNormal, 3, gl.FLOAT, false, totalBytes, 12); // offset = 12 = 4 * 3
                }

                // uvs
                if(program.aTextureCoord!=-1 ){
                    gl.enableVertexAttribArray(program.aTextureCoord);
                    gl.vertexAttribPointer( program.aTextureCoord,2, gl.FLOAT, false, totalBytes, 24 ); // offset = 24 = 4 * 6
                }

                // aVertexTangent
                if(program.aVertexTangent!=-1 ) {
                    gl.enableVertexAttribArray(program.aVertexTangent);
                    gl.vertexAttribPointer(program.aVertexTangent, 3, gl.FLOAT, false, totalBytes, 32); // offset = 32 = 4 * 8
                }

                // aVertexBitangent
                if(program.aVertexBitangent!=-1 ) {
                    gl.enableVertexAttribArray(program.aVertexBitangent);
                    gl.vertexAttribPointer(program.aVertexBitangent, 3, gl.FLOAT, false, totalBytes, 44); // offset = 44 = 4 * 11
                }

                // aVertexColor
                if(program.aVertexColor !=-1 ) {
                    gl.enableVertexAttribArray(program.aVertexColor);
                    gl.vertexAttribPointer(program.aVertexColor, 3, gl.FLOAT, false, totalBytes, 56);  // offset = 32 = 4 * 14
                }

                // aPickingColor
                if(program.aPickingColor !=-1 &&  subject.GetMesh().GetColorPickingBuffer() != undefined) {
                    gl.enableVertexAttribArray(program.aPickingColor);
                    gl.bindBuffer(gl.ARRAY_BUFFER, subject.GetMesh().GetColorPickingBuffer());
                    gl.vertexAttribPointer(program.aPickingColor, subject.GetMesh().GetColorPickingBuffer().itemSize, gl.FLOAT, false, 0, 0);
                }

                // indices
                gl.bindBuffer( gl.ELEMENT_ARRAY_BUFFER, subject.GetMesh().GetIndexBuffer() );



                // 4   DRAW OBJECT
                //gl.drawArrays( gl.TRIANGLES, 0, this.GetMesh().GetVertexBuffer().numItems);
               // if( subject.drawType == OMEGA.Omega3D.Object3D.DEFAULT        ) gl.drawArrays( gl.TRIANGLE_STRIP , 0, subject.GetMesh().GetGeometry().GetVertices().length/3);
                if( subject.drawType == OMEGA.Omega3D.Object3D.DEFAULT       ) gl.drawElements( gl.TRIANGLES , subject.GetMesh().GetIndexBuffer().numItems, gl.UNSIGNED_SHORT, subject.GetMesh().GetIndexBuffer());
                else if(subject.drawType == OMEGA.Omega3D.Object3D.WIREFRAME ) gl.drawElements( gl.LINES, subject.GetMesh().GetIndexBuffer().numItems, gl.UNSIGNED_SHORT,  subject.GetMesh().GetIndexBuffer());
                else if(subject.drawType == OMEGA.Omega3D.Object3D.POINTS    ) gl.drawArrays( gl.POINTS      , 0, subject.GetMesh().GetGeometry().GetVertices().length/3);
                else if(subject.drawType == OMEGA.Omega3D.Object3D.TRIANGLES ) gl.drawArrays( gl.TRIANGLES,0, subject.GetMesh().GetGeometry().GetVertices().length/3);
                else if(subject.drawType == OMEGA.Omega3D.Object3D.LINES     ) gl.drawArrays( gl.LINES,0, subject.GetMesh().GetGeometry().GetVertices().length/3);



                //disable arrays.
                if(program.aVertexPos!=-1   ) gl.disableVertexAttribArray(program.aVertexPos   );
                if(program.aTextureCoord!=-1) gl.disableVertexAttribArray(program.aTextureCoord);
                if(program.aVertexNormal!=-1) gl.disableVertexAttribArray(program.aVertexNormal);
                if(program.aVertexTangent!=-1     ) gl.disableVertexAttribArray(program.aVertexTangent);
                if(program.aVertexBitangent!=-1   ) gl.disableVertexAttribArray(program.aVertexBitangent);
            }

            //Disable the material.
            subject.material.Disable();

            //dispose gl.
            gl = null;
        }
        //Render children.
        for(var i = 0; i < subject.children.length; i++){
           if(subject.children[i].mayRender) this.RenderObject(scene, camera, subject.children[i] );
        }
    };

}
OMEGA.Omega3D.WebGLRenderer = WebGLRenderer;
