function RenderToScreenPass( renderer, cam, in_texture ){
    RenderPass.apply(this, [renderer, null, null]);

    this.cam = cam;
    this.in_texture = in_texture;

    this.render = function(){
        this.renderer.renderToScreen( this.cam, this.in_texture);
    };
};
RenderToScreenPass.prototype = new RenderPass();
OMEGA.Omega3D.RenderToScreenPass = RenderToScreenPass;