function DynamicCubeMapPass(renderer,scene, object, fromViewPoint ){
    RenderPass.apply(this, [renderer, scene, null]);
    this.object = object;
    this.texture = object.GetMaterial().GetTextures()[0];

    this.render = function(){
        //if(this.cam!=null)this.cam.update();
        //if(this.scene!=null) this.scene.update();

        object.mayRender = false;
        this.texture.UpdateFaces(fromViewPoint, renderer, scene );
        object.mayRender = true;

    };
};
DynamicCubeMapPass.prototype = new RenderPass();
OMEGA.Omega3D.DynamicCubeMapPass = DynamicCubeMapPass;




