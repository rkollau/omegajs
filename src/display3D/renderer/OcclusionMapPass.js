function OcclusionMapPass(renderer,scene, camera, texture){
    RenderPass.apply(this, [renderer, scene, camera]);
    this.texture = texture;

    this.material = new Omega3D.BasicMaterial( [], scene01, [0,0,0] );
    this.savedMaterials = [];

    this.render = function(){
        this.savedColor = this.scene.getColor();
        this.scene.setColor(0, 0, 0);
        this.gl.bindFramebuffer(this.gl.FRAMEBUFFER, this.texture.GetFrameBuffer());
        this.gl.enable( this.gl.DEPTH_TEST );

        var subject;

        //turn materials off.
        for(var i = 0; i < this.scene.children.length; i++){
            subject =  this.scene.children[i];
            if(!subject.mayRender) continue;
            this.turnOffMaterial(subject);
            //this.savedMaterials.push(subject.material );
            //subject.material.mayRender = false;
            //subject.SetMaterial( this.material );
            //subject.material.mayRender = false;
        }

        //turn lights bodies rendering on.
       // for(var i = 0; i < this.scene.getLights().length; i++){
            subject =  this.scene.getLights()[0];
            subject.savedMayRender =subject.mayRender;
            subject.mayRender = true;
       // }

         for(var i = 1; i < this.scene.getLights().length; i++){
            subject =  this.scene.getLights()[i];
             subject.savedMayRender =subject.mayRender;
             subject.mayRender = false;
         }


        this.renderer.viewPort( this.scene, 0, 0, this.texture.GetFrameBuffer().width ,this.texture.GetFrameBuffer().height );
        this.renderer.renderScene( this.scene, this.cam );


        //turn lights bodies rendering off.
       // for(var i = 0; i < this.scene.getLights().length; i++){
            subject =  this.scene.getLights()[0];
            subject.mayRender =  subject.savedMayRender;
            delete subject.savedMayRender;
       // }

        for(var i = 1; i < this.scene.getLights().length; i++){
            subject =  this.scene.getLights()[i];
            subject.mayRender =  subject.savedMayRender;
            delete subject.savedMayRender;
         }

        //turn materials back on.
        for(var i = 0; i < this.scene.children.length; i++){
            subject =  this.scene.children[i];
            if(!subject.mayRender) continue;
            this.turnOnMaterial(subject);
        }

        this.savedMaterials = [];
        this.gl.disable( this.gl.DEPTH_TEST );
        this.gl.bindFramebuffer(this.gl.FRAMEBUFFER, null);


        this.scene.setColor(this.savedColor.r,this.savedColor.g,this.savedColor.b);
    };

    this.turnOffMaterial = function( subject ){
        if(subject.material != null ){
            this.savedMaterials.push(subject.material );
            subject.material.mayRender = false;
            subject.SetMaterial( this.material );
        }


        for(var i = 0; i < subject.children.length; i++) {
            var child = subject.children[i];
            if (!child.mayRender) continue;
            this.turnOffMaterial(child);
        }
    };
    this.turnOnMaterial = function( subject ){
        if(subject.material != null ){
            subject.SetMaterial(this.savedMaterials.shift());
            subject.material.mayRender = true;
        }

        for(var i = 0; i < subject.children.length; i++) {
            var child = subject.children[i];
            if (!child.mayRender) continue;
            this.turnOnMaterial(child);
        }
    }
};
OcclusionMapPass.prototype = new RenderPass();
OMEGA.Omega3D.OcclusionMapPass = OcclusionMapPass;
