function FilterPass(renderer, shader, in_texture, out_texture){
    RenderPass.apply(this, [renderer, null, null]);

    this.in_texture = in_texture;
    this.out_texture = out_texture;
    this.shader = shader;

    this.render = function(){
       this.renderer.renderFilter( this.shader, this.in_texture, this.out_texture);
    };
};
FilterPass.prototype = new RenderPass();
OMEGA.Omega3D.FilterPass = FilterPass;
