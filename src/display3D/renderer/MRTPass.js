function MRTPass(renderer,scene, camera,material){
    RenderPass.apply(this, [renderer, scene, camera]);

    this.material = material;
    this.texture  = material.GetTextures()[material.GetTextures().length-1];

    this.render = function(){
        if(this.cam!=null)this.cam.update();
        if(this.scene!=null) this.scene.update();

        this.material.Enable();

        Omega3D.EXT.draw_buffers.drawBuffersWEBGL( this.texture.buffers );

        this.gl.enable( this.gl.DEPTH_TEST );
        this.renderer.viewPort( this.scene, 0, 0, this.texture.GetFrameBuffer().width ,this.texture.GetFrameBuffer().height );
        this.renderer.renderScene( this.scene, this.cam );
        this.gl.disable( this.gl.DEPTH_TEST );

        this.material.Disable();

    };
};
MRTPass.prototype = new RenderPass();
OMEGA.Omega3D.MRTPass = MRTPass;
