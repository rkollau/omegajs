var OMEGA = OMEGA || {};
OMEGA.Omega3D = OMEGA.Omega3D || {};
OMEGA.Omega3D = function(){
    this.canvas   =  document.getElementById("omega");
    this.scenes   = [];
    this.cameras  = [];
    this.shaders  = [];
    this.keylisteners = [];
    this.mouselisteners = [];

    this.gl = null;
    this.context_params = { alpha: true ,
                            depth: true,
                            stencil: false,
                            antialias: true,
                            premultipliedAlpha: false,
                            preserveDrawingBuffer: false,
                            failIfMajorPerformanceCaveat: false }


    try {
        this.gl = this.canvas.getContext("experimental-webgl", this.context_params );
        if(this.gl == null)this.gl = canvas.getContext("webgl", this.context_params );
        else if(this.gl == null)this.gl = canvas.getContext("webgl2", this.context_params );
        else if(this.gl == null)this.gl = canvas.getContext("experimental-webgl2", this.context_params );
    } catch (e) {}
    if (this.gl == null){
        console.log("[Omega3D] Cant initiate WebGL!");
        return;
    }

    OMEGA.Omega3D.AVAILABLE = true;
    this.gl.viewPortWidth  = this.canvas.width;
    this.gl.viewPortHeight = this.canvas.height;
    this.canvas.addEventListener('webglcontextlost', function(){ OMEGA.Omega3D.AVAILABLE = false; }, false);
    this.canvas.addEventListener('webglcontextrestored', function(){ OMEGA.Omega3D.AVAILABLE = true; }, false);

    window.addEventListener("resize", function(){
        Omega3D.Recalibrate();
    }, false);

    OMEGA.Omega3D.GL = this.gl;
    console.log(".: Omega3D v"+OMEGA.Omega3D.VERSION+" :.");

    //check extentions.
    OMEGA.Omega3D.LoadExtentions(this.gl);
};
OMEGA.Omega3D.LoadExtentions = function(gl){
    Omega3D.EXT = {};
    Omega3D.EXT.floating_point_textures =  gl.getExtension('OES_texture_float');;
    if(Omega3D.EXT.floating_point_textures == null) Omega3D.Log("[Omega3D] No OES_texture_float supported in this browser.");


    Omega3D.EXT.float_linear_textures =  gl.getExtension('OES_texture_float_linear');
    if(Omega3D.EXT.float_linear_textures == null) Omega3D.Log("[Omega3D] No OES_texture_float_linear supported in this browser.");


    Omega3D.EXT.standard_derivatives = ( gl.getExtension("OES_standard_derivatives") );
    if(Omega3D.EXT.standard_derivatives == null) Omega3D.Log("[Omega3D] No OES_standard_derivatives supported in this browser.");



    Omega3D.EXT.draw_buffers =  ( gl.getExtension('WEBGL_draw_buffers') || gl.getExtension('GL_EXT_draw_buffers') || gl.getExtension('EXT_draw_buffers') );
    if(Omega3D.EXT.draw_buffers == null) Omega3D.Log("[Omega3D] No WEBGL_draw_buffers supported in this browser.");



    Omega3D.EXT.depth_texture = ( gl.getExtension("WEBKIT_WEBGL_depth_texture") ||
                                gl.getExtension("MOZ_OES_depth_texture") ||
                                gl.getExtension("WEBKIT_OES_depth_texture") ||
                                gl.getExtension("WEBGL_depth_texture") ||
                                gl.getExtension("OES_depth_texture") ||
                                gl.getExtension( "MOZ_WEBGL_depth_texture" ) );
    if(Omega3D.EXT.depth_texture == null) Omega3D.Log("[Omega3D] No depth_textures supported in this browser.");


    Omega3D.EXT.anisotropic_filtering = ( gl.getExtension('EXT_texture_filter_anisotropic') ||
                                          gl.getExtension('MOZ_EXT_texture_filter_anisotropic') ||
                                          gl.getExtension('WEBKIT_EXT_texture_filter_anisotropic'));

    if( Omega3D.EXT.anisotropic_filtering == null ) Omega3D.Log("[Omega3D] No anisotropic_filtering supported in this browser.");


}



OMEGA.Omega3D.Recalibrate = function(){
    OMEGA.Omega3D.INSTANCE.canvas.width  = window.innerWidth - 3.25;
    OMEGA.Omega3D.INSTANCE.canvas.height = window.innerHeight - 3.25;
    OMEGA.Omega3D.INSTANCE.gl.viewPortWidth  = OMEGA.Omega3D.INSTANCE.canvas.width;
    OMEGA.Omega3D.INSTANCE.gl.viewPortHeight = OMEGA.Omega3D.INSTANCE.canvas.height;
}
OMEGA.Omega3D.GetGL = function(){
    return OMEGA.Omega3D.INSTANCE.gl;
};
OMEGA.Omega3D.AddKeyListener = function(listener){
    OMEGA.Omega3D.INSTANCE.keylisteners.push(listener);
    document.keylisteners = OMEGA.Omega3D.INSTANCE.keylisteners;

};
OMEGA.Omega3D.AddMouseListener = function(listener){
    OMEGA.Omega3D.INSTANCE.mouselisteners.push(listener);
    document.mouselisteners = OMEGA.Omega3D.INSTANCE.mouselisteners;

};
OMEGA.Omega3D.RemoveKeyListener = function(listener){
    OMEGA.Omega3D.INSTANCE.keylisteners.splice( OMEGA.Omega3D.INSTANCE.keylisteners.lastIndexOf(listener), 1 );
    document.keylisteners = OMEGA.Omega3D.INSTANCE.keylisteners;
};

OMEGA.Omega3D.ReadPixels = function( image,xpos, ypos, width, height ){
    var gl = OMEGA.Omega3D.INSTANCE.gl;
    var texture = gl.createTexture();
    gl.bindTexture(gl.TEXTURE_2D, texture);
    gl.pixelStorei(gl.UNPACK_FLIP_Y_WEBGL, false);
    gl.texImage2D(gl.TEXTURE_2D, 0, gl.RGBA, gl.RGBA, gl.UNSIGNED_BYTE, image);
    gl.texParameteri(gl.TEXTURE_2D, gl.TEXTURE_MAG_FILTER, gl.NEAREST);
    gl.texParameteri(gl.TEXTURE_2D, gl.TEXTURE_MIN_FILTER, gl.NEAREST);

    var framebuffer = gl.createFramebuffer(0124, 1024, 1);
    gl.bindFramebuffer(gl.FRAMEBUFFER, framebuffer);
    gl.framebufferTexture2D(gl.FRAMEBUFFER, gl.COLOR_ATTACHMENT0, gl.TEXTURE_2D, texture, 0);

    var data = new Uint8Array(width * height * 4);
    gl.readPixels(xpos, ypos, width,height, gl.RGBA, gl.UNSIGNED_BYTE, data);

    gl.deleteFramebuffer(framebuffer);
    gl.bindTexture(gl.TEXTURE_2D, null);
    gl.deleteTexture(texture);
    return data;
};
OMEGA.Omega3D.CreateArrayBuffer = function( data, itemSize ){
    var gl = OMEGA.Omega3D.INSTANCE.gl;
    var buffer = gl.createBuffer();
    gl.bindBuffer(gl.ARRAY_BUFFER, buffer);
    gl.bufferData(gl.ARRAY_BUFFER, data,gl.STATIC_DRAW);
    buffer.itemSize = itemSize;
    buffer.numItems = data.length / itemSize;
    return buffer;
};

OMEGA.Omega3D.CreateAtlas = function(width, height, cellWidth, cellHeight, out_vertices, out_uvs, out_indices ){
    var xSteps = width / cellWidth;
    var ySteps = height / cellHeight;
    for( var y = 0; y < ySteps; y++){
        for( var x = 0; x < xSteps; x++) {
           var xpos = x * cellWidth;
           var ypos = y * cellHeight;
           var zpos = 0.0;

           out_vertices.push( xpos, ypos, zpos );

           out_uvs.push( x / (xSteps - 1));
           out_uvs.push( y / (ySteps - 1));

           if (y < ySteps-1 && x < xSteps-1) {

               out_indices.push(y * xSteps + x);
               out_indices.push(y * xSteps + x + 1);
               out_indices.push((y+1) * xSteps + x);

               out_indices.push(y * xSteps + x + 1);
               out_indices.push((y+1) * xSteps + x + 1);
               out_indices.push((y+1) * xSteps + x);
           }
        }
    }
};





//LISTENERS
function onload(){
    var body = document.getElementsByTagName("body");
    OMEGA.Omega3D.INSTANCE = new OMEGA.Omega3D();
};


if (window.addEventListener)
    window.addEventListener("load", onload, false);
else if (window.attachEvent)
    window.attachEvent("onload", onload);
else
    window.onload = onload;

document.keylisteners = [];
document.mouselisteners = [];
document.onkeydown = function(e){
    for(var i = 0; i < document.keylisteners.length; i++){
        document.keylisteners[i].HandleKeyDown(e);
    }
};
document.onkeyup =  function(e){
    for(var i = 0; i < document.keylisteners.length; i++){
        document.keylisteners[i].HandleKeyUp(e);
    }
};
document.onmousemove = function(e){
    for(var i = 0; i < document.mouselisteners.length; i++){
        document.mouselisteners[i].HandleMouseMove(e);
    }
};



//PROTOTYPES OF NATIVES.
String.prototype.splice = function( idx, rem, s ) {
    return (this.slice(0,idx) + s + this.slice(idx + Math.abs(rem)));
};


OMEGA.Omega3D.Log = function( msg ){
    console.log(msg);
};



OMEGA.Omega3D.GL = null;
OMEGA.Omega3D.LIGHTS = [];
OMEGA.Omega3D.PointSize = 1.0;
OMEGA.Omega3D.VERSION = "0.0.1.8.4 Alpha";
OMEGA.Omega3D.ScenesCount   = 0;
OMEGA.Omega3D.ObjectsCount   = 0;
OMEGA.Omega3D.MaterialsCount = 0;
OMEGA.Omega3D.INSTANCE = null;

OMEGA.Omega3D.AVAILABLE = false;

OMEGA.Omega3D.WORLD_UP = new Vector4( 0, 1, 0);

OMEGA.Omega3D.ShadowType = {};
OMEGA.Omega3D.ShadowType.PCF = 0;
OMEGA.Omega3D.ShadowType.VSM = 1;

OMEGA.Omega3D.KEEP    = 7680;
OMEGA.Omega3D.REPLACE = 7681;
OMEGA.Omega3D.NEVER   = 512;
OMEGA.Omega3D.LESS    = 513;
OMEGA.Omega3D.EQUAL   = 514;
OMEGA.Omega3D.LEQUAL  = 515;
OMEGA.Omega3D.GREATER = 516;
OMEGA.Omega3D.NOTEQUAL= 517;
OMEGA.Omega3D.GEQUAL  = 518;
OMEGA.Omega3D.ALWAYS  = 519;
var Omega3D = OMEGA.Omega3D;









