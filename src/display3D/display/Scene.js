OMEGA.Omega3D.Scene = function( scene_params ) {
    this.scene_params = scene_params || { shadows: false };
    this.id = (++OMEGA.Omega3D.ScenesCount).toString();
    this.name = "Scene_"+this.id;
    this.renderer = null;
    this.gl       = OMEGA.Omega3D.GL;
    this.children = new Array();
    this.objects  = new Array();
    this.materials = new Array();
    this.list     = new Array();
    this.lights   = new Array();
    this.counter  = new OMEGA.Utils.Counter();
    this.color    = {r: 0.5, g:0.5, b:0.5, a:1.0};
    this.hasFog   = false;
    this.fogStart = 1.0;
    this.fogEnd   = 3.0;
    this.lightsON = true;


    //shadow stuff.
    if(this.scene_params.shadows == true ){
        this.shadowType = this.scene_params.shadowType | OMEGA.Omega3D.ShadowType.PCF;
        this.shadowScene    = new Omega3D.Scene();
        this.shadowScene.setColor( 1.0, 1.0, 0 );
        this.shadowSceneMat = new Omega3D.Material(new Omega3D.Shaders.DepthMap(), []);
    }




    this.getLights = function( ){
       return this.lights;
    };
    this.addLight = function( light ){
        light.setIndex( this.lights.length );
        this.lights.push(light);

        if(this.scene_params.shadows){
            light.SetCastShadows( this.shadowType );
        }
    };
    this.removeLight = function( light ){
        this.lights.splice(this.lights.indeOf(light), 1);
    };

    this.getColor = function(){
        return this.color;
    };
    this.setColor = function(r, g, b){
        this.color = {r: r, g:g, b:b, a:1.0};
        this.gl.clearColor(this.color.r, this.color.g, this.color.b, this.color.a);
    };
    this.checkForMaterial = function(id){
        for(var i = 0; i < this.materials.length; i++)
        {
            if( this.materials[i].id == id ) return true;
        }

        return false;
    }
    this.setName = function(value){
        this.name = value;
    };
    this.getName = function(){
        return this.name;
    };
    this.getID = function(){
        return this.id;
    };

    this.getMaterials = function( id ){
        return materials[id];
    };
    this.getObjectsFromMaterialID = function( id ){
        return objects[id];
    };
};
OMEGA.Omega3D.Scene.prototype.getGL = function(){
    return this.gl;
};
OMEGA.Omega3D.Scene.prototype.addChild = function( c, isShadowCaster  ){
    this.children.push( c );
    c.parentScene    = this;
    isShadowCaster   = isShadowCaster || false;
    c.isShadowCaster = isShadowCaster;

    //shadow stuff.
    if(c.isShadowCaster == true && this.scene_params.shadows == true){
        var clone = c.Clone(true,this.shadowSceneMat);
        this.shadowScene.addChild( clone );
    }
};
OMEGA.Omega3D.Scene.prototype.removeChild = function( c ){
    for(var i=0;i<this.children.length;i++){
        if( this.children[i] == c ){
            this.children.splice(i,1);
            break;
        }
    }

    if(c.isShadowCaster == true){
        for(var i=0;i<this.shadowScene.children.length;i++){
            if( this.shadowScene.children[i] == c ){
                var shadowChild = this.shadowScene.children.splice(i,1);
                shadowChild.parentScene = null;
                shadowChild = null;
                break;
            }
        }
    }


    c.parentScene = null;
    c = null;
};
OMEGA.Omega3D.Scene.prototype.removeChildAtIndex = function( removeChildAtIndex ){
    var c = this.children.splice(removeChildAtIndex,1);
    if(c.isShadowCaster == true){
        for(var i=0;i<this.shadowScene.children.length;i++){
            if( this.shadowScene.children[i] == c ){
                var shadowChild = this.shadowScene.children.splice(i,1);
                shadowChild.parentScene = null;
                shadowChild = null;
                break;
            }
        }
    }
    c.parentScene = null;
    c = null;
};


OMEGA.Omega3D.Scene.prototype.update = function(){
   this.counter.update();
};
OMEGA.Omega3D.Scene.prototype.getTime = function(){
    return this.counter.getTime();
};
OMEGA.Omega3D.Scene.prototype.getDeltaTime = function(){
    return this.counter.getDeltaTime();
};
OMEGA.Omega3D.Scene.prototype.getFPS = function(){
    return this.counter.getFPS();
};
OMEGA.Omega3D.Scene.prototype.getFrameNumber = function(){
    return this.counter.getFrame();
};
