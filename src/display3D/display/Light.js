OMEGA.Omega3D.Light = function(type, data ){
    this.data = data || { ambient:[0.0, 0.0, 0.0], diffuse:[1.0, 1.0, 1.0], specular:[1.0, 1.0, 1.0], cutOff: 0.9 };
    Object3D.apply(this, [new Omega3D.Mesh( new Omega3D.Geometry.SphereGeometry(0.1, 25, 25) ),
                          new Omega3D.BasicMaterial([], null, this.data.diffuse )]);

    this.SetPosition = function( x, y, z ){
        this.position.x = x;
        this.position.y = y;
        this.position.z = z;

        mat4.identity(this.tMatrix);
        mat4.translate(this.tMatrix, this.tMatrix, Vector4.ToArray( this.position ) );
        this.camera.LookAt(0.1, 0, 0.1, [ x, y-this.cameraOffset, z] );
    };

    this.SetCameraOffset = function( value ){
        this.cameraOffset = value;
        this.camera.LookAt(this.position[0], 0, this.position[2]/5, [ this.position[0], this.position[1]-this.cameraOffset, this.position[2]] );
    };
    this.SetCutOff = function( value ){
        this.cutOff = value;
    };
    this.SetRadius = function( value ){
        this.radius = value;
    };
    this.SetDirection = function(x,y,z){
        this.direction[0] = x; this.direction[1] = y; this.direction[2] = z;
        this.invdirection[0] = -x; this.invdirection[1] = -y; this.invdirection[2] = -z;
        //this.LookAt( x, y, z, this.position);
    };
    this.SetAmbient = function(r,g,b){
        this.ambientColor[0] = r; this.ambientColor[1] = g; this.ambientColor[2] = b;
        this.data.ambient = this.ambientColor;
    };
    this.SetDiffuse = function(r,g,b){
        this.diffuseColor[0] = r; this.diffuseColor[1] = g; this.diffuseColor[2] = b;
        this.data.diffuse = this.diffuseColor;
        this.material     = new Omega3D.BasicMaterial([], null, this.data.diffuse );
    };
    this.SetSpecular = function(r,g,b){
        this.specularColor[0] = r; this.specularColor[1] = g; this.specularColor[2] = b;
        this.data.specular = this.specularColor;
    };

    this.IsOn = function(){ return this.enabled; };
    this.GetType = function(){ return this.type; };
    this.GetDirection = function(){ return this.direction; };
    this.GetSpecular = function(){ return this.specularColor; };
    this.GetDiffuse = function(){ return this.diffuseColor; };
    this.GetAmbient = function(){ return this.ambientColor; };
    this.GetRadius = function(){ return this.radius;};
    this.GetCutOff = function(){ return this.cutOff; };
    this.GetInvDirection = function(){ return this.invdirection; };

    this.LookAt = function( x, y, z, position ){
        mat4.identity(laMatrix);
        mat4.lookAt(laMatrix,position || this.position,[x,y,z],  [0,1,0]);
        this.camera.LookAt( x, y, z, position );
    };
    this.GetMatrix    = function(){
        mat4.identity(this.modelView);
        mat4.multiply(this.modelView, this.rMatrix, this.tMatrix);
        mat4.multiply(this.modelView, this.modelView, this.sMatrix);
        mat4.multiply(this.modelView, this.modelView, laMatrix);
        return this.modelView;
    };
    this.GenerateGLSL = function(){
        if(this.type==0) this.glsl = this.genDirectional();
        else if(this.type == 1) this.glsl = this.genPoint();
        else if(this.type==2) this.glsl = this.genSpot();
    };
    this.genUniforms = function(){
        var uniforms = "";
            if( this.type == 0){ //directional light.
                uniforms += "uniform vec3 u" + this.id +"Direction;\n";
                this.uniforms["u"+this.id +"Direction"] = { type: "vec3", glsl: "uniform vec3 u" + this.id +"Direction;", value: this.GetDirection };


            }else if( this.type == 1 ){  // point light.
                uniforms += "uniform vec3 u" + this.id +"Position;\n";
                uniforms += "uniform float u" + this.id +"Radius;\n";
                this.uniforms["u"+this.id +"Position"] = { type: "vec3", glsl: "uniform vec3 u" + this.id +"Position;", value: this.GetPosition };
                this.uniforms["u"+this.id +"Radius"]   = { type: "float", glsl: "uniform float u" + this.id +"Radius;", value: this.GetRadius   };


            }else if( this.type == 2) { //spot light.
                uniforms += "uniform vec3 u" + this.id +"Position;\n";
                uniforms += "uniform float u" + this.id +"Radius;\n";
                uniforms += "uniform vec3 u" + this.id +"Direction;\n";

                this.uniforms["u"+this.id +"Position"]  = { type: "vec3", glsl: "uniform vec3 u" + this.id +"Position;", value: this.GetPosition };
                this.uniforms["u"+this.id +"Radius"]    = { type: "float", glsl: "uniform float u" + this.id +"Radius;", value: this.GetRadius   };
                this.uniforms["u"+this.id +"Direction"] = { type: "vec3", glsl: "uniform vec3 u" + this.id +"Direction;", value: this.GetDirection };
            }

            uniforms += "uniform vec3 u" + this.id +"Ambient;\n";
            uniforms += "uniform vec3 u" + this.id +"Diffuse;\n";
            uniforms += "uniform vec3 u" + this.id +"Specular;\n";

            this.uniforms["u"+this.id +"Ambient"]   = { type: "vec3", glsl: "uniform vec3 u" + this.id +"Ambient;", value: this.GetAmbient };
            this.uniforms["u"+this.id +"Diffuse"]   = { type: "vec3", glsl: "uniform vec3 u" + this.id +"Diffuse;", value: this.GetDiffuse };
            this.uniforms["u"+this.id +"Specular"]  = { type: "vec3", glsl: "uniform vec3 u" + this.id +"Specular;", value: this.GetSpecular };

       // this.uniforms["u"+this.id +"Specular"]  = { type: "vec3", glsl: "uniform vec3 u" + this.id +"Specular;", value: this.GetSpecular };

        return uniforms;
    };
    this.genVarying = function(){
        var varying =[
              "varying vec3 vTNormal;",
              "varying vec4 vMVertexPos;",
              "varying mat4 vViewMatrix;"
       ].join("\n");
        return varying;
    };
    this.genDirectional = function(){
        var uniforms = this.genUniforms();
        var varying = this.index == 0 ? this.genVarying() : "";

        //VERTEX SHADER:
        this.vs = [
            varying
        ].join("\n");


        //FRAGMENT SHADER:
        this.fs = [
            uniforms,
            varying,
            "vec3 " +this.id +"(vec3 color){",

                 //diffuse.
                "vec3 lightDir = normalize( u" + this.id +"Direction );",
                "float dirLightWeight = max(dot(normalize(vTNormal) , lightDir), 0.0);",

                //specular.
                "vec3 eyeWorldPos       = vec3(vViewMatrix[3].xyz);",
                "vec3 viewVectorEye     = normalize(vMVertexPos.xyz - eyeWorldPos);",
                 "vec3 halfVector        = normalize( viewVectorEye + lightDir );",
                "float strength         = pow(max(dot(normalize(vTNormal),halfVector), 0.0), 25.0);",

                //elements.
                "vec3 diffuse  = (uDiffuse * u"+this.id+"Diffuse *  dirLightWeight);",
                "vec3 ambient  = (uAmbient * u"+this.id+"Ambient);",
                "vec3 specular = (uSpecular *  u"+this.id+"Specular * strength);",

                //final color.
                "vec3 lightFinalColor =  ((ambient + diffuse + specular)* color);",
                "return lightFinalColor;",
            "}"
        ].join("\n");
    };
    this.genPoint = function(){
        var uniforms = this.genUniforms();
        var varying = this.index == 0 ? this.genVarying() : "";

        //VERTEX SHADER:
        this.vs = [
            varying
        ].join("\n");


        //FRAGMENT SHADER:
        this.fs = [
            uniforms,
            varying,
            "vec3 " +this.id +"( vec3 color ){",

                //diffuse.
                "vec4 mvLight           = vec4(u" +this.id +"Position, 1.0) ;",
                "vec3 lightVec          = vec3(vMVertexPos) - vec3(mvLight);",
                "float lightLength      = length(lightVec);",
                "vec3 lightDir          = normalize(lightVec/lightLength);",
                "lightLength            = lightLength * lightLength;",
                "float dirLightWeight   = max(dot(normalize(vTNormal), lightDir), 0.0);",

                //specular.
                "vec3 eyeWorldPos       = vec3(vViewMatrix[3].xyz);",
                "vec3 viewVectorEye     = normalize(vMVertexPos.xyz - eyeWorldPos );",
                "vec3 halfVector        = normalize( viewVectorEye + lightDir );",
                "float strength         = min(1.0, pow(max(dot(normalize(vTNormal),halfVector), 0.0), 25.0));",

                //attenuation
                "float cutoff = 0.0001;",
                "float radius = u"+this.id+"Radius;",
                "float d      = max(lightLength - radius, 0.0);",
                "float denom  = d/radius + 1.0;",
                "float attenuation = 1.0 / (denom*denom);",
                "attenuation = ( attenuation -cutoff) / (1.0-cutoff);",
                "attenuation = max( attenuation, 0.0);",

                //elements.
                "vec3 diffuse  = (uDiffuse * u"+this.id+"Diffuse *  dirLightWeight) *attenuation;",
                "vec3 ambient  = (uAmbient * u"+this.id+"Ambient);",
                "vec3 specular = (uSpecular *  u"+this.id+"Specular * strength) * attenuation;",


                //final color.
                "vec3 lightFinalColor = ((ambient + diffuse + specular)* color);",
                "return lightFinalColor;",
            "}"
        ].join("\n");
    };
    this.genSpot = function(){
        var uniforms = this.genUniforms();
        var varying = this.index == 0 ? this.genVarying() : "";

        //VERTEX SHADER:
        this.vs = [
            varying
        ].join("\n");


        //FRAGMENT SHADER:
        this.fs = [
            uniforms,
            varying,
            "vec3 " +this.id +"( vec3 color ){",

                //diffuse.
                "vec4 mvLight           = vec4(u" +this.id +"Position, 1.0) ;",
                "vec3 lightVec          = vec3(vMVertexPos) - vec3(mvLight);",
                "float lightLength      = length(lightVec);",
                "vec3 lightDir          = normalize(lightVec/lightLength);",
                "lightLength            = lightLength * lightLength;",
                "float dirLightWeight   = max(dot(normalize(vTNormal), lightDir), 0.0);",

                //specular.
                "vec3 eyeWorldPos       = vec3(vViewMatrix[3].xyz);",
                "vec3 viewVectorEye     = normalize(vMVertexPos.xyz - eyeWorldPos );",
                "vec3 halfVector        = normalize( viewVectorEye + lightDir );",
                "float strength         = min(1.0, pow(max(dot(normalize(vTNormal),halfVector), 0.0), 25.0));",

                //attenuation
                "float cutoff = 0.0001;",
                "float radius = u"+this.id+"Radius;",
                "float d      = max(lightLength -radius, 0.0);",
                "float denom  = d/radius + 1.0;",
                "float attenuation = 1.0 / (denom*denom);",
                "attenuation = ( attenuation -cutoff) / (1.0-cutoff);",
                "attenuation = max( attenuation, 0.0);",

                //elements.
                "vec3 diffuse  = (uDiffuse * u"+this.id+"Diffuse *  dirLightWeight) *attenuation;",
                "vec3 ambient  = (uAmbient * u"+this.id+"Ambient);",
                "vec3 specular = (uSpecular *  u"+this.id+"Specular * strength) * attenuation;",


                "vec3 dirToLight  = normalize( lightDir );",
                "vec3 dirFromSpot = normalize( -u" + this.id +"Direction );",



                "float cutOff = "+this.data.cutOff+";",
                "vec3 lightFinalColor = vec3(0);",
                "float angle = dot( dirFromSpot, dirToLight);",
                "if( angle > cutOff ){",
                    "lightFinalColor = ((ambient + diffuse + specular)* color) * (1.0 - (1.0 - angle) * 1.0 / (1.0 - cutOff));",
                "}",

                //final color.
                //"vec3 lightFinalColor = ((ambient + diffuse + specular)* color);",
               // "return vec3(angle, angle, angle);",
                "return lightFinalColor;",
            "}"
        ].join("\n");
    }

    this.setIndex = function( value ){
        this.index = value;
        this.id = "light"+this.index.toString();
    }

    this.SetCastShadows = function( type ){
       this.CreateShadowMap( type, 1024 );
    }
    this.CreateShadowMap = function( type, size ){
        if(type == OMEGA.Omega3D.ShadowType.PCF      ) this.shadowMap = new Omega3D.ShadowMapTexture( size, size);
        else if(type == OMEGA.Omega3D.ShadowType.VSM ) this.shadowMap = new Omega3D.FrameBufferTexture( size, size,1);
    };

    this.mayRender = false;
    this.uniforms = {};
    this.type = type || 0;
    this.vs = "";
    this.fs = "";
    this.index = 0;
    this.id = "light"+this.index.toString();
    this.direction     = [0.0, 0.0, 0.0];
    this.invdirection  = [0.0, 0.0, 0.0];
    var laMatrix = mat4.create();
    this.radius = 1.0;
    this.cutOff = 0.0001;
    this.enabled = true;
    this.ambientColor  = this.data.ambient  || [0, 0, 0];
    this.diffuseColor  = this.data.diffuse  || [0.1, 0.1,0.1];
    this.specularColor = this.data.specular || [0, 0, 0];
    this.camera        = new OMEGA.Omega3D.cameras.Camera();
    this.cameraOffset  = 10;
};
OMEGA.Omega3D.DIRECTIONAL_LIGHT = 0;
OMEGA.Omega3D.POINT_LIGHT = 1;
OMEGA.Omega3D.SPOT_LIGHT = 2;


