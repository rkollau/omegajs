function BlendObject3D( mesh, material ){
    Object3D.apply(this,[mesh, material]);

    this.Update = function(gl,camera){
        gl.disable(gl.DEPTH_TEST);
        gl.enable(gl.BLEND);
        gl.blendFunc(gl.SRC_ALPHA, gl.ONE_MINUS_SRC_ALPHA);
    };
    this.LateUpdate = function(gl,camera){
        gl.enable(gl.DEPTH_TEST);
        gl.disable(gl.BLEND);
    };
}
BlendObject3D.prototype = new Object3D();
OMEGA.Omega3D.BlendObject3D= BlendObject3D;
