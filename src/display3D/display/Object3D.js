function Object3D( mesh, material ){
    this.id   = (++OMEGA.Omega3D.ObjectsCount).toString();
    this.name = "Object3d_" + this.id.toString();
    this.mayRender = true;
    this.next      = null;
    this.prev      = null;
    this.parentScene = null;
    this.position = new Vector4(0, 0, 0, 1);
    this.rotation = new Vector4(0, 0, 0, 0);
    this.scale    = new Vector4(0, 0, 0, 0);
    this.sMatrix   = mat4.create();
    this.tMatrix   = mat4.create();
    this.rMatrix   = mat4.create();
    this.modelView = mat4.create();
    mat4.identity(this.modelView);

    this.mayRender = true;
    this.isShadowCaster = false;

    this.mesh     = mesh;
    this.material = material;
    this.drawType = 0;
    if(this.mesh){
        this.mesh.CreateBuffers();
        this.mesh.CreateColorPickingBuffer(this.id);
    }
    this.parent = null;
    this.children = [];

    this.SetName      = function(value){this.name = value;};
    this.SetMesh      = function(value){ this.mesh = value;      };
    this.SetMaterial  = function(value){ this.material= value;  };
    this.SetMatrix    = function(m){this.modelView = m; };

    this.GetPosition  = function(){ return this.position;  };
    this.GetRotation  = function(){ return this.rotation;  };
    this.GetMesh      = function(){ return this.mesh;      };
    this.GetMaterial  = function(){ return this.material;  };
    this.GetMatrix    = function(){
        mat4.identity(this.modelView);
        if(this.parent!=null)this.modelView = this.parent.GetMatrix();
        mat4.multiply(this.modelView, this.modelView, this.tMatrix);
        mat4.multiply(this.modelView, this.modelView, this.rMatrix);
        mat4.multiply(this.modelView, this.modelView, this.sMatrix);
        return this.modelView;
    };
    this.GetName      = function(){return this.name;};
    this.GetID        = function(){return this.id;};

    this.Identity = function(){
        this.position = [0, 0, 0];
        this.rotation = [0, 0, 0];
        this.scale    = [1, 1, 1];
        this.SetScale( this.scale[0], this.scale[1], this.scale[2] );
        this.SetPosition( this.position[0], this.position[1], this.position[2] );
        this.SetRotation( this.rotation[0],this.rotation[1],this.rotation[2] );
        mat4.identity(this.modelView);
    };

    this.PlaceBack = function(){
        this.SetScale( this.scale[0], this.scale[1], this.scale[2] );
        this.SetPosition( this.position[0], this.position[1], this.position[2] );
        this.SetRotation( this.rotation[0],this.rotation[1],this.rotation[2] );
       // mat4.identity(this.modelView);
    };

    this.SetScale = function( x, y, z ){
        this.scale = [x, y, z];
        mat4.identity(this.sMatrix);
        mat4.scale(this.sMatrix, this.sMatrix, this.scale);
    };


    this.SetPosition = function( x, y, z ){
       // this.position = [x, y, z];
        this.position.x = x;
        this.position.y = y;
        this.position.z = z;

        mat4.identity(this.tMatrix);
        mat4.translate(this.tMatrix, this.tMatrix, Vector4.ToArray( this.position ) );
    };
    this.Translate = function( x, y, z ){
        this.position.x += x;
        this.position.y += y;
        this.position.z += z;

        mat4.identity(this.tMatrix);
        mat4.translate( this.tMatrix, this.tMatrix, Vector4.ToArray( this.position ) );
    };


    this.SetRotation = function( x, y, z ){
        this.rotation.x = x;
        this.rotation.y = y;
        this.rotation.z = z;

        mat4.identity(this.rMatrix);
        mat4.rotate( this.rMatrix, this.rMatrix, this.rotation.x, [1.0, 0.0, 0.0] );
        mat4.rotate( this.rMatrix, this.rMatrix, this.rotation.y, [0.0, 1.0, 0.0] );
        mat4.rotate( this.rMatrix, this.rMatrix, this.rotation.z, [0.0, 0.0, 1.0] );
    };
    this.Rotate = function( x, y, z ){
        mat4.identity(this.rMatrix);
        this.RotateX(x);this.RotateY(y);this.RotateZ(z);
    };

    this.RotateX = function( value ){
       // this.rotation[0] += value;
        this.rotation.x += value;
        mat4.rotate( this.rMatrix, this.rMatrix, this.rotation.x, [1.0, 0.0, 0.0] );
    };
    this.RotateY = function( value ){
       // this.rotation[1] += value;
        this.rotation.y += value;
        mat4.rotate( this.rMatrix, this.rMatrix, this.rotation.y, [0.0, 1.0, 0.0] );
    };
    this.RotateZ = function( value ){
       // this.rotation[2] += value;
        this.rotation.z += value;
        mat4.rotate( this.rMatrix, this.rMatrix, this.rotation.z, [0.0, 0.0, 1.0] );
    };

    this.GetChildByName = function( name ){
        for( var i = 0; i < this.children.length; i++){
            if(this.children[i].name == name ) return this.children[i];
        }
        return null;
    }



    this.SetParent = function( parent ){
        if(this.parent != null){
            this.parent.children.splice(this.parent.children.indexOf( this ), 1 );
        }

        this.parent = parent;
        if(parent != null ){
            this.parent.children.push(this);
            if(this.parent.isShadowCaster){
                this.isShadowCaster = true;
            }
        }
    }



    this.Update = function(gl,camera){};
    this.LateUpdate = function(gl,camera){};

    //26-01-2016 *Removed Object3D render methods.

    this.Clone = function(isShadowCopy, shadowMat){
        var geom; var mat; var child;
        isShadowCopy = isShadowCopy || false;
        shadowMat    = shadowMat || null;

        if( this.GetMesh()) geom = this.GetMesh().GetGeometry();
        if( this.material &&  !isShadowCopy ) mat  = this.material.Clone();
        else if( isShadowCopy && this.material) mat = shadowMat;

        var obj = new Omega3D.Object3D(geom ? new Omega3D.Mesh(geom) : null, mat ? mat : null);
        obj.name = this.name + "_Clone_" + this.id;



        for(var  i = 0; i < this.children.length; i++){
            if( isShadowCopy && this.isShadowCaster){
                child = this.children[i].Clone(true, shadowMat );
                child.SetParent(obj);
            }else if( !isShadowCopy ){
                child = this.children[i].Clone();
                child.SetParent(obj);
            }
        }

        if(isShadowCopy){
            obj.sMatrix    = this.sMatrix;
            obj.tMatrix    = this.tMatrix;
            obj.rMatrix    = this.rMatrix;
            obj.position   = this.position;
            obj.rotation   = this.rotation;
            obj.scale      = this.scale;
        }
        return obj;
    }

    this.toString = function(){
        return "Object3D { id: " + this.id + ", name: " + this.name + "};"
    };

};
OMEGA.Omega3D.Object3D = Object3D;
OMEGA.Omega3D.Object3D.DEFAULT   = 0;
OMEGA.Omega3D.Object3D.POINTS    = 1;
OMEGA.Omega3D.Object3D.WIREFRAME = 2;
OMEGA.Omega3D.Object3D.TRIANGLES = 3;
OMEGA.Omega3D.Object3D.LINES     = 4;