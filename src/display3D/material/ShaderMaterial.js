function ShaderMaterial( data, scene ){

    this.scene = scene;

    this.fragmentOnlyUniforms = {};

    //var attribs = ;
    //attribs.aTextureCoord = { type: "vec2",glsl: "attribute vec2 aTextureCoord;", value:null};
    //attribs.aVertexPos    = { type: "vec3",glsl: "attribute vec3 aVertexPos;"   , value: null};
    //attribs.aVertexNormal = { type: "vec3",glsl: "attribute vec3 aVertexNormal;", value:null};
    //attribs.aVertexTangent      = { type: "vec3",glsl: "attribute vec3 aVertexTangent;", value:null};
    //attribs.aVertexBitangent    = { type: "vec3",glsl: "attribute vec3 aVertexBitangent;", value:null};
    //attribs.aBaricentric        = { type: "vec3",glsl: "attribute vec3 aBaricentric;"       , value:null};
    //attribs.aPickingColor       = { type: "vec3",glsl: "attribute vec3 aPickingColor;"      , value:null}
    this.attribs = OMEGA.Omega3D.Shaders.Components.StandardAttributes();

    //var uniforms = {};
    //uniforms.uModelMatrix      = { type: "mat4", glsl: "uniform mat4 uModelMatrix;"     , value: null };
    //uniforms.uProjectionMatrix = { type: "mat4", glsl: "uniform mat4 uProjectionMatrix;", value: null };
    //uniforms.uViewMatrix       = { type: "mat4", glsl: "uniform mat4 uViewMatrix;"      , value: null };
    //uniforms.uInvViewMatrix    = { type: "mat4", glsl: "uniform mat4 uInvViewMatrix;"   , value: null };
    //uniforms.uNormalMatrix     = { type: "mat3", glsl: "uniform mat3 uNormalMatrix;"    , value: null };

    this.uniforms = OMEGA.Omega3D.Shaders.Components.StandardUniforms();

    //
    //var custom_uniforms = {};
    var custom_attribs = {};
    this.custom_uniforms = {};
    this.custom_attribs = custom_attribs;

    var self = this;

    this.Clear = function(){
        for( var key in  this.custom_uniforms){
            this.custom_uniforms[key] = {type:  this.custom_uniforms[key].type, glsl:  this.custom_uniforms[key].glsl    , value: null };
        }

    };

    //this.SetValue = function( key, value ){
    //    console.log( key );
    //   var o =  uniforms[key] || self.attribs[key] || self.custom_uniforms[key];
    //   if(o){
    //
    //       o.value = value;
    //   }
    //};
    this.GetValue = function( key ){
        var o =  uniforms[key] || attribs[key] || this.custom_uniforms[key] || {value:null};
        return o.value;
    };



    this.processData = function( data ){
        var texCount = 0;
        for( var key in data.uniforms){

            var glsl = "";
            glsl = "uniform " + data.uniforms[key].type + " " + key + ";";
            if( data.uniforms[key].type != "sampler2D" &&
                data.uniforms[key].type != "samplerCube" ) {

                self.custom_uniforms[key] = { type: data.uniforms[key].type, glsl: glsl, value: data.uniforms[key].value };
            }else{
                self.fragmentOnlyUniforms[key] = { id:key, type: data.uniforms[key].type, glsl: glsl, value: null };
                data.uniforms[key].value.tex_id = key;
                data.uniforms[key].value.ID = texCount++;
                console.log( key )
                console.log( data.uniforms[key].value );
                this.textures.push( data.uniforms[key].value );
            }
        };
        for( var key in data.attribs){
            var glsl = "";
            glsl = "attribute " + data.attribs[key].type + " " + key + ";";
            self.custom_attribs[key] = { type: data.attribs[key].type, glsl: glsl, value: data.attribs[key].value };
        };

    };

    var createShader = function(){
        OMEGA.Omega3D.Log(" >> ShaderMaterial: Creating custom shader");
        var vertex_shader_src = "";
        vertex_shader_src += attachData( self.attribs );
        vertex_shader_src += attachData(self.custom_attribs );
        vertex_shader_src += attachData(self.uniforms );
        vertex_shader_src += data.vertex_src;

        var fragment_shader_src = "precision mediump float;";
        fragment_shader_src += data.fragment_src;

        //var injected = OMEGA.Omega3D.ShaderUtil.InjectGLSLForLights(vertex_shader_src, fragment_shader_src, uniforms, self.scene);
        //self.uniforms = injected.uniforms;
        //vertex_shader_src =injected.vs;
        //fragment_shader_src =injected.fs;

        //vars
        //1. attributes
        //2. uniforms
        //3. custom_uniforms
        //4. fragmentOnlyUniforms

       return new OMEGA.Omega3D.Shader(vertex_shader_src,fragment_shader_src,  [OMEGA.Omega3D.Shaders.Components.StandardAttributes(),
                                                                                OMEGA.Omega3D.Shaders.Components.StandardUniforms(),
                                                                                self.custom_uniforms,
                                                                                self.fragmentOnlyUniforms]);



        return new OMEGA.Omega3D.Shader( vertex_shader_src, fragment_shader_src, [ this.attribs, this.uniforms, self.custom_uniforms,  this.fragmentOnlyUniforms,  this.custom_attribs]);
    };
    var attachData = function(  a){
        var out = "";
        for( var key in a) out += "\n"+ a[key].glsl;
        return out;
    };

    Material.apply(this,[null, []]);
   // this.genUniforms();
    this.processData(data);
    this.shader = createShader();

};
ShaderMaterial.prototype = new Material();
OMEGA.Omega3D.ShaderMaterial = ShaderMaterial;
