function DynamicCubemapTexture(imgs, w, h, ID){
    Texture.apply( this, [null, true, ID] );
    var frameBuffer, depthBuffer;

    this.fov = 76.978;
    this.near =0.1;
    this.far = 1000.0;


    this.cubemapCam = new Omega3D.cameras.Camera();
    this.faces = [ this.gl_context.TEXTURE_CUBE_MAP_POSITIVE_X,
        this.gl_context.TEXTURE_CUBE_MAP_NEGATIVE_X,
        this.gl_context.TEXTURE_CUBE_MAP_POSITIVE_Y,
        this.gl_context.TEXTURE_CUBE_MAP_NEGATIVE_Y,
        this.gl_context.TEXTURE_CUBE_MAP_POSITIVE_Z,
        this.gl_context.TEXTURE_CUBE_MAP_NEGATIVE_Z ];

    this.tex =  this.gl_context.createTexture();
    this.gl_context.bindTexture(this.gl_context.TEXTURE_CUBE_MAP,  this.tex);
    this.gl_context.texParameteri(this.gl_context.TEXTURE_CUBE_MAP, this.gl_context.TEXTURE_WRAP_S, this.gl_context.CLAMP_TO_EDGE);
    this.gl_context.texParameteri(this.gl_context.TEXTURE_CUBE_MAP, this.gl_context.TEXTURE_WRAP_T, this.gl_context.CLAMP_TO_EDGE);
    this.gl_context.texParameteri(this.gl_context.TEXTURE_CUBE_MAP, this.gl_context.TEXTURE_MIN_FILTER, this.gl_context.LINEAR);
    this.gl_context.texParameteri(this.gl_context.TEXTURE_CUBE_MAP, this.gl_context.TEXTURE_MAG_FILTER, this.gl_context.LINEAR);

    if(Omega3D.EXT.anisotropic_filtering != null) {
        var max = this.gl_context.getParameter(Omega3D.EXT.anisotropic_filtering.MAX_TEXTURE_MAX_ANISOTROPY_EXT);
        this.gl_context.texParameterf(this.gl_context.TEXTURE_CUBE_MAP, Omega3D.EXT.anisotropic_filtering.TEXTURE_MAX_ANISOTROPY_EXT, max);
    }

    for (var i = 0; i < 6; i++) {
        this.gl_context.texImage2D(this.faces[i], 0,  this.gl_context.RGBA,  this.gl_context.RGBA,  this.gl_context.UNSIGNED_BYTE, imgs[i]);
    }

    //frame buffer
    frameBuffer =  this.gl_context.createFramebuffer();
    this.gl_context.bindFramebuffer(  this.gl_context.FRAMEBUFFER, frameBuffer);
    frameBuffer.width = w || 1024;
    frameBuffer.height = h || 1024;


    //depth buffer
    depthBuffer = this.gl_context.createRenderbuffer();
    this.gl_context.bindRenderbuffer( this.gl_context.RENDERBUFFER, depthBuffer );
    this.gl_context.renderbufferStorage(this.gl_context.RENDERBUFFER, this.gl_context.DEPTH_COMPONENT16, frameBuffer.width, frameBuffer.height);
    this.gl_context.framebufferRenderbuffer(this.gl_context.FRAMEBUFFER, this.gl_context.DEPTH_ATTACHMENT, this.gl_context.RENDERBUFFER, depthBuffer);


    this.gl_context.framebufferTexture2D(this.gl_context.FRAMEBUFFER, this.gl_context.COLOR_ATTACHMENT0, this.faces[0] , this.tex, 0);



    if(!this.gl_context.checkFramebufferStatus(this.gl_context.FRAMEBUFFER) === this.gl_context.FRAMEBUFFER_COMPLETE) {
        console.error("Framebuffer incomplete!");
    }

    this.gl_context.bindFramebuffer(this.gl_context.FRAMEBUFFER, null);
    this.gl_context.bindTexture(this.gl_context.TEXTURE_2D, null);



    this.UpdateFaces = function( fromViewPoint, renderer, scene ){
        this.gl_context.bindFramebuffer(  this.gl_context.FRAMEBUFFER, this.GetFrameBuffer() );
        this.cubemapCam.fov =  this.fov;
        this.cubemapCam.near =this.near;
        this.cubemapCam.far = this.far;
        this.cubemapCam.ratio = 1.0;

        for (var i = 0; i < 6; ++i){
            this.UpdateFace(i,fromViewPoint, renderer, scene, this.cubemapCam );
        }

        this.gl_context.bindFramebuffer( this.gl_context.FRAMEBUFFER, null);
    };
    this.UpdateFace = function( faceIndex, fromViewPoint, renderer, scene ){

        this.gl_context.framebufferTexture2D(this.gl_context.FRAMEBUFFER, this.gl_context.COLOR_ATTACHMENT0, this.faces[faceIndex], this.tex, 0);
        var pos = fromViewPoint;
        switch( faceIndex ) {
            case 0: //positive x
                this.cubemapCam.LookAtWithUp(pos[0]+1,pos[1] , pos[2], pos, [0, -1, 0] );
                break;
            case 1: //negative x
                this.cubemapCam.LookAtWithUp(pos[0]-1,pos[1] , pos[2], pos, [0, -1, 0]);
                break;
            case 2: //positive y
                this.cubemapCam.LookAtWithUp(pos[0],pos[1] +10, pos[2], pos, [0, 0,1]);
                break;
            case 3: //negative y
                this.cubemapCam.LookAtWithUp(pos[0],pos[1] -10, pos[2], pos, [0, 0, -1]);
                break;
            case 4: //positive z
                this.cubemapCam.LookAtWithUp(pos[0], pos[1], pos[2] +  1, pos, [0, -1, 0]);
                break;
            case 5: //negative z
                this.cubemapCam.LookAtWithUp(pos[0],pos[1], pos[2] -  1, pos, [0, -1, 0]);
                break;
        }
        this.gl_context.enable( this.gl_context.DEPTH_TEST );
        renderer.viewPort( scene, 0, 0, this.GetFrameBuffer().width ,this.GetFrameBuffer().height );
        renderer.renderScene( scene, this.cubemapCam );
        this.gl_context.disable( this.gl_context.DEPTH_TEST );
    };





    this.Enable = function( shader ){
        this.gl_context.activeTexture( this.gl_context.TEXTURE0+this.ID);
        this.gl_context.bindTexture(this.gl_context.TEXTURE_CUBE_MAP,  this.tex);
        if(shader){
            var samplerLocation = shader.GetSamplerLocation(this.tex_id);
            if(samplerLocation == -1) return;
            this.gl_context.uniform1i(samplerLocation , this.ID );
        }
    };
    this.GetFrameBuffer = function(){
        return frameBuffer;
    };
    this.Disable = function(){
        this.gl_context.bindTexture(this.gl_context.TEXTURE_2D, null);
        this.gl_context.bindRenderbuffer( this.gl_context.RENDERBUFFER, null );
        this.gl_context.bindFramebuffer(this.gl_context.FRAMEBUFFER, null);
    };

    this.Update = function() {
        if(!this.needsUpdate ) if(!this.isDirty ) return;
        this.isDirty    = false;
    };

};
DynamicCubemapTexture.prototype = new Texture();
OMEGA.Omega3D.DynamicCubemapTexture = DynamicCubemapTexture;
