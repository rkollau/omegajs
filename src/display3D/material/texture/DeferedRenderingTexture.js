function DeferedRenderingTexture( w, h, ID){
    Texture.apply( this, [null, true, ID] );
    if( Omega3D.EXT.draw_buffers==null ) alert("Draw Buffers not supported.");

    this.textures = new Array();
    var frameBuffer;


    this.handleTextureLoaded = function(image, texture ) {

        //frame buffer
        frameBuffer = this.gl_context.createFramebuffer();
        this.gl_context.bindFramebuffer(this.gl_context.FRAMEBUFFER, frameBuffer);
        frameBuffer.width = w || 1024;
        frameBuffer.height = h || 1024;


        //textures.
        this.depthTexture = this.gl_context.createTexture();
        this.normalTexture = this.gl_context.createTexture();
        this.positionTexture = this.gl_context.createTexture();
        this.colorTexture = this.gl_context.createTexture();
        this.depthRGBTexture = this.gl_context.createTexture();

        //normal texture.
        this.gl_context.bindTexture(this.gl_context.TEXTURE_2D, this.normalTexture);
        this.gl_context.texParameteri(this.gl_context.TEXTURE_2D, this.gl_context.TEXTURE_WRAP_S, this.gl_context.CLAMP_TO_EDGE);
        this.gl_context.texParameteri(this.gl_context.TEXTURE_2D, this.gl_context.TEXTURE_WRAP_T, this.gl_context.CLAMP_TO_EDGE);
        this.gl_context.texParameteri(this.gl_context.TEXTURE_2D, this.gl_context.TEXTURE_MIN_FILTER, this.gl_context.NEAREST);
        this.gl_context.texParameteri(this.gl_context.TEXTURE_2D, this.gl_context.TEXTURE_MAG_FILTER, this.gl_context.NEAREST);
        this.gl_context.texImage2D(this.gl_context.TEXTURE_2D, 0, this.gl_context.RGBA, w, h, 0, this.gl_context.RGBA, this.gl_context.FLOAT, null);

        //position texture.
        this.gl_context.bindTexture(this.gl_context.TEXTURE_2D, this.positionTexture);
        this.gl_context.texParameteri(this.gl_context.TEXTURE_2D, this.gl_context.TEXTURE_WRAP_S, this.gl_context.CLAMP_TO_EDGE);
        this.gl_context.texParameteri(this.gl_context.TEXTURE_2D, this.gl_context.TEXTURE_WRAP_T, this.gl_context.CLAMP_TO_EDGE);
        this.gl_context.texParameteri(this.gl_context.TEXTURE_2D, this.gl_context.TEXTURE_MIN_FILTER, this.gl_context.NEAREST);
        this.gl_context.texParameteri(this.gl_context.TEXTURE_2D, this.gl_context.TEXTURE_MAG_FILTER, this.gl_context.NEAREST);
        this.gl_context.texImage2D(this.gl_context.TEXTURE_2D, 0, this.gl_context.RGBA, w, h, 0, this.gl_context.RGBA, this.gl_context.FLOAT, null);


        //color texture.
        this.gl_context.bindTexture(this.gl_context.TEXTURE_2D, this.colorTexture);
        this.gl_context.texParameteri(this.gl_context.TEXTURE_2D, this.gl_context.TEXTURE_WRAP_S, this.gl_context.CLAMP_TO_EDGE);
        this.gl_context.texParameteri(this.gl_context.TEXTURE_2D, this.gl_context.TEXTURE_WRAP_T, this.gl_context.CLAMP_TO_EDGE);
        this.gl_context.texParameteri(this.gl_context.TEXTURE_2D, this.gl_context.TEXTURE_MIN_FILTER, this.gl_context.NEAREST);
        this.gl_context.texParameteri(this.gl_context.TEXTURE_2D, this.gl_context.TEXTURE_MAG_FILTER, this.gl_context.NEAREST);
        this.gl_context.texImage2D(this.gl_context.TEXTURE_2D, 0, this.gl_context.RGBA, w, h, 0, this.gl_context.RGBA, this.gl_context.FLOAT, null);


        //depth RGB texture.
        this.gl_context.bindTexture(this.gl_context.TEXTURE_2D, this.depthRGBTexture);
        this.gl_context.texParameteri(this.gl_context.TEXTURE_2D, this.gl_context.TEXTURE_WRAP_S, this.gl_context.CLAMP_TO_EDGE);
        this.gl_context.texParameteri(this.gl_context.TEXTURE_2D, this.gl_context.TEXTURE_WRAP_T, this.gl_context.CLAMP_TO_EDGE);
        this.gl_context.texParameteri(this.gl_context.TEXTURE_2D, this.gl_context.TEXTURE_MAG_FILTER, this.gl_context.NEAREST);
        this.gl_context.texParameteri(this.gl_context.TEXTURE_2D, this.gl_context.TEXTURE_MIN_FILTER, this.gl_context.NEAREST);
        this.gl_context.texImage2D( this.gl_context.TEXTURE_2D, 0,  this.gl_context.RGBA, w, h, 0,  this.gl_context.RGBA,  this.gl_context.FLOAT, null);

        if (Omega3D.EXT.draw_buffers != null) {
            this.buffers = [];
            this.buffers[0] = Omega3D.EXT.draw_buffers.COLOR_ATTACHMENT0_WEBGL;
            this.buffers[1] = Omega3D.EXT.draw_buffers.COLOR_ATTACHMENT1_WEBGL;
            this.buffers[2] = Omega3D.EXT.draw_buffers.COLOR_ATTACHMENT2_WEBGL;
            this.buffers[3] = Omega3D.EXT.draw_buffers.COLOR_ATTACHMENT3_WEBGL;

            var depthBuffer = this.gl_context.createRenderbuffer();
            this.gl_context.bindRenderbuffer(this.gl_context.RENDERBUFFER, depthBuffer);
            this.gl_context.renderbufferStorage(this.gl_context.RENDERBUFFER, this.gl_context.DEPTH_COMPONENT16, frameBuffer.width, frameBuffer.height);

            this.gl_context.framebufferTexture2D(this.gl_context.FRAMEBUFFER, this.buffers[0], this.gl_context.TEXTURE_2D, this.normalTexture, 0);
            this.gl_context.framebufferTexture2D(this.gl_context.FRAMEBUFFER, this.buffers[1], this.gl_context.TEXTURE_2D, this.positionTexture, 0);
            this.gl_context.framebufferTexture2D(this.gl_context.FRAMEBUFFER, this.buffers[2], this.gl_context.TEXTURE_2D, this.colorTexture, 0);
            this.gl_context.framebufferTexture2D(this.gl_context.FRAMEBUFFER, this.buffers[3], this.gl_context.TEXTURE_2D, this.depthRGBTexture, 0);

            this.gl_context.framebufferRenderbuffer(this.gl_context.FRAMEBUFFER, this.gl_context.DEPTH_ATTACHMENT, this.gl_context.RENDERBUFFER, depthBuffer);

            if (this.gl_context.checkFramebufferStatus(this.gl_context.FRAMEBUFFER) !== this.gl_context.FRAMEBUFFER_COMPLETE) {
                console.error("Framebuffer incomplete!");
            }

            Omega3D.EXT.draw_buffers.drawBuffersWEBGL(this.buffers);

            this.gl_context.bindTexture(this.gl_context.TEXTURE_2D, null);
            this.gl_context.bindFramebuffer(this.gl_context.FRAMEBUFFER, null);
        }
    }
    this.Enable = function( shader ){
        this.gl_context.bindFramebuffer(this.gl_context.FRAMEBUFFER, frameBuffer);
    };
    this.GetFrameBuffer = function(){
        return frameBuffer;
    };
    this.Disable = function(){
        this.gl_context.bindFramebuffer(this.gl_context.FRAMEBUFFER, null);
    };
    this.Update = function() {
        if(!this.needsUpdate ) if(!this.isDirty ) return;
        this.isDirty    = false;
    };
    this.handleTextureLoaded(this.img, this.tex);
};
DeferedRenderingTexture.prototype = new Texture();
OMEGA.Omega3D.DeferedRenderingTexture = DeferedRenderingTexture;
