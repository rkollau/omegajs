function ShadowMapMaterial( textures, scene ){
    var type = scene.shadowType | OMEGA.Omega3D.ShadowType.PCF;
    for(var i = 0; i < scene.getLights().length; i++){
        var l = scene.getLights()[i];
        if(l.shadowMap != undefined ) textures.push(l.shadowMap);
    }

    if(type == OMEGA.Omega3D.ShadowType.PCF)
    {

        Material.apply(this, [new OMEGA.Omega3D.Shaders.SM02(scene), textures ]);

    }
    else  if(type == OMEGA.Omega3D.ShadowType.VSM)
    {
        Material.apply(this, [new OMEGA.Omega3D.Shaders.VSM(scene), textures ]);
    }
    for(var i = 0; i < scene.getLights().length; i++){
        this.custom_uniforms["uLightM_"+(i).toString()] = { type:"mat4", glsl: "", value: scene.getLights()[i].camera.GetMatrix() };
    }
    this.custom_uniforms["uLightM"] = { type:"mat4", glsl: "", value: scene.getLights()[0].camera.GetMatrix() };
}
ShadowMapMaterial.prototype = new Material();
OMEGA.Omega3D.ShadowMapMaterial = ShadowMapMaterial;