function SkyBoxMaterial( textures, scene ){
    Material.apply(this, [ new OMEGA.Omega3D.Shaders.SkyBox(scene),textures]);
}
SkyBoxMaterial.prototype = new Material();
OMEGA.Omega3D.SkyBoxMaterial = SkyBoxMaterial;
