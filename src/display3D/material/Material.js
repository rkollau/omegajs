function Material( shader, textures) {
    this.materialID = "material_" + (++OMEGA.Omega3D.MaterialsCount);
    this.textures = textures || null;
    this.shader = shader || null;

    this.diffuse  = [1.0, 1.0, 1.0];
    this.ambient  = [1.0, 1.0, 1.0];
    this.specular = [1.0, 1.0, 1.0];

    this.mayRender = true;
    this.hasAtlas = false;
    this.atlas = null;

    this.GetTextures = function() {
        return this.textures;
    };
    this.SetTextures = function(textures) {
         this.textures = textures;
    };
    this.GetShader = function() {
        return this.shader;
    };
    this.SetShader = function( shader ) {
        this.shader = shader;
    };
    this.GetID = function() {
        return this.materialID;
    };

    this.SetAtlas = function( atlas_object ){
       this.hasAtlas = true;
        this.atlas    = atlas_object;
    };

    this.GetCurrentAtlasFrame = function(){
        return this.atlas.GetCurrentFrame().uvRect;
    }

    this.Enable = function() {
        if (this.shader  ) this.shader.Enable();
        if (this.textures && this.mayRender){
            for(var i = 0; i <  this.textures.length; i++) {
                //if(i > 0 ){
                //    this.textures[i].tex_id = "uSampler_" + i;
                //    this.textures[i].ID = i;
                //}
                this.textures[i].Enable(this.shader);
            }

            for(var i = 0; i <  this.textures.length; i++) {
                //if(i > 0 ){
                //    this.textures[i].tex_id = "uSampler";
                //    this.textures[i].ID = 0;
                //}
            }
        }
    };
    this.Disable = function() {
        if (this.shader) this.shader.Disable();
        if (this.textures && this.mayRender){
            var l = this.textures.length;
            for(var i = 0; i < l; i++) {
                this.textures[i].Disable();
            }
        }
    };

    this.GetSpecular = function(){ return this.specular; };
    this.GetDiffuse = function(){ return this.diffuse; };
    this.GetAmbient = function(){ return this.ambient; };
    this.SetAmbient = function(r,g,b){
        this.ambient[0] = r; this.ambient[1] = g; this.ambient[2] = b;
    };

    this.SetDiffuse = function(r,g,b){
        this.diffuse[0] = r; this.diffuse[1] = g; this.diffuse[2] = b;
    };
    this.SetSpecular = function(r,g,b){
        this.specular[0] = r; this.specular[1] = g; this.specular[2] = b;
    };


    this.SetValue = function( key, type, value ){

        if(this.GetShader()== null)return;
        var o =  this.custom_uniforms[key];
        if(o != null){
            o.value = value;
        }else{

            this.custom_uniforms[key] = { type: type, glsl: "uniform " + type + " " + key+";", value: value };
            this.GetShader().program[key] = this.GetShader().gl_context.getUniformLocation( this.GetShader().program, key);
        }
    };
    this.GetValue = function( key ){
        var o =  this.custom_uniforms[key];
        return o != null ? o.value : null;
    };


    this.genUniforms = function(){
        this.uniforms["uAmbient"]   = { type: "vec3", glsl: "uniform vec3 uAmbient;" , value: this.GetAmbient };
        this.uniforms["uDiffuse"]   = { type: "vec3", glsl: "uniform vec3 uDiffuse;" , value: this.GetDiffuse };
        this.uniforms["uSpecular"]  = { type: "vec3", glsl: "uniform vec3 uSpecular;", value: this.GetSpecular };
    };

    this.uniforms = {};
    this.custom_uniforms = {};
    this.custom_attribs = {};
    this.genUniforms();


    this.Clone = function(){
        var clone = new Omega3D.Material(this.shader.Clone(), this.textures);
        clone.uniforms        = this.uniforms;
        clone.custom_uniforms = this.custom_uniforms;
        clone.custom_attribs  = this.custom_attribs;
        clone.SetDiffuse( this.diffuse[0], this.diffuse[1], this.diffuse[2]);
        clone.SetSpecular( this.specular[0], this.specular[1], this.specular[2]);
        return clone;
    }
};
OMEGA.Omega3D.Material = Material;



