/**
 *  SHADER COMPONENTS
 */
OMEGA.Omega3D.Shaders = OMEGA.Omega3D.Shaders || {};
OMEGA.Omega3D.Shaders.Components = OMEGA.Omega3D.Shaders.Components || {};


/**
 *
 *  BASICS.
 *
 */

OMEGA.Omega3D.Shaders.Components.Basic_TextureCoords = function(){
    return "vTexCoord = aTextureCoord;";
};
OMEGA.Omega3D.Shaders.Components.Basic_Includes = function(){
    var script =
        "attribute vec2 aTextureCoord;"+
        "attribute vec3 aVertexPos;"+
        "attribute vec3 aVertexNormal;"+
        "attribute vec3 aVertexColor;"+
        "attribute vec3 aVertexTangent;"+
        "attribute vec3 aVertexBitangent;"+
        "attribute vec3 aPickingColor;"+

        "uniform mat4 uModelMatrix;"+
        "uniform mat4 uProjectionMatrix;"+
        "uniform mat4 uViewMatrix;"+
        "uniform mat4 uInvViewMatrix;"+
        "uniform vec3 uCamPos;"+
        "uniform mat3 uNormalMatrix;";
    return script;
};
OMEGA.Omega3D.Shaders.Components.Fragment_Precision_Mediump_Float = function(){
    return "precision mediump float;";
};
OMEGA.Omega3D.Shaders.Components.Fragment_Precision_Highp_Float = function(){
    return "precision highp float;";
};
OMEGA.Omega3D.Shaders.Components.Basic_Fragment_Includes = function(){
    var script =
        "uniform vec3 uDiffuse;"+
        "uniform vec3 uAmbient;"+
        "uniform vec3 uSpecular;";
    return script;
};
OMEGA.Omega3D.Shaders.Components.Basic_Fragment_Logic = function( color ){
    return "gl_FragColor = " + color + ";";
};
OMEGA.Omega3D.Shaders.Components.Basic_Includes_Post_Processing = function(){
    var script =
    "attribute vec2 aTextureCoord;"+
    "attribute vec3 aVertexPos;"+
    "attribute vec3 aVertexNormal;" +
    "uniform float uTime;"+
    "uniform mat4 uModelMatrix;"+
    "uniform mat4 uProjectionMatrix;"+
    "uniform mat4 uViewMatrix;"+
    "uniform mat3 uNormalMatrix;";
    return script;
};


/**
 *
 *  LIGHTING.
 *
 */
OMEGA.Omega3D.Shaders.Components.Struct_Light = function(){
    var str = [
        "struct Light {",
        "mat4 mat;",
        "vec3 position;",
        "vec3 direction;",
        "vec3 ambient;",
        "vec3 diffuse;",
        "vec3 specular;",
        "float radius;",
        "float cutOff;",
        "int type;",
        "bool enabled;",
        "};"
    ].join("\n");
    return str;
};
OMEGA.Omega3D.Shaders.Components.CalcLightAddition = function(){
    var str =  [
        //varyings from vertex shader:
        // vMVertexPos
        // vTNormal

        "vec3 CalcLightAddition( vec3 color, Light light, vec3 eyeWorldPos ) {",
            "if( light.enabled == false) return vec3(0.0, 0.0, 0.0);",
            "vec3 normalizedNormal = normalize( vTNormal );",
            "vec3 lightFinalColor = vec3(0);",
            //swith on type.
            // 0 - > directional
            // 1 - > point
            // 2 - > spot

            "if( light.type == 0 ){",
                //diffuse component.
                "vec3 lightDirection = normalize( light.direction );",
                "float diffuseWeight = max(dot(normalizedNormal , lightDirection), 0.0);",

                //specular.
                "vec3 viewVectorEye     = normalize(vMVertexPos.xyz - eyeWorldPos);",
                "vec3 halfVector        = normalize( viewVectorEye + lightDirection );",
                "float specularWeight   = pow(max(dot(normalizedNormal,halfVector), 0.0), 25.0);",

                //elements.
                "vec3 diffuse  = (uDiffuse * light.diffuse *  diffuseWeight);",
                "vec3 ambient  = (uAmbient * light.ambient );",
                "vec3 specular = (uSpecular * light.specular * specularWeight);",

                //final color.
                "lightFinalColor =  ((ambient + diffuse + specular)* color);",

            "}else if( light.type > 0){",
                //diffuse component.
                "vec3 lightVec         = vec3( vMVertexPos) - vec3( light.position);",
                "float lightLength     = length( lightVec );",
                "vec3 lightDirection   = normalize( lightVec / lightLength );",
                "float diffuseWeight   = max( dot( normalizedNormal, lightDirection) , 0.0 );",

                //specular component.
                "vec3 viewVectorEye     = normalize( vMVertexPos.xyz - eyeWorldPos );",
                "vec3 halfVector        = normalize( viewVectorEye + lightDirection );",
                "float specularWeight   = min(1.0, pow(max(dot( normalizedNormal,halfVector ), 0.0), 25.0));",

                //attenuation
                "float d      = max(lightLength - light.radius, 0.0);",
                "float denom  = d/light.radius + 1.0;",
                "float attenuationWeight = 1.0 / (denom*denom);",
                "attenuationWeight       = ( attenuationWeight -light.cutOff) / (1.0-light.cutOff);",
                "attenuationWeight       = max( attenuationWeight, 0.0);",

                //elements.
                "vec3 diffuse  = (uDiffuse * light.diffuse *  diffuseWeight) * attenuationWeight;",
                "vec3 ambient  = (uAmbient * light.ambient );",
                "vec3 specular = (uSpecular * light.specular * specularWeight) * attenuationWeight;",

                //final color.
                "if( light.type == 2 ) {",
                    "vec3 dirToLight  = normalize( lightDirection );",
                    "vec3 dirFromSpot = normalize( -light.direction );",

                    "vec3 lightFinalColor = vec3(0);",
                    "float angle = dot( dirFromSpot, dirToLight);",
                    "if( angle > light.cutOff ){",
                        "lightFinalColor = ((ambient + diffuse + specular)* color) * (1.0 - (1.0 - angle) * 1.0 / (1.0 - light.cutOff));",
                    "}else{",
                        "lightFinalColor = (ambient * color);",
                    "}",
                "}else{",
                    "lightFinalColor = ((ambient + diffuse + specular)* color);",
                "}",
            "}",
            "return lightFinalColor;",
        "}"
    ].join("\n");
    return str;
};
OMEGA.Omega3D.Shaders.Components.CalcShadowAdditionPCF = function(){
    var str = [
        "float CalcShadowAddition( vec4 lightPos, sampler2D sampler ){",
            
            "float bias       = 0.00002;",
          //  "float bias       = 0.000000001;",
            "vec3 nLightPos   = (lightPos.xyz / lightPos.w) / 2.0 + 0.5;",

            "vec4 depth        = texture2D(uSampler, vec2(nLightPos.xy));",
            "float divider    = 500.0*depth.r;",
            "float visibility = 1.0;",
            "float step       = 0.05;",

             "float cosTheta = dot( nLightPos, lightPos.xyz );",
            // "bias = 0.005*tan(acos(cosTheta));",
           //  "bias = clamp( );",

           // "if(bias <0.0) bias = 0.0;",
           // "if(bias > 0.01) bias = 0.01;",

           /* "vec2 poissonDisk[4];",
            "poissonDisk[0] = vec2( -0.94201624, -0.39906216 );",
            "poissonDisk[1] = vec2( 0.94558609, -0.76890725 );",
            "poissonDisk[2] = vec2( -0.094184101, -0.92938870 );",
            "poissonDisk[3] = vec2( 0.34495938, 0.29387760 );",

           // "if ( texture2D( sampler, nLightPos.xy).r <  nLightPos.z - bias ){ visibility = 0.5; }",

           "for( int i = 0; i < 4;i++){",
               // "if( nLightPos.z < 0.0){",
                    "if ( texture2D( sampler, nLightPos.xy + poissonDisk[i]/divider).r + bias   <  nLightPos.z ){ visibility -= 0.1; }",
               // "}",
           "}",*/


            "if ( texture2D( sampler, nLightPos.xy/divider ).r == 0.0  ){}",
            "else{",
            "if ( texture2D( sampler, nLightPos.xy ).r   + bias <  nLightPos.z  ){ visibility-=step; }",
            "if ( texture2D( sampler, nLightPos.xy+ vec2( -0.94201624 , -0.39906216 )/divider ).r   + bias<  nLightPos.z  ){visibility -=step; }",
            "if ( texture2D( sampler, nLightPos.xy+ vec2(  0.94558609 , -0.76890725 )/divider ).r   + bias<  nLightPos.z  ){ visibility-=step; }",
            "if ( texture2D( sampler, nLightPos.xy+ vec2( -0.094184101, -0.92938870 )/divider ).r   + bias<  nLightPos.z  ){ visibility-=step; }",
            "if ( texture2D( sampler, nLightPos.xy+ vec2(  0.34495938 ,  0.29387760 )/divider ).r   + bias<  nLightPos.z  ){ visibility-=step; }}",

            "return visibility;",
        "}"
    ].join("\n");
    return str;
};
OMEGA.Omega3D.Shaders.Components.CalcShadowAdditionVSM = function(){
    var str = [
        "float ChebyshevUpperBound( vec2 moments, float t ){",
            "float min_variance = 0.0002;",

            "if(t <= moments.x) return 1.0;",

            "float p = smoothstep(t-min_variance, t, moments.x);",
            "float variance = max( moments.y - moments.x * moments.x, min_variance );",

            "float d = t -moments.y;",
            "float p_max = linestep( 0.1, variance / (variance + d*d));",
            "return min(1.0, max(p, p_max));",
        "}",

        "float CalcShadowAddition( vec4 lightPos, Light light, sampler2D sampler ){",
            "vec3 nLightPos = (lightPos.xyz / lightPos.w)* 0.5 + 0.5;",
            "vec2 moments =  texture2D( sampler, vec2(nLightPos.xy)).rg;",
            //"vec4 texel = texture2D( uSampler_1, lightTexCoord);",
            //"vec2 moments = vec2(UnpackHalf(texel.rg), unpackHalf(texel.ba));",
            "return ChebyshevUpperBound( moments, nLightPos.z);",
        "}",
    ].join("\n");
    return str;
};




/**
 *
 * CONVERSIONS.
 *
 */
OMEGA.Omega3D.Shaders.Components.Vertex_World_Conversion_V3 = function( target, subject ){
    return target + " = uProjectionMatrix * uViewMatrix * uModelMatrix * vec4("+subject+", 1.0);";
};


/**
 *
 * PIXEL OPERATIONS.
 *
 */
OMEGA.Omega3D.Shaders.Components.PackHalf = function(){
    var str = [
        "vec2 packHalf( float depth ){",
        "const vec2 bias = vec2(1.0 / 255.0, 0.0);",
        "vec2 color = vec2(depth, fract(depth * 255.0));",
        "return color - (color.yy * bias);",
        "}"
    ].join("\n");
    return str;
};
OMEGA.Omega3D.Shaders.Components.UnpackHalf = function(){
    var str = [
        "float unpackHalf (vec2 color){",
        "return color.x + (color.y / 255.0);",
        "}"
    ].join("\n");
    return str;
};
OMEGA.Omega3D.Shaders.Components.LineStep = function(){
    var str = [
        "float linestep(float low,  float v){",
        "return clamp((v-low)/(1.0-low), 0.0, 1.0);",
        "}"
    ].join("\n");
    return str;
};

OMEGA.Omega3D.Shaders.Components.DistanceField = {};
OMEGA.Omega3D.Shaders.Components.DistanceField.Primitives = function(){
    var str = [
        "vec3 fSphere( vec3 p, float radius ){",
            "return length( p ) - radius;",
        "}",
        
        "vec3 fCube( vec3 p, float size){",
            "return length(max(abs(p)-b,0.0));",
        "}"
    ].join("\n");
    return str;
}



/**
 *
 * UTILS.
 *
 */
OMEGA.Omega3D.Shaders.Components.Noise3D = function(){
    var script =
        "vec3 mod289(vec3 x) {"+
        "return x - floor(x * (1.0 / 289.0)) * 289.0;"+
        "}"+

        "vec4 mod289(vec4 x) {"+
        "return x - floor(x * (1.0 / 289.0)) * 289.0;"+
        "}"+

        "vec4 permute(vec4 x) {"+
        "return mod289(((x*34.0)+1.0)*x);"+
        "}"+

        "vec4 taylorInvSqrt(vec4 r){"+
        "return 1.79284291400159 - 0.85373472095314 * r;"+
        "}"+

        "vec3 fade(vec3 t) {"+
        "return t*t*t*(t*(t*6.0-15.0)+10.0);"+
        "}"+

        "float pnoise(vec3 P, vec3 rep){"+
        "    vec3 Pi0 = mod(floor(P), rep);"+ // Integer part, modulo period
        "    vec3 Pi1 = mod(Pi0 + vec3(1.0), rep);"+ // Integer part + 1, mod period
        "    Pi0 = mod289(Pi0);"+
        "    Pi1 = mod289(Pi1);"+
        "    vec3 Pf0 = fract(P);"+ // Fractional part for interpolation
        "    vec3 Pf1 = Pf0 - vec3(1.0);"+ // Fractional part - 1.0
        "    vec4 ix = vec4(Pi0.x, Pi1.x, Pi0.x, Pi1.x);"+
        "    vec4 iy = vec4(Pi0.yy, Pi1.yy);"+
        "   vec4 iz0 = Pi0.zzzz;"+
        "   vec4 iz1 = Pi1.zzzz;"+

        "    vec4 ixy = permute(permute(ix) + iy);"+
        "    vec4 ixy0 = permute(ixy + iz0);"+
        "    vec4 ixy1 = permute(ixy + iz1);"+

        "    vec4 gx0 = ixy0 * (1.0 / 7.0);"+
        "    vec4 gy0 = fract(floor(gx0) * (1.0 / 7.0)) - 0.5;"+
        "    gx0 = fract(gx0);"+
        "    vec4 gz0 = vec4(0.5) - abs(gx0) - abs(gy0);"+
        "     vec4 sz0 = step(gz0, vec4(0.0));"+
        "    gx0 -= sz0 * (step(0.0, gx0) - 0.5);"+
        "    gy0 -= sz0 * (step(0.0, gy0) - 0.5);"+

        "    vec4 gx1 = ixy1 * (1.0 / 7.0);"+
        "    vec4 gy1 = fract(floor(gx1) * (1.0 / 7.0)) - 0.5;"+
        "    gx1 = fract(gx1);"+
        "    vec4 gz1 = vec4(0.5) - abs(gx1) - abs(gy1);"+
        "    vec4 sz1 = step(gz1, vec4(0.0));"+
        "    gx1 -= sz1 * (step(0.0, gx1) - 0.5);"+
        "    gy1 -= sz1 * (step(0.0, gy1) - 0.5);"+

        "    vec3 g000 = vec3(gx0.x,gy0.x,gz0.x);"+
        "    vec3 g100 = vec3(gx0.y,gy0.y,gz0.y);"+
        "    vec3 g010 = vec3(gx0.z,gy0.z,gz0.z);"+
        "    vec3 g110 = vec3(gx0.w,gy0.w,gz0.w);"+
        "   vec3 g001 = vec3(gx1.x,gy1.x,gz1.x);"+
        "    vec3 g101 = vec3(gx1.y,gy1.y,gz1.y);"+
        "    vec3 g011 = vec3(gx1.z,gy1.z,gz1.z);"+
        "   vec3 g111 = vec3(gx1.w,gy1.w,gz1.w);"+

        "    vec4 norm0 = taylorInvSqrt(vec4(dot(g000, g000), dot(g010, g010), dot(g100, g100), dot(g110, g110)));"+
        "    g000 *= norm0.x;"+
        "   g010 *= norm0.y;"+
        "    g100 *= norm0.z;"+
        "   g110 *= norm0.w;"+
        "    vec4 norm1 = taylorInvSqrt(vec4(dot(g001, g001), dot(g011, g011), dot(g101, g101), dot(g111, g111)));"+
        "    g001 *= norm1.x;"+
        "    g011 *= norm1.y;"+
        "   g101 *= norm1.z;"+
        "   g111 *= norm1.w;"+

        "    float n000 = dot(g000, Pf0);"+
        "    float n100 = dot(g100, vec3(Pf1.x, Pf0.yz));"+
        "    float n010 = dot(g010, vec3(Pf0.x, Pf1.y, Pf0.z));"+
        "   float n110 = dot(g110, vec3(Pf1.xy, Pf0.z));"+
        "    float n001 = dot(g001, vec3(Pf0.xy, Pf1.z));"+
        "    float n101 = dot(g101, vec3(Pf1.x, Pf0.y, Pf1.z));"+
        "    float n011 = dot(g011, vec3(Pf0.x, Pf1.yz));"+
        "    float n111 = dot(g111, Pf1);"+

        "    vec3 fade_xyz = fade(Pf0);"+
        "    vec4 n_z = mix(vec4(n000, n100, n010, n110), vec4(n001, n101, n011, n111), fade_xyz.z);"+
        "    vec2 n_yz = mix(n_z.xy, n_z.zw, fade_xyz.y);"+
        "    float n_xyz = mix(n_yz.x, n_yz.y, fade_xyz.x);"+
        "    return 2.2 * n_xyz;"+
        "}";
    return script;
};
OMEGA.Omega3D.Shaders.Components.Common_Math = function(){
    var str = [
        "vec3 Calculate_ProjectedCameraSspacePosition( vec3 vec_in, mat4 viewProjMatrix ){",
        "float temp_x = vec_in.x;",
        "float temp_y = vec_in.y;",
        "float temp_z = vec_in.z;",

        "float temp_x_2 = viewProjMatrix[0][0] * temp_x + viewProjMatrix[1][0] * temp_y + viewProjMatrix[2][0] * temp_z + viewProjMatrix[3][0];",
        "float temp_y_2 = viewProjMatrix[0][1] * temp_x + viewProjMatrix[1][1] * temp_y + viewProjMatrix[2][1] * temp_z + viewProjMatrix[3][1];",
        "float temp_z_2 = viewProjMatrix[0][2] * temp_x + viewProjMatrix[1][2] * temp_y + viewProjMatrix[2][2] * temp_z + viewProjMatrix[3][2];",
        "float tempFloat = 1.0/temp_z_2;",

        "return vec3( 0.5*temp_x_2*tempFloat + 0.5, 0.5 * temp_y_2 * tempFloat + 0.5, temp_z_2);",
        "}",

        "vec2 Calculate_ScreenPosition( vec3 vec_in, mat4 proj_mat, mat4 view_mat, float screenW, float screenH ){",
        "vec4 vec = vec4(vec_in, 1.0) * view_mat;",
        "vec = vec * proj_mat;",

        "vec.x /= vec.z;",
        "vec.y /= vec.z;",
        "vec.x = (vec.x + 1.0) * screenW / 2.0;",
        "vec.y = (vec.y + 1.0) * screenH / 2.0;",


        "return vec2( vec.x, vec.y);",
        "}"
    ].join("\n");
    return str;
};




//TODO: check if this is obsolete!
OMEGA.Omega3D.Shaders.Components.Vertex_View_Conversion_V3 = function( target, subject ){
    return target + " = uProjectionMatrix * uViewMatrix * uModelMatrix *    vec4("+subject+", 1.0);";
};

OMEGA.Omega3D.Shaders.Components.Vertex_ScreenSpace_Conversion_V3 = function( target, subject ){
    return target + " = vec4("+subject+", 1.0);"
}












/**
 *
 *  DIFFUSE SHADING.
 *
 */
OMEGA.Omega3D.Shaders.Components.Basic_Diffuse_Vertex_Logic = function(){
    var script =
        "vPositionEye4 = gl_Position;" +
        //"vec4 temp = uProjectionMatrix * uViewMatrix * uModelMatrix * vec4(uLightPosition, 1.0);" +
        "vec3 temp = uNormalMatrix *aVertexNormal;" +
        "vNormalEye = normalize(temp);";
    return script;
};
OMEGA.Omega3D.Shaders.Components.Basic_Diffuse_Fragment_Logic = function( color ){
    var script =
        "vec3 vectorToLightSource    = normalize(uLightPosition.xyz-vPositionEye4.xyz );" +
        //"vec3 vectorToLightSource    = normalize(uLightPosition.xyz );" +
        "float diffuseLightWeighting = max(dot(vNormalEye, vectorToLightSource), 0.0);" +
        "vec3 lightWeighting = uAmbientColor + " +
        "uDiffuseColor * diffuseLightWeighting;" +
        "vec4 texelColor = " + color +
        "gl_FragColor = vec4( lightWeighting.rgb * texelColor.rgb, texelColor.a);";
    return script;
};

/**
 *
 *  PHONG SHADING.
 *
 */

OMEGA.Omega3D.Shaders.Components.Basic_Phong_Fragment_Logic = function( color ){
    var script =
        "vec3 vectorToLightSource    = normalize(uLightPosition.xyz-vPositionEye4.xyz );" +
        //"vec3 vectorToLightSource    = normalize(uLightPosition.xyz );" +
        "float diffuseLightWeighting = max(dot(vNormalEye, vectorToLightSource), 0.0);" +
        "vec3 reflectionVector       = normalize(reflect(-vectorToLightSource, vNormalEye));" +
        "vec3 viewVectorEye  = normalize(vPositionEye4.xyz);" +
        "float rdotv  = max(dot(reflectionVector, viewVectorEye), 0.0);" +
        "float specularLightWeighting = pow(rdotv, 8.0);" +
        "vec3 lightWeighting = uAmbientColor + " +
        "uDiffuseColor * diffuseLightWeighting + " +
        "uSpecularColor * specularLightWeighting;" +
        "vec4 texelColor = " + color +
        "gl_FragColor = vec4( lightWeighting.rgb * texelColor.rgb, texelColor.a);";
    return script;
};



OMEGA.Omega3D.Shaders.Components.StandardAttributes = function(){
    var attribs = {};
    attribs.aTextureCoord = { type: "vec2",glsl: "attribute vec2 aTextureCoord;", value:null};
    attribs.aVertexPos    = { type: "vec3",glsl: "attribute vec3 aVertexPos;"   , value:null};
    attribs.aVertexNormal = { type: "vec3",glsl: "attribute vec3 aVertexNormal;", value:null};
    attribs.aVertexTangent      = { type: "vec3",glsl: "attribute vec3 aVertexTangent;"     , value:null};
    attribs.aVertexBitangent    = { type: "vec3",glsl: "attribute vec3 aVertexBitangent;"   , value:null};
    attribs.aVertexColor        = { type: "vec3",glsl: "attribute vec3 aVertexColor;"       , value:null};
    attribs.aPickingColor       = { type: "vec3",glsl: "attribute vec3 aPickingColor;"      , value:null};
    return attribs;
};
OMEGA.Omega3D.Shaders.Components.StandardUniforms = function(){
    var uniforms = {};
    uniforms.uDiffuse      = { type: "vec3", glsl: "uniform mat4 uDiffuse;"     , value: null };
    uniforms.uAmbient      = { type: "vec3", glsl: "uniform mat4 uAmbient;"     , value: null };
    uniforms.uSpecular      = { type: "vec3", glsl: "uniform mat4 uSpecular;"     , value: null };

    uniforms.uModelMatrix      = { type: "mat4", glsl: "uniform mat4 uModelMatrix;"     , value: null };
    uniforms.uProjectionMatrix = { type: "mat4", glsl: "uniform mat4 uProjectionMatrix;", value: null };
    uniforms.uViewMatrix       = { type: "mat4", glsl: "uniform mat4 uViewMatrix;"      , value: null };
    uniforms.uInvViewMatrix    = { type: "mat4", glsl: "uniform mat4 uInvViewMatrix;"   , value: null };
    uniforms.uNormalMatrix     = { type: "mat3", glsl: "uniform mat3 uNormalMatrix;"    , value: null };
    uniforms.uCamPos            = { type: "vec3", glsl: "uniform vec3 uCamPos;"          , value: null };
    //uniforms.uPointSize        = { type: "float", glsl: "uniform float uPointSize;"    , value: null };
    return uniforms;
};
OMEGA.Omega3D.Shaders.Components.StandardLightUniforms = function(){
    var uniforms = {};
    uniforms.uLightPosition  = { type: "vec3", glsl: "uniform vec3 uLightPosition;" , value: null };
    uniforms.uLightDirection = { type: "vec3", glsl: "uniform vec3 uLightDirection;", value: null };
//    uniforms.uAmbientColor   = { type: "vec3", glsl: "uniform vec3 uAmbientColor;"  , value: null };
   // uniforms.uDiffuseColor   = { type: "vec3", glsl: "uniform vec3 uDiffuseColor;"  , value: null };
   // uniforms.uSpecularColor  = { type: "vec3", glsl: "uniform vec3 uSpecularColor;" , value: null };
    return uniforms;
};



