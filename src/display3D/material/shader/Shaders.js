OMEGA.Omega3D.Shaders = OMEGA.Omega3D.Shaders || {};



/**
 *  Basic - Default shader
 *
 * @param isTextured
 * @returns {OMEGA.Omega3D.Shader}
 */
 OMEGA.Omega3D.Shaders.Basic = function( isTextured, scene, color ){
    //OMEGA.Omega3D.Log(" >> ShaderBuilder: Creating 'Basic' shader");
    //OMEGA.Omega3D.Log("   -- Textured: " + isTextured);
    //OMEGA.Omega3D.Log("   -- Color: " + color);
    var texture, fragmentColor;
    isTextured = isTextured || false;
    if(isTextured) texture = "varying vec2 vTexCoord;";
    else texture = "varying vec3 vVertexColor;";
    var vertex_source = OMEGA.Omega3D.Shaders.Components.Basic_Includes();
        vertex_source = vertex_source + texture;
        vertex_source = vertex_source + "void main(void){ gl_PointSize = 10.0;";
        vertex_source = vertex_source + OMEGA.Omega3D.Shaders.Components.Vertex_World_Conversion_V3("gl_Position", "aVertexPos");
        if(isTextured                      ) vertex_source = vertex_source + OMEGA.Omega3D.Shaders.Components.Basic_TextureCoords("vTexCoord");
        else if( color ) vertex_source = vertex_source + "vVertexColor = vec3("+color[0]+","+ +color[1]+"," +  +color[2]+");";
        else  vertex_source = vertex_source + "vVertexColor = aVertexColor;";
        vertex_source = vertex_source + "}";


    if(isTextured){
        texture = "uniform sampler2D uSampler;"+
                  "varying vec2 vTexCoord;";
        fragmentColor = "texture2D(uSampler, vec2(vTexCoord.s, vTexCoord.t));";
    }
    else{
        texture = "varying vec3 vVertexColor;";
       // if(color) fragmentColor = "vec4("+color[0]+","+ +color[1]+"," +  +color[2]+", 1.0);";
       // else  fragmentColor = "vec4(0.8,0.4,0.0, 1.0);";
       fragmentColor = "vec4(vVertexColor, 1.0);";
    }
    var fragment_source = OMEGA.Omega3D.Shaders.Components.Fragment_Precision_Mediump_Float();
    fragment_source = fragment_source + OMEGA.Omega3D.Shaders.Components.Basic_Fragment_Includes();
       // if( isTextured                     ){
            fragment_source = fragment_source + texture;
      //  }
        fragment_source = fragment_source + "void main(void){";
        fragment_source = fragment_source + "vec4 color = " + fragmentColor;
        fragment_source = fragment_source + OMEGA.Omega3D.Shaders.Components.Basic_Fragment_Logic("vec4(color.rgb, color.a)");
        fragment_source = fragment_source + "}";



    //lights
    var uniforms = OMEGA.Omega3D.Shaders.Components.StandardUniforms();
    if(scene != null){
        var injected = OMEGA.Omega3D.ShaderUtil.InjectGLSLForLights(vertex_source, fragment_source, uniforms, scene);
        uniforms = injected.uniforms;
        vertex_source =injected.vs;
        fragment_source =injected.fs;

        //fog
        if(scene.hasFog){
            injected = OMEGA.Omega3D.ShaderUtil.InjectGLSLForFog(vertex_source, fragment_source, uniforms,scene);
            uniforms = injected.uniforms;
            vertex_source =injected.vs;
            fragment_source =injected.fs;
        }
    }



    var fragmentOnlyUniforms = {};
    if(isTextured) fragmentOnlyUniforms["uSampler"] = { id:"uSampler", type: "sampler2D", glsl: "", value: null };
    return new OMEGA.Omega3D.Shader(vertex_source,fragment_source,  [OMEGA.Omega3D.Shaders.Components.StandardAttributes(),
                                                                     uniforms,
                                                                     {},
                                                                     fragmentOnlyUniforms]);
};
 OMEGA.Omega3D.Shaders.BasicParticle = function() {
     var vs = [
         OMEGA.Omega3D.Shaders.Components.Basic_Includes(),
         "void main(void){",
             "gl_PointSize  = 20.0;",
             "gl_Position    = uProjectionMatrix * uViewMatrix * uModelMatrix * vec4(aVertexPos.xyz, 1.0);",
         "}"
     ].join("\n");

     var fs = [
         "#extension GL_OES_standard_derivatives : enable",  //optional. IF Enable WebGL Draft Extensions has been enabled ( Chrome ).
         OMEGA.Omega3D.Shaders.Components.Fragment_Precision_Mediump_Float(),
         OMEGA.Omega3D.Shaders.Components.Basic_Fragment_Includes(),
         "uniform sampler2D uSampler;",
         "void main(void){",


            "vec4 color = texture2D(uSampler, gl_PointCoord);",
            "gl_FragColor = color;",
         "}"
     ].join("\n");


     var uniforms = OMEGA.Omega3D.Shaders.Components.StandardUniforms();
     return new OMEGA.Omega3D.Shader(vs,fs,  [OMEGA.Omega3D.Shaders.Components.StandardAttributes(),
         uniforms,
         {},
         {}]);
 };

/**
 *  Cubemap Shader
 *
 * @returns {{vs_src: string, fs_src: string}}
 */
OMEGA.Omega3D.Shaders.CubeMap = function(scene){
    var vertex_source =
        OMEGA.Omega3D.Shaders.Components.Basic_Includes() +
        "varying vec3 vTexCoord;"+
        "void main(void){"+
        OMEGA.Omega3D.Shaders.Components.Vertex_View_Conversion_V3("gl_Position", "aVertexPos") +
        "vTexCoord = (vec4(aVertexPos, 0.0)).xyz;"+
        "}";


    var fragment_source =
        OMEGA.Omega3D.Shaders.Components.Fragment_Precision_Mediump_Float()+
        OMEGA.Omega3D.Shaders.Components.Basic_Fragment_Includes() +
        "uniform samplerCube uSampler;"+
        "varying vec3 vTexCoord;"+
        "void main(void){"+
        "gl_FragColor = textureCube(uSampler, vTexCoord);"+
        "}";


    var uniforms = OMEGA.Omega3D.Shaders.Components.StandardUniforms();
    //var injected = OMEGA.Omega3D.ShaderUtil.InjectGLSLForLights(vertex_source, fragment_source, uniforms, scene);
    //uniforms = injected.uniforms;
    //vertex_source =injected.vs;
    //fragment_source =injected.fs;


    var fragmentOnlyUniforms = {};
    fragmentOnlyUniforms["uSampler"] = { id:"uSampler", type: "sampler2D", glsl: "", value: null };
    return new OMEGA.Omega3D.Shader(vertex_source,fragment_source,  [OMEGA.Omega3D.Shaders.Components.StandardAttributes(),
        uniforms,
        {},
        fragmentOnlyUniforms]);
};


/**
 * SkyBox - Cubemapped Environment
 *
 * @param scene
 * @returns {OMEGA.Omega3D.Shader}
 * @constructor
 */
OMEGA.Omega3D.Shaders.SkyBox = function(scene){

    var vs = [
        OMEGA.Omega3D.Shaders.Components.Basic_Includes(),
        "varying vec3 vTexCoord;",
        "void main(void){",

        "mat4 viewMatrix = mat4(mat3(uViewMatrix));",
        "gl_Position = uProjectionMatrix * viewMatrix * uModelMatrix * vec4(aVertexPos, 1.0);",
        "vTexCoord   = (vec4(aVertexPos, 0.0)).xyz;",
        "}",
    ].join("\n");

    var fs = [
        OMEGA.Omega3D.Shaders.Components.Fragment_Precision_Mediump_Float(),
        OMEGA.Omega3D.Shaders.Components.Basic_Fragment_Includes(),
        "uniform samplerCube uSampler;",
        "varying vec3 vTexCoord;",
        "void main(void){",
        "gl_FragColor = textureCube(uSampler, vTexCoord);",
        "}",
    ].join("\n");



    var uniforms = OMEGA.Omega3D.Shaders.Components.StandardUniforms();
    var fragmentOnlyUniforms = {};
    fragmentOnlyUniforms["uSampler"] = { id:"uSampler", type: "sampler2D", glsl: "", value: null };
    return new OMEGA.Omega3D.Shader(vs,fs,  [OMEGA.Omega3D.Shaders.Components.StandardAttributes(),
        uniforms,
        {},
        fragmentOnlyUniforms]);
};

/**
 * CubemapReflection
 *
 * @param scene
 * @returns {OMEGA.Omega3D.Shader}
 * @constructor
 */
OMEGA.Omega3D.Shaders.CubemapReflection = function(scene ) {
    var vertex_source =
        "attribute vec2 aTextureCoord;"+
        "attribute vec3 aVertexPos;"+
        "attribute vec3 aVertexNormal;"+

        "uniform mat4 uModelMatrix;"+
        "uniform mat4 uProjectionMatrix;"+
        "uniform mat4 uViewMatrix;"+
        "uniform mat4 uInvViewMatrix;"+
        "uniform mat3 uNormalMatrix;"+

        "varying vec3 vTexCoord;" +
        "varying vec3 vColor;" +
        "void main(void){"+
        "   vec3 vertexWorld =  (uModelMatrix * vec4(aVertexPos, 1.0)).xyz;"+
        "   vec3 normalWorld =  (uModelMatrix * vec4(aVertexNormal, 0.0)).xyz;"+
        "   vec3 eyeWorld       = vec3(uInvViewMatrix[3].xyz);" +
        "   vec3 vertexToEyeWorld =  normalize( vertexWorld - eyeWorld );"+
        "   vTexCoord             =  normalize( reflect( vertexToEyeWorld, normalWorld ) );"+
        "   vTexCoord.y           =  vTexCoord.y;"+


        "   gl_Position = uProjectionMatrix * uViewMatrix * uModelMatrix *vec4(aVertexPos, 1.0);" +
        "}";
    var fragment_source =
        OMEGA.Omega3D.Shaders.Components.Fragment_Precision_Mediump_Float() +
        OMEGA.Omega3D.Shaders.Components.Basic_Fragment_Includes() +
        "varying vec3 vTexCoord;" +
        "uniform samplerCube uSampler;"+

        "void main(void){"+
        "vec4 color = textureCube(uSampler, vTexCoord);"+
        OMEGA.Omega3D.Shaders.Components.Basic_Fragment_Logic("vec4(color.rgb, color.a)") +
        "}";

    var injected;
    var uniforms = OMEGA.Omega3D.Shaders.Components.StandardUniforms();
    if(scene != null){
        injected = OMEGA.Omega3D.ShaderUtil.InjectGLSLForLights(vertex_source, fragment_source, uniforms, scene);
        uniforms = injected.uniforms;
        vertex_source =injected.vs;
        fragment_source =injected.fs;

        //fog
        if(scene.hasFog){
            injected = OMEGA.Omega3D.ShaderUtil.InjectGLSLForFog(vertex_source, fragment_source, uniforms,scene);
            uniforms = injected.uniforms;
            vertex_source =injected.vs;
            fragment_source =injected.fs;
        }
    }

    var fragmentOnlyUniforms = {};
    fragmentOnlyUniforms["uSampler"] = { id:"uSampler", type: "sampler2D", glsl: "", value: null };
    return new OMEGA.Omega3D.Shader(vertex_source,fragment_source,  [OMEGA.Omega3D.Shaders.Components.StandardAttributes(),
        uniforms,
        {},
        fragmentOnlyUniforms]);
}



/**
 * SEM - Spherical Environment Mapping
 *
 * @param scene
 * @returns {OMEGA.Omega3D.Shader}
 * @constructor
 */
OMEGA.Omega3D.Shaders.SEM = function( scene ){
   // OMEGA.Omega3D.Log(" >> ShaderBuilder: Creating 'Spherical Environment Mapping' shader");

    var vertex_source = OMEGA.Omega3D.Shaders.Components.Basic_Includes();
    vertex_source += "varying vec3 e;";
    vertex_source += "varying vec3 n;";
    vertex_source += "void main(void){";
    vertex_source += "e = normalize( vec3( uViewMatrix * uModelMatrix * vec4(aVertexPos.xyz, 1.0) ) );";
    vertex_source += "n = normalize( uNormalMatrix * aVertexNormal );";
    vertex_source += OMEGA.Omega3D.Shaders.Components.Vertex_World_Conversion_V3("gl_Position", "aVertexPos");
    vertex_source += "}";





    var fragment_source = OMEGA.Omega3D.Shaders.Components.Fragment_Precision_Mediump_Float();
    fragment_source = fragment_source + OMEGA.Omega3D.Shaders.Components.Basic_Fragment_Includes();
    fragment_source += "uniform sampler2D uSampler;";
    fragment_source += "varying vec3 e;";
    fragment_source += "varying vec3 n;";
    fragment_source += "void main(void){";
    fragment_source += "vec3 r = reflect( e, n );";
    fragment_source += "float m = 2.0 * sqrt( pow( r.x, 2.0) + pow( r.y, 2.0) + pow( r.z + 1.0, 2.0) );";
    fragment_source += "vec2 vN = r.xy / m + 0.5;";
    fragment_source += "vec4 color = texture2D(uSampler, vN);";
    fragment_source += OMEGA.Omega3D.Shaders.Components.Basic_Fragment_Logic("vec4(color.rgb, color.a)");
    fragment_source += "}";


    var uniforms = OMEGA.Omega3D.Shaders.Components.StandardUniforms();
    if(scene != null){
        var injected = OMEGA.Omega3D.ShaderUtil.InjectGLSLForLights(vertex_source, fragment_source, uniforms, scene);
        uniforms = injected.uniforms;
        vertex_source =injected.vs;
        fragment_source =injected.fs;

        //fog
        if(scene.hasFog){
            injected = OMEGA.Omega3D.ShaderUtil.InjectGLSLForFog(vertex_source, fragment_source, uniforms,scene);
            uniforms = injected.uniforms;
            vertex_source =injected.vs;
            fragment_source =injected.fs;
        }
    }

    var fragmentOnlyUniforms = {};
    fragmentOnlyUniforms["uSampler"] = { id:"uSampler", type: "sampler2D", glsl: "", value: null };
    return new OMEGA.Omega3D.Shader(vertex_source,fragment_source,  [OMEGA.Omega3D.Shaders.Components.StandardAttributes(),
        uniforms,
        {},
        fragmentOnlyUniforms]);
};



/**
 * SM - Shadow Mapping
 *
 * @param scene
 * @returns {OMEGA.Omega3D.Shader}
 * @constructor
 */
OMEGA.Omega3D.Shaders.SM = function( scene  ){
   // OMEGA.Omega3D.Log(" >> ShaderBuilder: Creating 'Shadow Mapping' shader");

    var vertex_source = OMEGA.Omega3D.Shaders.Components.Basic_Includes();
    vertex_source += "uniform mat4 uLightM;";
    vertex_source += "varying vec2 vTexCoord;";
    vertex_source += "varying vec4 vertexPosLight;";

    vertex_source += "void main(void){";
    vertex_source += "mat4 mvp       = uProjectionMatrix * uViewMatrix * uModelMatrix;";
    vertex_source += "mat4 mvp_light = uProjectionMatrix * uLightM * uModelMatrix;";
    vertex_source += "vec4 pos       = mvp * vec4(aVertexPos.xyz, 1.0);";
    vertex_source += "vertexPosLight = mvp_light * vec4(aVertexPos.xyz, 1.0);";
    vertex_source += "gl_Position    = pos;";
    vertex_source += "vTexCoord      = aTextureCoord;";
    vertex_source += "}";


    var fragment_source = OMEGA.Omega3D.Shaders.Components.Fragment_Precision_Mediump_Float();
    fragment_source = fragment_source + OMEGA.Omega3D.Shaders.Components.Basic_Fragment_Includes();
    fragment_source += "uniform sampler2D uSampler;";
    fragment_source += "uniform sampler2D uSampler_1;";
    fragment_source += "varying vec2 vTexCoord;";
    fragment_source += "varying vec4 vertexPosLight;";

    fragment_source += "void main(void){";
    fragment_source += "vec3 nLightPos   = (vertexPosLight.xyz / vertexPosLight.w) / 2.0 + 0.5;";
    fragment_source += "vec4 depth       = texture2D(uSampler_1, vec2(nLightPos.xy));";
    fragment_source += "vec4 texture     = texture2D(uSampler, vec2(vTexCoord.st));";
    fragment_source += "float bias       = 0.0000000001;";
    fragment_source += "float visibility = 1.0;";//(nLightPos.z  > (depth.r  + bias)) ? 0.5 : 1.0;";
    fragment_source += "float notInSight = 0.0;";//(nLightPos.z  > (depth.r  + bias)) ? 0.5 : 1.0;";
    fragment_source += "float divider = 500.0*depth.r;";//(nLightPos.z  > (depth.r  + bias)) ? 0.5 : 1.0;";

    fragment_source += "if ( texture2D( uSampler_1, nLightPos.xy/divider ).r == notInSight  ){}";
    fragment_source += "else{";
    fragment_source += "if ( texture2D( uSampler_1, nLightPos.xy ).r   + bias <  nLightPos.z  ){ visibility-=0.05; }";
    fragment_source += "if ( texture2D( uSampler_1, nLightPos.xy+ vec2( -0.94201624 , -0.39906216 )/divider ).r   + bias<  nLightPos.z  ){visibility-=0.1; }";
    fragment_source += "if ( texture2D( uSampler_1, nLightPos.xy+ vec2(  0.94558609 , -0.76890725 )/divider ).r   + bias<  nLightPos.z  ){ visibility-=0.1; }";
    fragment_source += "if ( texture2D( uSampler_1, nLightPos.xy+ vec2( -0.094184101, -0.92938870 )/divider ).r   + bias<  nLightPos.z  ){ visibility-=0.1; }";
    fragment_source += "if ( texture2D( uSampler_1, nLightPos.xy+ vec2(  0.34495938 ,  0.29387760 )/divider ).r   + bias<  nLightPos.z  ){ visibility-=0.1; }}";
    fragment_source += "vec4 color       = vec4( texture.rgb*visibility, 1.0);";////vec4( texture.rgb*visibility, 1.0);
    fragment_source += OMEGA.Omega3D.Shaders.Components.Basic_Fragment_Logic("vec4(color.rgb, color.a)");
    fragment_source += "}";


    var uniforms = OMEGA.Omega3D.Shaders.Components.StandardUniforms();
    if(scene != null){
        var injected = OMEGA.Omega3D.ShaderUtil.InjectGLSLForLights(vertex_source, fragment_source, uniforms, scene);
        uniforms = injected.uniforms;
        vertex_source =injected.vs;
        fragment_source =injected.fs;

        //fog
        if(scene.hasFog){
            injected = OMEGA.Omega3D.ShaderUtil.InjectGLSLForFog(vertex_source, fragment_source, uniforms,scene);
            uniforms = injected.uniforms;
            vertex_source =injected.vs;
            fragment_source =injected.fs;
        }
    }


    var fragmentOnlyUniforms = {};
    fragmentOnlyUniforms["uSampler"]   = { id:"uSampler"  , type: "sampler2D", glsl: "", value: null };
    fragmentOnlyUniforms["uSampler_1"] = { id:"uSampler_1", type: "sampler2D", glsl: "", value: null };

    var custom_uniforms = {};
    custom_uniforms["uLightM"] = { type:"mat4", glsl: "", value: "" };
    return new OMEGA.Omega3D.Shader(vertex_source,fragment_source,  [OMEGA.Omega3D.Shaders.Components.StandardAttributes(),
                                                                    uniforms,
                                                                    custom_uniforms,
                                                                    fragmentOnlyUniforms]);
};
OMEGA.Omega3D.Shaders.SM02 = function( scene  ){
   // OMEGA.Omega3D.Log(" >> ShaderBuilder: Creating 'Shadow Mapping' shader");

    var lights = scene.getLights();
    var vertex_source = OMEGA.Omega3D.Shaders.Components.Basic_Includes();
    for( var i = 0; i < lights.length; i++){
        vertex_source += "uniform mat4 uLightM_"+i.toString()+";";
        vertex_source += "varying vec4 vertexPosLight_"+i.toString()+";";
    }
    
    vertex_source += "varying vec2 vTexCoord;";
   

    vertex_source += "void main(void){";
        vertex_source += "mat4 biasMatrix = mat4( -0.5, 0.0, 0.0, 0.0, ";
        vertex_source += "                        0.0, -0.5, 0.0, 0.0, ";
        vertex_source += "                        0.0, 0.0, -0.5, 0.0, ";
        vertex_source += "                        0.0, 0.0, 0.0, -1.0 );";

        vertex_source += "mat4 mvp = uProjectionMatrix * uViewMatrix * uModelMatrix;";

        for( var i = 0; i < lights.length; i++){
            vertex_source += "vertexPosLight_"+i.toString()+" = uProjectionMatrix * uLightM_"+i.toString()+" * uModelMatrix * vec4(aVertexPos.xyz, 1.0);";
        }

        vertex_source += "vec4 pos       = mvp * vec4(aVertexPos.xyz, 1.0);";
        vertex_source += "gl_Position    = pos;";
        vertex_source += "vTexCoord      = aTextureCoord;";
    vertex_source += "}";


    var fragment_source = "#extension GL_OES_standard_derivatives : enable\n";
    fragment_source += OMEGA.Omega3D.Shaders.Components.Fragment_Precision_Mediump_Float();
   // fragment_source += "#extension GL_OES_standard_derivatives : enable";
    fragment_source += OMEGA.Omega3D.Shaders.Components.Basic_Fragment_Includes();
    fragment_source += "uniform sampler2D uSampler;";
    fragment_source += "varying vec2 vTexCoord;";

    //fragment_source += "float random( vec3 seed4, int i ){";
    //fragment_source += "float dot_product = dot(seed4, vec4(12.9898,78.233,45.164,94.673));";
    //fragment_source += "return fract(sin(dot_product) * 43758.5453);}";
    
    for( var i = 0; i < lights.length; i++){
        fragment_source += "uniform sampler2D uSampler_"+(i+1).toString()+";";
        fragment_source += "varying vec4 vertexPosLight_"+i.toString()+";";
    }

    // shadow method.
    fragment_source += OMEGA.Omega3D.Shaders.Components.CalcShadowAdditionPCF();

    //main.
    fragment_source += "void main(void){";
        fragment_source += "vec4 texture     = texture2D(uSampler, vec2(vTexCoord.st));";


         fragment_source += "float totalVisibility = 1.0;";
        for( var i = 0; i < lights.length; i++){
            fragment_source += "totalVisibility = totalVisibility - ( 1.0 - CalcShadowAddition ( vertexPosLight_"+i.toString() + ", uSampler_"+(i+1).toString() + "));";
        }
        fragment_source += "vec4 color       = vec4( texture.rgb*totalVisibility, 1.0);";


        fragment_source += OMEGA.Omega3D.Shaders.Components.Basic_Fragment_Logic("vec4(color.rgb, color.a)");
    fragment_source += "}";


    var uniforms = OMEGA.Omega3D.Shaders.Components.StandardUniforms();
    if(scene != null){
        var injected = OMEGA.Omega3D.ShaderUtil.InjectGLSLForLights(vertex_source, fragment_source, uniforms, scene);
        uniforms = injected.uniforms;
        vertex_source =injected.vs;
        fragment_source =injected.fs;

        //fog
        if(scene.hasFog){
            injected = OMEGA.Omega3D.ShaderUtil.InjectGLSLForFog(vertex_source, fragment_source, uniforms,scene);
            uniforms = injected.uniforms;
            vertex_source =injected.vs;
            fragment_source =injected.fs;
        }
    }


    var fragmentOnlyUniforms = {};
    fragmentOnlyUniforms["uSampler"]   = { id:"uSampler"  , type: "sampler2D", glsl: "", value: null };
    for( var i = 0; i < lights.length; i++){
        fragmentOnlyUniforms["uSampler_" + (i+1).toString() ] = { id:"uSampler_" + (i+1).toString(), type: "sampler2D", glsl: "", value: null };
    }

    var custom_uniforms = {};
    for( var i = 0; i < lights.length; i++){
        custom_uniforms["uLightM_" + i.toString()] = { type:"mat4", glsl: "", value: "" };
    }
    return new OMEGA.Omega3D.Shader(vertex_source,fragment_source,  [OMEGA.Omega3D.Shaders.Components.StandardAttributes(),
                                                                    uniforms,
                                                                    custom_uniforms,
                                                                    fragmentOnlyUniforms]);
};


/**
* DepthMap - DepthMap for VSM ( Varience Shadow Mapping )
*
* @param scene
* @returns {OMEGA.Omega3D.Shader}
* @constructor
*/
OMEGA.Omega3D.Shaders.DepthMap = function(){
    var vs = [
        OMEGA.Omega3D.Shaders.Components.Basic_Includes(),
        "varying vec4 vPos;",
        "void main(void){",
        "gl_Position    = uProjectionMatrix * uViewMatrix * uModelMatrix * vec4(aVertexPos.xyz, 1.0);",
        "vPos           = gl_Position;",
        "}"
    ].join("\n");

    var fs = [
        "#extension GL_OES_standard_derivatives : enable",  //optional. IF Enable WebGL Draft Extensions has been enabled ( Chrome ).
        OMEGA.Omega3D.Shaders.Components.Fragment_Precision_Mediump_Float(),
        OMEGA.Omega3D.Shaders.Components.Basic_Fragment_Includes(),
        "varying vec4 vPos;",


        "vec2 packHalf( float depth ){",
            "const vec2 bias = vec2(1.0 / 255.0, 0.0);",
            "vec2 colour = vec2(depth, fract(depth * 255.0));",
            "return colour - (colour.yy * bias);",
        "}",

        "void main(void){",

            "float depth = vPos.z/ vPos.w;",
            "depth   = depth * 0.5 + 0.5;",
            "float moment1 = depth;",

            "float dx = dFdx(depth);",           //optional. IF Enable WebGL Draft Extensions has been enabled ( Chrome ).
            "float dy = dFdy(depth);",           //optional. IF Enable WebGL Draft Extensions has been enabled ( Chrome ).
            "float moment2 = (depth * depth) + 0.25 * (dx*dx+dy*dy);",

            "gl_FragColor = vec4( moment1, moment2, 0.0, 1.0);",
            // "gl_FragColor = vec4(packHalf(moment1), packHalf(moment2));",
        "}"
    ].join("\n");


    var uniforms = OMEGA.Omega3D.Shaders.Components.StandardUniforms();
    return new OMEGA.Omega3D.Shader(vs,fs,  [OMEGA.Omega3D.Shaders.Components.StandardAttributes(),
                                                                    uniforms,
                                                                    {},
                                                                    {}]);
};
OMEGA.Omega3D.Shaders.VSM = function( scene ){
    var vs = [
        OMEGA.Omega3D.Shaders.Components.Basic_Includes(),
        
        //for( var i = 0; i < lights.length; i++){
        //    vertex_source += "uniform mat4 uLightM_"+i.toString()+";";
        //    vertex_source += "varying vec4 vertexPosLight_"+i.toString()+";";
        //}
        
        "uniform mat4 uLightM;",
        "varying vec2 vTexCoord;",
        "varying vec4 vertexPosLight;",

        "void main(void){",
            "vec4 pos       = uProjectionMatrix * uViewMatrix * uModelMatrix * vec4(aVertexPos.xyz, 1.0);",
            "vertexPosLight = uProjectionMatrix * uLightM * uModelMatrix * vec4(aVertexPos.xyz, 1.0);",
            "gl_Position    = pos;",
            "vTexCoord      = aTextureCoord;",

        "}"
    ].join("\n");

    var fs = [
        OMEGA.Omega3D.Shaders.Components.Fragment_Precision_Mediump_Float(),
        OMEGA.Omega3D.Shaders.Components.Basic_Fragment_Includes(),
        "uniform sampler2D uSampler;",
        "uniform sampler2D uSampler_1;",
        "varying vec2 vTexCoord;",
        "varying vec4 vertexPosLight;",


        //OMEGA.Omega3D.Shaders.Components.UnpackHalf(),
        OMEGA.Omega3D.Shaders.Components.LineStep(),
        OMEGA.Omega3D.Shaders.Components.CalcShadowAdditionVSM(),


        "void main(void){",

            "float shadow = CalcShadowAddition( vertexPosLight,lights[0], uSampler_1 );",
            "vec4 texture = texture2D(uSampler, vec2(vTexCoord.st));",

            "vec4 color = vec4(shadow, shadow, shadow, 1.0) *texture;",
            OMEGA.Omega3D.Shaders.Components.Basic_Fragment_Logic("vec4(color.rgb, color.a)"),
        "}"
    ].join("\n");

    var uniforms = OMEGA.Omega3D.Shaders.Components.StandardUniforms();
    if(scene != null){
        var injected = OMEGA.Omega3D.ShaderUtil.InjectGLSLForLights(vs, fs, uniforms, scene);
        uniforms = injected.uniforms;
        vs =injected.vs;
        fs =injected.fs;

        //fog
        if(scene.hasFog){
            injected = OMEGA.Omega3D.ShaderUtil.InjectGLSLForFog(vs, fs, uniforms,scene);
            uniforms = injected.uniforms;
            vs =injected.vs;
            fs =injected.fs;
        }
    }

    var fragmentOnlyUniforms = {};
    fragmentOnlyUniforms["uSampler"]   = { id:"uSampler"  , type: "sampler2D", glsl: "", value: null };
    fragmentOnlyUniforms["uSampler_1"] = { id:"uSampler_1", type: "sampler2D", glsl: "", value: null };

    var custom_uniforms = {};
    custom_uniforms["uLightM"] = { type:"mat4", glsl: "", value: "" };

    return new OMEGA.Omega3D.Shader(vs,fs,  [OMEGA.Omega3D.Shaders.Components.StandardAttributes(),
                                            uniforms,
                                            custom_uniforms,
                                            fragmentOnlyUniforms]);
};
OMEGA.Omega3D.Shaders.VSM02 = function(){
    var lights = scene.getLights();
    
    //VERTEX SOURCE.
    var vertex_source = OMEGA.Omega3D.Shaders.Components.Basic_Includes();
    for( var i = 0; i < lights.length; i++){
        vertex_source += "uniform mat4 uLightM_"+i.toString()+";";
        vertex_source += "varying vec4 vertexPosLight_"+i.toString()+";";
    }
    vertex_source += "varying vec2 vTexCoord;";
    vertex_source += "void main(void){";

    for( var i = 0; i < lights.length; i++){
        vertex_source += "vertexPosLight_"+i.toString()+" = uProjectionMatrix * uLightM_"+i.toString()+" * uModelMatrix * vec4(aVertexPos.xyz, 1.0);";
    }
    
    vertex_source += "gl_Position    = uProjectionMatrix * uViewMatrix * uModelMatrix * vec4(aVertexPos.xyz, 1.0);";
    vertex_source += "vTexCoord      = aTextureCoord;";
    vertex_source += "}";
    
    
    //FRAGMENT SOURCE.
    var fragment_source = OMEGA.Omega3D.Shaders.Components.Fragment_Precision_Mediump_Float();
    fragment_source += OMEGA.Omega3D.Shaders.Components.Basic_Fragment_Includes();
    fragment_source += "uniform sampler2D uSampler;";
    fragment_source += "varying vec2 vTexCoord;";
 
    for( var i = 0; i < lights.length; i++){
        fragment_source += "uniform sampler2D uSampler_"+(i+1).toString()+";";
        fragment_source += "varying vec4 vertexPosLight_"+i.toString()+";";
    }

    
    
}

/**
 * ColorPicking
 *
 * @returns {OMEGA.Omega3D.Shader}
 * @constructor
 */
OMEGA.Omega3D.Shaders.ColorPicking = function(){
   // OMEGA.Omega3D.Log(" >> ShaderBuilder: Creating 'ColorPicking' shader");

    var vertex_source = OMEGA.Omega3D.Shaders.Components.Basic_Includes();
    vertex_source = vertex_source + "varying vec3 vPickingColor;";
    vertex_source = vertex_source + "void main(void){";
    vertex_source = vertex_source + "vPickingColor = aPickingColor;";
    vertex_source = vertex_source + OMEGA.Omega3D.Shaders.Components.Vertex_World_Conversion_V3("gl_Position", "aVertexPos");
    vertex_source = vertex_source + "}";

    var fragment_source = OMEGA.Omega3D.Shaders.Components.Fragment_Precision_Mediump_Float();
    fragment_source = fragment_source + "varying vec3 vPickingColor;";
    fragment_source = fragment_source + "void main(void){";
    fragment_source = fragment_source + "vec4 color = vec4(vPickingColor.r,0, 0, 1.0);";
    fragment_source = fragment_source + OMEGA.Omega3D.Shaders.Components.Basic_Fragment_Logic("vec4(color.rgb, color.a)");
    fragment_source = fragment_source + "}";

    var uniforms = OMEGA.Omega3D.Shaders.Components.StandardUniforms();
    var fragmentOnlyUniforms = {};

    return new OMEGA.Omega3D.Shader(vertex_source,fragment_source,  [OMEGA.Omega3D.Shaders.Components.StandardAttributes(),
        uniforms,
        {},
        fragmentOnlyUniforms]);
};



OMEGA.Omega3D.Shaders.BumpMapping = function(scene){
    var vs = [
        OMEGA.Omega3D.Shaders.Components.Basic_Includes(),
        "varying vec2 vTexCoord;",

        "void main(void){",
        "mat4 mvp       = uProjectionMatrix * uViewMatrix * uModelMatrix;",
        "vec4 pos       = mvp * vec4(aVertexPos.xyz, 1.0);",
        "gl_Position    = pos;",
        "vTexCoord      = aTextureCoord;",

        "}"
    ].join("\n");

    var fs = [
        OMEGA.Omega3D.Shaders.Components.Fragment_Precision_Mediump_Float(),
        OMEGA.Omega3D.Shaders.Components.Basic_Fragment_Includes(),
        "uniform sampler2D uSampler;",
        "uniform sampler2D uSampler_1;",
        "varying vec2 vTexCoord;",

        "void main(void){",
            "vec3 texture        = texture2D(uSampler  , vec2(vTexCoord.st)).rgb;",
            "vec3 texture_normal = texture2D(uSampler_1, vec2(vTexCoord.st)).rgb;",
            "texture_normal      = normalize(texture_normal* 2.0 - 1.0);",

            "vec4 color = vec4(texture.xyz, 1.0);",
            OMEGA.Omega3D.Shaders.Components.Basic_Fragment_Logic("vec4(color.rgb, color.a)"),
        "}"
    ].join("\n");

    var uniforms = OMEGA.Omega3D.Shaders.Components.StandardUniforms();
    //if(scene != null){
    //    var injected = OMEGA.Omega3D.ShaderUtil.InjectGLSLForLights(vs, fs, uniforms, scene);
    //    uniforms = injected.uniforms;
    //    vs =injected.vs;
    //    fs =injected.fs;
    //
    //    //fog
    //    if(scene.hasFog){
    //        injected = OMEGA.Omega3D.ShaderUtil.InjectGLSLForFog(vs, fs, uniforms,scene);
    //        uniforms = injected.uniforms;
    //        vs =injected.vs;
    //        fs =injected.fs;
    //    }
    //}

    var fragmentOnlyUniforms = {};
    fragmentOnlyUniforms["uSampler"  ] = { id:"uSampler"  , type: "sampler2D", glsl: "", value: null };
    fragmentOnlyUniforms["uSampler_1"] = { id:"uSampler_1", type: "sampler2D", glsl: "", value: null };

    var custom_uniforms = {};

    return new OMEGA.Omega3D.Shader(vs,fs,  [OMEGA.Omega3D.Shaders.Components.StandardAttributes(),
        uniforms,
        custom_uniforms,
        fragmentOnlyUniforms]);

}


/**
 * Fresnel (Rim and Reflection)
 *
 * @param scene
 * @param data -  { scale:"1.0",  power:"1.0",  minimum:"0.0",  color:"1.0, 1.0, 1.0"}
 * @returns {OMEGA.Omega3D.Shader}
 * @constructor
 */
OMEGA.Omega3D.Shaders.FresnelRim = function(scene, data){
    data = data || { scale:"1.0", power:"1.0", minimum:"0.0", color:"1.0, 1.0, 1.0"};
    var vs = [
        OMEGA.Omega3D.Shaders.Components.Basic_Includes(),
        "varying vec2 vTexCoord;",
        "varying vec3 vNormal;",
        "varying float vRatio;",

        "void main(void){",
            "vec3 posWorld  = vec3( uModelMatrix * vec4(aVertexPos, 1.0)).xyz;",
            "gl_Position = uProjectionMatrix * uViewMatrix * vec4(posWorld, 1.0);",
            OMEGA.Omega3D.Shaders.Components.Basic_TextureCoords("vTexCoord"),


            "vec3 vNormal = normalize(mat3(uModelMatrix) * aVertexNormal );",

            "vec3 I = normalize( uCamPos - posWorld);",
            "float MINIMUM = " + data.minimum + ";",
            "float SCALE = "+data.scale+";",
            "float POWER = "+data.power+";",


            "vRatio = MINIMUM + SCALE * pow(1.0 + dot(I, vNormal), POWER);",
           // "vRatio = min(1.0, vRatio );",
           // "vRatio = max(0.0, vRatio );",
        "}"
    ].join("\n");

    var fs = [
        OMEGA.Omega3D.Shaders.Components.Fragment_Precision_Mediump_Float(),
        OMEGA.Omega3D.Shaders.Components.Basic_Fragment_Includes(),
        "uniform sampler2D uSampler;",
        "varying vec2 vTexCoord;",
        "varying float vRatio;",


        "void main(void){",
            "vec4 RIM_COLOR = vec4("+data.color+", 1.0);",
            "vec4 c     = texture2D( uSampler, vTexCoord.st);",
            "vec4 color = mix( c, RIM_COLOR, vRatio);",
            OMEGA.Omega3D.Shaders.Components.Basic_Fragment_Logic("vec4(color.rgb, color.a)"),
        "}"
    ].join("\n");


    var uniforms = OMEGA.Omega3D.Shaders.Components.StandardUniforms();
    if(scene != null){
        var injected = OMEGA.Omega3D.ShaderUtil.InjectGLSLForLights(vs, fs, uniforms, scene);
        uniforms = injected.uniforms;
        vs =injected.vs;
        fs =injected.fs;

        //fog
        if(scene.hasFog){
            injected = OMEGA.Omega3D.ShaderUtil.InjectGLSLForFog(vs, fs, uniforms,scene);
            uniforms = injected.uniforms;
            vs =injected.vs;
            fs =injected.fs;
        }
    }

    var fragmentOnlyUniforms = {};
    fragmentOnlyUniforms["uSampler"]   = { id:"uSampler"  , type: "sampler2D", glsl: "", value: null };

    var custom_uniforms = {};
    return new OMEGA.Omega3D.Shader(vs,fs,  [OMEGA.Omega3D.Shaders.Components.StandardAttributes(),
                                             uniforms,
                                             custom_uniforms,
                                             fragmentOnlyUniforms]);

};
OMEGA.Omega3D.Shaders.FresnelReflection = function(scene, data){
    data = data || { scale:"1.0", power:"1.0", minimum:"0.0",color:"1.0, 1.0, 1.0"};
    var vs = [
        OMEGA.Omega3D.Shaders.Components.Basic_Includes(),
        "varying vec2 vTexCoord;",
        "varying vec3 vTexCoordCubeMap;",
        "varying float vRatio;",

        "void main(void){",
            "vec3 posWorld  = vec3( uModelMatrix * vec4(aVertexPos, 1.0)).xyz;",
            "vec3 worldNormal = normalize(mat3(uModelMatrix) * aVertexNormal );",
            "vec3 vertexToCamWorld = normalize( uCamPos - posWorld);",

            "gl_Position = uProjectionMatrix * uViewMatrix * vec4(posWorld, 1.0);",
            OMEGA.Omega3D.Shaders.Components.Basic_TextureCoords("vTexCoord"),

            "vTexCoordCubeMap =  normalize( reflect( -vertexToCamWorld, worldNormal ) );"+

            "float MINIMUM = " + data.minimum + ";",
            "float SCALE = " + data.scale + ";",
            "float POWER =  " + data.power + ";",
            "vRatio      = MINIMUM + SCALE * pow(1.0 + dot(vertexToCamWorld, worldNormal), POWER);",
        "}"
    ].join("\n");

    var fs = [
        OMEGA.Omega3D.Shaders.Components.Fragment_Precision_Mediump_Float(),
        OMEGA.Omega3D.Shaders.Components.Basic_Fragment_Includes(),
        "uniform samplerCube uSampler;",
        "uniform sampler2D uSampler_1;",
        "varying vec2 vTexCoord;",
        "varying vec3 vTexCoordCubeMap;",
        "varying float vRatio;",

        "void main(void){",
            "vec4 c     =  texture2D  ( uSampler_1, vTexCoord.st    );",
            "vec4 cm    = textureCube( uSampler  , vTexCoordCubeMap);",
            "vec4 color = mix( c, cm, vRatio);",
            OMEGA.Omega3D.Shaders.Components.Basic_Fragment_Logic("vec4(color.rgb, color.a)"),
        "}"
    ].join("\n");


    //lights
    var uniforms = OMEGA.Omega3D.Shaders.Components.StandardUniforms();
    if(scene != null){
        var injected = OMEGA.Omega3D.ShaderUtil.InjectGLSLForLights(vs, fs, uniforms, scene);
        uniforms = injected.uniforms;
        vs =injected.vs;
        fs =injected.fs;

        //fog
        if(scene.hasFog){
            injected = OMEGA.Omega3D.ShaderUtil.InjectGLSLForFog(vs, fs, uniforms,scene);
            uniforms = injected.uniforms;
            vs =injected.vs;
            fs =injected.fs;
        }
    }

    //uniforms
    var fragmentOnlyUniforms = {};
    fragmentOnlyUniforms["uSampler"]   = { id:"uSampler", type: "samplerCube", glsl: "", value: null };
    fragmentOnlyUniforms["uSampler_1"] = { id:"uSampler_1", type: "sampler2D", glsl: "", value: null };
    return new OMEGA.Omega3D.Shader( vs,fs,
                                    [OMEGA.Omega3D.Shaders.Components.StandardAttributes(),
                                     uniforms,
                                     {},
                                     fragmentOnlyUniforms]);
}







//===================================================================================
// PostProcessing
//===================================================================================
OMEGA.Omega3D.Shaders.PostProcessing = function( isTextured, scene, color ){
    var texture, fragmentColor;
    isTextured = isTextured || false;
    if(isTextured) texture = "varying vec2 vTexCoord;";
    else texture = "";
    var vertex_source = OMEGA.Omega3D.Shaders.Components.Basic_Includes();
    vertex_source = vertex_source + texture;
    vertex_source = vertex_source + "void main(void){";
    vertex_source = vertex_source + OMEGA.Omega3D.Shaders.Components.Vertex_ScreenSpace_Conversion_V3("gl_Position", "aVertexPos");
    if(isTextured                      ) vertex_source = vertex_source + OMEGA.Omega3D.Shaders.Components.Basic_TextureCoords("vTexCoord");
    vertex_source = vertex_source + "}";


    if(isTextured){
        texture = "uniform sampler2D uSampler;"+
        "varying vec2 vTexCoord;";
        fragmentColor = "texture2D(uSampler, vec2(1.0-vTexCoord.s, 1.0 - vTexCoord.t));";
    }
    else{
        if(color) fragmentColor = "vec4("+color[0]+","+ +color[1]+"," +  +color[2]+", 1.0);";
        else  fragmentColor = "vec4(0.8,0.4,0.0, 1.0);";
    }
    var fragment_source = OMEGA.Omega3D.Shaders.Components.Fragment_Precision_Mediump_Float();
    if( isTextured                     ){
        fragment_source = fragment_source + OMEGA.Omega3D.Shaders.Components.Basic_Fragment_Includes();
        fragment_source = fragment_source + texture;
    }
    fragment_source = fragment_source + "void main(void){";
    fragment_source = fragment_source + "vec4 color = "+fragmentColor;
    fragment_source = fragment_source + OMEGA.Omega3D.Shaders.Components.Basic_Fragment_Logic("vec4(color.rgb, color.a)");
    fragment_source = fragment_source + "}";


    var uniforms = OMEGA.Omega3D.Shaders.Components.StandardUniforms();

    var fragmentOnlyUniforms = {};
    if(isTextured) fragmentOnlyUniforms["uSampler"] = { id:"uSampler", type: "sampler2D", glsl: "", value: null };
    return new OMEGA.Omega3D.Shader(vertex_source,fragment_source,  [OMEGA.Omega3D.Shaders.Components.StandardAttributes(),
        uniforms,
        {},
        fragmentOnlyUniforms]);
};
OMEGA.Omega3D.Shaders.ExposureToneMapping = function(){
    var vs = [
        OMEGA.Omega3D.Shaders.Components.Basic_Includes(),
        "varying vec2 vTexCoord;",
        "void main(void){",
            "mat4 mvp       = uProjectionMatrix * uViewMatrix * uModelMatrix;",
            "vec4 pos       = vec4(aVertexPos.xyz, 1.0);",
            "gl_Position    = pos;",
            OMEGA.Omega3D.Shaders.Components.Basic_TextureCoords("vTexCoord"),
        "}"
    ].join("\n");

    var fs = [
        OMEGA.Omega3D.Shaders.Components.Fragment_Precision_Mediump_Float(),
        OMEGA.Omega3D.Shaders.Components.Basic_Fragment_Includes(),
        "uniform sampler2D uSampler;",
        "varying vec2 vTexCoord;",
        "void main(void){",
            "float gamma = 2.2;",
            "float exposure = 0.5;",

            "vec3 hdrColor = texture2D(uSampler, vTexCoord ).rgb;",
            "vec3 mapped = hdrColor / (hdrColor + vec3(1.0));", // Reinhard tone mapping
            //"vec3 mapped   = vec3(1.0) - exp(-hdrColor * exposure);",
            //"mapped        = pow(mapped, vec3(1.0/gamma));",

            "vec4 color = vec4(mapped, 1.0);",

            OMEGA.Omega3D.Shaders.Components.Basic_Fragment_Logic("vec4(color.rgb, color.a)"),
        "}"
    ].join("\n");



    var uniforms = OMEGA.Omega3D.Shaders.Components.StandardUniforms();

    var fragmentOnlyUniforms = {};
    fragmentOnlyUniforms["uSampler"] = { id:"uSampler", type: "sampler2D", glsl: "", value: null };
    return new OMEGA.Omega3D.Shader(vs,fs,  [OMEGA.Omega3D.Shaders.Components.StandardAttributes(),
        uniforms,
        {},
        fragmentOnlyUniforms]);
};
OMEGA.Omega3D.Shaders.BloomMap = function(factor){
    var vs = [
        OMEGA.Omega3D.Shaders.Components.Basic_Includes(),
        "varying vec2 vTexCoord;",
        "void main(void){",
        "mat4 mvp       = uProjectionMatrix * uViewMatrix * uModelMatrix;",
        "vec4 pos       = vec4(aVertexPos.xyz, 1.0);",
        "gl_Position    = pos;",
        OMEGA.Omega3D.Shaders.Components.Basic_TextureCoords("vTexCoord"),
        "}"
    ].join("\n");

    var fs = [
        OMEGA.Omega3D.Shaders.Components.Fragment_Precision_Mediump_Float(),
        OMEGA.Omega3D.Shaders.Components.Basic_Fragment_Includes(),
        "uniform sampler2D uSampler;",
        "varying vec2 vTexCoord;",
        "void main(void){",
        "vec4 color = vec4(vec3(0), 1.0);",
        "vec4 fragcolor = texture2D(uSampler, vTexCoord );",
        "float brightness = dot( fragcolor.rgb, vec3( 0.2126, 0.7152, 0.0722));",

        "if(brightness > "+factor.toFixed(2).toString()+"){",
            "color = vec4( fragcolor.rgb, 1.0);",
        "}",

        OMEGA.Omega3D.Shaders.Components.Basic_Fragment_Logic("vec4(color.rgb, color.a)"),
        "}"
    ].join("\n");



    var uniforms = OMEGA.Omega3D.Shaders.Components.StandardUniforms();

    var fragmentOnlyUniforms = {};
    fragmentOnlyUniforms["uSampler"] = { id:"uSampler", type: "sampler2D", glsl: "", value: null };
    return new OMEGA.Omega3D.Shader(vs,fs,  [OMEGA.Omega3D.Shaders.Components.StandardAttributes(),
        uniforms,
        {},
        fragmentOnlyUniforms]);
};
OMEGA.Omega3D.Shaders.RenderToScreen = function(){
    var vs = [
        OMEGA.Omega3D.Shaders.Components.Basic_Includes(),
        "varying vec2 vTexCoord;",
        "void main(void){",
        "mat4 mvp       = uProjectionMatrix * uViewMatrix * uModelMatrix;",
        "vec4 pos       = vec4(aVertexPos.xyz, 1.0);",
        "gl_Position    = pos;",
        OMEGA.Omega3D.Shaders.Components.Basic_TextureCoords("vTexCoord"),
        "}"
    ].join("\n");

    var fs = [
        OMEGA.Omega3D.Shaders.Components.Fragment_Precision_Mediump_Float(),
        OMEGA.Omega3D.Shaders.Components.Basic_Fragment_Includes(),
        "uniform sampler2D uSampler;",
        "varying vec2 vTexCoord;",
        "void main(void){",
        "vec3 sample = texture2D(uSampler, vec2(1.0) - vTexCoord.st ).rgb;",
        "vec4 color = vec4(sample, 1.0);",
        OMEGA.Omega3D.Shaders.Components.Basic_Fragment_Logic("vec4(color.rgb, color.a)"),
        "}"
    ].join("\n");

    var fragmentOnlyUniforms = {};
    fragmentOnlyUniforms["uSampler"] = { id:"uSampler", type: "sampler2D", glsl: "", value: null };
    return new OMEGA.Omega3D.Shader(vs,fs,  [OMEGA.Omega3D.Shaders.Components.StandardAttributes(),
        OMEGA.Omega3D.Shaders.Components.StandardUniforms(),
        {},
        fragmentOnlyUniforms]);
};
OMEGA.Omega3D.Shaders.TextureBlend = function(){

    var vs = [
        OMEGA.Omega3D.Shaders.Components.Basic_Includes(),
        "varying vec2 vTexCoord;",
        "void main(void){",
        "mat4 mvp       = uProjectionMatrix * uViewMatrix * uModelMatrix;",
        "vec4 pos       = vec4(aVertexPos.xyz, 1.0);",
        "gl_Position    = pos;",
        OMEGA.Omega3D.Shaders.Components.Basic_TextureCoords("vTexCoord"),
        "}"
    ].join("\n");

    var fs = [
        OMEGA.Omega3D.Shaders.Components.Fragment_Precision_Mediump_Float(),
        OMEGA.Omega3D.Shaders.Components.Basic_Fragment_Includes(),
        "uniform sampler2D uSampler;",
        "uniform sampler2D uSampler_1;",
        "varying vec2 vTexCoord;",
        "void main(void){",
        "float gamma = 2.2;",
        "float exposure = 1.0;",

        "vec3 colorA   = texture2D(uSampler, vTexCoord ).rgb;",
        "vec3 colorB   = texture2D(uSampler_1,vTexCoord ).rgb;",
        //"colorB = vec3(1.0) - exp(-colorB * exposure);",
        "colorA       += colorB;",
       // "colorA = colorA * 0.5;",

        "vec3 result = vec3(1.0) - exp(-colorA * exposure);",
        "result      = pow( result, vec3(1.0/gamma));",

        "vec4 color = vec4(result, 1.0);",

        OMEGA.Omega3D.Shaders.Components.Basic_Fragment_Logic("vec4(color.rgb, color.a)"),
        "}"
    ].join("\n");



    var uniforms = OMEGA.Omega3D.Shaders.Components.StandardUniforms();

    var fragmentOnlyUniforms = {};
    fragmentOnlyUniforms["uSampler"] = { id:"uSampler", type: "sampler2D", glsl: "", value: null };
    fragmentOnlyUniforms["uSampler_1"] = { id:"uSampler_1", type: "sampler2D", glsl: "", value: null };
    return new OMEGA.Omega3D.Shader(vs,fs,  [OMEGA.Omega3D.Shaders.Components.StandardAttributes(),
        uniforms,
        {},
        fragmentOnlyUniforms]);
};
OMEGA.Omega3D.Shaders.DownSample = function(){

    var vs = [
        OMEGA.Omega3D.Shaders.Components.Basic_Includes(),
        "varying vec2 vTexCoord;",
        "void main(void){",
        "mat4 mvp       = uProjectionMatrix * uViewMatrix * uModelMatrix;",
        "vec4 pos       = vec4(aVertexPos.xyz, 1.0);",
        "gl_Position    = pos;",
        OMEGA.Omega3D.Shaders.Components.Basic_TextureCoords("vTexCoord"),
        "}"
    ].join("\n");

    var fs = [
        OMEGA.Omega3D.Shaders.Components.Fragment_Precision_Mediump_Float(),
        OMEGA.Omega3D.Shaders.Components.Basic_Fragment_Includes(),
        "uniform sampler2D uSampler;",
        "varying vec2 vTexCoord;",
        "void main(void){",


        "vec4 color = texture2D(uSampler, vec2(1.0, 1.0) - vTexCoord );",

        OMEGA.Omega3D.Shaders.Components.Basic_Fragment_Logic("vec4(color.rgb, color.a)"),
        "}"
    ].join("\n");



    var uniforms = OMEGA.Omega3D.Shaders.Components.StandardUniforms();

    var fragmentOnlyUniforms = {};
    fragmentOnlyUniforms["uSampler"] = { id:"uSampler", type: "sampler2D", glsl: "", value: null };
    return new OMEGA.Omega3D.Shader(vs,fs,  [OMEGA.Omega3D.Shaders.Components.StandardAttributes(),
        uniforms,
        {},
        fragmentOnlyUniforms]);
};
OMEGA.Omega3D.Shaders.DirectionalBlur = function( horinzontal, resolution ){
    var vs = [
        OMEGA.Omega3D.Shaders.Components.Basic_Includes(),
        "varying vec2 vTexCoord;",
        "void main(void){",
        "mat4 mvp       = uProjectionMatrix * uViewMatrix * uModelMatrix;",
        "vec4 pos       = vec4(aVertexPos.xyz, 1.0);",
        "gl_Position    = pos;",
        OMEGA.Omega3D.Shaders.Components.Basic_TextureCoords("vTexCoord"),
        "}"
    ].join("\n");

    var fs = [
        OMEGA.Omega3D.Shaders.Components.Fragment_Precision_Mediump_Float(),
        OMEGA.Omega3D.Shaders.Components.Basic_Fragment_Includes(),
        "uniform sampler2D uSampler;",
        "varying vec2 vTexCoord;",

        "float weight[5];",

        "void main(void){",
            "weight[0] = 0.227027;",
            "weight[1] = 0.1945946;",
            "weight[2] = 0.1216216;",
            "weight[3] = 0.054054;",
            "weight[4] = 0.016216;",

            "vec4 color      = vec4(vec3(0), 1.0);",
            "vec2 tex_offset = vec2(1.0 / "+resolution.toFixed(1).toString()+");",
            "vec3 result     = texture2D( uSampler, vTexCoord).rgb * weight[0];",
            "float t = 1.0;",
            "if("+horinzontal+" == 1){",
                "for(int i = 1; i < 5; ++i){",
                    "result += texture2D(uSampler, vTexCoord + vec2( tex_offset.x * t, 0.0)).rgb * weight[i];",
                    "result += texture2D(uSampler, vTexCoord - vec2( tex_offset.x * t, 0.0)).rgb * weight[i];",
                    "t += 1.0;",
                "}",
            "}else{",
                "for(int i = 1; i < 5; ++i){",
                    "result += texture2D(uSampler, vTexCoord + vec2( 0.0, tex_offset.y * t)).rgb * weight[i];",
                    "result += texture2D(uSampler, vTexCoord - vec2( 0.0, tex_offset.y * t)).rgb * weight[i];",
                    "t += 1.0;",
                "}",
            "}",
            "color = vec4(result, 1.0);",
            OMEGA.Omega3D.Shaders.Components.Basic_Fragment_Logic("vec4(color.rgb, color.a)"),
        "}"
    ].join("\n");



    var uniforms = OMEGA.Omega3D.Shaders.Components.StandardUniforms();

    var fragmentOnlyUniforms = {};
    fragmentOnlyUniforms["uSampler"] = { id:"uSampler", type: "sampler2D", glsl: "", value: null };
    return new OMEGA.Omega3D.Shader(vs,fs,  [OMEGA.Omega3D.Shaders.Components.StandardAttributes(),
        uniforms,
        {},
        fragmentOnlyUniforms]);
}
OMEGA.Omega3D.Shaders.GodRays = function(){
    var vs = [
        OMEGA.Omega3D.Shaders.Components.Basic_Includes(),
        "uniform vec3 uLightPos;",
        "varying vec2 vTexCoord;",
        "varying vec3 vLightPos;",
        "varying mat4 vInvViewMatrix;",
        
        "void main(void){",
            "vec4 pos    = vec4(aVertexPos.xyz, 1.0);",
            "gl_Position = pos;",
            "vLightPos = uLightPos;",
             OMEGA.Omega3D.Shaders.Components.Basic_TextureCoords("vTexCoord"),
        "}"
    ].join("\n");

    var fs = [
        OMEGA.Omega3D.Shaders.Components.Fragment_Precision_Mediump_Float(),
        OMEGA.Omega3D.Shaders.Components.Basic_Fragment_Includes(),
        "uniform sampler2D uSampler;",

        "varying vec3 vLightPos;",
        "varying vec2 vTexCoord;",
        "const int NUM_SAMPLES = 100;",
        
        "void main(void){",
          
            "vec2 texCoo    =  vec2(1.0) - vTexCoord;",
            "vec4 color     = vec4(vec3(0), 1.0);",
            "float exposure = 0.9;",
            "float decay    = 0.9;",
            "float density  = 1.0;",
            "float weight   = 0.9;",
            "float illuminationDecay = 1.0;",
            
            "vec2 deltaTexCoord = vec2( texCoo.st - vec2(vLightPos.xy));",
            "deltaTexCoord *= density / float(NUM_SAMPLES);", 
            "vec4 sample = texture2D( uSampler, texCoo);",
            "for(int i = 0; i < NUM_SAMPLES; i++){",
                "texCoo  -= deltaTexCoord;",
                "vec4 sample = texture2D( uSampler, texCoo );",
                "sample *= illuminationDecay * weight;",

                "color += sample;",
                "illuminationDecay *= decay;",
            "}",
            "color *= exposure;",
            OMEGA.Omega3D.Shaders.Components.Basic_Fragment_Logic("vec4(color.rgb, color.a)"),
        "}"
    ].join("\n");



    var uniforms = OMEGA.Omega3D.Shaders.Components.StandardUniforms();

    var fragmentOnlyUniforms = {};
    fragmentOnlyUniforms["uSampler"] = { id:"uSampler", type: "sampler2D", glsl: "", value: null };
   // fragmentOnlyUniforms["uLightPos"] = { id:"uLightPos", type: "vec3", glsl: "", value: null };
    return new OMEGA.Omega3D.Shader(vs,fs,  [OMEGA.Omega3D.Shaders.Components.StandardAttributes(),
        uniforms,
        {},
        fragmentOnlyUniforms]);
}



//NEEDS TO BE REVISED.
OMEGA.Omega3D.Shaders.PP_ColorMultiply = function(val){
    //OMEGA.Omega3D.Log(" >> ShaderBuilder: Creating 'Basic PostProcessing' shader");
    var texture, fragmentColor;

    texture = "varying vec2 vTexCoord;";
    var vertex_source = OMEGA.Omega3D.Shaders.Components.Basic_Includes();
    vertex_source = vertex_source + texture;
    vertex_source = vertex_source + "void main(void){";
    vertex_source = vertex_source + OMEGA.Omega3D.Shaders.Components.Vertex_ScreenSpace_Conversion_V3("gl_Position", "aVertexPos");
    vertex_source = vertex_source + OMEGA.Omega3D.Shaders.Components.Basic_TextureCoords("vTexCoord");
    vertex_source = vertex_source + "}";



   texture = "uniform sampler2D uSampler;"+
             "varying vec2 vTexCoord;";


    var fragment_source = OMEGA.Omega3D.Shaders.Components.Fragment_Precision_Mediump_Float();
    fragment_source = fragment_source + OMEGA.Omega3D.Shaders.Components.Basic_Fragment_Includes();
    fragment_source = fragment_source + texture;
    fragment_source = fragment_source + "void main(void){";
    fragment_source = fragment_source + "vec4 color = texture2D(uSampler, vec2(vTexCoord.s, vTexCoord.t));";
    fragment_source = fragment_source + OMEGA.Omega3D.Shaders.Components.Basic_Fragment_Logic("vec4(color.rgb*"+val+", color.a)");
    fragment_source = fragment_source + "}";


    var uniforms = OMEGA.Omega3D.Shaders.Components.StandardUniforms();

    var fragmentOnlyUniforms = {};
    fragmentOnlyUniforms["uSampler"] = { id:"uSampler", type: "sampler2D", glsl: "", value: null };
    return new OMEGA.Omega3D.Shader(vertex_source,fragment_source,  [OMEGA.Omega3D.Shaders.Components.StandardAttributes(),
        uniforms,
        {},
        fragmentOnlyUniforms]);
};
OMEGA.Omega3D.Shaders.PP_Blur = function(){
   // OMEGA.Omega3D.Log(" >> ShaderBuilder: Creating 'Basic PostProcessing' shader");

    var vs = [
        OMEGA.Omega3D.Shaders.Components.Basic_Includes(),
        "varying vec2 vTexCoord;",

        "void main(void){",
        "mat4 mvp       = uProjectionMatrix * uViewMatrix * uModelMatrix;",
        "vec4 pos       = vec4(aVertexPos.xyz, 1.0);",
        "gl_Position    = pos;",
        "vTexCoord      = vec2(1.0, 1.0) - aTextureCoord;",

        "}"
    ].join("\n");


    var fs = [
        OMEGA.Omega3D.Shaders.Components.Fragment_Precision_Mediump_Float(),
        OMEGA.Omega3D.Shaders.Components.Basic_Fragment_Includes(),
        "uniform sampler2D uSampler;",
        "varying vec2 vTexCoord;",



        "void main(void){",
            "vec4 color = vec4(0);",
            "if ( Orientation == 0 ){",
                // Blur horizontal
                "for (int i = 0; i &lt; 10; ++i){",
                    "if ( i &gt;= BlurAmount ) break;",
                    "float offset = float(i) - halfBlur;",
                    "color += texture2D(Sample0, vUv + vec2(offset * TexelSize.x, 0.0))", /* Gaussian(offset, deviation)*/
                "}",
            "}else{",
            // Blur vertical
            "for (int i = 0; i &lt; 10; ++i){",
                "if ( i &gt;= BlurAmount )break;",
                "float offset = float(i) - halfBlur;",
                "color += texture2D(Sample0, vUv + vec2(0.0, offset * TexelSize.y));", /* Gaussian(offset, deviation)*/
                "}",
            "}",

        // Calculate average
        "color = color / float(BlurAmount);",



        OMEGA.Omega3D.Shaders.Components.Basic_Fragment_Logic("vec4(color.rgb, color.a)"),
        "}"
    ].join("\n");



    var uniforms = OMEGA.Omega3D.Shaders.Components.StandardUniforms();

    var fragmentOnlyUniforms = {};
    fragmentOnlyUniforms["uSampler"] = { id:"uSampler", type: "sampler2D", glsl: "", value: null };
    return new OMEGA.Omega3D.Shader(vs,fs,  [OMEGA.Omega3D.Shaders.Components.StandardAttributes(),
        uniforms,
        {},
        fragmentOnlyUniforms]);
};
OMEGA.Omega3D.Shaders.Gausian_Blur = function(resolution){
    // OMEGA.Omega3D.Log(" >> ShaderBuilder: Creating 'Basic PostProcessing' shader");

    var vs = [
        OMEGA.Omega3D.Shaders.Components.Basic_Includes(),

        "void main(void){",
        "mat4 mvp       = uProjectionMatrix * uViewMatrix * uModelMatrix;",
        "vec4 pos       = vec4(aVertexPos.xyz, 1.0);",
        "gl_Position    = pos;",
        "}"
    ].join("\n");





    var fs = [
        OMEGA.Omega3D.Shaders.Components.Fragment_Precision_Mediump_Float(),
        OMEGA.Omega3D.Shaders.Components.Basic_Fragment_Includes(),
        "uniform sampler2D uSampler;",

        "float normpdf(in float x, in float sigma){",
            "return 0.39894*exp(-0.5*x*x/(sigma*sigma))/sigma;",
        "}",





        "void main(void){",
            "vec4 color = vec4(0);",
    //declare stuff
        "const int mSize = 8;",
        "const int kSize = (mSize-1)/2;",
        "float kernel[mSize];",
        "vec3 final_color = vec3(0.0);",

    //create the 1-D kernel
        "float sigma = 8.0;",
        "float Z = 0.0;",
        "for (int j = 0; j <= kSize; ++j){",
            "kernel[kSize+j] = kernel[kSize-j] = normpdf(float(j), sigma);",
        "}",

    //get the normalization factor (as the gaussian has been clamped)
        "for (int j = 0; j < mSize; ++j){",
            "Z += kernel[j];",
        "}",

    //read out the texels
        "vec2 uv =vec2(gl_FragCoord.xy);",
        "for (int i=-kSize; i <= kSize; ++i){",
            "for (int j=-kSize; j <= kSize; ++j){",
                "vec2 newUV = (uv.xy+vec2(float(i),float(j))) / vec2( "+resolution.toFixed(1).toString()+", "+resolution.toFixed(1).toString()+");",
                "final_color += kernel[kSize+j]*kernel[kSize+i]*texture2D(uSampler, newUV).rgb;",

            "}",
        "}",

        "color = vec4(final_color/(Z*Z), 1.0);",

        OMEGA.Omega3D.Shaders.Components.Basic_Fragment_Logic("vec4(color.rgb, color.a)"),
        "}"
    ].join("\n");



    var uniforms = OMEGA.Omega3D.Shaders.Components.StandardUniforms();

    var fragmentOnlyUniforms = {};
    fragmentOnlyUniforms["uSampler"] = { id:"uSampler", type: "sampler2D", glsl: "", value: null };
    return new OMEGA.Omega3D.Shader(vs,fs,  [OMEGA.Omega3D.Shaders.Components.StandardAttributes(),
        uniforms,
        {},
        fragmentOnlyUniforms]);
};
OMEGA.Omega3D.Shaders.PP_Bloom = function(amount){
    //OMEGA.Omega3D.Log(" >> ShaderBuilder: Creating 'Basic PostProcessing' shader");
    var texture, fragmentColor;

    texture = "varying vec2 vTexCoord;";
    var vertex_source = OMEGA.Omega3D.Shaders.Components.Basic_Includes();
    vertex_source = vertex_source + texture;
    vertex_source = vertex_source + "void main(void){";
    vertex_source = vertex_source + OMEGA.Omega3D.Shaders.Components.Vertex_ScreenSpace_Conversion_V3("gl_Position", "aVertexPos");
    vertex_source = vertex_source + OMEGA.Omega3D.Shaders.Components.Basic_TextureCoords("vTexCoord");
    vertex_source = vertex_source + "}";



    texture = "uniform sampler2D uSampler;"+
    "varying vec2 vTexCoord;";


    amount = amount == null ? "3" : amount;
    var fragment_source = OMEGA.Omega3D.Shaders.Components.Fragment_Precision_Mediump_Float();
    fragment_source = fragment_source + OMEGA.Omega3D.Shaders.Components.Basic_Fragment_Includes();
    fragment_source = fragment_source + texture;
    fragment_source = fragment_source + "void main(void){";
    fragment_source = fragment_source + "vec4 sum = vec4(0.0);";
    fragment_source = fragment_source + "vec2 tc = vec2(1.0, 1.0) - vTexCoord;";
    fragment_source = fragment_source + "int amount = 5;";
    fragment_source = fragment_source + "for( int i= -"+amount+" ;i < "+amount+"; i++){";
        fragment_source = fragment_source + "for ( int j = -"+amount+"; j < "+amount+"; j++){";
            fragment_source = fragment_source + "sum += texture2D(uSampler, tc + vec2(j, i)*0.004) * 0.25;";
        fragment_source = fragment_source + "}";
    fragment_source = fragment_source + "}";
    fragment_source = fragment_source + "if (texture2D(uSampler, tc).r < 0.3){";
    fragment_source = fragment_source + "gl_FragColor = sum*sum*0.012 + texture2D(uSampler, tc);";
    fragment_source = fragment_source + "}else{";

    fragment_source = fragment_source + "if (texture2D(uSampler, tc).r < 0.5){";

    fragment_source = fragment_source + "gl_FragColor = sum*sum*0.009 + texture2D(uSampler, tc);";
    fragment_source = fragment_source + "}else{";
    fragment_source = fragment_source + "gl_FragColor = sum*sum*0.0075 + texture2D(uSampler, tc);";
    fragment_source = fragment_source + "}}}";



    var uniforms = OMEGA.Omega3D.Shaders.Components.StandardUniforms();

    var fragmentOnlyUniforms = {};
    fragmentOnlyUniforms["uSampler"] = { id:"uSampler", type: "sampler2D", glsl: "", value: null };
    return new OMEGA.Omega3D.Shader(vertex_source,fragment_source,  [OMEGA.Omega3D.Shaders.Components.StandardAttributes(),
        uniforms,
        {},
        fragmentOnlyUniforms]);
};
















//Deprecated
//TODO: static lights
OMEGA.Omega3D.Shaders.Diffuse = function( isTextured ){
    var texture, fragmentColor;
    isTextured = isTextured || false;
    if(isTextured) texture = "varying vec2 vTexCoord;";
    else texture = "";
    var vertex_source = OMEGA.Omega3D.Shaders.Components.Basic_Includes();
    if(OMEGA.Omega3D.LIGHTS.length > 0 ) vertex_source = vertex_source + OMEGA.Omega3D.Shaders.Components.Basic_Light_Varying();
    vertex_source = vertex_source + texture;
    vertex_source = vertex_source + "void main(void){";
    vertex_source = vertex_source + OMEGA.Omega3D.Shaders.Components.Vertex_World_Conversion_V3("gl_Position", "aVertexPos");
    if(isTextured                      ) vertex_source = vertex_source + OMEGA.Omega3D.Shaders.Components.Basic_TextureCoords("vTexCoord");
    if(OMEGA.Omega3D.LIGHTS.length > 0 ) vertex_source = vertex_source + OMEGA.Omega3D.Shaders.Components.Basic_Diffuse_Vertex_Logic();
    vertex_source = vertex_source + "}";


    if(isTextured){
        texture = "uniform sampler2D uSampler;"+
            "varying vec2 vTexCoord;";
        fragmentColor = "texture2D(uSampler, vec2(vTexCoord.s, vTexCoord.t));";
    }
    else{
        fragmentColor = "vec4(0.8, 0.4, 0.0, 1.0);";
    }
    var fragment_source = OMEGA.Omega3D.Shaders.Components.Fragment_Precision_Mediump_Float();
    if(OMEGA.Omega3D.LIGHTS.length > 0 ) fragment_source = fragment_source + OMEGA.Omega3D.Shaders.Components.Basic_Light_Includes() + OMEGA.Omega3D.Shaders.Components.Basic_Light_Varying();
    if( isTextured                     ){
        fragment_source = fragment_source + OMEGA.Omega3D.Shaders.Components.Basic_Fragment_Includes();
        fragment_source = fragment_source + texture;
    }
    fragment_source = fragment_source + "void main(void){";
    if(OMEGA.Omega3D.LIGHTS.length > 0 ) fragment_source = fragment_source + OMEGA.Omega3D.Shaders.Components.Basic_Diffuse_Fragment_Logic(fragmentColor);
    else                                 fragment_source = fragment_source + OMEGA.Omega3D.Shaders.Components.Basic_Fragment_Logic(fragmentColor);
    fragment_source = fragment_source + "}";

    var fragmentOnlyUniforms = {};
    if(isTextured) fragmentOnlyUniforms["uSampler"] = { id:"uSampler", type: "sampler2D", glsl: "", value: null };
    return new OMEGA.Omega3D.Shader(vertex_source,fragment_source,  [OMEGA.Omega3D.Shaders.Components.StandardAttributes(),
                                                                     OMEGA.Omega3D.Shaders.Components.StandardUniforms(),
                                                                     OMEGA.Omega3D.Shaders.Components.StandardLightUniforms(),
                                                                     fragmentOnlyUniforms]);
};
//Deprecated
//TODO: static lights

OMEGA.Omega3D.Shaders.Phong = function( isTextured ){
    var texture, fragmentColor;
    isTextured = isTextured || false;
    if(isTextured) texture = "varying vec2 vTexCoord;";
    else texture = "";
    var vertex_source = OMEGA.Omega3D.Shaders.Components.Basic_Includes();
    if(OMEGA.Omega3D.LIGHTS.length > 0 ) vertex_source = vertex_source + OMEGA.Omega3D.Shaders.Components.Basic_Light_Varying();
    vertex_source = vertex_source + texture;
    vertex_source = vertex_source + "void main(void){";
    vertex_source = vertex_source + OMEGA.Omega3D.Shaders.Components.Vertex_World_Conversion_V3("gl_Position", "aVertexPos");
    if(isTextured                      ) vertex_source = vertex_source + OMEGA.Omega3D.Shaders.Components.Basic_TextureCoords("vTexCoord");
    if(OMEGA.Omega3D.LIGHTS.length > 0 ) vertex_source = vertex_source + OMEGA.Omega3D.Shaders.Components.Basic_Diffuse_Vertex_Logic();
    vertex_source = vertex_source + "}";


    if(isTextured){
        texture = "uniform sampler2D uSampler;"+
            "varying vec2 vTexCoord;";
        fragmentColor = "texture2D(uSampler, vec2(vTexCoord.s, vTexCoord.t));";
    }
    else{
        fragmentColor = "vec4(0.8, 0.4, 0.0, 1.0);";
    }
    var fragment_source = OMEGA.Omega3D.Shaders.Components.Fragment_Precision_Mediump_Float();
    if(OMEGA.Omega3D.LIGHTS.length > 0 ) fragment_source = fragment_source + OMEGA.Omega3D.Shaders.Components.Basic_Light_Includes() + OMEGA.Omega3D.Shaders.Components.Basic_Light_Varying();
    if( isTextured                     ){
        fragment_source = fragment_source + OMEGA.Omega3D.Shaders.Components.Basic_Fragment_Includes();
        fragment_source = fragment_source + texture;
    }
    fragment_source = fragment_source + "void main(void){";
    if(OMEGA.Omega3D.LIGHTS.length > 0 ) fragment_source = fragment_source + OMEGA.Omega3D.Shaders.Components.Basic_Phong_Fragment_Logic(fragmentColor);
    else                                 fragment_source = fragment_source + OMEGA.Omega3D.Shaders.Components.Basic_Fragment_Logic(fragmentColor);
    fragment_source = fragment_source + "}";


    var fragmentOnlyUniforms = {};
    if(isTextured) fragmentOnlyUniforms["uSampler"] = { id:"uSampler", type: "sampler2D", glsl: "", value: null };
    return new OMEGA.Omega3D.Shader(vertex_source,fragment_source,  [OMEGA.Omega3D.Shaders.Components.StandardAttributes(),
                                                                     OMEGA.Omega3D.Shaders.Components.StandardUniforms(),
                                                                     OMEGA.Omega3D.Shaders.Components.StandardLightUniforms(),
                                                                     fragmentOnlyUniforms]);
};








OMEGA.Omega3D.Shaders.Morph = function( ){
    var vertex_source =

        OMEGA.Omega3D.Shaders.Components.Basic_Includes_Post_Processing() +
        OMEGA.Omega3D.Shaders.Components.Noise3D();
        if(OMEGA.Omega3D.LIGHTS.length > 0 ) vertex_source = vertex_source + OMEGA.Omega3D.Shaders.Components.Basic_Light_Varying();
        vertex_source = vertex_source +
        "varying float noise;" +
        "float turbulence( vec3 p ) {" +
            "float t = -0.5;" +
            "for (float f = 1.0 ; f <= 10.0 ; f++){" +
                "float power = pow( 2.0, f );" +
                "t += abs( pnoise( vec3( power * p ), vec3( 10.0, 10.0, 10.0 ) ) / power );" +
            "}" +
            " return t;" +
        "}" +




        "varying float vTime;" +
        "varying vec2 vTexCoord;" +
        "varying vec3 vVertexNormal;" +
        "void main(void){"+
            "vec4 pos = uProjectionMatrix * uViewMatrix * uModelMatrix * vec4(aVertexPos.xy, 0.0, 1.0);" +
            "noise = 10.0 *  -.10 * turbulence(.5*aVertexNormal + uTime * 0.025);"+ //
            "float b = 5.0 * pnoise(0.05 * aVertexPos, vec3(100.0) );" +
            "float displacement = - 1.5 * noise + b;" +
            "vec3 newPos = aVertexPos + aVertexNormal * displacement;" +
            "vVertexNormal = abs(aVertexNormal * displacement);"+

            "gl_Position = uProjectionMatrix * uViewMatrix * uModelMatrix *vec4(newPos, 1.0);" +
            "vTexCoord = aTextureCoord;" +
            "vTime = uTime;";
           if(OMEGA.Omega3D.LIGHTS.length > 0 ) vertex_source = vertex_source + OMEGA.Omega3D.Shaders.Components.Basic_Diffuse_Vertex_Logic();
        vertex_source = vertex_source + "}";


    var fragment_source =
        OMEGA.Omega3D.Shaders.Components.Fragment_Precision_Mediump_Float() +
        OMEGA.Omega3D.Shaders.Components.Basic_Fragment_Includes();
        if(OMEGA.Omega3D.LIGHTS.length > 0 ) fragment_source = fragment_source + OMEGA.Omega3D.Shaders.Components.Basic_Light_Includes() + OMEGA.Omega3D.Shaders.Components.Basic_Light_Varying();
        fragment_source = fragment_source +
        "uniform sampler2D uSampler;"+
        "varying float vTime;" +
        "varying vec2 vTexCoord;"+
        "varying float noise;" +
        "void main(void){" +
        "float n = noise;" +
        "if(noise < 0.05)n = 0.05;" +
        "if(noise > 0.4)n = 0.4;" +
        "float ypos = 1.0-1.3*n;" +
        "if(ypos < 0.0) ypos = 0.0;" +
        "vec2 tPos = vec2(0, ypos*1.7);";
            var fragmentColor = "texture2D(uSampler, tPos);";
             if(OMEGA.Omega3D.LIGHTS.length > 0 ) fragment_source = fragment_source + OMEGA.Omega3D.Shaders.Components.Basic_Phong_Fragment_Logic(fragmentColor);
             else                                 fragment_source = fragment_source + OMEGA.Omega3D.Shaders.Components.Basic_Fragment_Logic(fragmentColor);
        fragment_source = fragment_source + "}";
    var fragmentOnlyUniforms = {};
    fragmentOnlyUniforms["uSampler"] = { id:"uSampler", type: "sampler2D", glsl: "", value: null };
    return new OMEGA.Omega3D.Shader(vertex_source,fragment_source,  [OMEGA.Omega3D.Shaders.Components.StandardAttributes(),
        OMEGA.Omega3D.Shaders.Components.StandardUniforms(),
        {},
        fragmentOnlyUniforms]);
};




/**
 *  Post Processing Kernel Shader.
 *
 * @returns {OMEGA.Omega3D.Shader}
 * @constructor
 */
OMEGA.Omega3D.Shaders.PostProcessingKernel = function( kernel ){
    var vertex_source =
        OMEGA.Omega3D.Shaders.Components.Basic_Includes_Post_Processing() +
        "varying vec2 vTexCoord;" +
        "void main(void){"+
            "vec4 pos = uProjectionMatrix * uViewMatrix * uModelMatrix * vec4(aVertexPos.xy, 0.0, 1.0);" +
            "gl_Position = pos;" +
             "vTexCoord = aTextureCoord;" +
        "}";


    var fragment_source =
        OMEGA.Omega3D.Shaders.Components.Fragment_Precision_Mediump_Float()+
        OMEGA.Omega3D.Shaders.Components.Basic_Fragment_Includes() +
        "uniform float uKernel[9];" +
        "uniform sampler2D uSampler;"+
        "varying vec2 vTexCoord;"+
        "void main(void){"+
        "vec4 color = texture2D(uSampler, vec2(vTexCoord.s, vTexCoord.t));" +
        "vec2 onePixel = vec2(1.0, 1.0) / vec2(512, 512);" +
        "vec4 colorSum =" +
        "texture2D(uSampler, vTexCoord + onePixel * vec2(-1, -1)) * uKernel[1] + " +
        "texture2D(uSampler, vTexCoord + onePixel * vec2( 0, -1)) * uKernel[1] + " +
        "texture2D(uSampler, vTexCoord + onePixel * vec2( 1, -1)) * uKernel[2] + " +
        "texture2D(uSampler, vTexCoord + onePixel * vec2(-1,  0)) * uKernel[3] + " +
        "texture2D(uSampler, vTexCoord + onePixel * vec2( 0,  0)) * uKernel[4] + " +
        "texture2D(uSampler, vTexCoord + onePixel * vec2( 1,  0)) * uKernel[5] + " +
        "texture2D(uSampler, vTexCoord + onePixel * vec2(-1,  1)) * uKernel[6] + " +
        "texture2D(uSampler, vTexCoord + onePixel * vec2( 0,  1)) * uKernel[7] + " +
        "texture2D(uSampler, vTexCoord + onePixel * vec2( 1,  1)) * uKernel[8];" ;



        var str = "float kernelWeight = uKernel[0] + uKernel[1] + uKernel[2] + uKernel[3] + uKernel[4] + uKernel[5] + uKernel[6] +uKernel[7] +uKernel[8];" +
                  "if(kernelWeight <= 0.0){"+
                    "kernelWeight = 1.0;"+
                  "}";
        fragment_source = fragment_source + str;


        fragment_source = fragment_source  +
        "gl_FragColor = vec4((colorSum / kernelWeight).rgb, 1);" +
        "}";
    return new OMEGA.Omega3D.PostProcessingKernelShader(vertex_source,fragment_source, kernel);
};
OMEGA.Omega3D.Shaders.PostProcessingKernel.Kernels = {
    normal: [
        0, 0, 0,
        0, 1, 0,
        0, 0, 0
    ],
    gaussianBlur: [
        0.045, 0.122, 0.045,
        0.122, 0.332, 0.122,
        0.045, 0.122, 0.045
    ],
    gaussianBlur2: [
        1, 2, 1,
        2, 4, 2,
        1, 2, 1
    ],
    gaussianBlur3: [
        0, 1, 0,
        1, 1, 1,
        0, 1, 0
    ],
    gaussianBlur4: [
        0, 16, 0,
        16, 0, 16,
        0, 16, 0
    ],
    unsharpen: [
        -1, -1, -1,
        -1,  9, -1,
        -1, -1, -1
    ],
    sharpness: [
        0,-1, 0,
        -1, 5,-1,
        0,-1, 0
    ],
    sharpen: [
        0, -1, 0,
        -1, 16, -1,
        0, -1, 0
    ],
    edgeDetect: [
        -0.125, -0.125, -0.125,
        -0.125,  1,     -0.125,
        -0.125, -0.125, -0.125
    ],
    edgeDetect2: [
        -1, -1, -1,
        -1,  8, -1,
        -1, -1, -1
    ],
    edgeDetect3: [
        -5, 0, 0,
        0, 0, 0,
        0, 0, 5
    ],
    edgeDetect4: [
        -1, -1, -1,
        0,  0,  0,
        1,  1,  1
    ],
    edgeDetect5: [
        -1, -1, -1,
        2,  2,  2,
        -1, -1, -1
    ],
    edgeDetect6: [
        -5, -5, -5,
        -5, 39, -5,
        -5, -5, -5
    ],
    sobelHorizontal: [
        1,  2,  1,
        0,  0,  0,
        -1, -2, -1
    ],
    sobelVertical: [
        1,  0, -1,
        2,  0, -2,
        1,  0, -1
    ],
    previtHorizontal: [
        1,  1,  1,
        0,  0,  0,
        -1, -1, -1
    ],
    previtVertical: [
        1,  0, -1,
        1,  0, -1,
        1,  0, -1
    ],
    boxBlur: [
        0.111, 0.111, 0.111,
        0.111, 0.111, 0.111,
        0.111, 0.111, 0.111
    ],
    robbieBlur: [
        0, 0, 0,
        0, 12, 0,
        0, 0, 0
    ],
    triangleBlur: [
        0.0625, 0.125, 0.0625,
        0.125,  0.25,  0.125,
        0.0625, 0.125, 0.0625
    ],
    emboss: [
        -2, -1,  0,
        -1,  1,  1,
         0,  1,  2
    ]
};

