function Atlas( image_rect ){
    this.image_rect = image_rect;
    this.segments = new Array();
    var currentFrame = null;
    var frameCounter = 0;
    this.AddSegment = function( id, rect ){
        this.segments.push( { rect: rect, uvRect:new Rect(1/ (this.image_rect.w / rect.x),
                                                          1 / (this.image_rect.h / rect.y),
                                                          rect.w / this.image_rect.w ,
                                                          rect.h / this.image_rect.h)});
        this.Update();
    }

    var c = 0;
    this.Update = function(scene){
       if(scene == null) return;
        //c++;
        //if(c==2) c = 0;
        //else return;



        c = (scene.getTime()*30)%(this.segments.length);

        currentFrame = this.segments[Math.floor(c)];
        if(c > this.segments.length-1) c = 0;
    }

    this.GetCurrentFrame = function(){
        return currentFrame;
    }
}
OMEGA.Omega3D.Atlas = Atlas;