function VSMMaterial( textures, scene,  depthCam ){
    Material.apply(this, [new OMEGA.Omega3D.Shaders.VSM(scene), textures ]);
    this.custom_uniforms["uLightM"] = { type:"mat4", glsl: "", value: depthCam.GetMatrix() };
}
VSMMaterial.prototype = new Material();
OMEGA.Omega3D.VSMMaterial = VSMMaterial;