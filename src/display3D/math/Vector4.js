/**
 *  Vector4
 *
 * @param x
 * @param y
 * @param z
 * @return {{x: *, y: *, z: *}}
 * @constructor
 */

function Vector4( x, y, z, w){
    this.x = x;
    this.y = y;
    this.z = z;
    this.w = w || 1.0;

    __construct = function(){
    }();

    this.add = function( v ){
        this.x += v.x;
        this.y += v.y;
        this.z += v.z;
        return this;
    };
    this.addScalar = function( v ){
        this.x += v;
        this.y += v;
        this.z += v;
    };
    this.subtract = function( v ){
        this.x -= v.x;
        this.y -= v.y;
        this.z -= v.z;
        return this;
    };
    this.multiply = function( v ){
        this.x *= v.x;
        this.y *= v.y;
        this.z *= v.z;
        return this;
    }
    this.multiplyScalar = function( v ){
        this.x *= v;
        this.y *= v;
        this.z *= v;
        return this;
    };

    this.length = function(){
        return ( this.x*this.x+this.y*this.y+this.z*this.z);
    };
    this.lengthSQRT = function(){
        return Math.sqrt( this.length() );
    };
    this.zero = function() {
        this.x = 0;
        this.y = 0;
        this.z = 0;
        return this;
    };
};
Vector4.Add = function( a, b ){
  return new Vector4(a.x+ b.x, a.y+ b.y, a.z+ b.z, 1.0);
};
Vector4.Subtract = function( a, b){
    return new Vector4(a.x- b.x, a.y- b.y, a.z- b.z, 1.0);
};
Vector4.Multiply = function( a, b){
    return new Vector4(a.x* b.x, a.y* b.y, a.z* b.z, 1.0);
};
Vector4.MultiplyByValue = function( a, b){
    return new Vector4(a.x* b, a.y* b, a.z* b);
};
Vector4.Equals = function( a, b ){
    return (a.x==b.x&&a.y==b.y&&a.z== b.z&&a.w== b.w);
};
Vector4.ToArray = function( v ){
    return [v.x, v.y, v.z, v.w];
};
Vector4.ToArray3 = function( v ){
    return [v.x, v.y, v.z];
};
Vector4.Cross = function( a, b ){
  return new Vector4(a.y* b.z - a.z* b.y,
                     a.z* b.x - a.x* b.z,
                     a.x* b.y - a.y* b.x,
                     1.0);
};
Vector4.Dot = function( a, b ){
  return a.x* b.x + a.y* b.y + a.z* b.z + a.w * b.w;
};
Vector4.Normalize = function( a ){
  var l = a.lengthSQRT();
  return new Vector4(a.x/l, a.y/l, a.z/l, a.w/l );
};
Vector4.Lerp = function( a, b, i){
  return new Vector4( ( 1.0 - i ) * a.x + i * b.x,
                      ( 1.0 - i ) * a.y + i * b.y,
                      ( 1.0 - i ) * a.z + i * b.z,
                      ( 1.0 - i ) * a.w + i * b.w );
};
Vector4.Length = function( a,b ){
    var d = new Vector4(b.x - a.x, b.y - a.y, b.z - a.z, 1.0);
    return Math.sqrt( d.x*d.x+d.y*d.y+d.z*d.z);
};
