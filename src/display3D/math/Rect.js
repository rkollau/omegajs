function Rect( x, y, w, h){
    this.x = x;
    this.y = y;
    this.w = w;
    this.h = h;
    this.lt = new Vector2( x, y);
    this.lb = new Vector2( x, y+h);
    this.rt = new Vector2( x+w, y);
    this.rb = new Vector2( x+w, y+h);
}