/**
 *
 *  MATRIX3D
 *
 * @constructor
 */
function Matrix4(){
    var raw = null;
    var m0  = 0; var m1  = 0; var m2  = 0; var m3  = 0;
    var m4  = 0; var m5  = 0; var m6  = 0; var m7  = 0;
    var m8  = 0; var m9  = 0; var m10 = 0; var m11 = 0;
    var m12 = 0; var m13 = 0; var m14 = 0; var m15 = 0;




    __construct = function(){
        console.log("matrix made! " );
        identity();
        update();
    }()

    function identity(){
        m0  = 1; m1  = 0; m2  = 0; m3  = 0;
        m4  = 0; m5  = 1; m6  = 0; m7  = 0;
        m8  = 0; m9  = 0; m10 = 1; m11 = 0;
        m12 = 0; m13 = 0; m14 = 0; m15 = 1;
        update();
    }

    function scale(x, y, z ){
        m0  *= x;
        m5  *= y;
        m10 *= z;
        update();
    }

    function translate( x, y, z ){
        m12 += x;
        m13 += y;
        m14 += z;
        update();
    }


    function getScale(){
        return  new Vector2( m0, m5, m10);
    }
    function getPosition(){
        return new Vector2( m12, m13, m14);
    }

    function rotate( axis, radians ){
        //axis   : Unit Vector2
        //radians: float
        identity();
        this.c = Math.cos( radians );
        this.s = Math.sin( radians );
        this.t = 1 - Math.cos( radians );
        this.x2 = Math.pow( axis.x, 2 );
        this.y2 = Math.pow( axis.y, 2);
        this.z2 = Math.pow( axis.z, 2);

        m0 = t*x2 + c;                         m1 = t * axis.x * axis.y - s * axis.z;  m2  = t * axis.x * axis.z + s * axis.y;  m3  = 0;
        m4 = t * axis.x * axis.y + s * axis.z; m5 = t * y2 + c;                        m6  = t * axis.y * axis.z - s * axis.x;  m7  = 0;
        m8 = t * axis.x * axis.z - s * axis.y; m9 = t * axis.y * axis.z + s * axis.x;  m10 = t * z2 + c;                        m11 = 0;
        m12 = 0;                               m13 = 0;                                m14 = 0;                                  m15 = 1;
        update();
    }

    function projection( fov, ratio, zNear, zFar ){
        identity();
        m0  = 1 / Math.tan( fov/2);
        m5  = m0 / ratio;
        m10 = -(zFar + zNear/zFar-zNear);
        m11 = -(2* zFar * zNear/zFar - zNear);
        m14 = -1;
        m15 = 0;
        update();
    }


    function update(){
        raw = [
            m0,  m1,   m2,  m3,
            m4,  m5,   m6,  m7,
            m8,  m9,  m10, m11,
            m12, m13, m14, m15
        ]
    }

    return {
        GetRaw:raw,
        Identity:function(){
            identity();
        },
        Scale:function(x,y,z){
            scale(x, y, z);
        },
        Translate:function(x, y, z){
            translate( x, y, z );
        },
        Rotate:function( axis, radians){
            rotate( axis, radians );
        },
        Projection:projection,
        GetScale:getScale,
        GetPosition:getPosition
    }
}


Matrix4.Create = function() {
    var mat = new GLMAT_ARRAY_TYPE(16);
    mat[0]  = 1; mat[1]  = 0; mat[2]  = 0; mat[3]  = 0;
    mat[4]  = 0; mat[5]  = 1; mat[6]  = 0; mat[7]  = 0;
    mat[8]  = 0; mat[9]  = 0; mat[10] = 1; mat[11] = 0;
    mat[12] = 0; mat[13] = 0; mat[14] = 0; mat[15] = 1;
    return mat;
};
Matrix4.Identity = function( out )
{
    out[0]  = 1; out[1]  = 0; out[2]  = 0; out[3]  = 0;
    out[4]  = 0; out[5]  = 1; out[6]  = 0; out[7]  = 0;
    out[8]  = 0; out[9]  = 0; out[10] = 1; out[11] = 0;
    out[12] = 0; out[13] = 0; out[14] = 0; out[15] = 1;
    return out;
};
Matrix4.Multiply = function( out, a, b )
{
    var a_row_0 = new Vector4( a[0], a[1], a[2], a[3] );
    var a_row_1 = new Vector4( a[4], a[5], a[6], a[7] );
    var a_row_2 = new Vector4( a[8], a[9], a[10], a[11] );
    var a_row_3 = new Vector4( a[12], a[13], a[14], a[15] );

    var b_col_0 = new Vector4( b[0], b[4], b[8], b[12] );
    var b_col_1 = new Vector4( b[1], b[5], b[9], b[13] );
    var b_col_2 = new Vector4( b[2], b[6], b[10], b[14] );
    var b_col_3 = new Vector4( b[3], b[7], b[11], b[15] );

    out[0]  = Vector4.Dot(a_row_0, b_col_0 ); out[1]  = Vector4.Dot(a_row_0, b_col_1 ); out[2]  = Vector4.Dot(a_row_0, b_col_2 ); out[3]  = Vector4.Dot(a_row_0, b_col_3 );
    out[4]  = Vector4.Dot(a_row_1, b_col_0 ); out[5]  = Vector4.Dot(a_row_1, b_col_1 ); out[6]  = Vector4.Dot(a_row_1, b_col_2 ); out[7]  = Vector4.Dot(a_row_1, b_col_3 );
    out[8]  = Vector4.Dot(a_row_2, b_col_0 ); out[9]  = Vector4.Dot(a_row_2, b_col_1 ); out[10] = Vector4.Dot(a_row_2, b_col_2 ); out[11] = Vector4.Dot(a_row_2, b_col_3 );
    out[12] = Vector4.Dot(a_row_3, b_col_0 ); out[13] = Vector4.Dot(a_row_3, b_col_1 ); out[14] = Vector4.Dot(a_row_3, b_col_2 ); out[14] = Vector4.Dot(a_row_3, b_col_3 );

    delete a_row_0;delete a_row_1;delete a_row_2;delete a_row_3;
    delete b_col_0;delete b_col_1;delete b_col_2;delete b_col_3;

    return out;
};