var OMEGA = OMEGA || {};
OMEGA.HUD = function(){
    this.elementCount = 0;
    this.body   = document.getElementsByTagName("body")[0];
    
    //configure.
    this.configureElement = function( element , config ){
        config.position = config.position || "absolute";
        config.zIndex   = config.zIndex || "-1";  
        for( var key in config ){
            element.style[key] = config[key];
        }
        element.id = "OmegaHudElement_"+ this.elementCount.toString();
    };
    
    console.log( ".: OmegaHUD v" + OMEGA.HUD.VERSION + " :.");
};

// TEXT
OMEGA.HUD.prototype.CreateText = function( text, config ){
    this.elementCount++;
    var element     = document.createElement("div");
    var textElement = document.createTextNode( text );
   
    element.appendChild( textElement );
    this.configureElement( element, config );
    textElement.id = "textElement";
    this.body.appendChild( element );
    
    // --> getters / setters
    element.GetText = function(){
        return element.childNodes[0].nodeValue;
    }
    element.SetText = function( text ){
        return element.childNodes[0].nodeValue = text;
    }
    return element;
};

// IMAGE
OMEGA.HUD.prototype.CreateImage = function( url, config ){
    this.elementCount++;
    var element = document.createElement("img");
    element.src = url;
    this.body.appendChild( element );
    this.configureElement( element, config );
    return element;
};

// BUTTON
OMEGA.HUD.prototype.CreateButton = function( UP_image, DOWN_image, onClick, config  ){
    this.elementCount++;
    if(UP_image.parentNode   == this.body ) this.body.removeChild( UP_image );
    if(DOWN_image.parentNode == this.body ) this.body.removeChild( DOWN_image );
  
    var element = document.createElement("div");
    element.appendChild( UP_image );
    element.appendChild( DOWN_image );
    DOWN_image.style.display = "none";

    //lock vars into element.
    element.UP   = UP_image;
    element.DOWN = DOWN_image;

    //mouse interaction.
    element.onmousedown = function(){ element.DOWN.style.display = "inline"; element.UP.style.display   = "none"; };
    element.onmouseup   = function(){ element.UP.style.display   = "inline"; element.DOWN.style.display = "none"; };
    element.onclick     = function(){ onClick(); };
    this.body.appendChild( element );

    element.style.cursor = "pointer";
    element.style.cursor = "hand";

    this.configureElement( element, config );
    
    // -> getters / setters
    element.GetUpImage = function(){
        return element.UP;
    };
    element.GetDownImage = function(){
        return element.DOWN;
    };
    element.SetUpImage = function( img ){
        if(img.parentNode        == this.body ) this.body.removeChild( img );
        if(element.UP.parentNode == element ) element.removeChild( element.UP );
        img.style   = element.UP.style;
        img.style.display = element.UP.style.display;
        element.UP  = img;
        element.appendChild( img );
    };
    element.SetDownImage = function( img ){
        if(img.parentNode          == this.body ) this.body.removeChild( img );
        if(element.DOWN.parentNode == element ) element.removeChild( element.DOWN );
        img.style         = element.DOWN.style;
        img.style.display = element.DOWN.style.display;
        element.DOWN = img;
        element.appendChild( img );
    };
   
    return element;
};

OMEGA.HUD.prototype.CreateLoader = function( config ){
    this.elementCount++;
    var element = document.createElement("div");
    this.configureElement( element, config );
    this.body.appendChild( element );
    
    var animation = function(){  
        element.angle += 1.0;
        element.style.webkitTransform = 'rotate('+element.angle+'deg)'; 
        element.style.mozTransform    = 'rotate('+element.angle+'deg)'; 
        element.style.msTransform     = 'rotate('+element.angle+'deg)'; 
        element.style.oTransform      = 'rotate('+element.angle+'deg)'; 
        element.style.transform       = 'rotate('+element.angle+'deg)';  
    };
    element.interval = setInterval( animation, 10 );
    element.angle    = 0;
    
    var items = document.getElementsByTagName("img");
    element.loadSize = items.length;
    
    // -> load complete.
    element.loadComplete = function(){
        element.loadSize--;
        if(element.loadSize == 0 && element.parentNode != null){
            element.parentNode.removeChild( element );
            clearInterval( element.interval );
        }
    };

    for( var index = 0; index < items.length; index++){
        items[index].onload = element.loadComplete;
    };
    return element;
}



OMEGA.HUD.VERSION = "0.0.1 Alpha";

