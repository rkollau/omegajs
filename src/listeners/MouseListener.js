function MouseListener(){
    this.active = true;
    this.mouse  = new Vector2(0, 0);
    this.delta_mouse  = new Vector2(0, 0);
    this.targetSize = new Vector2(0, 0);
    this.HandleMouseMove = function(e){
        if(!this.active)return;
        //console.log(event.keyCode);
        this.delta_mouse.x = Math.abs( e.clientX - this.mouse.x);
        this.delta_mouse.y = Math.abs( e.clientY - this.mouse.y);

        this.mouse.x = e.clientX;
        this.mouse.y = e.clientY;
        this.targetSize.x = e.target.width;
        this.targetSize.y = e.target.height;
        //console.log("mouse location:", this.mouse,  this.targetSize);
    };

    this.Enable = function(){
        if(this.active)return;
        this.mouse      = new Vector2(0, 0);
        this.targetSize = new Vector2(0, 0);
        this.active = true;
    };
    this.Disable = function(){
        if(!this.active)return;
        this.mouse      = new Vector2(0, 0);
        this.targetSize = new Vector2(0, 0);
        this.active = false;
    };
}
OMEGA.listeners = OMEGA.listeners || {};
OMEGA.listeners.MouseListener = MouseListener;