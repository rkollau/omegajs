var OMEGA = OMEGA || {};
OMEGA.Timeline = function()
{
    var currentTime = 0;
    var previousTime = 0;
    var clips = new Array();


    this.AddClip = function( clip )
    {
        clips.push( clip );
    };

    this.Start = function()
    {
        currentTime  = Date.now();
        previousTime = currentTime;

      //  this.Update();
    };


    this.Update = function( time )
    {
        currentTime=time;
        var subject, startTime, endTime;
        for( var i = 0, N = clips.length; i < N; i++)
        {
            subject = clips[i];
            startTime = subject.startTime;
            endTime   = subject.endTime > 0 ? subject.endTime : currentTime + 1.0 ;

            if( startTime <= currentTime &&
                endTime > currentTime &&
                subject.state == subject.STATE_IDLE)
            {
                subject.state = subject.STATE_AWAKE;
            }
            subject.Update( time );
        }
    }
};


OMEGA.TimelineClip = function( startTime, endTime, target ) {

    this.Awake = function ()
    {
        if(target.Awake != undefined)
            target.Awake();

        //called when event will awake.
        this.state = this.STATE_START;
    };

    this.Start = function ()
    {
        if(target.Start != undefined)
            target.Start();

        //called when event starts.
        this.state = this.STATE_UPDATE;
    };

    this.Update = function ( currentTime )
    {
        //if state idle, we do nothing.
        if(this.state == this.STATE_IDLE)
            return;



        //check if we hit the end time.
        if( this.endTime > 0 ){
            if( currentTime >= this.endTime && this.state ==  this.STATE_UPDATE )
                this.state = this.STATE_END;
        }


        //call state methods.
        if( this.state == this.STATE_AWAKE       ) this.Awake();
        else if( this.state == this.STATE_START  ) this.Start();
        else if( this.state == this.STATE_END    ) this.End();
        else if( this.state == this.STATE_SLEEP  ) this.Sleep();


        if(target.Update != undefined && this.state == this.STATE_UPDATE)
            target.Update( currentTime );
    };

    this.End = function ()
    {
        if(target.End != undefined)
            target.End();

        //called when event ends.
        this.state = this.STATE_SLEEP;
    };

    this.Sleep = function()
    {
        if(target.Sleep != undefined)
            target.Sleep();

        //called when event will sleep.
        this.state = this.STATE_IDLE;
    }


    //states.
    this.STATE_AWAKE  = "TimelineEvent.State.AWAKE";
    this.STATE_START  = "TimelineEvent.State.START";
    this.STATE_UPDATE = "TimelineEvent.State.UPDATE";
    this.STATE_END    = "TimelineEvent.State.END";
    this.STATE_SLEEP  = "TimelineEvent.State.SLEEP";
    this.STATE_IDLE   = "TimelineEvent.State.IDLE";
    this.state        = this.STATE_IDLE;

    //times.
    this.startTime = startTime;
    this.endTime   = endTime;
};


