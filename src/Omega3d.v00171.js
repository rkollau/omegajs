/* Copyright (c) 2013, Brandon Jones, Colin MacKenzie IV. All rights reserved.

 Redistribution and use in source and binary forms, with or without modification,
 are permitted provided that the following conditions are met:

 * Redistributions of source code must retain the above copyright notice, this
 list of conditions and the following disclaimer.
 * Redistributions in binary form must reproduce the above copyright notice,
 this list of conditions and the following disclaimer in the documentation
 and/or other materials provided with the distribution.

 THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
 ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR
 ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON
 ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE. */

if(!GLMAT_EPSILON) {
    var GLMAT_EPSILON = 0.000001;
}

if(!GLMAT_ARRAY_TYPE) {
    var GLMAT_ARRAY_TYPE = (typeof Float32Array !== 'undefined') ? Float32Array : Array;
}

if(!GLMAT_RANDOM) {
    var GLMAT_RANDOM = Math.random;
}

/**
 * @class Common utilities
 * @name glMatrix
 */
var glMatrix = {};

/**
 * Sets the type of array used when creating new vectors and matrices
 *
 * @param {Type} type Array type, such as Float32Array or Array
 */
glMatrix.setMatrixArrayType = function(type) {
    GLMAT_ARRAY_TYPE = type;
}

if(typeof(exports) !== 'undefined') {
    exports.glMatrix = glMatrix;
}

var degree = Math.PI / 180;

/**
 * Convert Degree To Radian
 *
 * @param {Number} Angle in Degrees
 */
glMatrix.toRadian = function(a){
    return a * degree;
}
var OMEGA = OMEGA || {};
OMEGA.Utils = function(){
    this.instance = null;

};
OMEGA.Utils.GetInstance = function(){
    if(this.instance == null){
        console.log("created.");
        this.instance = new OMEGA.Utils();
    }
    return this.instance;
};
OMEGA.Utils.prototype.LoadScript = function( url, callback ){
    var head    = document.getElementsByTagName('head')[0];
    var script  = document.createElement('script');
    script.type = 'text/javascript';
    script.src  = url;
    script.onreadystatechange = callback;
    script.onload = callback;
    head.appendChild(script);
};
OMEGA.Utils.prototype.LoadScripts = function( urls, callback ){
    for( var i =0; i<urls.length;i++){
        this.LoadScript( urls[i], i<urls.length-1?null:callback);
    }
};
OMEGA.Utils.prototype.LoadImages = function( urls, callback ){
    var images = [];
    var loads  = urls.length;
    var onLoaded = function(){
        if(--loads == 0) callback(images);
    }
    for( var i = 0; i < loads; ++i){
        var image = this.LoadImage( urls[i], onLoaded );
        images.push( image );
    }
};
OMEGA.Utils.prototype.LoadImage = function( url, callback ){
    var image = new Image();
    image.src = url;
    image.onload = callback;
    return image;
};
OMEGA.Utils.prototype.LoadFile = function( url, callback ){
    var xmlhttp = new XMLHttpRequest() || new ActiveXObject('MSXML2.XMLHTTP');
    xmlhttp.open ("GET", url, false);
    xmlhttp.onreadystatechange = callback;
    xmlhttp.send (null);
}




OMEGA.Utils.Counter = function(){
    this.startTime = new Date().getTime();
    this.time = new Date().getTime();
    this.frameTime = 0;  this.framesNumber = 0;
    this.FPS = 0;
};
OMEGA.Utils.Counter.prototype.update = function(){
    this.framesNumber++;
    this.frameTime = (new Date().getTime() -  this.time) / 1000;
    if( this.frameTime > 1){
        this.FPS = Math.round( this.framesNumber/ this.frameTime);
        this.time = new Date().getTime();
        this.framesNumber = 0;
    }
};
OMEGA.Utils.Counter.prototype.getFPS = function(){
    return this.FPS;
};
OMEGA.Utils.Counter.prototype.getFrame = function(){
    return this.framesNumber;
};
OMEGA.Utils.Counter.prototype.getFrameTime = function(){
    return this.frameTime;
};
OMEGA.Utils.Counter.prototype.getTime = function(){
    return (new Date().getTime() -  this.startTime) / 1000;
};


OMEGA.Utils.Timeline = function(debug){
    this.triggers = new Array();
    this.trigger_set = new Array();
    this.time = 0; this.trigger_set_index = 0;
    this.startTime = new Date().getTime();
    this.deltaTime = this.savedTime = 0;
    this.upCommingTriggerTime = 0;
    this.isEnabled = false;
    this.isDebug = debug || false;
    this.frames = new Array();
};
OMEGA.Utils.Timeline.prototype.On = function(){
    this.isEnabled = true;
};
OMEGA.Utils.Timeline.prototype.Off = function(){
    this.isEnabled = false;
};
OMEGA.Utils.Timeline.prototype.Add = function( time, trigger){
    if( this.triggers[time] == null){
        this.triggers[time] = new Array();
    }
    this.triggers[time].push( trigger );


    if(this.trigger_set.indexOf(time) == -1 ) this.trigger_set.push( time );
};
OMEGA.Utils.Timeline.prototype.Reset = function(){
    this.Off();
    this.trigger_set_index    = -1;
    if(this.isDebug) console.log("[OMEGA :: Timeline] - Reset!");
    this.startTime = new Date().getTime();
    this.savedTime = 0;
    this.On();
};
OMEGA.Utils.Timeline.prototype.Clear = function(){
    this.triggers = new Array();
    this.upCommingTriggerTime = 0;
    this.trigger_set_index    = 0;
};
OMEGA.Utils.Timeline.prototype.Tick = function( time ){
    if(!this.isEnabled) return;
    this.time = time == undefined ?  (new Date().getTime() -  this.startTime) : time;
    this.deltaTime = (this.time - this.savedTime);
    this.upCommingTriggerTime = this.trigger_set[this.trigger_set_index];

    if( this.triggers[this.upCommingTriggerTime] != null && (this.time-this.deltaTime <= this.upCommingTriggerTime && this.time+this.deltaTime >= this.upCommingTriggerTime)){
        if(this.isDebug) console.log("[OMEGA :: Timeline] - Trigger fired! -> { trigger_time: " + this.upCommingTriggerTime + "ms current_time: " + this.time + "ms }");
        for( var i = 0; i < this.triggers[this.upCommingTriggerTime].length; i++) this.triggers[this.upCommingTriggerTime][i]();
        if( this.trigger_set.length-1 >this.trigger_set_index) this.trigger_set_index++;
    }
    this.savedTime = this.time;
};



OMEGA.Utils.StringParser = function(str){
    this.str;
    this.index;
    this.words;
    this.Init(str);
};
OMEGA.Utils.StringParser.prototype.Init = function(str){
    this.str = str;
    this.index = 0;
};
OMEGA.Utils.StringParser.prototype.skipDelimiters = function(){
    for(var i = this.index, len = this.str.length; i < len; i++){
        var c = this.str.charAt(i);
        if(c=='\t' || c == ' ' || c == '(' || c == ')' || c == '"')continue;
        break;
    }
    this.index = i;
};
OMEGA.Utils.StringParser.prototype.GetFirstWord = function(){
    this.words = this.str.split(String.fromCharCode(32));
    if(this.words.length == 0 ) return null;
    else return this.words[0];
};








var OMEGA = OMEGA || {};
OMEGA.Omega3D = OMEGA.Omega3D || {};
OMEGA.Omega3D = function(){
    this.canvas   =  document.getElementById("omega");
    this.scenes   = [];
    this.cameras  = [];
    this.shaders  = [];
    this.keylisteners = [];

    this.gl = null;
    try {
        this.gl = this.canvas.getContext("experimental-webgl" );
        if(this.gl == null)this.gl = canvas.getContext("webgl");
    } catch (e) {}
    if (this.gl == null){
        console.log("[Omega3D] Cant initiate WebGL!");
        return;
    }

    OMEGA.Omega3D.AVAILABLE = true;
    this.gl.viewPortWidth  = this.canvas.width;
    this.gl.viewPortHeight = this.canvas.height;
    this.canvas.addEventListener('webglcontextlost', function(){ OMEGA.Omega3D.AVAILABLE = false; }, false);
    this.canvas.addEventListener('webglcontextrestored', function(){ OMEGA.Omega3D.AVAILABLE = true; }, false);

    window.addEventListener("resize", function(){
        Omega3D.Recalibrate();
    }, false);

    OMEGA.Omega3D.GL = this.gl;
    console.log(".: Omega3D v"+OMEGA.Omega3D.VERSION+" :.");

    //check extentions.
    var floating_point_textures =  this.gl.getExtension('OES_texture_float');;
    if(floating_point_textures == null) Omega3D.Log("[Omega3D] No OES_texture_float supported in this browser.");
    else Omega3D.Log("[Omega3D] OES_texture_float supported in this browser.");
    //
    var float_linear_textures =  this.gl.getExtension('OES_texture_float_linear');;
    if(float_linear_textures == null) Omega3D.Log("[Omega3D] No OES_texture_float_linear supported in this browser.");
    else Omega3D.Log("[Omega3D] OES_texture_float_linear supported in this browser.");

    var standard_derivatives = this.gl.getExtension("OES_standard_derivatives");
    if(standard_derivatives == null) Omega3D.Log("[Omega3D] No OES_standard_derivatives supported in this browser.");
    else Omega3D.Log("[Omega3D] OES_standard_derivatives supported in this browser.");



};
OMEGA.Omega3D.Recalibrate = function(){
    OMEGA.Omega3D.INSTANCE.canvas.width  = window.innerWidth - 3.25;
    OMEGA.Omega3D.INSTANCE.canvas.height = window.innerHeight - 3.25;
    OMEGA.Omega3D.INSTANCE.gl.viewPortWidth  = OMEGA.Omega3D.INSTANCE.canvas.width;
    OMEGA.Omega3D.INSTANCE.gl.viewPortHeight = OMEGA.Omega3D.INSTANCE.canvas.height;
}
OMEGA.Omega3D.GetGL = function(){
    return OMEGA.Omega3D.INSTANCE.gl;
};
OMEGA.Omega3D.AddKeyListener = function(listener){
    OMEGA.Omega3D.INSTANCE.keylisteners.push(listener);
    document.keylisteners = OMEGA.Omega3D.INSTANCE.keylisteners;

};
OMEGA.Omega3D.RemoveKeyListener = function(listener){
    OMEGA.Omega3D.INSTANCE.keylisteners.splice( OMEGA.Omega3D.INSTANCE.keylisteners.lastIndexOf(listener), 1 );
    document.keylisteners = OMEGA.Omega3D.INSTANCE.keylisteners;
};

OMEGA.Omega3D.ReadPixels = function( image,xpos, ypos, width, height ){
    var gl = OMEGA.Omega3D.INSTANCE.gl;
    var texture = gl.createTexture();
    gl.bindTexture(gl.TEXTURE_2D, texture);
    gl.texImage2D(gl.TEXTURE_2D, 0, gl.RGBA, gl.RGBA, gl.UNSIGNED_BYTE, image);
    gl.texParameteri(gl.TEXTURE_2D, gl.TEXTURE_MAG_FILTER, gl.NEAREST);
    gl.texParameteri(gl.TEXTURE_2D, gl.TEXTURE_MIN_FILTER, gl.NEAREST);

    var framebuffer = gl.createFramebuffer();
    gl.bindFramebuffer(gl.FRAMEBUFFER, framebuffer);
    gl.framebufferTexture2D(gl.FRAMEBUFFER, gl.COLOR_ATTACHMENT0, gl.TEXTURE_2D, texture, 0);

    var data = new Uint8Array(width * height * 4);
    gl.readPixels(xpos, ypos, width,height, gl.RGBA, gl.UNSIGNED_BYTE, data);

    gl.deleteFramebuffer(framebuffer);
    gl.bindTexture(gl.TEXTURE_2D, null);
    gl.deleteTexture(texture);
    return data;
};
OMEGA.Omega3D.CreateArrayBuffer = function( data, itemSize ){
    var gl = OMEGA.Omega3D.INSTANCE.gl;
    var buffer = gl.createBuffer();
    gl.bindBuffer(gl.ARRAY_BUFFER, buffer);
    gl.bufferData(gl.ARRAY_BUFFER, data,gl.STATIC_DRAW);
    buffer.itemSize = itemSize;
    buffer.numItems = data.length / itemSize;
    return buffer;
};

OMEGA.Omega3D.CreateAtlas = function(width, height, cellWidth, cellHeight, out_vertices, out_uvs, out_indices ){
    var xSteps = width / cellWidth;
    var ySteps = height / cellHeight;
    for( var y = 0; y < ySteps; y++){
        for( var x = 0; x < xSteps; x++) {
            var xpos = x * cellWidth;
            var ypos = y * cellHeight;
            var zpos = 0.0;

            out_vertices.push( xpos, ypos, zpos );

            out_uvs.push( x / (xSteps - 1));
            out_uvs.push( y / (ySteps - 1));

            if (y < ySteps-1 && x < xSteps-1) {

                out_indices.push(y * xSteps + x);
                out_indices.push(y * xSteps + x + 1);
                out_indices.push((y+1) * xSteps + x);

                out_indices.push(y * xSteps + x + 1);
                out_indices.push((y+1) * xSteps + x + 1);
                out_indices.push((y+1) * xSteps + x);
            }
        }
    }
};





//LISTENERS
function onload(){
    var body = document.getElementsByTagName("body");
    OMEGA.Omega3D.INSTANCE = new OMEGA.Omega3D();
};


if (window.addEventListener)
    window.addEventListener("load", onload, false);
else if (window.attachEvent)
    window.attachEvent("onload", onload);
else
    window.onload = onload;

document.keylisteners = [];
document.onkeydown = function(e){
    for(var i = 0; i < document.keylisteners.length; i++){
        document.keylisteners[i].HandleKeyDown(e);
    }
};
document.onkeyup =  function(e){
    for(var i = 0; i < document.keylisteners.length; i++){
        document.keylisteners[i].HandleKeyUp(e);
    }
};



//PROTOTYPES OF NATIVES.
String.prototype.splice = function( idx, rem, s ) {
    return (this.slice(0,idx) + s + this.slice(idx + Math.abs(rem)));
};


OMEGA.Omega3D.Log = function( msg ){
    console.log(msg);
};



OMEGA.Omega3D.GL = null;
OMEGA.Omega3D.LIGHTS = [];
OMEGA.Omega3D.PointSize = 1.0;
OMEGA.Omega3D.VERSION = "0.0.1.7 Alpha";
OMEGA.Omega3D.ScenesCount   = 0;
OMEGA.Omega3D.ObjectsCount   = 0;
OMEGA.Omega3D.MaterialsCount = 0;
OMEGA.Omega3D.INSTANCE = null;

OMEGA.Omega3D.AVAILABLE = false;


OMEGA.Omega3D.KEEP    = 7680;
OMEGA.Omega3D.REPLACE = 7681;
OMEGA.Omega3D.NEVER   = 512;
OMEGA.Omega3D.LESS    = 513;
OMEGA.Omega3D.EQUAL   = 514;
OMEGA.Omega3D.LEQUAL  = 515;
OMEGA.Omega3D.GREATER = 516;
OMEGA.Omega3D.NOTEQUAL= 517;
OMEGA.Omega3D.GEQUAL  = 518;
OMEGA.Omega3D.ALWAYS  = 519;
var Omega3D = OMEGA.Omega3D;









function Object3D( mesh, material ){
    this.id   = (++OMEGA.Omega3D.ObjectsCount).toString();
    this.name = "Object3d_" + this.id.toString();
    this.mayRender = true;
    this.next      = null;
    this.prev      = null;
    this.parentScene = null;
    this.position = [0, 0, 0];
    this.rotation = [0, 0, 0];
    this.scale    = [1, 1, 1];
    this.sMatrix   = mat4.create();
    this.tMatrix   = mat4.create();
    this.rMatrix   = mat4.create();
    this.modelView = mat4.create();
    mat4.identity(this.modelView);

    this.alphaBlend = false;

    this.mesh     = mesh;
    this.material = material;
    this.drawType = 0;
    if(this.mesh){
        this.mesh.CreateBuffers();
        this.mesh.CreateColorPickingBuffer(this.id);
    }
    this.parent = null;
    this.children = [];

    this.SetName      = function(value){this.name = value;};
    this.SetMesh      = function(value){ this.mesh = value;      };
    this.SetMaterial  = function(value){ this.material= value;  };
    this.SetMatrix    = function(m){this.modelView = m; };

    this.GetPosition  = function(){ return this.position;  };
    this.GetRotation  = function(){ return this.rotation;  };
    this.GetMesh      = function(){ return this.mesh;      };
    this.GetMaterial  = function(){ return this.material;  };
    this.GetMatrix    = function(){
        mat4.identity(this.modelView);
        if(this.parent!=null)this.modelView = this.parent.GetMatrix();
        mat4.multiply(this.modelView, this.modelView, this.tMatrix);
        mat4.multiply(this.modelView, this.modelView, this.rMatrix);
        mat4.multiply(this.modelView, this.modelView, this.sMatrix);
        return this.modelView;
    };
    this.GetName      = function(){return this.name;};
    this.GetID        = function(){return this.id;};

    this.Identity = function(){
        this.position = [0, 0, 0];
        this.rotation = [0, 0, 0];
        this.scale    = [1, 1, 1];
        this.SetScale( this.scale[0], this.scale[1], this.scale[2] );
        this.SetPosition( this.position[0], this.position[1], this.position[2] );
        this.SetRotation( this.rotation[0],this.rotation[1],this.rotation[2] );
        mat4.identity(this.modelView);
    };

    this.PlaceBack = function(){
        this.SetScale( this.scale[0], this.scale[1], this.scale[2] );
        this.SetPosition( this.position[0], this.position[1], this.position[2] );
        this.SetRotation( this.rotation[0],this.rotation[1],this.rotation[2] );
        // mat4.identity(this.modelView);
    };

    this.SetScale = function( x, y, z ){
        this.scale = [x, y, z];
        mat4.identity(this.sMatrix);
        mat4.scale(this.sMatrix, this.sMatrix, this.scale);
    };


    this.SetPosition = function( x, y, z ){
        this.position = [x, y, z];

        mat4.identity(this.tMatrix);
        mat4.translate(this.tMatrix, this.tMatrix, this.position );
    };
    this.Translate = function( x, y, z ){
        this.position[0] += x;
        this.position[1] += y;
        this.position[2] += z;

        mat4.identity(this.tMatrix);
        mat4.translate( this.tMatrix, this.tMatrix, this.position );
    };


    this.SetRotation = function( x, y, z ){
        mat4.identity(this.rMatrix);
        this.rotation = [x,y,z];
        mat4.rotate( this.rMatrix, this.rMatrix, this.rotation[0], [1.0, 0.0, 0.0] );
        mat4.rotate( this.rMatrix, this.rMatrix, this.rotation[1], [0.0, 1.0, 0.0] );
        mat4.rotate( this.rMatrix, this.rMatrix, this.rotation[2], [0.0, 0.0, 1.0] );
    };
    this.Rotate = function( x, y, z ){
        mat4.identity(this.rMatrix);
        this.RotateX(x);this.RotateY(y);this.RotateZ(z);
    };

    this.RotateX = function( value ){
        this.rotation[0] += value;
        mat4.rotate( this.rMatrix, this.rMatrix, this.rotation[0], [1.0, 0.0, 0.0] );
    };
    this.RotateY = function( value ){
        this.rotation[1] += value;
        mat4.rotate( this.rMatrix, this.rMatrix, this.rotation[1], [0.0, 1.0, 0.0] );
    };
    this.RotateZ = function( value ){
        this.rotation[2] += value;
        mat4.rotate( this.rMatrix, this.rMatrix, this.rotation[2], [0.0, 0.0, 1.0] );
    };

    this.GetChildByName = function( name ){
        for( var i = 0; i < this.children.length; i++){
            if(this.children[i].name == name ) return this.children[i];
        }
        return null;
    }



    this.SetParent = function( parent ){
        if(this.parent != null){
            this.parent.children.splice(this.parent.children.indexOf( this ), 1 );
        }

        this.parent = parent;
        if(parent != null ) this.parent.children.push(this);
    }



    this.Update = function(gl,camera){
        if(this.alphaBlend ){
            gl.disable(gl.DEPTH_TEST);
            gl.enable(gl.BLEND);
            gl.blendFunc(gl.SRC_ALPHA, gl.ONE_MINUS_SRC_ALPHA);
        }
    };
    this.LateUpdate = function(gl,camera){
        if(this.alphaBlend){
            gl.enable(gl.DEPTH_TEST);
            gl.disable(gl.BLEND);
        }
    };

    this.UpdateMaterial = function( gl, camera,scene ){
        if(this.material == null)return;
        var program = this.material.GetShader().GetProgram();
        if(program == null)return;

        //update if scene has fog.
        if(scene.hasFog){
            gl.uniform3fv(program["uFogColor"], [scene.getColor().r, scene.getColor().g, scene.getColor().b ] );
            gl.uniform2fv(program["uFogDist" ], [scene.fogStart, scene.fogEnd] );
        }

        //update textures.
        if( this.material.GetTextures() != null ) {
            var t = this.material.GetTextures();
            var l = t.length;
            for (var j = 0; j < l; j++) if (t[j].needsUpdate)t[j].Update();
        }

        //set view matrix.
        if(program.uProjectionMatrix && camera != null ) gl.uniformMatrix4fv( program.uProjectionMatrix, false, camera.GetProjectionMatrix() );
        if(program.uViewMatrix       && camera != null ) gl.uniformMatrix4fv( program.uViewMatrix   , false,  camera.GetMatrix() );
        if(program.uInvViewMatrix    && camera != null ) gl.uniformMatrix4fv( program.uInvViewMatrix, false, camera.GetInverseMatrix() );

        //Set custom vars in material.
        for(var key in this.material.custom_uniforms){
            var u = this.material.custom_uniforms[key];
            if(u.type == "int" ) gl.uniform1i(program[key], u.value );
            else if(u.type == "float" ) gl.uniform1f(program[key], u.value );
            else if(u.type == "mat4" ) gl.uniformMatrix4fv(program[key],false, u.value );
            else if(u.type == "mat3" ) gl.uniformMatrix3fv(program[key],false, u.value );
            else if(u.type == "vec4" ) gl.uniform4fv(program[key], u.value );
            else if(u.type == "vec3" ) gl.uniform3fv(program[key], u.value );
            else if(u.type == "vec2" ) gl.uniform2fv(program[key], u.value );
        }
        //render final material.
        this.RenderMaterial(gl);
    };



    this.Render = function( scene, camera  ){
        var gl = scene.getGL();
        if(this.material != null) this.material.Enable();
        this.Update(gl,camera);
        if(scene.getLights().length > 0 && scene.lightsON ) this.RenderLight( scene );
        this.UpdateMaterial(gl, camera, scene);


        if(this instanceof LODObject3D) this.adjustLODLevel(camera);

        //Render self
        this.RenderObject(gl, camera );

        //disable material
        if(this.material != null)this.material.Disable();

        //Render children.
        for(var i = 0; i < this.children.length; i++){
            this.children[i].Render(scene, camera );
        }

        //reset position.
        this.PlaceBack();

        if(camera!=null) this.LateUpdate(gl,camera);
    };
    this.RenderObject = function( gl, camera ){
        if(this.material == null || this.mesh == null) return;

        var mat = this.material;
        var p   = mat.GetShader().GetProgram();
        if(p == null) return;

        //custom attributes.
        for( var key in mat.custom_attribs){
            gl.enableVertexAttribArray(p[key]);
            gl.bindBuffer( gl.ARRAY_BUFFER, mat.custom_attribs[key].value);
            gl.vertexAttribPointer( p[key],mat.custom_attribs[key].value.itemSize, gl.FLOAT, false, 0, 0 );
        };

        /* model matrix */
        gl.uniformMatrix4fv( p.uModelMatrix , false, this.GetMatrix() );

        /* normal matrix */
        var normalMatrix4 = mat4.create();
        var normalMatrix3 = mat3.create();
        if(camera != null){
            mat4.multiply(normalMatrix4, camera.GetMatrix(), this.GetMatrix() );
            mat3.fromMat4(normalMatrix3,normalMatrix4 );
            mat3.invert(normalMatrix3,normalMatrix3 );
            mat3.transpose(normalMatrix3,normalMatrix3);
        }
        gl.uniformMatrix3fv( p.uNormalMatrix, false, normalMatrix3 );


        //Bind combined buffer -> [ vertex, normal, uv, barycentric ] = (3 + 3 + 2  + 3) * 4 bytes = 44 bytes
        gl.bindBuffer(gl.ARRAY_BUFFER, this.GetMesh().GetCombinedBuffer());

        /*vertices*/
        if(p.aVertexPos !=-1 ) {
            gl.enableVertexAttribArray(p.aVertexPos);
            gl.vertexAttribPointer(p.aVertexPos, 3, gl.FLOAT, false, 44, 0);
        }

        /*aVertexNormals*/
        if(p.aVertexNormal!=-1 ) {
            gl.enableVertexAttribArray(p.aVertexNormal);
            gl.vertexAttribPointer(p.aVertexNormal, 3, gl.FLOAT, false, 44, 12); // offset = 12 = 4 * 3
        }

        /*uvs*/
        if(p.aTextureCoord!=-1 ){
            gl.enableVertexAttribArray(p.aTextureCoord);
            gl.vertexAttribPointer( p.aTextureCoord,2, gl.FLOAT, false, 44, 24 ); // offset = 24 = 4 * 6
        }

        /*aTangents*/
        //if(p.aVertexTangent !=-1) {
        //    gl.enableVertexAttribArray(p.aVertexTangent);
        //    gl.vertexAttribPointer(p.aVertexTangent, 3, gl.FLOAT, false, 68, 32); // offset = 24 = 4 * 8
        //}
        //
        ///*aBitangents*/
        //if(p.aVertexBitangent !=-1 ) {
        //    gl.enableVertexAttribArray(p.aVertexBitangent);
        //    gl.vertexAttribPointer(p.aVertexBitangent, 3, gl.FLOAT, false, 68, 44);  // offset = 24 = 4 * 11
        //}

        /*aBaricentric*/
        if(p.aBaricentric !=-1 ) {
            gl.enableVertexAttribArray(p.aBaricentric);
            gl.vertexAttribPointer(p.aBaricentric, 3, gl.FLOAT, false, 44, 32);  // offset = 32 = 4 * 8
        }

        /*aPickingColor*/
        if(p.aPickingColor !=-1 &&  this.GetMesh().GetColorPickingBuffer() != undefined) {
            gl.enableVertexAttribArray(p.aPickingColor);
            gl.bindBuffer(gl.ARRAY_BUFFER, this.GetMesh().GetColorPickingBuffer());
            gl.vertexAttribPointer(p.aPickingColor, this.GetMesh().GetColorPickingBuffer().itemSize, gl.FLOAT, false, 0, 0);
        }

        /*indices*/
        gl.bindBuffer( gl.ELEMENT_ARRAY_BUFFER, this.GetMesh().GetIndexBuffer() );

        /* 4   DRAW OBJECT*/
        //gl.drawArrays( gl.TRIANGLES, 0, this.GetMesh().GetVertexBuffer().numItems);
        if( this.drawType == OMEGA.Omega3D.Object3D.DEFAULT        ) gl.drawElements( gl.TRIANGLES , this.GetMesh().GetIndexBuffer().numItems, gl.UNSIGNED_SHORT, this.GetMesh().GetIndexBuffer());
        else if(this.drawType == OMEGA.Omega3D.Object3D.WIREFRAME ) gl.drawElements( gl.LINE_STRIP, this.GetMesh().GetIndexBuffer().numItems, gl.UNSIGNED_SHORT,  this.GetMesh().GetIndexBuffer());
        else if(this.drawType == OMEGA.Omega3D.Object3D.POINTS    ) gl.drawArrays( gl.POINTS      , 0, this.GetMesh().GetVertexBuffer().numItems);
        else if(this.drawType == OMEGA.Omega3D.Object3D.TRIANGLES ){
            //  for(  var i = 0; i < current.GetMesh().GetGeometry().GetVertices().length; i+=3)
            gl.drawArrays( gl.TRIANGLES,0, this.GetMesh().GetGeometry().GetVertices().length/3);
        }

        //disable arrays.
        if(p.aVertexPos!=-1   )gl.disableVertexAttribArray(p.aVertexPos   );
        if(p.aTextureCoord!=-1)gl.disableVertexAttribArray(p.aTextureCoord);
        if(p.aVertexNormal!=-1)gl.disableVertexAttribArray(p.aVertexNormal);
        if(p.aTangent!=-1)gl.disableVertexAttribArray(p.aBitangent);
        if(p.aBitangent!=-1)gl.disableVertexAttribArray(p.aBitangent);
    };
    this.RenderMaterial = function( gl ){
        if(this.material == null)return;
        var p = this.material.GetShader().GetProgram();
        for(var key in this.material.uniforms){
            gl.uniform3fv( p[key], this.material.uniforms[key].value.apply(this.material, null));
        }
    };
    this.RenderLight = function( scene ){
        if(this.material == null)return;
        var gl = scene.getGL();
        var lights = scene.getLights();
        var p = this.material.GetShader().GetProgram();
        if(p == null)return;
        for( var index in lights){
            for(var key in lights[index].uniforms){
                gl.uniform3fv( p[key], lights[index].uniforms[key].value.apply( lights[index], null));
            }
        }
    };


    this.Clone = function(){
        var geom; var mat; var child;
        if( this.GetMesh() ) geom = this.GetMesh().GetGeometry();
        if( this.material  ) mat  = this.material.Clone();
        var obj = new Omega3D.Object3D(geom ? new Omega3D.Mesh(geom) : null, mat ? mat : null);
        obj.name = this.name + "_Clone_" + this.id;
        for(var  i = 0; i < this.children.length; i++){
            child = this.children[i].Clone();
            child.SetParent(obj);
        }
        return obj;
    }

    this.toString = function(){
        return "Object3D { id: " + this.id + ", name: " + this.name + "};"
    };

};
OMEGA.Omega3D.Object3D = Object3D;
OMEGA.Omega3D.Object3D.DEFAULT   = 0;
OMEGA.Omega3D.Object3D.POINTS    = 1;
OMEGA.Omega3D.Object3D.WIREFRAME = 2;
OMEGA.Omega3D.Object3D.TRIANGLES = 3;function Camera(){
    Object3D.apply(this);

    this.projectionMatrix = mat4.create();
    this.laMatrix = mat4.create();

    this.near = 0.1;
    this.far  = 1000.0;

    this.x = this.y = this.z = 0;
    this.lookX = this.lookY = this.lookZ = 0;

    this.GetProjectionMatrix = function(){
        mat4.perspective(this.projectionMatrix,45, OMEGA.Omega3D.GL.viewPortWidth / OMEGA.Omega3D.GL.viewPortHeight,  this.near, this.far );
        return this.projectionMatrix;
    };
    this.GetMatrix    = function(){
        mat4.identity(this.modelView);
        mat4.multiply(this.modelView, this.rMatrix, this.tMatrix);
        mat4.multiply(this.modelView, this.modelView, this.sMatrix);
        mat4.multiply(this.modelView, this.modelView, this.laMatrix);
        return this.modelView;
    };


    this.GetInverseMatrix = function(){
        var invViewMatrix = mat4.create();
        mat4.invert(invViewMatrix, this.GetMatrix() );
        //mat4.transpose(invViewMatrix, invViewMatrix);
        return invViewMatrix;
    };

    this.update = function(){ this.LookAt(this.lookX, this.lookY, this.lookZ, [this.x, this.y, this.z]); };
    this.LookAt = function( x, y, z, position ){
        this.lookX =x; this.lookY =y;this.lookZ =z;
        this.x =position[0]; this.y = position[1]; this.z = position[2];
        mat4.identity(this.laMatrix);
        mat4.lookAt(this.laMatrix,position || this.position,[this.lookX,this.lookY,this.lookZ],  [0,-1,0]);
    };

};
Camera.prototype = new Object3D();
OMEGA.Omega3D.cameras = OMEGA.Omega3D.cameras || {};
OMEGA.Omega3D.cameras.Camera = Camera;


function Material( shader, textures) {
    this.materialID = "material_" + (++OMEGA.Omega3D.MaterialsCount);
    this.textures = textures || null;
    this.shader = shader || null;

    this.diffuse  = [1.0, 1.0, 1.0];
    this.ambient  = [1.0, 1.0, 1.0];
    this.specular = [1.0, 1.0, 1.0];

    this.hasAtlas = false;
    var atlas = null;

    this.GetTextures = function() {
        return this.textures;
    };
    this.SetTextures = function(textures) {
        this.textures = textures;
    };
    this.GetShader = function() {
        return this.shader;
    };
    this.SetShader = function( shader ) {
        this.shader = shader;
    };
    this.GetID = function() {
        return this.materialID;
    };

    this.SetAtlas = function( atlas_object ){
        this.hasAtlas = true;
        atlas    = atlas_object;
    };

    this.GetCurrentAtlasFrame = function(){
        return atlas.GetCurrentFrame().uvRect;
    }

    this.Enable = function() {
        if(this.hasAtlas) atlas.Update();
        if (this.shader ) this.shader.Enable();
        if (this.textures){

            for(var i = 0; i <  this.textures.length; i++) {
                if(i > 0 ){
                    this.textures[i].tex_id = "uSampler_" + i;
                    this.textures[i].ID = i;
                }
                this.textures[i].Enable(this.shader);
            }

            for(var i = 0; i <  this.textures.length; i++) {
                if(i > 0 ){
                    this.textures[i].tex_id = "uSampler";
                    this.textures[i].ID = 0;
                }
            }
        }
    };
    this.Disable = function() {
        if (this.shader) this.shader.Disable();
        if (this.textures){
            var l = this.textures.length;
            for(var i = 0; i < l; i++) {
                this.textures[i].Disable();
            }
        }
    };

    this.GetSpecular = function(){ return this.specular; };
    this.GetDiffuse = function(){ return this.diffuse; };
    this.GetAmbient = function(){ return this.ambient; };
    this.SetAmbient = function(r,g,b){
        this.ambient[0] = r; this.ambient[1] = g; this.ambient[2] = b;
    };

    this.SetDiffuse = function(r,g,b){
        this.diffuse[0] = r; this.diffuse[1] = g; this.diffuse[2] = b;
    };
    this.SetSpecular = function(r,g,b){
        this.specular[0] = r; this.specular[1] = g; this.specular[2] = b;
    };

    this.genUniforms = function(){
        this.uniforms["uAmbient"]   = { type: "vec3", glsl: "uniform vec3 uAmbient;" , value: this.GetAmbient };
        this.uniforms["uDiffuse"]   = { type: "vec3", glsl: "uniform vec3 uDiffuse;" , value: this.GetDiffuse };
        this.uniforms["uSpecular"]  = { type: "vec3", glsl: "uniform vec3 uSpecular;", value: this.GetSpecular };
    };

    this.uniforms = {};
    this.custom_uniforms = {};
    this.custom_attribs = {};
    this.genUniforms();


    this.Clone = function(){
        var clone = new Omega3D.Material(this.shader.Clone(), this.textures);
        clone.uniforms        = this.uniforms;
        clone.custom_uniforms = this.custom_uniforms;
        clone.custom_attribs  = this.custom_attribs;
        clone.SetDiffuse( this.diffuse[0], this.diffuse[1], this.diffuse[2]);
        clone.SetSpecular( this.specular[0], this.specular[1], this.specular[2]);
        return clone;
    }
};
OMEGA.Omega3D.Material = Material;



function Shader(vsrc, fsrc, vars ){
    this.vars = vars;
    this.vertex_shader_src   = vsrc;
    this.fragment_shader_src = fsrc;


    this.GetVertexShaderSource = function(){
        return this.vertex_shader_src;
    };
    this.GetFragmentShaderSource = function(){
        return this.fragment_shader_src;
    };
    this.GetVertexShader = function(){
        return this.vertex_shader;
    };
    this.GetFragmentShader = function(){
        return this.fragment_shader;
    };
    this.GetProgram = function(){
        return this.program;
    };
    this.Enable = function(){
        if(!this.program) return;
        this.gl_context.useProgram( this.program );
    };
    this.Disable = function(){
        if(!this.program)return;
        this.gl_context.useProgram(null);
    };

    this.createProgram = function ( ){
        if(this.vertex_shader==null||this.fragment_shader==null)return;
        this.program = this.gl_context.createProgram();
        this.gl_context.attachShader(this.program, this.vertex_shader);
        this.gl_context.attachShader(this.program, this.fragment_shader);
        this.gl_context.linkProgram(this.program);

        if(!this.gl_context.getProgramParameter( this.program, this.gl_context.LINK_STATUS) ){
            OMEGA.Omega3D.Log("   -- Could not initialize shaders / program");
            return null;
        }


        if(vars){
            for( var key in vars ){
                for(var k in vars[key]){

                    if(key == 0 || key == 4){
                        this.program[k] = this.gl_context.getAttribLocation( this.program, k.toString());
                    }
                    else{

                        this.program[k] = this.gl_context.getUniformLocation( this.program, k.toString());
                    }
                }
            }
        }
        this.gl_context.validateProgram(this.program);
        if (!this.gl_context.getProgramParameter(this.program,this.gl_context.VALIDATE_STATUS)){
            OMEGA.Omega3D.Log("   -- Program validation failed!");
            return null;
        }

        this.gl_context.detachShader( this.program, this.vertex_shader );
        this.gl_context.detachShader( this.program, this.fragment_shader );
        this.gl_context.deleteShader(this.vertex_shader);
        this.gl_context.deleteShader( this.fragment_shader);
        return this.program;
    };
    this.createShaderElement = function( type, src ) {
        if(!src)return null;
        var shader_element = this.gl_context.createShader( type );
        this.gl_context.shaderSource( shader_element, src );
        this.gl_context.compileShader( shader_element );

        if(!this.gl_context.getShaderParameter( shader_element, this.gl_context.COMPILE_STATUS )) {
            OMEGA.Omega3D.Log( (type == this.gl_context.VERTEX_SHADER ? "  -- Vertex component: " : "   -- Fragment component: ") + this.gl_context.getShaderInfoLog( shader_element ));
            var lines = src.split("\n");
            for( var i in lines) OMEGA.Omega3D.Log( i, lines[i] );
            return null;
        }

        return shader_element;
    };
    this.GetSamplerLocation = function( sampler_id ){
        for( var key in vars ){
            for(var k in vars[key]){
                if( vars[key][k].type == "sampler2D" || vars[key][k].type == "samplerCube" ){
                    if( sampler_id != null){
                        if(sampler_id == k) return this.program[k];
                    }else{
                        return this.program[k];
                    }
                }
            }
        }
        return -1;
    };



    this.Clone = function(){
        return new OMEGA.Omega3D.Shader( this.vertex_shader_src, this.fragment_shader_src, this.vars );
    }







    this.gl_context          = OMEGA.Omega3D.GL;
    this.vertex_shader       = this.gl_context ? this.createShaderElement( this.gl_context.VERTEX_SHADER, this.vertex_shader_src ) : null;
    this.fragment_shader     = this.gl_context ? this.createShaderElement( this.gl_context.FRAGMENT_SHADER, this.fragment_shader_src  ) : null;
    this.program             = this.gl_context ? this.createProgram() : null;
};
OMEGA.Omega3D.Shader = Shader;function Texture( img, needsUpdate, ID ){
    if(!OMEGA.Omega3D.GL) return null;
    this.ID = ID ||  0;
    this.gl_context = OMEGA.Omega3D.GL;
    this.img        = img;
    this.isCubeMap  =  true;
    this.needsUpdate= needsUpdate || false;
    this.tex        =  this.gl_context.createTexture();
    this.tex_id     = this.ID == 0 ? "uSampler" : "uSampler"+this.ID;
    this.isEnabled  = false;
    this.isDirty    = true;

    console.log();

    this.GetTexture = function(){ return this.tex; };
    this.GetImage   = function(){ return this.img; };
    this.SetImage   = function(value){ this.img = value; this.isDirty = true;};
    this.handleTextureLoaded = function(image, texture ){ };

    this.Enable = function( shader ){ };
    this.Disable = function(){ };

    this.IsPowerOf2 = function( value ){
        return ( (value & (value-1)) == 0);
    };

    this.Update = function() {
        if(!this.needsUpdate ) if(!this.isDirty ) return;
        this.gl_context.bindTexture(this.gl_context.TEXTURE_2D, this.tex);
        //this.gl_context.pixelStorei(this.gl_context.UNPACK_FLIP_Y_WEBGL, true);
        if(this.img == null){
            this.isDirty = false;
            return;
        }

        if(this.img.toString() == "[object HTMLVideoElement]")
            if(this.img.paused)return;
        if(this.img) this.gl_context.texImage2D(this.gl_context.TEXTURE_2D, 0, this.gl_context.RGBA, this.gl_context.RGBA, this.gl_context.UNSIGNED_BYTE, this.img);
        this.isDirty = false;
    };

    this.GenerateMipmap = function(){
        this.gl_context.bindTexture(this.gl_context.TEXTURE_2D, this.tex);
        this.gl_context.generateMipmap(this.gl_context.TEXTURE_2D);
        this.gl_context.bindTexture(this.gl_context.TEXTURE_2D, null);
    };
};
OMEGA.Omega3D.Texture = Texture;
OMEGA.Omega3D.Texture.TEXTURE0 = 33984;
OMEGA.Omega3D.Scene = function() {
    this.id = (++OMEGA.Omega3D.ScenesCount).toString();
    this.name = "Scene_"+this.id;
    this.renderer = null;
    this.gl       = OMEGA.Omega3D.GL;
    this.children = new Array();
    this.objects  = new Array();
    this.materials = new Array();
    this.list = new Array();
    this.lights = new Array();
    this.counter  = new OMEGA.Utils.Counter();
    this.color = {r: 0.5, g:0.5, b:0.5, a:1.0};
    this.hasFog = false;
    this.fogStart = 1.0;
    this.fogEnd   = 3.0;
    this.lightsON = true;


    this.getLights = function( ){
        return this.lights;
    };
    this.addLight = function( light ){
        light.setIndex( this.lights.length );
        light.GenerateGLSL();
        this.lights.push(light);
    };
    this.removeLight = function( light ){
        this.lights.splice(this.lights.indeOf(light), 1);
    };

    this.getColor = function(){
        return this.color;
    };
    this.setColor = function(r, g, b){
        this.color = {r: r, g:g, b:b, a:1.0};
        this.gl.clearColor(this.color.r, this.color.g, this.color.b, this.color.a);
    };
    this.checkForMaterial = function(id){
        for(var i = 0; i < this.materials.length; i++)
        {
            if( this.materials[i].id == id ) return true;
        }

        return false;
    }
    this.setName = function(value){
        this.name = value;
    };
    this.getName = function(){
        return this.name;
    };
    this.getID = function(){
        return this.id;
    };

    this.getMaterials = function( id ){
        return materials[id];
    };
    this.getObjectsFromMaterialID = function( id ){
        return objects[id];
    };
};
OMEGA.Omega3D.Scene.prototype.getGL = function(){
    return this.gl;
};
OMEGA.Omega3D.Scene.prototype.addChild = function( c  ){
    this.children.push( c );
    c.parentScene = this;
};
OMEGA.Omega3D.Scene.prototype.removeChild = function( c ){
    for(var i=0;i<this.children.length;i++){
        if( this.children[i] == c )
            this.children.splice(i,1);
    }
    c.parentScene = null;
    c = null;
};
OMEGA.Omega3D.Scene.prototype.removeChildAtIndex = function( removeChildAtIndex ){
    var c = this.children.splice(removeChildAtIndex,1);
    c.parentScene = null;
    c = null;
};


OMEGA.Omega3D.Scene.prototype.update = function(){
    this.counter.update();
};
OMEGA.Omega3D.Scene.prototype.getTime = function(){
    return this.counter.getTime();
};
OMEGA.Omega3D.Scene.prototype.getFPS = function(){
    return this.counter.getFPS();
};
OMEGA.Omega3D.Scene.prototype.getFrameNumber = function(){
    return this.counter.getFrame();
};
function KeyListener(){
    this.active = true;
    this.currentlyPressedKeys = [];
    this.HandleKeyDown = function(event){
        if(!this.active)return;

        this.currentlyPressedKeys[event.keyCode] = event.keyCode;
    };
    this.HandleKeyUp = function(event){
        if(!this.active)return;
        delete this.currentlyPressedKeys[event.keyCode];
    };
    this.Enable = function(){
        if(this.active)return;
        this.currentlyPressedKeys = [];
        this.active = true;
    };
    this.Disable = function(){
        if(!this.active)return;
        this.currentlyPressedKeys = [];
        this.active = false;
    };
}
OMEGA.listeners = OMEGA.listeners || {};
OMEGA.listeners.KeyListener = KeyListener;function Rect( x, y, w, h){
    this.x = x;
    this.y = y;
    this.w = w;
    this.h = h;
    this.lt = new Vector2( x, y);
    this.lb = new Vector2( x, y+h);
    this.rt = new Vector2( x+w, y);
    this.rb = new Vector2( x+w, y+h);
}/**
 *  Vector2
 *
 * @param x
 * @param y
 * @return {{x: *, y: *, z: *}}
 * @constructor
 */

function Vector2( x, y){
    this.x = x;
    this.y = y;

    __construct = function(){
    }();

    this.add = function( v ){
        this.x += v.x;
        this.y += v.y;
        return this;
    }
    this.subtract= function( v ){
        this.x -= v.x;
        this.y -= v.y;
        return this;
    }
    this.multiply= function( v ){
        this.x *= v;
        this.y *= v;
        return this;
    }
    this.zero = function() {
        this.x = 0;
        this.y = 0;
        return this;
    };
};
Vector2.Multiply = function( a, b){
    return new Vector2(a.x* b.x, a.y* b.y);
};
Vector2.MultiplyByValue = function( a, b){
    return new Vector2(a.x* b, a.y* b);
};
Vector2.Subtract = function( a, b){
    return new Vector2(a.x- b.x, a.y- b.y);
};
Vector2.Equals = function( a, b ){
    return (a.x==b.x&&a.y==b.y);
}
/**
 *  Vector3
 *
 * @param x
 * @param y
 * @param z
 * @return {{x: *, y: *, z: *}}
 * @constructor
 */

function Vector3( x, y, z){
    this.x = x;
    this.y = y;
    this.z = z;

    __construct = function(){
    }();

    this.add = function( v ){
        this.x += v.x;
        this.y += v.y;
        this.z += v.z;
        return this;
    }
    this.subtract= function( v ){
        this.x -= v.x;
        this.y -= v.y;
        this.z -= v.z;
        return this;
    }
    this.multiply= function( v ){
        this.x *= v;
        this.y *= v;
        this.z *= v;
        return this;
    }
    this.zero = function() {
        this.x = 0;
        this.y = 0;
        this.z = 0;
        return this;
    };
};
Vector3.Multiply = function( a, b){
    return new Vector3(a.x* b.x, a.y* b.y, a.z* b.z);
};
Vector3.MultiplyByValue = function( a, b){
    return new Vector3(a.x* b, a.y* b, a.z* b);
};
Vector3.Subtract = function( a, b){
    return new Vector3(a.x- b.x, a.y- b.y, a.z- b.z);
};
Vector3.Equals = function( a, b ){
    return (a.x==b.x&&a.y==b.y&&a.z== b.z);
}
/* Copyright (c) 2013, Brandon Jones, Colin MacKenzie IV. All rights reserved.

 Redistribution and use in source and binary forms, with or without modification,
 are permitted provided that the following conditions are met:

 * Redistributions of source code must retain the above copyright notice, this
 list of conditions and the following disclaimer.
 * Redistributions in binary form must reproduce the above copyright notice,
 this list of conditions and the following disclaimer in the documentation
 and/or other materials provided with the distribution.

 THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
 ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR
 ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON
 ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE. */

/**
 * @class 3x3 Matrix
 * @name mat3
 */
var mat3 = {};

/**
 * Creates a new identity mat3
 *
 * @returns {mat3} a new 3x3 matrix
 */
mat3.create = function() {
    var out = new GLMAT_ARRAY_TYPE(9);
    out[0] = 1;
    out[1] = 0;
    out[2] = 0;
    out[3] = 0;
    out[4] = 1;
    out[5] = 0;
    out[6] = 0;
    out[7] = 0;
    out[8] = 1;
    return out;
};

/**
 * Copies the upper-left 3x3 values into the given mat3.
 *
 * @param {mat3} out the receiving 3x3 matrix
 * @param {mat4} a   the source 4x4 matrix
 * @returns {mat3} out
 */
mat3.fromMat4 = function(out, a) {
    out[0] = a[0];
    out[1] = a[1];
    out[2] = a[2];
    out[3] = a[4];
    out[4] = a[5];
    out[5] = a[6];
    out[6] = a[8];
    out[7] = a[9];
    out[8] = a[10];
    return out;
};

/**
 * Creates a new mat3 initialized with values from an existing matrix
 *
 * @param {mat3} a matrix to clone
 * @returns {mat3} a new 3x3 matrix
 */
mat3.clone = function(a) {
    var out = new GLMAT_ARRAY_TYPE(9);
    out[0] = a[0];
    out[1] = a[1];
    out[2] = a[2];
    out[3] = a[3];
    out[4] = a[4];
    out[5] = a[5];
    out[6] = a[6];
    out[7] = a[7];
    out[8] = a[8];
    return out;
};

/**
 * Copy the values from one mat3 to another
 *
 * @param {mat3} out the receiving matrix
 * @param {mat3} a the source matrix
 * @returns {mat3} out
 */
mat3.copy = function(out, a) {
    out[0] = a[0];
    out[1] = a[1];
    out[2] = a[2];
    out[3] = a[3];
    out[4] = a[4];
    out[5] = a[5];
    out[6] = a[6];
    out[7] = a[7];
    out[8] = a[8];
    return out;
};

/**
 * Set a mat3 to the identity matrix
 *
 * @param {mat3} out the receiving matrix
 * @returns {mat3} out
 */
mat3.identity = function(out) {
    out[0] = 1;
    out[1] = 0;
    out[2] = 0;
    out[3] = 0;
    out[4] = 1;
    out[5] = 0;
    out[6] = 0;
    out[7] = 0;
    out[8] = 1;
    return out;
};

/**
 * Transpose the values of a mat3
 *
 * @param {mat3} out the receiving matrix
 * @param {mat3} a the source matrix
 * @returns {mat3} out
 */
mat3.transpose = function(out, a) {
    // If we are transposing ourselves we can skip a few steps but have to cache some values
    if (out === a) {
        var a01 = a[1], a02 = a[2], a12 = a[5];
        out[1] = a[3];
        out[2] = a[6];
        out[3] = a01;
        out[5] = a[7];
        out[6] = a02;
        out[7] = a12;
    } else {
        out[0] = a[0];
        out[1] = a[3];
        out[2] = a[6];
        out[3] = a[1];
        out[4] = a[4];
        out[5] = a[7];
        out[6] = a[2];
        out[7] = a[5];
        out[8] = a[8];
    }

    return out;
};

/**
 * Inverts a mat3
 *
 * @param {mat3} out the receiving matrix
 * @param {mat3} a the source matrix
 * @returns {mat3} out
 */
mat3.invert = function(out, a) {
    var a00 = a[0], a01 = a[1], a02 = a[2],
        a10 = a[3], a11 = a[4], a12 = a[5],
        a20 = a[6], a21 = a[7], a22 = a[8],

        b01 = a22 * a11 - a12 * a21,
        b11 = -a22 * a10 + a12 * a20,
        b21 = a21 * a10 - a11 * a20,

    // Calculate the determinant
        det = a00 * b01 + a01 * b11 + a02 * b21;

    if (!det) {
        return null;
    }
    det = 1.0 / det;

    out[0] = b01 * det;
    out[1] = (-a22 * a01 + a02 * a21) * det;
    out[2] = (a12 * a01 - a02 * a11) * det;
    out[3] = b11 * det;
    out[4] = (a22 * a00 - a02 * a20) * det;
    out[5] = (-a12 * a00 + a02 * a10) * det;
    out[6] = b21 * det;
    out[7] = (-a21 * a00 + a01 * a20) * det;
    out[8] = (a11 * a00 - a01 * a10) * det;
    return out;
};

/**
 * Calculates the adjugate of a mat3
 *
 * @param {mat3} out the receiving matrix
 * @param {mat3} a the source matrix
 * @returns {mat3} out
 */
mat3.adjoint = function(out, a) {
    var a00 = a[0], a01 = a[1], a02 = a[2],
        a10 = a[3], a11 = a[4], a12 = a[5],
        a20 = a[6], a21 = a[7], a22 = a[8];

    out[0] = (a11 * a22 - a12 * a21);
    out[1] = (a02 * a21 - a01 * a22);
    out[2] = (a01 * a12 - a02 * a11);
    out[3] = (a12 * a20 - a10 * a22);
    out[4] = (a00 * a22 - a02 * a20);
    out[5] = (a02 * a10 - a00 * a12);
    out[6] = (a10 * a21 - a11 * a20);
    out[7] = (a01 * a20 - a00 * a21);
    out[8] = (a00 * a11 - a01 * a10);
    return out;
};

/**
 * Calculates the determinant of a mat3
 *
 * @param {mat3} a the source matrix
 * @returns {Number} determinant of a
 */
mat3.determinant = function (a) {
    var a00 = a[0], a01 = a[1], a02 = a[2],
        a10 = a[3], a11 = a[4], a12 = a[5],
        a20 = a[6], a21 = a[7], a22 = a[8];

    return a00 * (a22 * a11 - a12 * a21) + a01 * (-a22 * a10 + a12 * a20) + a02 * (a21 * a10 - a11 * a20);
};

/**
 * Multiplies two mat3's
 *
 * @param {mat3} out the receiving matrix
 * @param {mat3} a the first operand
 * @param {mat3} b the second operand
 * @returns {mat3} out
 */
mat3.multiply = function (out, a, b) {
    var a00 = a[0], a01 = a[1], a02 = a[2],
        a10 = a[3], a11 = a[4], a12 = a[5],
        a20 = a[6], a21 = a[7], a22 = a[8],

        b00 = b[0], b01 = b[1], b02 = b[2],
        b10 = b[3], b11 = b[4], b12 = b[5],
        b20 = b[6], b21 = b[7], b22 = b[8];

    out[0] = b00 * a00 + b01 * a10 + b02 * a20;
    out[1] = b00 * a01 + b01 * a11 + b02 * a21;
    out[2] = b00 * a02 + b01 * a12 + b02 * a22;

    out[3] = b10 * a00 + b11 * a10 + b12 * a20;
    out[4] = b10 * a01 + b11 * a11 + b12 * a21;
    out[5] = b10 * a02 + b11 * a12 + b12 * a22;

    out[6] = b20 * a00 + b21 * a10 + b22 * a20;
    out[7] = b20 * a01 + b21 * a11 + b22 * a21;
    out[8] = b20 * a02 + b21 * a12 + b22 * a22;
    return out;
};

/**
 * Alias for {@link mat3.multiply}
 * @function
 */
mat3.mul = mat3.multiply;

/**
 * Translate a mat3 by the given vector
 *
 * @param {mat3} out the receiving matrix
 * @param {mat3} a the matrix to translate
 * @param {vec2} v vector to translate by
 * @returns {mat3} out
 */
mat3.translate = function(out, a, v) {
    var a00 = a[0], a01 = a[1], a02 = a[2],
        a10 = a[3], a11 = a[4], a12 = a[5],
        a20 = a[6], a21 = a[7], a22 = a[8],
        x = v[0], y = v[1];

    out[0] = a00;
    out[1] = a01;
    out[2] = a02;

    out[3] = a10;
    out[4] = a11;
    out[5] = a12;

    out[6] = x * a00 + y * a10 + a20;
    out[7] = x * a01 + y * a11 + a21;
    out[8] = x * a02 + y * a12 + a22;
    return out;
};

/**
 * Rotates a mat3 by the given angle
 *
 * @param {mat3} out the receiving matrix
 * @param {mat3} a the matrix to rotate
 * @param {Number} rad the angle to rotate the matrix by
 * @returns {mat3} out
 */
mat3.rotate = function (out, a, rad) {
    var a00 = a[0], a01 = a[1], a02 = a[2],
        a10 = a[3], a11 = a[4], a12 = a[5],
        a20 = a[6], a21 = a[7], a22 = a[8],

        s = Math.sin(rad),
        c = Math.cos(rad);

    out[0] = c * a00 + s * a10;
    out[1] = c * a01 + s * a11;
    out[2] = c * a02 + s * a12;

    out[3] = c * a10 - s * a00;
    out[4] = c * a11 - s * a01;
    out[5] = c * a12 - s * a02;

    out[6] = a20;
    out[7] = a21;
    out[8] = a22;
    return out;
};

/**
 * Scales the mat3 by the dimensions in the given vec2
 *
 * @param {mat3} out the receiving matrix
 * @param {mat3} a the matrix to rotate
 * @param {vec2} v the vec2 to scale the matrix by
 * @returns {mat3} out
 **/
mat3.scale = function(out, a, v) {
    var x = v[0], y = v[1];

    out[0] = x * a[0];
    out[1] = x * a[1];
    out[2] = x * a[2];

    out[3] = y * a[3];
    out[4] = y * a[4];
    out[5] = y * a[5];

    out[6] = a[6];
    out[7] = a[7];
    out[8] = a[8];
    return out;
};

/**
 * Copies the values from a mat2d into a mat3
 *
 * @param {mat3} out the receiving matrix
 * @param {mat2d} a the matrix to copy
 * @returns {mat3} out
 **/
mat3.fromMat2d = function(out, a) {
    out[0] = a[0];
    out[1] = a[1];
    out[2] = 0;

    out[3] = a[2];
    out[4] = a[3];
    out[5] = 0;

    out[6] = a[4];
    out[7] = a[5];
    out[8] = 1;
    return out;
};

/**
 * Calculates a 3x3 matrix from the given quaternion
 *
 * @param {mat3} out mat3 receiving operation result
 * @param {quat} q Quaternion to create matrix from
 *
 * @returns {mat3} out
 */
mat3.fromQuat = function (out, q) {
    var x = q[0], y = q[1], z = q[2], w = q[3],
        x2 = x + x,
        y2 = y + y,
        z2 = z + z,

        xx = x * x2,
        yx = y * x2,
        yy = y * y2,
        zx = z * x2,
        zy = z * y2,
        zz = z * z2,
        wx = w * x2,
        wy = w * y2,
        wz = w * z2;

    out[0] = 1 - yy - zz;
    out[3] = yx - wz;
    out[6] = zx + wy;

    out[1] = yx + wz;
    out[4] = 1 - xx - zz;
    out[7] = zy - wx;

    out[2] = zx - wy;
    out[5] = zy + wx;
    out[8] = 1 - xx - yy;

    return out;
};

/**
 * Calculates a 3x3 normal matrix (transpose inverse) from the 4x4 matrix
 *
 * @param {mat3} out mat3 receiving operation result
 * @param {mat4} a Mat4 to derive the normal matrix from
 *
 * @returns {mat3} out
 */
mat3.normalFromMat4 = function (out, a) {
    var a00 = a[0], a01 = a[1], a02 = a[2], a03 = a[3],
        a10 = a[4], a11 = a[5], a12 = a[6], a13 = a[7],
        a20 = a[8], a21 = a[9], a22 = a[10], a23 = a[11],
        a30 = a[12], a31 = a[13], a32 = a[14], a33 = a[15],

        b00 = a00 * a11 - a01 * a10,
        b01 = a00 * a12 - a02 * a10,
        b02 = a00 * a13 - a03 * a10,
        b03 = a01 * a12 - a02 * a11,
        b04 = a01 * a13 - a03 * a11,
        b05 = a02 * a13 - a03 * a12,
        b06 = a20 * a31 - a21 * a30,
        b07 = a20 * a32 - a22 * a30,
        b08 = a20 * a33 - a23 * a30,
        b09 = a21 * a32 - a22 * a31,
        b10 = a21 * a33 - a23 * a31,
        b11 = a22 * a33 - a23 * a32,

    // Calculate the determinant
        det = b00 * b11 - b01 * b10 + b02 * b09 + b03 * b08 - b04 * b07 + b05 * b06;

    if (!det) {
        return null;
    }
    det = 1.0 / det;

    out[0] = (a11 * b11 - a12 * b10 + a13 * b09) * det;
    out[1] = (a12 * b08 - a10 * b11 - a13 * b07) * det;
    out[2] = (a10 * b10 - a11 * b08 + a13 * b06) * det;

    out[3] = (a02 * b10 - a01 * b11 - a03 * b09) * det;
    out[4] = (a00 * b11 - a02 * b08 + a03 * b07) * det;
    out[5] = (a01 * b08 - a00 * b10 - a03 * b06) * det;

    out[6] = (a31 * b05 - a32 * b04 + a33 * b03) * det;
    out[7] = (a32 * b02 - a30 * b05 - a33 * b01) * det;
    out[8] = (a30 * b04 - a31 * b02 + a33 * b00) * det;

    return out;
};

/**
 * Returns a string representation of a mat3
 *
 * @param {mat3} mat matrix to represent as a string
 * @returns {String} string representation of the matrix
 */
mat3.str = function (a) {
    return 'mat3(' + a[0] + ', ' + a[1] + ', ' + a[2] + ', ' +
        a[3] + ', ' + a[4] + ', ' + a[5] + ', ' +
        a[6] + ', ' + a[7] + ', ' + a[8] + ')';
};

/**
 * Returns Frobenius norm of a mat3
 *
 * @param {mat3} a the matrix to calculate Frobenius norm of
 * @returns {Number} Frobenius norm
 */
mat3.frob = function (a) {
    return(Math.sqrt(Math.pow(a[0], 2) + Math.pow(a[1], 2) + Math.pow(a[2], 2) + Math.pow(a[3], 2) + Math.pow(a[4], 2) + Math.pow(a[5], 2) + Math.pow(a[6], 2) + Math.pow(a[7], 2) + Math.pow(a[8], 2)))
};


if(typeof(exports) !== 'undefined') {
    exports.mat3 = mat3;
}
/* Copyright (c) 2013, Brandon Jones, Colin MacKenzie IV. All rights reserved.

 Redistribution and use in source and binary forms, with or without modification,
 are permitted provided that the following conditions are met:

 * Redistributions of source code must retain the above copyright notice, this
 list of conditions and the following disclaimer.
 * Redistributions in binary form must reproduce the above copyright notice,
 this list of conditions and the following disclaimer in the documentation
 and/or other materials provided with the distribution.

 THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
 ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR
 ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON
 ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE. */

/**
 * @class 4x4 Matrix
 * @name mat4
 */
var mat4 = {};

/**
 * Creates a new identity mat4
 *
 * @returns {mat4} a new 4x4 matrix
 */
mat4.create = function() {
    var out = new GLMAT_ARRAY_TYPE(16);
    out[0] = 1;
    out[1] = 0;
    out[2] = 0;
    out[3] = 0;
    out[4] = 0;
    out[5] = 1;
    out[6] = 0;
    out[7] = 0;
    out[8] = 0;
    out[9] = 0;
    out[10] = 1;
    out[11] = 0;
    out[12] = 0;
    out[13] = 0;
    out[14] = 0;
    out[15] = 1;
    return out;
};

/**
 * Creates a new mat4 initialized with values from an existing matrix
 *
 * @param {mat4} a matrix to clone
 * @returns {mat4} a new 4x4 matrix
 */
mat4.clone = function(a) {
    var out = new GLMAT_ARRAY_TYPE(16);
    out[0] = a[0];
    out[1] = a[1];
    out[2] = a[2];
    out[3] = a[3];
    out[4] = a[4];
    out[5] = a[5];
    out[6] = a[6];
    out[7] = a[7];
    out[8] = a[8];
    out[9] = a[9];
    out[10] = a[10];
    out[11] = a[11];
    out[12] = a[12];
    out[13] = a[13];
    out[14] = a[14];
    out[15] = a[15];
    return out;
};

/**
 * Copy the values from one mat4 to another
 *
 * @param {mat4} out the receiving matrix
 * @param {mat4} a the source matrix
 * @returns {mat4} out
 */
mat4.copy = function(out, a) {
    out[0] = a[0];
    out[1] = a[1];
    out[2] = a[2];
    out[3] = a[3];
    out[4] = a[4];
    out[5] = a[5];
    out[6] = a[6];
    out[7] = a[7];
    out[8] = a[8];
    out[9] = a[9];
    out[10] = a[10];
    out[11] = a[11];
    out[12] = a[12];
    out[13] = a[13];
    out[14] = a[14];
    out[15] = a[15];
    return out;
};

/**
 * Set a mat4 to the identity matrix
 *
 * @param {mat4} out the receiving matrix
 * @returns {mat4} out
 */
mat4.identity = function(out) {
    out[0] = 1;
    out[1] = 0;
    out[2] = 0;
    out[3] = 0;
    out[4] = 0;
    out[5] = 1;
    out[6] = 0;
    out[7] = 0;
    out[8] = 0;
    out[9] = 0;
    out[10] = 1;
    out[11] = 0;
    out[12] = 0;
    out[13] = 0;
    out[14] = 0;
    out[15] = 1;
    return out;
};

/**
 * Transpose the values of a mat4
 *
 * @param {mat4} out the receiving matrix
 * @param {mat4} a the source matrix
 * @returns {mat4} out
 */
mat4.transpose = function(out, a) {
    // If we are transposing ourselves we can skip a few steps but have to cache some values
    if (out === a) {
        var a01 = a[1], a02 = a[2], a03 = a[3],
            a12 = a[6], a13 = a[7],
            a23 = a[11];

        out[1] = a[4];
        out[2] = a[8];
        out[3] = a[12];
        out[4] = a01;
        out[6] = a[9];
        out[7] = a[13];
        out[8] = a02;
        out[9] = a12;
        out[11] = a[14];
        out[12] = a03;
        out[13] = a13;
        out[14] = a23;
    } else {
        out[0] = a[0];
        out[1] = a[4];
        out[2] = a[8];
        out[3] = a[12];
        out[4] = a[1];
        out[5] = a[5];
        out[6] = a[9];
        out[7] = a[13];
        out[8] = a[2];
        out[9] = a[6];
        out[10] = a[10];
        out[11] = a[14];
        out[12] = a[3];
        out[13] = a[7];
        out[14] = a[11];
        out[15] = a[15];
    }

    return out;
};

/**
 * Inverts a mat4
 *
 * @param {mat4} out the receiving matrix
 * @param {mat4} a the source matrix
 * @returns {mat4} out
 */
mat4.invert = function(out, a) {
    var a00 = a[0], a01 = a[1], a02 = a[2], a03 = a[3],
        a10 = a[4], a11 = a[5], a12 = a[6], a13 = a[7],
        a20 = a[8], a21 = a[9], a22 = a[10], a23 = a[11],
        a30 = a[12], a31 = a[13], a32 = a[14], a33 = a[15],

        b00 = a00 * a11 - a01 * a10,
        b01 = a00 * a12 - a02 * a10,
        b02 = a00 * a13 - a03 * a10,
        b03 = a01 * a12 - a02 * a11,
        b04 = a01 * a13 - a03 * a11,
        b05 = a02 * a13 - a03 * a12,
        b06 = a20 * a31 - a21 * a30,
        b07 = a20 * a32 - a22 * a30,
        b08 = a20 * a33 - a23 * a30,
        b09 = a21 * a32 - a22 * a31,
        b10 = a21 * a33 - a23 * a31,
        b11 = a22 * a33 - a23 * a32,

    // Calculate the determinant
        det = b00 * b11 - b01 * b10 + b02 * b09 + b03 * b08 - b04 * b07 + b05 * b06;

    if (!det) {
        return null;
    }
    det = 1.0 / det;

    out[0] = (a11 * b11 - a12 * b10 + a13 * b09) * det;
    out[1] = (a02 * b10 - a01 * b11 - a03 * b09) * det;
    out[2] = (a31 * b05 - a32 * b04 + a33 * b03) * det;
    out[3] = (a22 * b04 - a21 * b05 - a23 * b03) * det;
    out[4] = (a12 * b08 - a10 * b11 - a13 * b07) * det;
    out[5] = (a00 * b11 - a02 * b08 + a03 * b07) * det;
    out[6] = (a32 * b02 - a30 * b05 - a33 * b01) * det;
    out[7] = (a20 * b05 - a22 * b02 + a23 * b01) * det;
    out[8] = (a10 * b10 - a11 * b08 + a13 * b06) * det;
    out[9] = (a01 * b08 - a00 * b10 - a03 * b06) * det;
    out[10] = (a30 * b04 - a31 * b02 + a33 * b00) * det;
    out[11] = (a21 * b02 - a20 * b04 - a23 * b00) * det;
    out[12] = (a11 * b07 - a10 * b09 - a12 * b06) * det;
    out[13] = (a00 * b09 - a01 * b07 + a02 * b06) * det;
    out[14] = (a31 * b01 - a30 * b03 - a32 * b00) * det;
    out[15] = (a20 * b03 - a21 * b01 + a22 * b00) * det;

    return out;
};

/**
 * Calculates the adjugate of a mat4
 *
 * @param {mat4} out the receiving matrix
 * @param {mat4} a the source matrix
 * @returns {mat4} out
 */
mat4.adjoint = function(out, a) {
    var a00 = a[0], a01 = a[1], a02 = a[2], a03 = a[3],
        a10 = a[4], a11 = a[5], a12 = a[6], a13 = a[7],
        a20 = a[8], a21 = a[9], a22 = a[10], a23 = a[11],
        a30 = a[12], a31 = a[13], a32 = a[14], a33 = a[15];

    out[0]  =  (a11 * (a22 * a33 - a23 * a32) - a21 * (a12 * a33 - a13 * a32) + a31 * (a12 * a23 - a13 * a22));
    out[1]  = -(a01 * (a22 * a33 - a23 * a32) - a21 * (a02 * a33 - a03 * a32) + a31 * (a02 * a23 - a03 * a22));
    out[2]  =  (a01 * (a12 * a33 - a13 * a32) - a11 * (a02 * a33 - a03 * a32) + a31 * (a02 * a13 - a03 * a12));
    out[3]  = -(a01 * (a12 * a23 - a13 * a22) - a11 * (a02 * a23 - a03 * a22) + a21 * (a02 * a13 - a03 * a12));
    out[4]  = -(a10 * (a22 * a33 - a23 * a32) - a20 * (a12 * a33 - a13 * a32) + a30 * (a12 * a23 - a13 * a22));
    out[5]  =  (a00 * (a22 * a33 - a23 * a32) - a20 * (a02 * a33 - a03 * a32) + a30 * (a02 * a23 - a03 * a22));
    out[6]  = -(a00 * (a12 * a33 - a13 * a32) - a10 * (a02 * a33 - a03 * a32) + a30 * (a02 * a13 - a03 * a12));
    out[7]  =  (a00 * (a12 * a23 - a13 * a22) - a10 * (a02 * a23 - a03 * a22) + a20 * (a02 * a13 - a03 * a12));
    out[8]  =  (a10 * (a21 * a33 - a23 * a31) - a20 * (a11 * a33 - a13 * a31) + a30 * (a11 * a23 - a13 * a21));
    out[9]  = -(a00 * (a21 * a33 - a23 * a31) - a20 * (a01 * a33 - a03 * a31) + a30 * (a01 * a23 - a03 * a21));
    out[10] =  (a00 * (a11 * a33 - a13 * a31) - a10 * (a01 * a33 - a03 * a31) + a30 * (a01 * a13 - a03 * a11));
    out[11] = -(a00 * (a11 * a23 - a13 * a21) - a10 * (a01 * a23 - a03 * a21) + a20 * (a01 * a13 - a03 * a11));
    out[12] = -(a10 * (a21 * a32 - a22 * a31) - a20 * (a11 * a32 - a12 * a31) + a30 * (a11 * a22 - a12 * a21));
    out[13] =  (a00 * (a21 * a32 - a22 * a31) - a20 * (a01 * a32 - a02 * a31) + a30 * (a01 * a22 - a02 * a21));
    out[14] = -(a00 * (a11 * a32 - a12 * a31) - a10 * (a01 * a32 - a02 * a31) + a30 * (a01 * a12 - a02 * a11));
    out[15] =  (a00 * (a11 * a22 - a12 * a21) - a10 * (a01 * a22 - a02 * a21) + a20 * (a01 * a12 - a02 * a11));
    return out;
};

/**
 * Calculates the determinant of a mat4
 *
 * @param {mat4} a the source matrix
 * @returns {Number} determinant of a
 */
mat4.determinant = function (a) {
    var a00 = a[0], a01 = a[1], a02 = a[2], a03 = a[3],
        a10 = a[4], a11 = a[5], a12 = a[6], a13 = a[7],
        a20 = a[8], a21 = a[9], a22 = a[10], a23 = a[11],
        a30 = a[12], a31 = a[13], a32 = a[14], a33 = a[15],

        b00 = a00 * a11 - a01 * a10,
        b01 = a00 * a12 - a02 * a10,
        b02 = a00 * a13 - a03 * a10,
        b03 = a01 * a12 - a02 * a11,
        b04 = a01 * a13 - a03 * a11,
        b05 = a02 * a13 - a03 * a12,
        b06 = a20 * a31 - a21 * a30,
        b07 = a20 * a32 - a22 * a30,
        b08 = a20 * a33 - a23 * a30,
        b09 = a21 * a32 - a22 * a31,
        b10 = a21 * a33 - a23 * a31,
        b11 = a22 * a33 - a23 * a32;

    // Calculate the determinant
    return b00 * b11 - b01 * b10 + b02 * b09 + b03 * b08 - b04 * b07 + b05 * b06;
};

/**
 * Multiplies two mat4's
 *
 * @param {mat4} out the receiving matrix
 * @param {mat4} a the first operand
 * @param {mat4} b the second operand
 * @returns {mat4} out
 */
mat4.multiply = function (out, a, b) {
    var a00 = a[0], a01 = a[1], a02 = a[2], a03 = a[3],
        a10 = a[4], a11 = a[5], a12 = a[6], a13 = a[7],
        a20 = a[8], a21 = a[9], a22 = a[10], a23 = a[11],
        a30 = a[12], a31 = a[13], a32 = a[14], a33 = a[15];

    // Cache only the current line of the second matrix
    var b0  = b[0], b1 = b[1], b2 = b[2], b3 = b[3];
    out[0] = b0*a00 + b1*a10 + b2*a20 + b3*a30;
    out[1] = b0*a01 + b1*a11 + b2*a21 + b3*a31;
    out[2] = b0*a02 + b1*a12 + b2*a22 + b3*a32;
    out[3] = b0*a03 + b1*a13 + b2*a23 + b3*a33;

    b0 = b[4]; b1 = b[5]; b2 = b[6]; b3 = b[7];
    out[4] = b0*a00 + b1*a10 + b2*a20 + b3*a30;
    out[5] = b0*a01 + b1*a11 + b2*a21 + b3*a31;
    out[6] = b0*a02 + b1*a12 + b2*a22 + b3*a32;
    out[7] = b0*a03 + b1*a13 + b2*a23 + b3*a33;

    b0 = b[8]; b1 = b[9]; b2 = b[10]; b3 = b[11];
    out[8] = b0*a00 + b1*a10 + b2*a20 + b3*a30;
    out[9] = b0*a01 + b1*a11 + b2*a21 + b3*a31;
    out[10] = b0*a02 + b1*a12 + b2*a22 + b3*a32;
    out[11] = b0*a03 + b1*a13 + b2*a23 + b3*a33;

    b0 = b[12]; b1 = b[13]; b2 = b[14]; b3 = b[15];
    out[12] = b0*a00 + b1*a10 + b2*a20 + b3*a30;
    out[13] = b0*a01 + b1*a11 + b2*a21 + b3*a31;
    out[14] = b0*a02 + b1*a12 + b2*a22 + b3*a32;
    out[15] = b0*a03 + b1*a13 + b2*a23 + b3*a33;
    return out;
};

/**
 * Alias for {@link mat4.multiply}
 * @function
 */
mat4.mul = mat4.multiply;

/**
 * Translate a mat4 by the given vector
 *
 * @param {mat4} out the receiving matrix
 * @param {mat4} a the matrix to translate
 * @param {vec3} v vector to translate by
 * @returns {mat4} out
 */
mat4.translate = function (out, a, v) {
    var x = v[0], y = v[1], z = v[2],
        a00, a01, a02, a03,
        a10, a11, a12, a13,
        a20, a21, a22, a23;

    if (a === out) {
        out[12] = a[0] * x + a[4] * y + a[8] * z + a[12];
        out[13] = a[1] * x + a[5] * y + a[9] * z + a[13];
        out[14] = a[2] * x + a[6] * y + a[10] * z + a[14];
        out[15] = a[3] * x + a[7] * y + a[11] * z + a[15];
    } else {
        a00 = a[0]; a01 = a[1]; a02 = a[2]; a03 = a[3];
        a10 = a[4]; a11 = a[5]; a12 = a[6]; a13 = a[7];
        a20 = a[8]; a21 = a[9]; a22 = a[10]; a23 = a[11];

        out[0] = a00; out[1] = a01; out[2] = a02; out[3] = a03;
        out[4] = a10; out[5] = a11; out[6] = a12; out[7] = a13;
        out[8] = a20; out[9] = a21; out[10] = a22; out[11] = a23;

        out[12] = a00 * x + a10 * y + a20 * z + a[12];
        out[13] = a01 * x + a11 * y + a21 * z + a[13];
        out[14] = a02 * x + a12 * y + a22 * z + a[14];
        out[15] = a03 * x + a13 * y + a23 * z + a[15];
    }

    return out;
};

/**
 * Scales the mat4 by the dimensions in the given vec3
 *
 * @param {mat4} out the receiving matrix
 * @param {mat4} a the matrix to scale
 * @param {vec3} v the vec3 to scale the matrix by
 * @returns {mat4} out
 **/
mat4.scale = function(out, a, v) {
    var x = v[0], y = v[1], z = v[2];

    out[0] = a[0] * x;
    out[1] = a[1] * x;
    out[2] = a[2] * x;
    out[3] = a[3] * x;
    out[4] = a[4] * y;
    out[5] = a[5] * y;
    out[6] = a[6] * y;
    out[7] = a[7] * y;
    out[8] = a[8] * z;
    out[9] = a[9] * z;
    out[10] = a[10] * z;
    out[11] = a[11] * z;
    out[12] = a[12];
    out[13] = a[13];
    out[14] = a[14];
    out[15] = a[15];
    return out;
};

/**
 * Rotates a mat4 by the given angle
 *
 * @param {mat4} out the receiving matrix
 * @param {mat4} a the matrix to rotate
 * @param {Number} rad the angle to rotate the matrix by
 * @param {vec3} axis the axis to rotate around
 * @returns {mat4} out
 */
mat4.rotate = function (out, a, rad, axis) {
    var x = axis[0], y = axis[1], z = axis[2],
        len = Math.sqrt(x * x + y * y + z * z),
        s, c, t,
        a00, a01, a02, a03,
        a10, a11, a12, a13,
        a20, a21, a22, a23,
        b00, b01, b02,
        b10, b11, b12,
        b20, b21, b22;

    if (Math.abs(len) < GLMAT_EPSILON) { return null; }

    len = 1 / len;
    x *= len;
    y *= len;
    z *= len;

    s = Math.sin(rad);
    c = Math.cos(rad);
    t = 1 - c;

    a00 = a[0]; a01 = a[1]; a02 = a[2]; a03 = a[3];
    a10 = a[4]; a11 = a[5]; a12 = a[6]; a13 = a[7];
    a20 = a[8]; a21 = a[9]; a22 = a[10]; a23 = a[11];

    // Construct the elements of the rotation matrix
    b00 = x * x * t + c; b01 = y * x * t + z * s; b02 = z * x * t - y * s;
    b10 = x * y * t - z * s; b11 = y * y * t + c; b12 = z * y * t + x * s;
    b20 = x * z * t + y * s; b21 = y * z * t - x * s; b22 = z * z * t + c;

    // Perform rotation-specific matrix multiplication
    out[0] = a00 * b00 + a10 * b01 + a20 * b02;
    out[1] = a01 * b00 + a11 * b01 + a21 * b02;
    out[2] = a02 * b00 + a12 * b01 + a22 * b02;
    out[3] = a03 * b00 + a13 * b01 + a23 * b02;
    out[4] = a00 * b10 + a10 * b11 + a20 * b12;
    out[5] = a01 * b10 + a11 * b11 + a21 * b12;
    out[6] = a02 * b10 + a12 * b11 + a22 * b12;
    out[7] = a03 * b10 + a13 * b11 + a23 * b12;
    out[8] = a00 * b20 + a10 * b21 + a20 * b22;
    out[9] = a01 * b20 + a11 * b21 + a21 * b22;
    out[10] = a02 * b20 + a12 * b21 + a22 * b22;
    out[11] = a03 * b20 + a13 * b21 + a23 * b22;

    if (a !== out) { // If the source and destination differ, copy the unchanged last row
        out[12] = a[12];
        out[13] = a[13];
        out[14] = a[14];
        out[15] = a[15];
    }
    return out;
};

/**
 * Rotates a matrix by the given angle around the X axis
 *
 * @param {mat4} out the receiving matrix
 * @param {mat4} a the matrix to rotate
 * @param {Number} rad the angle to rotate the matrix by
 * @returns {mat4} out
 */
mat4.rotateX = function (out, a, rad) {
    var s = Math.sin(rad),
        c = Math.cos(rad),
        a10 = a[4],
        a11 = a[5],
        a12 = a[6],
        a13 = a[7],
        a20 = a[8],
        a21 = a[9],
        a22 = a[10],
        a23 = a[11];

    if (a !== out) { // If the source and destination differ, copy the unchanged rows
        out[0]  = a[0];
        out[1]  = a[1];
        out[2]  = a[2];
        out[3]  = a[3];
        out[12] = a[12];
        out[13] = a[13];
        out[14] = a[14];
        out[15] = a[15];
    }

    // Perform axis-specific matrix multiplication
    out[4] = a10 * c + a20 * s;
    out[5] = a11 * c + a21 * s;
    out[6] = a12 * c + a22 * s;
    out[7] = a13 * c + a23 * s;
    out[8] = a20 * c - a10 * s;
    out[9] = a21 * c - a11 * s;
    out[10] = a22 * c - a12 * s;
    out[11] = a23 * c - a13 * s;
    return out;
};

/**
 * Rotates a matrix by the given angle around the Y axis
 *
 * @param {mat4} out the receiving matrix
 * @param {mat4} a the matrix to rotate
 * @param {Number} rad the angle to rotate the matrix by
 * @returns {mat4} out
 */
mat4.rotateY = function (out, a, rad) {
    var s = Math.sin(rad),
        c = Math.cos(rad),
        a00 = a[0],
        a01 = a[1],
        a02 = a[2],
        a03 = a[3],
        a20 = a[8],
        a21 = a[9],
        a22 = a[10],
        a23 = a[11];

    if (a !== out) { // If the source and destination differ, copy the unchanged rows
        out[4]  = a[4];
        out[5]  = a[5];
        out[6]  = a[6];
        out[7]  = a[7];
        out[12] = a[12];
        out[13] = a[13];
        out[14] = a[14];
        out[15] = a[15];
    }

    // Perform axis-specific matrix multiplication
    out[0] = a00 * c - a20 * s;
    out[1] = a01 * c - a21 * s;
    out[2] = a02 * c - a22 * s;
    out[3] = a03 * c - a23 * s;
    out[8] = a00 * s + a20 * c;
    out[9] = a01 * s + a21 * c;
    out[10] = a02 * s + a22 * c;
    out[11] = a03 * s + a23 * c;
    return out;
};

/**
 * Rotates a matrix by the given angle around the Z axis
 *
 * @param {mat4} out the receiving matrix
 * @param {mat4} a the matrix to rotate
 * @param {Number} rad the angle to rotate the matrix by
 * @returns {mat4} out
 */
mat4.rotateZ = function (out, a, rad) {
    var s = Math.sin(rad),
        c = Math.cos(rad),
        a00 = a[0],
        a01 = a[1],
        a02 = a[2],
        a03 = a[3],
        a10 = a[4],
        a11 = a[5],
        a12 = a[6],
        a13 = a[7];

    if (a !== out) { // If the source and destination differ, copy the unchanged last row
        out[8]  = a[8];
        out[9]  = a[9];
        out[10] = a[10];
        out[11] = a[11];
        out[12] = a[12];
        out[13] = a[13];
        out[14] = a[14];
        out[15] = a[15];
    }

    // Perform axis-specific matrix multiplication
    out[0] = a00 * c + a10 * s;
    out[1] = a01 * c + a11 * s;
    out[2] = a02 * c + a12 * s;
    out[3] = a03 * c + a13 * s;
    out[4] = a10 * c - a00 * s;
    out[5] = a11 * c - a01 * s;
    out[6] = a12 * c - a02 * s;
    out[7] = a13 * c - a03 * s;
    return out;
};

/**
 * Creates a matrix from a quaternion rotation and vector translation
 * This is equivalent to (but much faster than):
 *
 *     mat4.identity(dest);
 *     mat4.translate(dest, vec);
 *     var quatMat = mat4.create();
 *     quat4.toMat4(quat, quatMat);
 *     mat4.multiply(dest, quatMat);
 *
 * @param {mat4} out mat4 receiving operation result
 * @param {quat4} q Rotation quaternion
 * @param {vec3} v Translation vector
 * @returns {mat4} out
 */
mat4.fromRotationTranslation = function (out, q, v) {
    // Quaternion math
    var x = q[0], y = q[1], z = q[2], w = q[3],
        x2 = x + x,
        y2 = y + y,
        z2 = z + z,

        xx = x * x2,
        xy = x * y2,
        xz = x * z2,
        yy = y * y2,
        yz = y * z2,
        zz = z * z2,
        wx = w * x2,
        wy = w * y2,
        wz = w * z2;

    out[0] = 1 - (yy + zz);
    out[1] = xy + wz;
    out[2] = xz - wy;
    out[3] = 0;
    out[4] = xy - wz;
    out[5] = 1 - (xx + zz);
    out[6] = yz + wx;
    out[7] = 0;
    out[8] = xz + wy;
    out[9] = yz - wx;
    out[10] = 1 - (xx + yy);
    out[11] = 0;
    out[12] = v[0];
    out[13] = v[1];
    out[14] = v[2];
    out[15] = 1;

    return out;
};

mat4.fromQuat = function (out, q) {
    var x = q[0], y = q[1], z = q[2], w = q[3],
        x2 = x + x,
        y2 = y + y,
        z2 = z + z,

        xx = x * x2,
        yx = y * x2,
        yy = y * y2,
        zx = z * x2,
        zy = z * y2,
        zz = z * z2,
        wx = w * x2,
        wy = w * y2,
        wz = w * z2;

    out[0] = 1 - yy - zz;
    out[1] = yx + wz;
    out[2] = zx - wy;
    out[3] = 0;

    out[4] = yx - wz;
    out[5] = 1 - xx - zz;
    out[6] = zy + wx;
    out[7] = 0;

    out[8] = zx + wy;
    out[9] = zy - wx;
    out[10] = 1 - xx - yy;
    out[11] = 0;

    out[12] = 0;
    out[13] = 0;
    out[14] = 0;
    out[15] = 1;

    return out;
};

/**
 * Generates a frustum matrix with the given bounds
 *
 * @param {mat4} out mat4 frustum matrix will be written into
 * @param {Number} left Left bound of the frustum
 * @param {Number} right Right bound of the frustum
 * @param {Number} bottom Bottom bound of the frustum
 * @param {Number} top Top bound of the frustum
 * @param {Number} near Near bound of the frustum
 * @param {Number} far Far bound of the frustum
 * @returns {mat4} out
 */
mat4.frustum = function (out, left, right, bottom, top, near, far) {
    var rl = 1 / (right - left),
        tb = 1 / (top - bottom),
        nf = 1 / (near - far);
    out[0] = (near * 2) * rl;
    out[1] = 0;
    out[2] = 0;
    out[3] = 0;
    out[4] = 0;
    out[5] = (near * 2) * tb;
    out[6] = 0;
    out[7] = 0;
    out[8] = (right + left) * rl;
    out[9] = (top + bottom) * tb;
    out[10] = (far + near) * nf;
    out[11] = -1;
    out[12] = 0;
    out[13] = 0;
    out[14] = (far * near * 2) * nf;
    out[15] = 0;
    return out;
};

/**
 * Generates a perspective projection matrix with the given bounds
 *
 * @param {mat4} out mat4 frustum matrix will be written into
 * @param {number} fovy Vertical field of view in radians
 * @param {number} aspect Aspect ratio. typically viewport width/height
 * @param {number} near Near bound of the frustum
 * @param {number} far Far bound of the frustum
 * @returns {mat4} out
 */
mat4.perspective = function (out, fovy, aspect, near, far) {
    var f = 1.0 / Math.tan(fovy / 2),
        nf = 1 / (near - far);
    out[0] = f / aspect;
    out[1] = 0;
    out[2] = 0;
    out[3] = 0;
    out[4] = 0;
    out[5] = f;
    out[6] = 0;
    out[7] = 0;
    out[8] = 0;
    out[9] = 0;
    out[10] = (far + near) * nf;
    out[11] = -1;
    out[12] = 0;
    out[13] = 0;
    out[14] = (2 * far * near) * nf;
    out[15] = 0;
    return out;
};

/**
 * Generates a orthogonal projection matrix with the given bounds
 *
 * @param {mat4} out mat4 frustum matrix will be written into
 * @param {number} left Left bound of the frustum
 * @param {number} right Right bound of the frustum
 * @param {number} bottom Bottom bound of the frustum
 * @param {number} top Top bound of the frustum
 * @param {number} near Near bound of the frustum
 * @param {number} far Far bound of the frustum
 * @returns {mat4} out
 */
mat4.ortho = function (out, left, right, bottom, top, near, far) {
    var lr = 1 / (left - right),
        bt = 1 / (bottom - top),
        nf = 1 / (near - far);
    out[0] = -2 * lr;
    out[1] = 0;
    out[2] = 0;
    out[3] = 0;
    out[4] = 0;
    out[5] = -2 * bt;
    out[6] = 0;
    out[7] = 0;
    out[8] = 0;
    out[9] = 0;
    out[10] = 2 * nf;
    out[11] = 0;
    out[12] = (left + right) * lr;
    out[13] = (top + bottom) * bt;
    out[14] = (far + near) * nf;
    out[15] = 1;
    return out;
};

/**
 * Generates a look-at matrix with the given eye position, focal point, and up axis
 *
 * @param {mat4} out mat4 frustum matrix will be written into
 * @param {vec3} eye Position of the viewer
 * @param {vec3} center Point the viewer is looking at
 * @param {vec3} up vec3 pointing up
 * @returns {mat4} out
 */
mat4.lookAt = function (out, eye, center, up) {
    var x0, x1, x2, y0, y1, y2, z0, z1, z2, len,
        eyex = eye[0],
        eyey = eye[1],
        eyez = eye[2],
        upx = up[0],
        upy = up[1],
        upz = up[2],
        centerx = center[0],
        centery = center[1],
        centerz = center[2];

    if (Math.abs(eyex - centerx) < GLMAT_EPSILON &&
        Math.abs(eyey - centery) < GLMAT_EPSILON &&
        Math.abs(eyez - centerz) < GLMAT_EPSILON) {
        return mat4.identity(out);
    }

    z0 = eyex - centerx;
    z1 = eyey - centery;
    z2 = eyez - centerz;

    len = 1 / Math.sqrt(z0 * z0 + z1 * z1 + z2 * z2);
    z0 *= len;
    z1 *= len;
    z2 *= len;

    x0 = upy * z2 - upz * z1;
    x1 = upz * z0 - upx * z2;
    x2 = upx * z1 - upy * z0;
    len = Math.sqrt(x0 * x0 + x1 * x1 + x2 * x2);
    if (!len) {
        x0 = 0;
        x1 = 0;
        x2 = 0;
    } else {
        len = 1 / len;
        x0 *= len;
        x1 *= len;
        x2 *= len;
    }

    y0 = z1 * x2 - z2 * x1;
    y1 = z2 * x0 - z0 * x2;
    y2 = z0 * x1 - z1 * x0;

    len = Math.sqrt(y0 * y0 + y1 * y1 + y2 * y2);
    if (!len) {
        y0 = 0;
        y1 = 0;
        y2 = 0;
    } else {
        len = 1 / len;
        y0 *= len;
        y1 *= len;
        y2 *= len;
    }

    out[0] = x0;
    out[1] = y0;
    out[2] = z0;
    out[3] = 0;
    out[4] = x1;
    out[5] = y1; //y1
    out[6] = z1;
    out[7] = 0;
    out[8] = x2;
    out[9] = y2;
    out[10] = z2; //z2
    out[11] = 0;
    out[12] = -(x0 * eyex + x1 * eyey + x2 * eyez);
    out[13] = -(y0 * eyex + y1 * eyey + y2 * eyez);
    out[14] = -(z0 * eyex + z1 * eyey + z2 * eyez);
    out[15] = 1;

    return out;
};

/**
 * Returns a string representation of a mat4
 *
 * @param {mat4} mat matrix to represent as a string
 * @returns {String} string representation of the matrix
 */
mat4.str = function (a) {
    return 'mat4(' + a[0] + ', ' + a[1] + ', ' + a[2] + ', ' + a[3] + ', ' +
        a[4] + ', ' + a[5] + ', ' + a[6] + ', ' + a[7] + ', ' +
        a[8] + ', ' + a[9] + ', ' + a[10] + ', ' + a[11] + ', ' +
        a[12] + ', ' + a[13] + ', ' + a[14] + ', ' + a[15] + ')';
};

/**
 * Returns Frobenius norm of a mat4
 *
 * @param {mat4} a the matrix to calculate Frobenius norm of
 * @returns {Number} Frobenius norm
 */
mat4.frob = function (a) {
    return(Math.sqrt(Math.pow(a[0], 2) + Math.pow(a[1], 2) + Math.pow(a[2], 2) + Math.pow(a[3], 2) + Math.pow(a[4], 2) + Math.pow(a[5], 2) + Math.pow(a[6], 2) + Math.pow(a[7], 2) + Math.pow(a[8], 2) + Math.pow(a[9], 2) + Math.pow(a[10], 2) + Math.pow(a[11], 2) + Math.pow(a[12], 2) + Math.pow(a[13], 2) + Math.pow(a[14], 2) + Math.pow(a[15], 2) ))
};


if(typeof(exports) !== 'undefined') {
    exports.mat4 = mat4;
}
OMEGA.Omega3D.ShaderUtil = OMEGA.Omega3D.ShaderUtil || {};
OMEGA.Omega3D.ShaderUtil.InjectGLSLForLights = function(vs, fs, uniforms, scene ){
    var lights = scene.getLights();
    if(lights==null) return { vs: vs, fs:fs, uniforms:uniforms};
    var vs_lightSource = "";
    var fs_lightSource = "";



    if(lights.length == 0) return { vs: vs, fs:fs, uniforms:uniforms};
    for( var index in lights ){

        //get light vertex and fragment shader components.
        vs_lightSource += lights[index].vs;
        fs_lightSource += lights[index].fs;

        //add light uniforms to standard uniforms.
        for( var key in lights[index].uniforms ){
            uniforms[key] =  lights[index].uniforms[key];
        }
    }

    //first we inject uniforms and methods at start of page.
    var start = vs.indexOf("void main");
    start = start < 0 ? 0 : start;
    vs = vs.splice(start, 0, vs_lightSource );//vs_lightSource + vs;

    //second we modify the main function so that our light calculations are called.
    var header = vs.match(/(void main\(\)|void main\(void\))(\{|\s\{)?/g);
    var modHeader = header;
    modHeader = modHeader + "vTNormal = vec3( uModelMatrix * vec4(aVertexNormal, 0.0) );vViewMatrix = uViewMatrix;";



    var tick = true;
    for(var index in lights){
        if(lights[index].type == OMEGA.Omega3D.POINT_LIGHT ) {
            if(tick){
                modHeader = modHeader + "vMVertexPos = uModelMatrix * vec4(aVertexPos.xyz, 1.0);";
                tick = false;
            }//modHeader = modHeader + "vLightWeight += light" + index + "(injctd_tNormal);";
        }else{// modHeader = modHeader + "vLightWeight += light" + index + "(injctd_tNormal);";
        }
    }
    vs = vs.replace(/(void main\(\)|void main\(void\))(\{|\s\{)?/g, modHeader);

    //third we inject the fragment source.
    fs = fs.splice(fs.indexOf("void main"),0, fs_lightSource);//fs_lightSource + fs;

    //adjust main for light methods.
    header = fs.match(/(void main\(\)|void main\(void\))(\{|\s\{)?/g);
    modHeader = header;
    modHeader = modHeader + "vec3 vLightWeight = vec3(0, 0, 0);";
    //for(var index in lights) {
    //    modHeader = modHeader + "vLightWeight += light" + index + "( );";
    //}
    fs = fs.replace(/(void main\(\)|void main\(void\))(\{|\s\{)?/g, modHeader);


    //fourth modify the gl fragColor.sd
    //TODO: find smarter way to do this instead of forcing gl_FragColor = vec4(color.rgb, color.a); template.
    var inj = "";
    for(var index in lights) {
        inj = inj + "vLightWeight += light" + index + "( color.rgb);";
    }
    inj = inj + "color = vec4( vLightWeight.rgb, color.a);";
    fs = fs.splice(fs.indexOf("gl_FragColor"),0, inj);//fs_lightSource + fs;

    //var footer = fs.match(/gl_FragColor.+;/g);
    //var modFooter =footer;
    //for(var index in lights) {
    //    modFooter = modFooter + "vLightWeight += light" + index + "( color.rgb);";
    //}
    //modFooter = modFooter + "color = vec4( vLightWeight.rgb, color.a);"+
    //                        "gl_FragColor = color;";
    //fs = fs.replace(/gl_FragColor.+;/g, modFooter );
    //console.log( fs );
    //OMEGA.Omega3D.Log(" >> ShaderUtil: Injected " + lights.length + " lights into Shader.");
    return { vs: vs, fs:fs, uniforms:uniforms};
};

OMEGA.Omega3D.ShaderUtil.InjectGLSLForFog = function(vs, fs, uniforms, scene ){

    var fog_uniforms = "uniform vec3 uFogColor;"+
        "uniform vec2 uFogDist;";

    uniforms["uFogColor"] = { id:"uFogColor", type: "vec3", glsl: "uniform vec3 uFogColor;", value: [scene.getColor().r, scene.getColor().g, scene.getColor().b ] };
    uniforms["uFogDist" ] = { id:"uFogDist" , type: "vec2", glsl: "uniform vec3 uFogDist;" , value: [scene.getColor().r, scene.getColor().g, scene.getColor().b ] };

    var fog_varying ="varying vec3 vColor;"+
        "varying float vDist;";

    //first we inject uniforms and methods at start of page.
    var start = vs.indexOf("void main");
    start = start < 0 ? 0 : start;
    vs = vs.splice(start, 0, fog_varying );//vs_lightSource + vs;

    //second we modify the main function so that our light calculations are called.
    var header = vs.match(/(void main\(\)|void main\(void\))(\{|\s\{)?/g);
    var modHeader = header;
    modHeader = modHeader + "vDist  = distance(uModelMatrix*vec4(aVertexPos, 1.0), vec4(0, -0.5, -1, 1.0));";
    vs = vs.replace(/(void main\(\)|void main\(void\))(\{|\s\{)?/g, modHeader);


    //third we inject the fragment source.
    fs = fs.splice(fs.indexOf("void main"),0, fog_uniforms + fog_varying);//fs_lightSource + fs;

    //adjust main for light methods.
    //header = fs.match(/(void main\(\)|void main\(void\))(\{|\s\{)?/g);
    //modHeader = header;
    //modHeader = modHeader + "vec3 fogColor = vec3("+scene.getColor().r+","+scene.getColor().g+","+scene.getColor().b+");";
    //modHeader = modHeader + "vec2 fogDist  = vec2("+scene.fogStart+","+scene.fogEnd+");";
    //modHeader = modHeader + "float fogFactor = clamp((uFogDist.y - vDist)/(uFogDist.y - uFogDist.x), 0.0, 1.0);";
    //fs = fs.replace(/(void main\(\)|void main\(void\))(\{|\s\{)?/g, modHeader);

    var inj = "";
    var id = "void main(void){";
    inj = inj + "vec3 fogColor = vec3("+scene.getColor().r+","+scene.getColor().g+","+scene.getColor().b+");";
    inj = inj + "vec2 fogDist  = vec2("+scene.fogStart+","+scene.fogEnd+");";
    inj = inj + "float fogFactor = clamp((uFogDist.y - vDist)/(uFogDist.y - uFogDist.x), 0.0, 1.0);";
    fs = fs.splice(fs.indexOf("void main")  + id.length,0, inj);//fs_lightSource + fs;

    //fourth modify the gl fragColor.sd
    //TODO: find smarter way to do this instead of forcing gl_FragColor = vec4(color.rgb, color.a); template.
    inj = "color = vec4(mix(uFogColor, vec3(color), fogFactor), color.a);";
    fs = fs.splice(fs.indexOf("gl_FragColor"),0, inj);

    //var footer = fs.match(/gl_FragColor.+;/g);
    //var modFooter = "color = vec4(mix(uFogColor, vec3(color), fogFactor), color.a);"+
    //                "gl_FragColor = color;";
    //fs = fs.replace(/gl_FragColor.+;/g, modFooter );

    //console.log( fs );
    return { vs: vs, fs:fs, uniforms:uniforms};
};


OMEGA.Omega3D.VBOUtil = OMEGA.Omega3D.VBOUtil || {};
OMEGA.Omega3D.VBOUtil.GetSimularVertexIndex = function( in_vertex, in_uv, in_normal, out_verts, out_uvs, out_normals, result){
    var len = out_verts.length;
    for( var i = 0; i < len; i++){
        if( in_vertex.x == out_verts[i].x &&
            in_vertex.y == out_verts[i].y &&
            in_vertex.z == out_verts[i].z &&
            in_uv.x == out_uvs[i].x &&
            in_uv.y == out_uvs[i].y &&
            in_normal.x == out_normals[i].x &&
            in_normal.y == out_normals[i].y &&
            in_normal.z == out_normals[i].z ){
            result = i;
            return true;
        }
    }
    return false;
};
OMEGA.Omega3D.VBOUtil.IndexTB = function( in_vertices, in_uvs, in_normals, in_tangents, in_bitangents, out_vertices, out_uvs, out_normals, out_tangents, out_bitangents){
    for( var i = 0; i < in_vertices.length; i++){
        var index = -1;
        var found = OMEGA.Omega3D.VBOUtil.GetSimularVertexIndex(in_vertices[i], in_uvs[i], in_normals[i], out_vertices, out_uvs, out_normals, index );
        if(found){
            //out_indices.push(index);
            out_tangents[index].add( in_tangents[i] );
            out_bitangents[index].add( in_bitangents[i] );
        }else{
            out_vertices.push(in_vertices[i] );
            out_uvs.push(in_uvs[i] );
            out_normals.push(in_normals[i] );
            out_tangents.push(in_tangents[i] );
            out_bitangents.push(in_bitangents[i] );
            //  out_indices.push( out_vertices.length - 1);
        }
    }
};
OMEGA.Omega3D.VBOUtil.ComputeIndices = function(segmentsW, segmentsH,  out_indices ){
    for ( var i = 0; i < segmentsH; i++) {
        for ( var j = 0; j < segmentsW; j++) {
            if ( i < segmentsH -1 && j < segmentsW-1 ) {
                out_indices.push( i * segmentsW + j    , i * segmentsW + j + 1     , (i + 1) * segmentsW + j );
                out_indices.push( i * segmentsW + j + 1, (i + 1) *segmentsW + j + 1, (i + 1) * segmentsW + j );
            }
        }
    }
};
OMEGA.Omega3D.VBOUtil.FlattenMeshDataTB = function( in_vertices, in_uvs, in_normals, in_tangents, in_bitangents,out_vertices, out_uvs, out_normals, out_tangents, out_bitangents){

    for( var i = 0; i < in_vertices.length; i++) {
        out_vertices.push( in_vertices[i].x, in_vertices[i].y, in_vertices[i].z );
        out_uvs.push( in_uvs[i].x, in_uvs[i].y );
        out_normals.push( in_normals[i].x, in_normals[i].y, in_normals[i].z );


        out_tangents.push( in_tangents[i].x, in_tangents[i].y, in_tangents[i].z );
        out_bitangents.push( in_bitangents[i].x, in_bitangents[i].y, in_bitangents[i].z );
    }
};
OMEGA.Omega3D.VBOUtil.ComputeTangentBasis = function( vertices, uvs, normals, tangents_out, bitangents_out){
    var totalAmountPerVerticeStep = 3;
    for( var i = 0; i < vertices.length; i+=totalAmountPerVerticeStep){
        var vec0 = vertices[i];
        var vec1 = vertices[i+1];
        var vec2 = vertices[i+2];
        if(vec0 == null || vec1 == null || vec2 == null) break;

        var uv0 = uvs[i];
        var uv1 = uvs[i+1];
        var uv2 = uvs[i+2];



        var deltaPos1 = Vector3.Subtract(vec1, vec0);
        var deltaPos2 = Vector3.Subtract(vec2, vec0);


        var deltaUV1 = Vector2.Subtract(uv1, uv0);
        var deltaUV2 = Vector2.Subtract(uv2, uv0);

        var val = (deltaUV1.x * deltaUV2.y - deltaUV1.y * deltaUV2.x);
        var r = val > 0 ? 1.0 / (deltaUV1.x * deltaUV2.y - deltaUV1.y * deltaUV2.x) : 1.0;
        var tangent = Vector3.MultiplyByValue(
            Vector3.Subtract(
                Vector3.MultiplyByValue(deltaPos1,deltaUV2.y),
                Vector3.MultiplyByValue(deltaPos2,deltaUV1.y)
            ),r);

        var bitangent = Vector3.MultiplyByValue(
            Vector3.Subtract(
                Vector3.MultiplyByValue(deltaPos2,deltaUV1.x),
                Vector3.MultiplyByValue(deltaPos1,deltaUV2.x)
            ),r);






        tangents_out.push( tangent );
        tangents_out.push( tangent );
        tangents_out.push( tangent );


        bitangents_out.push( bitangent );
        bitangents_out.push( bitangent);
        bitangents_out.push( bitangent);
    }
    tangents_out.push( tangent );
    bitangents_out.push( bitangent);
};

function Cube(scale, material){
    Object3D.apply(this,[new Omega3D.Mesh( new Omega3D.Geometry.CubeGeometry(scale) ), material]);
};
Cube.prototype = new Object3D();
OMEGA.Omega3D.Cube = Cube;
function Cylinder(scale, material){
    Object3D.apply(this,[new Omega3D.Mesh( new Omega3D.Geometry.CylinderGeometry(scale, scale/4) ), material]);
};
Cylinder.prototype = new Object3D();
OMEGA.Omega3D.Cylinder = Cylinder;
OMEGA.Omega3D.Light = function(type, ambientColor,diffuseColor,specularColor){
    Object3D.apply(this, [null, null]);

    this.SetRadius = function( value ){
        this.radius = value;
    }
    this.GetRadius = function(){
        return this.radius;
    }

    this.SetDirection = function(x,y,z){
        this.direction[0] = x; this.direction[1] = y; this.direction[2] = z;
        this.invdirection[0] = -x; this.invdirection[1] = -y; this.invdirection[2] = -z;
        this.LookAt( x, y, z, this.position);
    };
    this.SetAmbient = function(r,g,b){
        this.ambientColor[0] = r; this.ambientColor[1] = g; this.ambientColor[2] = b;
    };

    this.SetDiffuse = function(r,g,b){
        this.diffuseColor[0] = r; this.diffuseColor[1] = g; this.diffuseColor[2] = b;
    };
    this.SetSpecular = function(r,g,b){
        this.specularColor[0] = r; this.specularColor[1] = g; this.specularColor[2] = b;
    };

    this.GetDirection = function(){ return this.direction; };
    this.GetSpecular = function(){ return this.specularColor; };
    this.GetDiffuse = function(){ return this.diffuseColor; };
    this.GetAmbient = function(){ return this.ambientColor; };

    this.GetInvDirection = function(){ return this.invdirection; };

    this.LookAt = function( x, y, z, position ){
        mat4.identity(laMatrix);
        mat4.lookAt(laMatrix,position || this.position,[x,y,z],  [0,1,0]);
    };
    this.GetMatrix    = function(){
        mat4.identity(this.modelView);
        mat4.multiply(this.modelView, this.rMatrix, this.tMatrix);
        mat4.multiply(this.modelView, this.modelView, this.sMatrix);
        mat4.multiply(this.modelView, this.modelView, laMatrix);
        return this.modelView;
    };
    this.GenerateGLSL = function(){
        if(this.type==0) this.glsl = this.genDirectional();
        else if(this.type == 1) this.glsl = this.genPoint();
        else if(this.type==2) this.glsl = this.genSpot();
    };
    this.genUniforms = function(){
        var uniforms = "";
        if( this.type == 0){ //directional light.
            uniforms += "uniform vec3 u" + this.id +"Direction;\n";
            this.uniforms["u"+this.id +"Direction"] = { type: "vec3", glsl: "uniform vec3 u" + this.id +"Direction;", value: this.GetDirection };
        }else if( this.type == 1 ){  // point light.
            uniforms += "uniform vec3 u" + this.id +"Position;\n";
            uniforms += "uniform float u" + this.id +"Radius;\n";
            this.uniforms["u"+this.id +"Position"] = { type: "vec3", glsl: "uniform vec3 u" + this.id +"Position;", value: this.GetPosition };
            this.uniforms["u"+this.id +"Radius"]   = { type: "float", glsl: "uniform float u" + this.id +"Radius;", value: this.GetRadius   };
        }else if( this.type == 2) { //spot light.
            uniforms += "uniform vec3 u" + this.id +"Position;\n";
            uniforms += "uniform float u" + this.id +"Radius;\n";
            uniforms += "uniform vec3 u" + this.id +"Direction;\n";

            this.uniforms["u"+this.id +"Position"]  = { type: "vec3", glsl: "uniform vec3 u" + this.id +"Position;", value: this.GetPosition };
            this.uniforms["u"+this.id +"Radius"]    = { type: "float", glsl: "uniform float u" + this.id +"Radius;", value: this.GetRadius   };
            this.uniforms["u"+this.id +"Direction"] = { type: "vec3", glsl: "uniform vec3 u" + this.id +"Direction;", value: this.GetDirection };
        }

        uniforms += "uniform vec3 u" + this.id +"Ambient;\n";
        uniforms += "uniform vec3 u" + this.id +"Diffuse;\n";
        uniforms += "uniform vec3 u" + this.id +"Specular;\n";

        this.uniforms["u"+this.id +"Ambient"]   = { type: "vec3", glsl: "uniform vec3 u" + this.id +"Ambient;", value: this.GetAmbient };
        this.uniforms["u"+this.id +"Diffuse"]   = { type: "vec3", glsl: "uniform vec3 u" + this.id +"Diffuse;", value: this.GetDiffuse };
        this.uniforms["u"+this.id +"Specular"]  = { type: "vec3", glsl: "uniform vec3 u" + this.id +"Specular;", value: this.GetSpecular };

        return uniforms;
    };
    this.genVarying = function(){
        var varying =[
            "varying vec3 vTNormal;",
            "varying vec4 vMVertexPos;",
            "varying mat4 vViewMatrix;"
        ].join("\n");
        return varying;
    };
    this.genDirectional = function(){
        var uniforms = this.genUniforms();
        var varying = this.index == 0 ? this.genVarying() : "";

        //VERTEX SHADER:
        this.vs = [
            varying
        ].join("\n");


        //FRAGMENT SHADER:
        this.fs = [
            uniforms,
            varying,
            "vec3 " +this.id +"(vec3 color){",

            //diffuse.
            "vec3 lightDir = normalize( u" + this.id +"Direction );",
            "float dirLightWeight = max(dot(normalize(vTNormal) , lightDir), 0.0);",

            //specular.
            "vec3 eyeWorldPos       = vec3(vViewMatrix[3].xyz);",
            "vec3 viewVectorEye     = normalize(vMVertexPos.xyz - eyeWorldPos);",
            "vec3 halfVector        = normalize( viewVectorEye + lightDir );",
            "float strength         = pow(max(dot(normalize(vTNormal),halfVector), 0.0), 25.0);",

            //elements.
            "vec3 diffuse  = (uDiffuse * u"+this.id+"Diffuse *  dirLightWeight);",
            "vec3 ambient  = (uAmbient * u"+this.id+"Ambient);",
            "vec3 specular = (uSpecular *  u"+this.id+"Specular * strength);",

            //final color.
            "vec3 lightFinalColor =  ((ambient + diffuse + specular)* color);",
            "return lightFinalColor;",
            "}"
        ].join("\n");
    };
    this.genPoint = function(){
        var uniforms = this.genUniforms();
        var varying = this.index == 0 ? this.genVarying() : "";

        //VERTEX SHADER:
        this.vs = [
            varying
        ].join("\n");


        //FRAGMENT SHADER:
        this.fs = [
            uniforms,
            varying,
            "vec3 " +this.id +"( vec3 color ){",

            //diffuse.
            "vec4 mvLight           = vec4(u" +this.id +"Position, 1.0) ;",
            "vec3 lightVec          = vec3(vMVertexPos) - vec3(mvLight);",
            "float lightLength      =  length(lightVec);",
            "vec3 lightDir          = normalize(lightVec/lightLength);",
            "lightLength            = lightLength * lightLength;",
            "float dirLightWeight   = max(dot(normalize(vTNormal), lightDir), 0.0);",

            //specular.
            "vec3 eyeWorldPos       = vec3(vViewMatrix[3].xyz);",
            "vec3 viewVectorEye     = normalize(vMVertexPos.xyz - eyeWorldPos );",
            "vec3 halfVector        = normalize( viewVectorEye + lightDir );",
            "float strength         = min(1.0, pow(max(dot(normalize(vTNormal),halfVector), 0.0), 25.0));",

            //attenuation
            "float cutoff = 0.0001;",
            "float radius = u"+this.id+"Radius;",
            "float d      = max(lightLength -radius, 0.0);",
            "float denom  = d/radius + 1.0;",
            "float attenuation = 1.0 / (denom*denom);",
            "attenuation = ( attenuation -cutoff) / (1.0-cutoff);",
            "attenuation = max( attenuation, 0.0);",

            //elements.
            "vec3 diffuse  = (uDiffuse * u"+this.id+"Diffuse *  dirLightWeight) *attenuation;",
            "vec3 ambient  = (uAmbient * u"+this.id+"Ambient);",
            "vec3 specular = (uSpecular *  u"+this.id+"Specular * strength) * attenuation;",


            //final color.
            "vec3 lightFinalColor = ((ambient + diffuse + specular)* color);",
            "return lightFinalColor;",
            "}"
        ].join("\n");
    };
    this.genSpot = function(){
        var uniforms = this.genUniforms();
        var varying = this.index == 0 ? this.genVarying() : "";

        //VERTEX SHADER:
        this.vs = [
            varying
        ].join("\n");


        //FRAGMENT SHADER:
        this.fs = [
            uniforms,
            varying,
            "vec3 " +this.id +"( vec3 color ){",

            //diffuse.
            "vec4 mvLight           = vec4(u" +this.id +"Position, 1.0) ;",
            "vec3 lightVec          = vec3(vMVertexPos) - vec3(mvLight);",
            "float lightLength      = length(lightVec);",
            "vec3 lightDir          = normalize(lightVec/lightLength);",
            "float dirLightWeight   = max(dot(vTNormal, lightDir), 0.0);",

            "lightLength = lightLength / 2.0;",

            //http://www.mbsoftworks.sk/index.php?page=tutorials&series=1&tutorial=20
            "float fConeCosine =  0.8;",
            "float fCosine  = dot(  u" + this.id +"Direction, lightDir);",
            "float fDif     = 1.0 - fConeCosine;",
            "float fFactor  = clamp( ( fCosine - fConeCosine ) / fDif, 0.0, 1.0);",


            "vec3 ambient  = (uAmbient * u"+this.id+"Ambient);",
            "vec3 diffuse  = (uDiffuse * u"+this.id+"Diffuse *  dirLightWeight);",



            "color = vec3(0);",
            "vec3 lightFinalColor = vec3(0);",
            "if( fCosine > fConeCosine)",
            "lightFinalColor = (diffuse) * fFactor / ( lightLength * 1.0);",

            "return lightFinalColor;",


            //"lightLength            = lightLength * lightLength;",
            //"float dirLightWeight   = max(dot(vTNormal, lightDir), 0.0);",
            //
            ////specular.
            //"vec3 eyeWorldPos       = vec3(-vViewMatrix[3].xyz);",
            //"vec3 viewVectorEye     = normalize(eyeWorldPos - vMVertexPos.xyz);",
            //"vec3 halfVector        = normalize( viewVectorEye + lightDir );",
            //"float strength         = pow(max(dot(normalize(vTNormal),halfVector), 0.0), 25.0);",
            //
            ////attenuation
            //"float cutoff = 0.05;",
            //"float radius = u"+this.id+"Radius;",
            //"float d      = max(lightLength -radius, 0.0);",
            //"float denom  = d/radius + 1.0;",
            //"float attenuation = 1.0 / (denom*denom);",
            //"attenuation = ( attenuation -cutoff) / (1.0-cutoff);",
            //"attenuation = max( attenuation, 0.0);",
            //
            ////elements.
            //"vec3 diffuse  = (uDiffuse * u"+this.id+"Diffuse *  dirLightWeight) *attenuation;",
            //"vec3 ambient  = (uAmbient * u"+this.id+"Ambient);",
            //"vec3 specular = (uSpecular *  u"+this.id+"Specular * strength) * attenuation;",
            //
            ////final color.
            //"vec3 lightFinalColor = ((ambient + diffuse)* color) + specular;",
            //"return lightFinalColor;",
            "}"
        ].join("\n");
    }

    this.setIndex = function( value ){
        this.index = value;
        this.id = "light"+this.index.toString();
    }

    this.uniforms = {};
    this.type = type || 0;
    this.vs = "";
    this.fs = "";
    this.index = 0;
    this.id = "light"+this.index.toString();
    this.direction     = [0.0, 0.0, 0.0];
    this.invdirection  = [0.0, 0.0, 0.0];
    var laMatrix = mat4.create();
    this.radius = 100.0;
    this.ambientColor  = ambientColor  || [0, 0, 0];
    this.diffuseColor  = diffuseColor  || [0.1, 0.1,0.1];
    this.specularColor = specularColor || [0, 0, 0];
    // OMEGA.Omega3D.LIGHTS.push( this );
    // this.GenerateGLSL();
};
OMEGA.Omega3D.DIRECTIONAL_LIGHT = 0;
OMEGA.Omega3D.POINT_LIGHT = 1;
OMEGA.Omega3D.SPOT_LIGHT = 2;


function LODObject3D( mesh, material, levels){
    Object3D.apply( this, [mesh, material]);
    this.levels = levels;
    this.current_level = 0;
    this.meshes = new Array();

    this.createMeshes = function(){
        //var type = this.mesh.GetGeometry().constructor;
        for( var i = 0; i < this.levels+1; i++){
            var g = new Omega3D.Geometry.SphereGeometry(this.mesh.GetGeometry().scale,6 + i * 3, 2 + i * 5);
            var m = new Mesh( g );
            m.CreateBuffers();
            this.meshes.push( m );
        }
        this.SetMesh( this.meshes[this.current_level] );
    };


    this.adjustLODLevel = function( camera ){
        var camPos = camera.GetPosition();
        var objPos = this.GetPosition();
        var delta = [ -camPos[0] - objPos[0], -camPos[1] - objPos[1], -camPos[2] - objPos[2] ];
        var deltaSQRT = Math.sqrt(delta[0]*delta[0] + delta[1]*delta[1] + delta[2]*delta[2]) / (this.mesh.GetGeometry().scale * 2);
        var newLevel = this.levels - Math.floor(deltaSQRT);
        if(newLevel==this.current_level)return;
        if(newLevel > this.levels) this.current_level = this.levels;
        else if(newLevel < 0     ) this.current_level = 0;
        else this.current_level= newLevel;
        this.SetMesh( this.meshes[this.current_level] );
    };


    //create meshes.
    if(this.mesh) this.createMeshes();
}
LODObject3D.prototype = Object3D;
OMEGA.Omega3D.LODObject3D = LODObject3D;
function ParticleEmitter( material){
    var m = new OMEGA.Omega3D.Mesh( new OMEGA.Omega3D.Geometry());
    Object3D.apply(this, [m, material]);
    this.alphaBlend = true;
    this.drawType = OMEGA.Omega3D.Object3D.POINTS;
    this.sortParticles = true;
    this.particles = new Array();
    this.AddParticle = function( p ){
        this.particles.push(p.x, p.y, p.z);
        this.GetMesh().GetGeometry().SetVertices( this.particles );
        this.GetMesh().CreateBuffers();
    };
    this.AddParticles = function( particles ){
        this.particles = particles;
        this.GetMesh().GetGeometry().SetVertices( this.particles );
        this.GetMesh().CreateBuffers();
    };

    this.Update = function(gl,camera){
        if(this.alphaBlend ){
            gl.disable(gl.DEPTH_TEST);
            gl.enable(gl.BLEND);
            gl.blendEquation( gl.FUNC_ADD  );
            gl.blendFunc(gl.ONE, gl.ONE);
        }
    };
    this.LateUpdate = function(gl,camera){
        if(this.alphaBlend){
            gl.enable(gl.DEPTH_TEST);
            gl.disable(gl.BLEND);
        }
    };


    this.sort = function(camera){
        this.particles.sort(function(a,b){
            return a[2] - b[2];
        })
    }

}
ParticleEmitter.prototype = new Object3D();
OMEGA.Omega3D.Particles = OMEGA.Omega3D.Particles || {};
OMEGA.Omega3D.Particles.ParticleEmitter = ParticleEmitter;

function Plane(width, height, material){
    Object3D.apply(this,[new Omega3D.Mesh( new Omega3D.Geometry.SquareGeometry(width, height) ), material]);
};
Plane.prototype = new Object3D();
OMEGA.Omega3D.Plane = Plane;
function Sphere(scale, material, segW, segH){
    Object3D.apply(this,[new Omega3D.Mesh( new Omega3D.Geometry.SphereGeometry(scale, segW, segH) ), material]);
};
Sphere.prototype = new Object3D();
OMEGA.Omega3D.Sphere = Sphere;
function Torus(scale, material){
    Object3D.apply(this,[new Omega3D.Mesh( new Omega3D.Geometry.TorusGeometry(scale, scale/4*2) ), material]);
};
Torus.prototype = new Object3D();
OMEGA.Omega3D.Torus = Torus;function DepthCamera( light ){
    Camera.apply(this);
    this.light = light;

    this.size = 5.0;
    this.GetProjectionMatrix = function(){
        mat4.ortho(this.projectionMatrix,-this.size,this.size,-this.size, this.size,-this.size,this.size*2);
        return this.projectionMatrix;
    };
//    this.GetMatrix    = function(){
//        mat4.identity(this.modelView);
//        mat4.lookAt( this.modelView, this.light.GetPosition(),[0,0,0],[0,1,0]);
//        return this.modelView;
//    };
    // this.SetPosition(l.GetPosition());
    this.LookAt(0, 0, 0, this.light.GetPosition());
}
DepthCamera.prototype = new Camera();
OMEGA.Omega3D.cameras = OMEGA.Omega3D.cameras || {};
OMEGA.Omega3D.cameras.DepthCamera = DepthCamera;function FreeCamera(){
    Camera.apply(this);

    this.speed = 0.1;
    this.drag  = 0.1;
    this.mass  = 1;
    this.force = 0;
    this.pos = new Vector3(0,0,0);
    this.vel = new Vector3(0,0,0);
    this.rot = new Vector3(0,0,0);
    this.rotF = new Vector3(0,0,0);
    this.acc = new Vector3(0,0,0);
    this.rot_acc = new Vector3(0,0,0);

    this.enabled = true;
    this.listener = null;

    this.dirX, this.dirY, this.dirZ;

    this.SetListener = function(l){ this.listener = l;Omega3D.AddKeyListener(l);};
    this.RemoveListener = function(){ Omega3D.RemoveKeyListener(this.listener);};
    this.GetListener = function(){ return this.listener;};
    this.SetSpeed = function(value){ this.speed = value;};
    this.update = function(){
        if(!this.listener || !this.enabled )return;
        this.HandleKeys();

        this.dirX  = Math.cos(this.rot.y+ Math.PI/2);
        this.dirZ  = Math.sin(this.rot.y+ Math.PI/2);
        this.dirY  = Math.sin(this.rot.x);


        this.vel.z = this.force * this.dirZ;
        this.vel.x = this.acc.x + this.force * this.dirX;
        this.vel.y = this.force * this.dirY;
        this.pos.z += this.vel.z;
        this.pos.x += this.vel.x;
        this.pos.y += this.vel.y;
        this.force *= this.drag;

        this.acc.x *= this.drag;

        this.x = this.pos.x;
        this.y = this.pos.y;
        this.z = this.pos.z;

        this.lookX = this.x + this.dirX;
        this.lookY = this.y + this.dirY;
        this.lookZ = this.z + this.dirZ;

        this.LookAt(this.lookX, this.lookY, this.lookZ, [this.x, this.y, this.z]);
    };



    this.HandleKeys = function(){

        if ( this.listener.currentlyPressedKeys[87]) {
            this.vel.z = 0;
            this.force += this.speed;
        }

        if ( this.listener.currentlyPressedKeys[83]) {
            this.vel.z = 0;
            this.force += -this.speed;
        }

        if (this.listener.currentlyPressedKeys[39] || this.listener.currentlyPressedKeys[68]|| this.listener.currentlyPressedKeys[102]) {


            this.rotF.y = -0.05;
            this.rot.y += this.rotF.y;

        }
        if (this.listener.currentlyPressedKeys[37] || this.listener.currentlyPressedKeys[65] || this.listener.currentlyPressedKeys[100]) {

            this.rotF.y = 0.05;
            this.rot.y += this.rotF.y;
        }

        if (this.listener.currentlyPressedKeys[38] ||this.listener.currentlyPressedKeys[104]) {

            this.rotF.x = -0.05;
            this.rot.x += this.rotF.x;
        }
        if (this.listener.currentlyPressedKeys[40] ||this.listener.currentlyPressedKeys[98]) {

            this.rotF.x = 0.05;
            this.rot.x += this.rotF.x;
        }
    };

    this.Enable = function(){
        if(this.enabled) return;
        if(this.listener) this.listener.Enable();
        this.enabled = true;
    };
    this.Disable = function(){
        if(!this.enabled)return;
        if(this.listener) this.listener.Disable();
        this.enabled = false;

    };

    this.Enable();
};
FreeCamera.prototype = new Camera();
OMEGA.Omega3D.cameras = OMEGA.Omega3D.cameras || {};
OMEGA.Omega3D.cameras.FreeCamera = FreeCamera;



function Atlas( image_rect ){
    this.image_rect = image_rect;
    this.segments = new Array();
    var currentFrame = null;
    var frameCounter = 0;
    this.AddSegment = function( id, rect ){
        this.segments.push( { rect: rect, uvRect:new Rect(1/ (this.image_rect.w / rect.x),
            1 / (this.image_rect.h / rect.y),
            rect.w / this.image_rect.w ,
            rect.h / this.image_rect.h)});
        this.Update();
    }

    var c = 0;
    this.Update = function(){
        c++;
        if(c==2) c = 0;
        else return;
        currentFrame = this.segments[frameCounter++];
        if(frameCounter > this.segments.length-1) frameCounter = 0;
    }

    this.GetCurrentFrame = function(){
        return currentFrame;
    }
}
OMEGA.Omega3D.Atlas = Atlas;function BasicMaterial( textures, scene, color ){
    Material.apply(this, [new OMEGA.Omega3D.Shaders.Basic(textures.length>0, scene,color ? color : [1.0, 1.0, 1.0]),textures]);
}
BasicMaterial.prototype = new Material();
OMEGA.Omega3D.BasicMaterial = BasicMaterial;
function CubeMapMaterial( textures, scene ){
    Material.apply(this, [ new OMEGA.Omega3D.Shaders.CubeMap(scene),textures]);
}
CubeMapMaterial.prototype = new Material();
OMEGA.Omega3D.CubeMapMaterial = CubeMapMaterial;
function SEMMaterial( textures, scene){
    Material.apply(this, [new Omega3D.Shaders.SEM(scene),textures]);
}
SEMMaterial.prototype = new Material();
OMEGA.Omega3D.SEMMaterial = SEMMaterial;
function ShaderMaterial( data, scene ){

    this.scene = scene;

    var fragmentOnlyUniforms = {};

    var attribs = {};
    attribs.aTextureCoord = { type: "vec2",glsl: "attribute vec2 aTextureCoord;", value:null};
    attribs.aVertexPos    = { type: "vec3",glsl: "attribute vec3 aVertexPos;"   , value: null};
    attribs.aVertexNormal = { type: "vec3",glsl: "attribute vec3 aVertexNormal;", value:null};
    attribs.aVertexTangent      = { type: "vec3",glsl: "attribute vec3 aVertexTangent;", value:null};
    attribs.aVertexBitangent    = { type: "vec3",glsl: "attribute vec3 aVertexBitangent;", value:null};
    attribs.aBaricentric        = { type: "vec3",glsl: "attribute vec3 aBaricentric;"       , value:null};
    attribs.aPickingColor       = { type: "vec3",glsl: "attribute vec3 aPickingColor;"      , value:null}
    this.attribs = attribs;

    var uniforms = {};
    uniforms.uModelMatrix      = { type: "mat4", glsl: "uniform mat4 uModelMatrix;"     , value: null };
    uniforms.uProjectionMatrix = { type: "mat4", glsl: "uniform mat4 uProjectionMatrix;", value: null };
    uniforms.uViewMatrix       = { type: "mat4", glsl: "uniform mat4 uViewMatrix;"      , value: null };
    uniforms.uInvViewMatrix    = { type: "mat4", glsl: "uniform mat4 uInvViewMatrix;"   , value: null };
    uniforms.uNormalMatrix     = { type: "mat3", glsl: "uniform mat3 uNormalMatrix;"    , value: null };

    this.uniforms = uniforms;

    //
    //var custom_uniforms = {};
    var custom_attribs = {};
    this.custom_uniforms = {};
    this.custom_attribs = custom_attribs;

    var self = this;

    this.Clear = function(){
        for( var key in  this.custom_uniforms){
            this.custom_uniforms[key] = {type:  this.custom_uniforms[key].type, glsl:  this.custom_uniforms[key].glsl    , value: null };
        }

    };

    this.SetValue = function( key, value ){
        var o =  uniforms[key] || attribs[key] || this.custom_uniforms[key];
        if(o){
            o.value = value;
        }
    };
    this.GetValue = function( key ){
        var o =  uniforms[key] || attribs[key] || this.custom_uniforms[key] || {value:null};
        return o.value;
    };



    this.processData = function( data ){
        var texCount = 0;
        for( var key in data.uniforms){
            var glsl = "";
            glsl = "uniform " + data.uniforms[key].type + " " + key + ";";
            if( data.uniforms[key].type != "sampler2D" &&
                data.uniforms[key].type != "samplerCube" ) {
                this.custom_uniforms[key] = { type: data.uniforms[key].type, glsl: glsl, value: data.uniforms[key].value };
            }else{
                fragmentOnlyUniforms[key] = { id:key, type: data.uniforms[key].type, glsl: glsl, value: null };
                data.uniforms[key].value.tex_id = key;
                data.uniforms[key].value.ID = texCount++;
                this.textures.push( data.uniforms[key].value );
            }
        };
        for( var key in data.attribs){
            var glsl = "";
            glsl = "attribute " + data.attribs[key].type + " " + key + ";";
            this.custom_attribs[key] = { type: data.attribs[key].type, glsl: glsl, value: data.attribs[key].value };
        };

    };

    var createShader = function(){
        OMEGA.Omega3D.Log(" >> ShaderMaterial: Creating custom shader");
        var vertex_shader_src = "";
        vertex_shader_src += attachData(attribs );
        vertex_shader_src += attachData(custom_attribs );
        vertex_shader_src += attachData(uniforms );
        vertex_shader_src += data.vertex_src;

        var fragment_shader_src = "precision mediump float;";
        fragment_shader_src += data.fragment_src;

        //var injected = OMEGA.Omega3D.ShaderUtil.InjectGLSLForLights(vertex_shader_src, fragment_shader_src, uniforms, self.scene);
        //self.uniforms = injected.uniforms;
        //vertex_shader_src =injected.vs;
        //fragment_shader_src =injected.fs;

        return new OMEGA.Omega3D.Shader( vertex_shader_src, fragment_shader_src, [attribs, uniforms, self.custom_uniforms, fragmentOnlyUniforms, custom_attribs]);
    };
    var attachData = function(  a){
        var out = "";
        for( var key in a) out += "\n"+ a[key].glsl;
        return out;
    };

    Material.apply(this,[null, []]);
    // this.genUniforms();
    this.processData(data);
    this.shader = createShader();
};
ShaderMaterial.prototype = new Material();
OMEGA.Omega3D.ShaderMaterial = ShaderMaterial;
function ShadowMapMaterial( textures, scene, depthCam ){
    Material.apply(this, [new OMEGA.Omega3D.Shaders.SM(scene), textures ]);
    this.shadowMapTexture = new ShadowMapTexture(1024, 1024, textures.length);
    this.textures.push(this.shadowMapTexture );
    this.custom_uniforms["uLightM"] = { type:"mat4", glsl: "", value: depthCam.GetMatrix() };
}
ShadowMapMaterial.prototype = new Material();
OMEGA.Omega3D.ShadowMapMaterial = ShadowMapMaterial;function SkyBoxMaterial( textures, scene ){
    Material.apply(this, [ new OMEGA.Omega3D.Shaders.SkyBox(scene),textures]);
}
SkyBoxMaterial.prototype = new Material();
OMEGA.Omega3D.SkyBoxMaterial = SkyBoxMaterial;
function VSMMaterial( textures, scene,  depthCam ){
    Material.apply(this, [new OMEGA.Omega3D.Shaders.VSM(scene), textures ]);
    this.custom_uniforms["uLightM"] = { type:"mat4", glsl: "", value: depthCam.GetMatrix() };
}
VSMMaterial.prototype = new Material();
OMEGA.Omega3D.VSMMaterial = VSMMaterial;/**
 *  SHADER COMPONENTS
 */
OMEGA.Omega3D.Shaders = OMEGA.Omega3D.Shaders || {};
OMEGA.Omega3D.Shaders.Components = OMEGA.Omega3D.Shaders.Components || {};
OMEGA.Omega3D.Shaders.Components.Fragment_Precision_Mediump_Float = function(){
    return "precision mediump float;";
}
OMEGA.Omega3D.Shaders.Components.Basic_TextureCoords = function(){
    return "vTexCoord = aTextureCoord;";
}
OMEGA.Omega3D.Shaders.Components.Basic_Includes = function(){
    var script =
        "attribute vec2 aTextureCoord;"+
        "attribute vec3 aVertexPos;"+
        "attribute vec3 aVertexNormal;"+
        "attribute vec3 aBaricentric;"+
        "attribute vec3 aPickingColor;"+

            //"uniform float uPointSize;"+
            //"uniform vec3 uDiffuse;"+
            //"uniform vec3 uAmbient;"+
            //"uniform vec3 uSpecular;"+

        "uniform mat4 uModelMatrix;"+
        "uniform mat4 uProjectionMatrix;"+
        "uniform mat4 uViewMatrix;"+
        "uniform mat4 uInvViewMatrix;"+
        "uniform mat3 uNormalMatrix;";
    return script;
};
OMEGA.Omega3D.Shaders.Components.Basic_Fragment_Includes = function(){
    var script =
        "uniform vec3 uDiffuse;"+
        "uniform vec3 uAmbient;"+
        "uniform vec3 uSpecular;";
    return script;
};
OMEGA.Omega3D.Shaders.Components.Basic_Includes_Post_Processing = function(){
    var script =
        "attribute vec2 aTextureCoord;"+
        "attribute vec3 aVertexPos;"+
        "attribute vec3 aVertexNormal;" +
        "uniform float uTime;"+
        "uniform mat4 uModelMatrix;"+
        "uniform mat4 uProjectionMatrix;"+
        "uniform mat4 uViewMatrix;"+
        "uniform mat3 uNormalMatrix;";
    return script;
};

OMEGA.Omega3D.Shaders.Components.Vertex_World_Conversion_V3 = function( target, subject ){
    return target + " = uProjectionMatrix * uViewMatrix * uModelMatrix *    vec4("+subject+", 1.0);";
};

//TODO: check if this is obsolete!
OMEGA.Omega3D.Shaders.Components.Vertex_View_Conversion_V3 = function( target, subject ){
    return target + " = uProjectionMatrix * uViewMatrix * uModelMatrix *    vec4("+subject+", 1.0);";
};

OMEGA.Omega3D.Shaders.Components.Vertex_ScreenSpace_Conversion_V3 = function( target, subject ){
    return target + " = vec4("+subject+", 1.0);"
}



OMEGA.Omega3D.Shaders.Components.Basic_Fragment_Logic = function( color ){
    return "gl_FragColor = " + color + ";";
};


OMEGA.Omega3D.Shaders.Components.Noise3D = function(){
    var script =
        "vec3 mod289(vec3 x) {"+
        "return x - floor(x * (1.0 / 289.0)) * 289.0;"+
        "}"+

        "vec4 mod289(vec4 x) {"+
        "return x - floor(x * (1.0 / 289.0)) * 289.0;"+
        "}"+

        "vec4 permute(vec4 x) {"+
        "return mod289(((x*34.0)+1.0)*x);"+
        "}"+

        "vec4 taylorInvSqrt(vec4 r){"+
        "return 1.79284291400159 - 0.85373472095314 * r;"+
        "}"+

        "vec3 fade(vec3 t) {"+
        "return t*t*t*(t*(t*6.0-15.0)+10.0);"+
        "}"+

        "float pnoise(vec3 P, vec3 rep){"+
        "    vec3 Pi0 = mod(floor(P), rep);"+ // Integer part, modulo period
        "    vec3 Pi1 = mod(Pi0 + vec3(1.0), rep);"+ // Integer part + 1, mod period
        "    Pi0 = mod289(Pi0);"+
        "    Pi1 = mod289(Pi1);"+
        "    vec3 Pf0 = fract(P);"+ // Fractional part for interpolation
        "    vec3 Pf1 = Pf0 - vec3(1.0);"+ // Fractional part - 1.0
        "    vec4 ix = vec4(Pi0.x, Pi1.x, Pi0.x, Pi1.x);"+
        "    vec4 iy = vec4(Pi0.yy, Pi1.yy);"+
        "   vec4 iz0 = Pi0.zzzz;"+
        "   vec4 iz1 = Pi1.zzzz;"+

        "    vec4 ixy = permute(permute(ix) + iy);"+
        "    vec4 ixy0 = permute(ixy + iz0);"+
        "    vec4 ixy1 = permute(ixy + iz1);"+

        "    vec4 gx0 = ixy0 * (1.0 / 7.0);"+
        "    vec4 gy0 = fract(floor(gx0) * (1.0 / 7.0)) - 0.5;"+
        "    gx0 = fract(gx0);"+
        "    vec4 gz0 = vec4(0.5) - abs(gx0) - abs(gy0);"+
        "     vec4 sz0 = step(gz0, vec4(0.0));"+
        "    gx0 -= sz0 * (step(0.0, gx0) - 0.5);"+
        "    gy0 -= sz0 * (step(0.0, gy0) - 0.5);"+

        "    vec4 gx1 = ixy1 * (1.0 / 7.0);"+
        "    vec4 gy1 = fract(floor(gx1) * (1.0 / 7.0)) - 0.5;"+
        "    gx1 = fract(gx1);"+
        "    vec4 gz1 = vec4(0.5) - abs(gx1) - abs(gy1);"+
        "    vec4 sz1 = step(gz1, vec4(0.0));"+
        "    gx1 -= sz1 * (step(0.0, gx1) - 0.5);"+
        "    gy1 -= sz1 * (step(0.0, gy1) - 0.5);"+

        "    vec3 g000 = vec3(gx0.x,gy0.x,gz0.x);"+
        "    vec3 g100 = vec3(gx0.y,gy0.y,gz0.y);"+
        "    vec3 g010 = vec3(gx0.z,gy0.z,gz0.z);"+
        "    vec3 g110 = vec3(gx0.w,gy0.w,gz0.w);"+
        "   vec3 g001 = vec3(gx1.x,gy1.x,gz1.x);"+
        "    vec3 g101 = vec3(gx1.y,gy1.y,gz1.y);"+
        "    vec3 g011 = vec3(gx1.z,gy1.z,gz1.z);"+
        "   vec3 g111 = vec3(gx1.w,gy1.w,gz1.w);"+

        "    vec4 norm0 = taylorInvSqrt(vec4(dot(g000, g000), dot(g010, g010), dot(g100, g100), dot(g110, g110)));"+
        "    g000 *= norm0.x;"+
        "   g010 *= norm0.y;"+
        "    g100 *= norm0.z;"+
        "   g110 *= norm0.w;"+
        "    vec4 norm1 = taylorInvSqrt(vec4(dot(g001, g001), dot(g011, g011), dot(g101, g101), dot(g111, g111)));"+
        "    g001 *= norm1.x;"+
        "    g011 *= norm1.y;"+
        "   g101 *= norm1.z;"+
        "   g111 *= norm1.w;"+

        "    float n000 = dot(g000, Pf0);"+
        "    float n100 = dot(g100, vec3(Pf1.x, Pf0.yz));"+
        "    float n010 = dot(g010, vec3(Pf0.x, Pf1.y, Pf0.z));"+
        "   float n110 = dot(g110, vec3(Pf1.xy, Pf0.z));"+
        "    float n001 = dot(g001, vec3(Pf0.xy, Pf1.z));"+
        "    float n101 = dot(g101, vec3(Pf1.x, Pf0.y, Pf1.z));"+
        "    float n011 = dot(g011, vec3(Pf0.x, Pf1.yz));"+
        "    float n111 = dot(g111, Pf1);"+

        "    vec3 fade_xyz = fade(Pf0);"+
        "    vec4 n_z = mix(vec4(n000, n100, n010, n110), vec4(n001, n101, n011, n111), fade_xyz.z);"+
        "    vec2 n_yz = mix(n_z.xy, n_z.zw, fade_xyz.y);"+
        "    float n_xyz = mix(n_yz.x, n_yz.y, fade_xyz.x);"+
        "    return 2.2 * n_xyz;"+
        "}";
    return script;
};


/**
 *
 *  LIGHTING.
 *
 */
OMEGA.Omega3D.Shaders.Components.Basic_Light_Includes = function(){
    var script =
        "uniform vec3 uLightPosition;" +
        "uniform vec3 uLightDirection;" +
        "uniform vec3 uAmbientColor;" +
        "uniform vec3 uDiffuseColor;" +
        "uniform vec3 uSpecularColor;";
    return script;
};
OMEGA.Omega3D.Shaders.Components.Basic_Light_Varying = function(){
    var script =
        "varying vec3 vNormalEye;"+
        "varying vec4 vPositionEye4;";
    return script;
};


/**
 *
 *  DIFFUSE SHADING.
 *
 */
OMEGA.Omega3D.Shaders.Components.Basic_Diffuse_Vertex_Logic = function(){
    var script =
        "vPositionEye4 = gl_Position;" +
            //"vec4 temp = uProjectionMatrix * uViewMatrix * uModelMatrix * vec4(uLightPosition, 1.0);" +
        "vec3 temp = uNormalMatrix *aVertexNormal;" +
        "vNormalEye = normalize(temp);";
    return script;
};
OMEGA.Omega3D.Shaders.Components.Basic_Diffuse_Fragment_Logic = function( color ){
    var script =
        "vec3 vectorToLightSource    = normalize(uLightPosition.xyz-vPositionEye4.xyz );" +
            //"vec3 vectorToLightSource    = normalize(uLightPosition.xyz );" +
        "float diffuseLightWeighting = max(dot(vNormalEye, vectorToLightSource), 0.0);" +
        "vec3 lightWeighting = uAmbientColor + " +
        "uDiffuseColor * diffuseLightWeighting;" +
        "vec4 texelColor = " + color +
        "gl_FragColor = vec4( lightWeighting.rgb * texelColor.rgb, texelColor.a);";
    return script;
};

/**
 *
 *  PHONG SHADING.
 *
 */

OMEGA.Omega3D.Shaders.Components.Basic_Phong_Fragment_Logic = function( color ){
    var script =
        "vec3 vectorToLightSource    = normalize(uLightPosition.xyz-vPositionEye4.xyz );" +
            //"vec3 vectorToLightSource    = normalize(uLightPosition.xyz );" +
        "float diffuseLightWeighting = max(dot(vNormalEye, vectorToLightSource), 0.0);" +
        "vec3 reflectionVector       = normalize(reflect(-vectorToLightSource, vNormalEye));" +
        "vec3 viewVectorEye  = normalize(vPositionEye4.xyz);" +
        "float rdotv  = max(dot(reflectionVector, viewVectorEye), 0.0);" +
        "float specularLightWeighting = pow(rdotv, 8.0);" +
        "vec3 lightWeighting = uAmbientColor + " +
        "uDiffuseColor * diffuseLightWeighting + " +
        "uSpecularColor * specularLightWeighting;" +
        "vec4 texelColor = " + color +
        "gl_FragColor = vec4( lightWeighting.rgb * texelColor.rgb, texelColor.a);";
    return script;
};



OMEGA.Omega3D.Shaders.Components.StandardAttributes = function(){
    var attribs = {};
    attribs.aTextureCoord = { type: "vec2",glsl: "attribute vec2 aTextureCoord;", value:null};
    attribs.aVertexPos    = { type: "vec3",glsl: "attribute vec3 aVertexPos;"   , value:null};
    attribs.aVertexNormal = { type: "vec3",glsl: "attribute vec3 aVertexNormal;", value:null};
    attribs.aVertexTangent      = { type: "vec3",glsl: "attribute vec3 aVertexTangent;"     , value:null};
    attribs.aVertexBitangent    = { type: "vec3",glsl: "attribute vec3 aVertexBitangent;"   , value:null};
    attribs.aBaricentric        = { type: "vec3",glsl: "attribute vec3 aBaricentric;"       , value:null};
    attribs.aPickingColor       = { type: "vec3",glsl: "attribute vec3 aPickingColor;"      , value:null};
    return attribs;
};
OMEGA.Omega3D.Shaders.Components.StandardUniforms = function(){
    var uniforms = {};
    uniforms.uDiffuse      = { type: "vec3", glsl: "uniform mat4 uDiffuse;"     , value: null };
    uniforms.uAmbient      = { type: "vec3", glsl: "uniform mat4 uAmbient;"     , value: null };
    uniforms.uSpecular      = { type: "vec3", glsl: "uniform mat4 uSpecular;"     , value: null };

    uniforms.uModelMatrix      = { type: "mat4", glsl: "uniform mat4 uModelMatrix;"     , value: null };
    uniforms.uProjectionMatrix = { type: "mat4", glsl: "uniform mat4 uProjectionMatrix;", value: null };
    uniforms.uViewMatrix       = { type: "mat4", glsl: "uniform mat4 uViewMatrix;"      , value: null };
    uniforms.uInvViewMatrix    = { type: "mat4", glsl: "uniform mat4 uInvViewMatrix;"   , value: null };
    uniforms.uNormalMatrix     = { type: "mat3", glsl: "uniform mat3 uNormalMatrix;"    , value: null };
    //uniforms.uPointSize        = { type: "float", glsl: "uniform float uPointSize;"    , value: null };
    return uniforms;
};
OMEGA.Omega3D.Shaders.Components.StandardLightUniforms = function(){
    var uniforms = {};
    uniforms.uLightPosition  = { type: "vec3", glsl: "uniform vec3 uLightPosition;" , value: null };
    uniforms.uLightDirection = { type: "vec3", glsl: "uniform vec3 uLightDirection;", value: null };
//    uniforms.uAmbientColor   = { type: "vec3", glsl: "uniform vec3 uAmbientColor;"  , value: null };
    // uniforms.uDiffuseColor   = { type: "vec3", glsl: "uniform vec3 uDiffuseColor;"  , value: null };
    // uniforms.uSpecularColor  = { type: "vec3", glsl: "uniform vec3 uSpecularColor;" , value: null };
    return uniforms;
};

OMEGA.Omega3D.Shaders = OMEGA.Omega3D.Shaders || {};


/**
 *  Basic - Default shader
 *
 * @param isTextured
 * @returns {OMEGA.Omega3D.Shader}
 */
OMEGA.Omega3D.Shaders.Basic = function( isTextured, scene, color ){
    //OMEGA.Omega3D.Log(" >> ShaderBuilder: Creating 'Basic' shader");
    //OMEGA.Omega3D.Log("   -- Textured: " + isTextured);
    //OMEGA.Omega3D.Log("   -- Color: " + color);
    var texture, fragmentColor;
    isTextured = isTextured || false;
    if(isTextured) texture = "varying vec2 vTexCoord;";
    else texture = "";
    var vertex_source = OMEGA.Omega3D.Shaders.Components.Basic_Includes();
    vertex_source = vertex_source + texture;
    vertex_source = vertex_source + "void main(void){";
    vertex_source = vertex_source + OMEGA.Omega3D.Shaders.Components.Vertex_World_Conversion_V3("gl_Position", "aVertexPos");
    if(isTextured                      ) vertex_source = vertex_source + OMEGA.Omega3D.Shaders.Components.Basic_TextureCoords("vTexCoord");
    vertex_source = vertex_source + "}";


    if(isTextured){
        texture = "uniform sampler2D uSampler;"+
            "varying vec2 vTexCoord;";
        fragmentColor = "texture2D(uSampler, vec2(vTexCoord.s, vTexCoord.t));";
    }
    else{
        if(color) fragmentColor = "vec4("+color[0]+","+ +color[1]+"," +  +color[2]+", 1.0);";
        else  fragmentColor = "vec4(0.8,0.4,0.0, 1.0);";
    }
    var fragment_source = OMEGA.Omega3D.Shaders.Components.Fragment_Precision_Mediump_Float();
    fragment_source = fragment_source + OMEGA.Omega3D.Shaders.Components.Basic_Fragment_Includes();
    if( isTextured                     ){
        fragment_source = fragment_source + texture;
    }
    fragment_source = fragment_source + "void main(void){";
    fragment_source = fragment_source + "vec4 color = " + fragmentColor;
    fragment_source = fragment_source + OMEGA.Omega3D.Shaders.Components.Basic_Fragment_Logic("vec4(color.rgb, color.a)");
    fragment_source = fragment_source + "}";



    //lights
    var uniforms = OMEGA.Omega3D.Shaders.Components.StandardUniforms();
    if(scene != null){
        var injected = OMEGA.Omega3D.ShaderUtil.InjectGLSLForLights(vertex_source, fragment_source, uniforms, scene);
        uniforms = injected.uniforms;
        vertex_source =injected.vs;
        fragment_source =injected.fs;

        //fog
        if(scene.hasFog){
            injected = OMEGA.Omega3D.ShaderUtil.InjectGLSLForFog(vertex_source, fragment_source, uniforms,scene);
            uniforms = injected.uniforms;
            vertex_source =injected.vs;
            fragment_source =injected.fs;
        }
    }



    var fragmentOnlyUniforms = {};
    if(isTextured) fragmentOnlyUniforms["uSampler"] = { id:"uSampler", type: "sampler2D", glsl: "", value: null };
    return new OMEGA.Omega3D.Shader(vertex_source,fragment_source,  [OMEGA.Omega3D.Shaders.Components.StandardAttributes(),
        uniforms,
        {},
        fragmentOnlyUniforms]);
};

/**
 * SEM - Spherical Environment Mapping
 *
 * @param scene
 * @returns {OMEGA.Omega3D.Shader}
 * @constructor
 */
OMEGA.Omega3D.Shaders.SEM = function( scene ){
    // OMEGA.Omega3D.Log(" >> ShaderBuilder: Creating 'Spherical Environment Mapping' shader");

    var vertex_source = OMEGA.Omega3D.Shaders.Components.Basic_Includes();
    vertex_source += "varying vec3 e;";
    vertex_source += "varying vec3 n;";
    vertex_source += "void main(void){";
    vertex_source += "e = normalize( vec3( uViewMatrix * uModelMatrix * vec4(aVertexPos.xyz, 1.0) ) );";
    vertex_source += "n = normalize( uNormalMatrix * aVertexNormal );";
    vertex_source += OMEGA.Omega3D.Shaders.Components.Vertex_World_Conversion_V3("gl_Position", "aVertexPos");
    vertex_source += "}";





    var fragment_source = OMEGA.Omega3D.Shaders.Components.Fragment_Precision_Mediump_Float();
    fragment_source = fragment_source + OMEGA.Omega3D.Shaders.Components.Basic_Fragment_Includes();
    fragment_source += "uniform sampler2D uSampler;";
    fragment_source += "varying vec3 e;";
    fragment_source += "varying vec3 n;";
    fragment_source += "void main(void){";
    fragment_source += "vec3 r = reflect( e, n );";
    fragment_source += "float m = 2.0 * sqrt( pow( r.x, 2.0) + pow( r.y, 2.0) + pow( r.z + 1.0, 2.0) );";
    fragment_source += "vec2 vN = r.xy / m + 0.5;";
    fragment_source += "vec4 color = texture2D(uSampler, vN);";
    fragment_source += OMEGA.Omega3D.Shaders.Components.Basic_Fragment_Logic("vec4(color.rgb, color.a)");
    fragment_source += "}";


    var uniforms = OMEGA.Omega3D.Shaders.Components.StandardUniforms();
    if(scene != null){
        var injected = OMEGA.Omega3D.ShaderUtil.InjectGLSLForLights(vertex_source, fragment_source, uniforms, scene);
        uniforms = injected.uniforms;
        vertex_source =injected.vs;
        fragment_source =injected.fs;

        //fog
        if(scene.hasFog){
            injected = OMEGA.Omega3D.ShaderUtil.InjectGLSLForFog(vertex_source, fragment_source, uniforms,scene);
            uniforms = injected.uniforms;
            vertex_source =injected.vs;
            fragment_source =injected.fs;
        }
    }

    var fragmentOnlyUniforms = {};
    fragmentOnlyUniforms["uSampler"] = { id:"uSampler", type: "sampler2D", glsl: "", value: null };
    return new OMEGA.Omega3D.Shader(vertex_source,fragment_source,  [OMEGA.Omega3D.Shaders.Components.StandardAttributes(),
        uniforms,
        {},
        fragmentOnlyUniforms]);
};



/**
 * SM - Shadow Mapping
 *
 * @param scene
 * @returns {OMEGA.Omega3D.Shader}
 * @constructor
 */
OMEGA.Omega3D.Shaders.SM = function( scene  ){
    // OMEGA.Omega3D.Log(" >> ShaderBuilder: Creating 'Shadow Mapping' shader");

    var vertex_source = OMEGA.Omega3D.Shaders.Components.Basic_Includes();
    vertex_source += "uniform mat4 uLightM;";
    vertex_source += "varying vec2 vTexCoord;";
    vertex_source += "varying vec4 vertexPosLight;";

    vertex_source += "void main(void){";
    vertex_source += "mat4 mvp       = uProjectionMatrix * uViewMatrix * uModelMatrix;";
    vertex_source += "mat4 mvp_light = uProjectionMatrix * uLightM * uModelMatrix;";
    vertex_source += "vec4 pos       = mvp * vec4(aVertexPos.xyz, 1.0);";
    vertex_source += "vertexPosLight = mvp_light * vec4(aVertexPos.xyz, 1.0);";
    vertex_source += "gl_Position    = pos;";
    vertex_source += "vTexCoord      = aTextureCoord;";
    vertex_source += "}";


    var fragment_source = OMEGA.Omega3D.Shaders.Components.Fragment_Precision_Mediump_Float();
    fragment_source = fragment_source + OMEGA.Omega3D.Shaders.Components.Basic_Fragment_Includes();
    fragment_source += "uniform sampler2D uSampler;";
    fragment_source += "uniform sampler2D uSampler_1;";
    fragment_source += "varying vec2 vTexCoord;";
    fragment_source += "varying vec4 vertexPosLight;";

    fragment_source += "void main(void){";
    fragment_source += "vec3 nLightPos   = (vertexPosLight.xyz / vertexPosLight.w) / 2.0 + 0.5;";
    fragment_source += "vec4 depth       = texture2D(uSampler_1, vec2(nLightPos.xy));";
    fragment_source += "vec4 texture     = texture2D(uSampler, vec2(vTexCoord.st));";
    fragment_source += "float bias       = 0.0000000001;";
    fragment_source += "float visibility = 1.0;";//(nLightPos.z  > (depth.r  + bias)) ? 0.5 : 1.0;";
    fragment_source += "float notInSight = 0.0;";//(nLightPos.z  > (depth.r  + bias)) ? 0.5 : 1.0;";
    fragment_source += "float divider = 500.0*depth.r;";//(nLightPos.z  > (depth.r  + bias)) ? 0.5 : 1.0;";

    fragment_source += "if ( texture2D( uSampler_1, nLightPos.xy/divider ).r == notInSight  ){}";
    fragment_source += "else{";
    fragment_source += "if ( texture2D( uSampler_1, nLightPos.xy ).r   + bias <  nLightPos.z  ){ visibility-=0.05; }";
    fragment_source += "if ( texture2D( uSampler_1, nLightPos.xy+ vec2( -0.94201624 , -0.39906216 )/divider ).r   + bias<  nLightPos.z  ){visibility-=0.1; }";
    fragment_source += "if ( texture2D( uSampler_1, nLightPos.xy+ vec2(  0.94558609 , -0.76890725 )/divider ).r   + bias<  nLightPos.z  ){ visibility-=0.1; }";
    fragment_source += "if ( texture2D( uSampler_1, nLightPos.xy+ vec2( -0.094184101, -0.92938870 )/divider ).r   + bias<  nLightPos.z  ){ visibility-=0.1; }";
    fragment_source += "if ( texture2D( uSampler_1, nLightPos.xy+ vec2(  0.34495938 ,  0.29387760 )/divider ).r   + bias<  nLightPos.z  ){ visibility-=0.1; }}";
    fragment_source += "vec4 color       = vec4( texture.rgb*visibility, 1.0);";////vec4( texture.rgb*visibility, 1.0);
    fragment_source += OMEGA.Omega3D.Shaders.Components.Basic_Fragment_Logic("vec4(color.rgb, color.a)");
    fragment_source += "}";


    var uniforms = OMEGA.Omega3D.Shaders.Components.StandardUniforms();
    if(scene != null){
        var injected = OMEGA.Omega3D.ShaderUtil.InjectGLSLForLights(vertex_source, fragment_source, uniforms, scene);
        uniforms = injected.uniforms;
        vertex_source =injected.vs;
        fragment_source =injected.fs;

        //fog
        if(scene.hasFog){
            injected = OMEGA.Omega3D.ShaderUtil.InjectGLSLForFog(vertex_source, fragment_source, uniforms,scene);
            uniforms = injected.uniforms;
            vertex_source =injected.vs;
            fragment_source =injected.fs;
        }
    }


    var fragmentOnlyUniforms = {};
    fragmentOnlyUniforms["uSampler"]   = { id:"uSampler"  , type: "sampler2D", glsl: "", value: null };
    fragmentOnlyUniforms["uSampler_1"] = { id:"uSampler_1", type: "sampler2D", glsl: "", value: null };

    var custom_uniforms = {};
    custom_uniforms["uLightM"] = { type:"mat4", glsl: "", value: "" };
    return new OMEGA.Omega3D.Shader(vertex_source,fragment_source,  [OMEGA.Omega3D.Shaders.Components.StandardAttributes(),
        uniforms,
        custom_uniforms,
        fragmentOnlyUniforms]);
};




/**
 * DepthMap - DepthMap for VSM ( Varience Shadow Mapping )
 *
 * @param scene
 * @returns {OMEGA.Omega3D.Shader}
 * @constructor
 */
OMEGA.Omega3D.Shaders.DepthMap = function( scene ){
    var vs = [
        OMEGA.Omega3D.Shaders.Components.Basic_Includes(),
        "varying vec4 vPos;",
        "void main(void){",
        "gl_Position    = uProjectionMatrix * uViewMatrix * uModelMatrix * vec4(aVertexPos.xyz, 1.0);",
        "vPos           = gl_Position;",
        "}"
    ].join("\n");

    var fs = [
        "#extension GL_OES_standard_derivatives : enable",  //optional. IF Enable WebGL Draft Extensions has been enabled ( Chrome ).
        OMEGA.Omega3D.Shaders.Components.Fragment_Precision_Mediump_Float(),
        OMEGA.Omega3D.Shaders.Components.Basic_Fragment_Includes(),
        "varying vec4 vPos;",


        "vec2 packHalf( float depth ){",
        "const vec2 bias = vec2(1.0 / 255.0, 0.0);",
        "vec2 colour = vec2(depth, fract(depth * 255.0));",
        "return colour - (colour.yy * bias);",
        "}",

        "void main(void){",

        "float depth = vPos.z / vPos.w;",
        "depth   = depth * 0.5 + 0.5;",
        "float moment1 = depth;",

        "float dx = dFdx(depth);",           //optional. IF Enable WebGL Draft Extensions has been enabled ( Chrome ).
        "float dy = dFdy(depth);",           //optional. IF Enable WebGL Draft Extensions has been enabled ( Chrome ).
        "float moment2 = (depth * depth) + 0.25 * (dx*dx+dy*dy);",

        "gl_FragColor = vec4( moment1, moment2, 0.0, 1.0);",
        // "gl_FragColor = vec4(packHalf(moment1), packHalf(moment2));",
        "}"
    ].join("\n");


    var uniforms = OMEGA.Omega3D.Shaders.Components.StandardUniforms();
    return new OMEGA.Omega3D.Shader(vs,fs,  [OMEGA.Omega3D.Shaders.Components.StandardAttributes(),
        uniforms,
        {},
        {}]);
};
OMEGA.Omega3D.Shaders.VSM = function( scene ){
    var vs = [
        OMEGA.Omega3D.Shaders.Components.Basic_Includes(),
        "uniform mat4 uLightM;",
        "varying vec2 vTexCoord;",
        "varying vec4 vertexPosLight;",

        "void main(void){",
        "mat4 mvp       = uProjectionMatrix * uViewMatrix * uModelMatrix;",
        "mat4 mvp_light = uProjectionMatrix * uLightM * uModelMatrix;",
        "vec4 pos       = mvp * vec4(aVertexPos.xyz, 1.0);",
        "vertexPosLight = mvp_light * vec4(aVertexPos.xyz, 1.0);",
        "gl_Position    = pos;",
        "vTexCoord      = aTextureCoord;",

        "}"
    ].join("\n");

    var fs = [
        OMEGA.Omega3D.Shaders.Components.Fragment_Precision_Mediump_Float(),
        OMEGA.Omega3D.Shaders.Components.Basic_Fragment_Includes(),
        "uniform sampler2D uSampler;",
        "uniform sampler2D uSampler_1;",
        "varying vec2 vTexCoord;",
        "varying vec4 vertexPosLight;",


        "float unpackHalf (vec2 color){",
        "return color.x + (color.y / 255.0);",
        "}",

        "float linestep(float low,  float v){",
        "return clamp((v-low)/(1.0-low), 0.0, 1.0);",
        "}",
        "float ChebyshevUpperBound( vec2 moments, float t ){",
        "float min_variance = 0.000002;",

        "if(t <= moments.x) return 1.0;",

        "float p = smoothstep(t-min_variance, t, moments.x);",
        "float variance = max( moments.y - moments.x * moments.x, min_variance );",


        "float d = t - moments.x;",
        "float p_max = linestep( 0.2, variance / (variance + d*d));",
        "return min(1.0, max(p, p_max));",
        "}",
        "float ShadowContribution( vec2 lightTexCoord, float DistanceToLight){",
        "vec2 moments =  texture2D( uSampler_1, lightTexCoord).rg;",
        //"vec4 texel = texture2D( uSampler_1, lightTexCoord);",
        //"vec2 moments = vec2(unpackHalf(texel.rg), unpackHalf(texel.ba));",
        "return ChebyshevUpperBound( moments, DistanceToLight);",
        "}",


        "void main(void){",
        "vec3 nLightPos = (vertexPosLight.xyz / vertexPosLight.w)* 0.5 + 0.5;",
        "float shadow = ShadowContribution( vec2(nLightPos.xy), nLightPos.z);",
        "vec4 texture = texture2D(uSampler, vec2(vTexCoord.st));",

        "vec4 color = vec4(shadow, shadow, shadow, 1.0) *texture;",
        OMEGA.Omega3D.Shaders.Components.Basic_Fragment_Logic("vec4(color.rgb, color.a)"),
        "}"
    ].join("\n");

    var uniforms = OMEGA.Omega3D.Shaders.Components.StandardUniforms();
    if(scene != null){
        var injected = OMEGA.Omega3D.ShaderUtil.InjectGLSLForLights(vs, fs, uniforms, scene);
        uniforms = injected.uniforms;
        vs =injected.vs;
        fs =injected.fs;

        //fog
        if(scene.hasFog){
            injected = OMEGA.Omega3D.ShaderUtil.InjectGLSLForFog(vs, fs, uniforms,scene);
            uniforms = injected.uniforms;
            vs =injected.vs;
            fs =injected.fs;
        }
    }

    var fragmentOnlyUniforms = {};
    fragmentOnlyUniforms["uSampler"]   = { id:"uSampler"  , type: "sampler2D", glsl: "", value: null };
    fragmentOnlyUniforms["uSampler_1"] = { id:"uSampler_1", type: "sampler2D", glsl: "", value: null };

    var custom_uniforms = {};
    custom_uniforms["uLightM"] = { type:"mat4", glsl: "", value: "" };

    return new OMEGA.Omega3D.Shader(vs,fs,  [OMEGA.Omega3D.Shaders.Components.StandardAttributes(),
        uniforms,
        custom_uniforms,
        fragmentOnlyUniforms]);
};








OMEGA.Omega3D.Shaders.ColorPicking = function(){
    // OMEGA.Omega3D.Log(" >> ShaderBuilder: Creating 'ColorPicking' shader");

    var vertex_source = OMEGA.Omega3D.Shaders.Components.Basic_Includes();
    vertex_source = vertex_source + "varying vec3 vPickingColor;";
    vertex_source = vertex_source + "void main(void){";
    vertex_source = vertex_source + "vPickingColor = aPickingColor;";
    vertex_source = vertex_source + OMEGA.Omega3D.Shaders.Components.Vertex_World_Conversion_V3("gl_Position", "aVertexPos");
    vertex_source = vertex_source + "}";

    var fragment_source = OMEGA.Omega3D.Shaders.Components.Fragment_Precision_Mediump_Float();
    fragment_source = fragment_source + "varying vec3 vPickingColor;";
    fragment_source = fragment_source + "void main(void){";
    fragment_source = fragment_source + "vec4 color = vec4(vPickingColor.r,0, 0, 1.0);";
    fragment_source = fragment_source + OMEGA.Omega3D.Shaders.Components.Basic_Fragment_Logic("vec4(color.rgb, color.a)");
    fragment_source = fragment_source + "}";

    var uniforms = OMEGA.Omega3D.Shaders.Components.StandardUniforms();
    var fragmentOnlyUniforms = {};

    return new OMEGA.Omega3D.Shader(vertex_source,fragment_source,  [OMEGA.Omega3D.Shaders.Components.StandardAttributes(),
        uniforms,
        {},
        fragmentOnlyUniforms]);
};



OMEGA.Omega3D.Shaders.BumpMapping = function(scene){
    var vs = [
        OMEGA.Omega3D.Shaders.Components.Basic_Includes(),
        "varying vec2 vTexCoord;",

        "void main(void){",
        "mat4 mvp       = uProjectionMatrix * uViewMatrix * uModelMatrix;",
        "vec4 pos       = mvp * vec4(aVertexPos.xyz, 1.0);",
        "gl_Position    = pos;",
        "vTexCoord      = aTextureCoord;",

        "}"
    ].join("\n");

    var fs = [
        OMEGA.Omega3D.Shaders.Components.Fragment_Precision_Mediump_Float(),
        OMEGA.Omega3D.Shaders.Components.Basic_Fragment_Includes(),
        "uniform sampler2D uSampler;",
        "uniform sampler2D uSampler_1;",
        "varying vec2 vTexCoord;",

        "void main(void){",
        "vec3 texture        = texture2D(uSampler  , vec2(vTexCoord.st)).rgb;",
        "vec3 texture_normal = texture2D(uSampler_1, vec2(vTexCoord.st)).rgb;",
        "texture_normal = normalize(texture_normal* 2.0 - 1.0);;",

        "vec4 color = texture_normal;",
        OMEGA.Omega3D.Shaders.Components.Basic_Fragment_Logic("vec4(color.rgb, color.a)"),
        "}"
    ].join("\n");

    var uniforms = OMEGA.Omega3D.Shaders.Components.StandardUniforms();
    //if(scene != null){
    //    var injected = OMEGA.Omega3D.ShaderUtil.InjectGLSLForLights(vs, fs, uniforms, scene);
    //    uniforms = injected.uniforms;
    //    vs =injected.vs;
    //    fs =injected.fs;
    //
    //    //fog
    //    if(scene.hasFog){
    //        injected = OMEGA.Omega3D.ShaderUtil.InjectGLSLForFog(vs, fs, uniforms,scene);
    //        uniforms = injected.uniforms;
    //        vs =injected.vs;
    //        fs =injected.fs;
    //    }
    //}

    var fragmentOnlyUniforms = {};
    fragmentOnlyUniforms["uSampler"  ] = { id:"uSampler"  , type: "sampler2D", glsl: "", value: null };
    fragmentOnlyUniforms["uSampler_1"] = { id:"uSampler_1", type: "sampler2D", glsl: "", value: null };

    var custom_uniforms = {};

    return new OMEGA.Omega3D.Shader(vs,fs,  [OMEGA.Omega3D.Shaders.Components.StandardAttributes(),
        uniforms,
        custom_uniforms,
        fragmentOnlyUniforms]);

}











//===================================================================================
// PostProcessing
//===================================================================================

OMEGA.Omega3D.Shaders.PostProcessing = function( isTextured, scene, color ){
    //OMEGA.Omega3D.Log(" >> ShaderBuilder: Creating 'Basic PostProcessing' shader");
    //OMEGA.Omega3D.Log("   -- Textured: " + isTextured);
    //OMEGA.Omega3D.Log("   -- Color: " + color);
    var texture, fragmentColor;
    isTextured = isTextured || false;
    if(isTextured) texture = "varying vec2 vTexCoord;";
    else texture = "";
    var vertex_source = OMEGA.Omega3D.Shaders.Components.Basic_Includes();
    vertex_source = vertex_source + texture;
    vertex_source = vertex_source + "void main(void){";
    vertex_source = vertex_source + OMEGA.Omega3D.Shaders.Components.Vertex_ScreenSpace_Conversion_V3("gl_Position", "aVertexPos");
    if(isTextured                      ) vertex_source = vertex_source + OMEGA.Omega3D.Shaders.Components.Basic_TextureCoords("vTexCoord");
    vertex_source = vertex_source + "}";


    if(isTextured){
        texture = "uniform sampler2D uSampler;"+
            "varying vec2 vTexCoord;";
        fragmentColor = "texture2D(uSampler, vec2(1.0-vTexCoord.s, 1.0 - vTexCoord.t));";
    }
    else{
        if(color) fragmentColor = "vec4("+color[0]+","+ +color[1]+"," +  +color[2]+", 1.0);";
        else  fragmentColor = "vec4(0.8,0.4,0.0, 1.0);";
    }
    var fragment_source = OMEGA.Omega3D.Shaders.Components.Fragment_Precision_Mediump_Float();
    if( isTextured                     ){
        fragment_source = fragment_source + OMEGA.Omega3D.Shaders.Components.Basic_Fragment_Includes();
        fragment_source = fragment_source + texture;
    }
    fragment_source = fragment_source + "void main(void){";
    fragment_source = fragment_source + "vec4 color = "+fragmentColor;
    fragment_source = fragment_source + OMEGA.Omega3D.Shaders.Components.Basic_Fragment_Logic("vec4(color.rgb, color.a)");
    fragment_source = fragment_source + "}";


    var uniforms = OMEGA.Omega3D.Shaders.Components.StandardUniforms();

    var fragmentOnlyUniforms = {};
    if(isTextured) fragmentOnlyUniforms["uSampler"] = { id:"uSampler", type: "sampler2D", glsl: "", value: null };
    return new OMEGA.Omega3D.Shader(vertex_source,fragment_source,  [OMEGA.Omega3D.Shaders.Components.StandardAttributes(),
        uniforms,
        {},
        fragmentOnlyUniforms]);
};
OMEGA.Omega3D.Shaders.ExposureToneMapping = function(){
    var vs = [
        OMEGA.Omega3D.Shaders.Components.Basic_Includes(),
        "varying vec2 vTexCoord;",
        "void main(void){",
        "mat4 mvp       = uProjectionMatrix * uViewMatrix * uModelMatrix;",
        "vec4 pos       = vec4(aVertexPos.xyz, 1.0);",
        "gl_Position    = pos;",
        OMEGA.Omega3D.Shaders.Components.Basic_TextureCoords("vTexCoord"),
        "}"
    ].join("\n");

    var fs = [
        OMEGA.Omega3D.Shaders.Components.Fragment_Precision_Mediump_Float(),
        OMEGA.Omega3D.Shaders.Components.Basic_Fragment_Includes(),
        "uniform sampler2D uSampler;",
        "varying vec2 vTexCoord;",
        "void main(void){",
        "float gamma = 2.2;",
        "float exposure = 0.5;",

        "vec3 hdrColor = texture2D(uSampler, vTexCoord ).rgb;",
        "vec3 mapped = hdrColor / (hdrColor + vec3(1.0));", // Reinhard tone mapping
        //"vec3 mapped   = vec3(1.0) - exp(-hdrColor * exposure);",
        //"mapped        = pow(mapped, vec3(1.0/gamma));",

        "vec4 color = vec4(mapped, 1.0);",

        OMEGA.Omega3D.Shaders.Components.Basic_Fragment_Logic("vec4(color.rgb, color.a)"),
        "}"
    ].join("\n");



    var uniforms = OMEGA.Omega3D.Shaders.Components.StandardUniforms();

    var fragmentOnlyUniforms = {};
    fragmentOnlyUniforms["uSampler"] = { id:"uSampler", type: "sampler2D", glsl: "", value: null };
    return new OMEGA.Omega3D.Shader(vs,fs,  [OMEGA.Omega3D.Shaders.Components.StandardAttributes(),
        uniforms,
        {},
        fragmentOnlyUniforms]);
};
OMEGA.Omega3D.Shaders.BloomMap = function(factor){
    var vs = [
        OMEGA.Omega3D.Shaders.Components.Basic_Includes(),
        "varying vec2 vTexCoord;",
        "void main(void){",
        "mat4 mvp       = uProjectionMatrix * uViewMatrix * uModelMatrix;",
        "vec4 pos       = vec4(aVertexPos.xyz, 1.0);",
        "gl_Position    = pos;",
        OMEGA.Omega3D.Shaders.Components.Basic_TextureCoords("vTexCoord"),
        "}"
    ].join("\n");

    var fs = [
        OMEGA.Omega3D.Shaders.Components.Fragment_Precision_Mediump_Float(),
        OMEGA.Omega3D.Shaders.Components.Basic_Fragment_Includes(),
        "uniform sampler2D uSampler;",
        "varying vec2 vTexCoord;",
        "void main(void){",
        "vec4 color = vec4(vec3(0), 1.0);",
        "vec4 fragcolor = texture2D(uSampler, vTexCoord );",
        "float brightness = dot( fragcolor.rgb, vec3( 0.2126, 0.7152, 0.0722));",

        "if(brightness > "+factor.toFixed(2).toString()+"){",
        "color = vec4( fragcolor.rgb, 1.0);",
        "}",

        OMEGA.Omega3D.Shaders.Components.Basic_Fragment_Logic("vec4(color.rgb, color.a)"),
        "}"
    ].join("\n");



    var uniforms = OMEGA.Omega3D.Shaders.Components.StandardUniforms();

    var fragmentOnlyUniforms = {};
    fragmentOnlyUniforms["uSampler"] = { id:"uSampler", type: "sampler2D", glsl: "", value: null };
    return new OMEGA.Omega3D.Shader(vs,fs,  [OMEGA.Omega3D.Shaders.Components.StandardAttributes(),
        uniforms,
        {},
        fragmentOnlyUniforms]);
};
OMEGA.Omega3D.Shaders.TextureBlend = function(){

    var vs = [
        OMEGA.Omega3D.Shaders.Components.Basic_Includes(),
        "varying vec2 vTexCoord;",
        "void main(void){",
        "mat4 mvp       = uProjectionMatrix * uViewMatrix * uModelMatrix;",
        "vec4 pos       = vec4(aVertexPos.xyz, 1.0);",
        "gl_Position    = pos;",
        OMEGA.Omega3D.Shaders.Components.Basic_TextureCoords("vTexCoord"),
        "}"
    ].join("\n");

    var fs = [
        OMEGA.Omega3D.Shaders.Components.Fragment_Precision_Mediump_Float(),
        OMEGA.Omega3D.Shaders.Components.Basic_Fragment_Includes(),
        "uniform sampler2D uSampler;",
        "uniform sampler2D uSampler_1;",
        "varying vec2 vTexCoord;",
        "void main(void){",
        "float gamma = 2.2;",
        "float exposure = 2.0;",

        "vec3 colorA   = texture2D(uSampler, vTexCoord ).rgb;",
        "vec3 colorB   = texture2D(uSampler_1, vec2(vTexCoord.s,vTexCoord.t)  ).rgb;",
        "colorA += colorB;",

        "vec3 result = vec3(1.0) - exp(-colorA * exposure);",
        // "result      = pow( result, vec3(1.0/gamma));",

        "vec4 color = vec4(colorA, 1.0);",

        OMEGA.Omega3D.Shaders.Components.Basic_Fragment_Logic("vec4(color.rgb, color.a)"),
        "}"
    ].join("\n");



    var uniforms = OMEGA.Omega3D.Shaders.Components.StandardUniforms();

    var fragmentOnlyUniforms = {};
    fragmentOnlyUniforms["uSampler"] = { id:"uSampler", type: "sampler2D", glsl: "", value: null };
    fragmentOnlyUniforms["uSampler_1"] = { id:"uSampler_1", type: "sampler2D", glsl: "", value: null };
    return new OMEGA.Omega3D.Shader(vs,fs,  [OMEGA.Omega3D.Shaders.Components.StandardAttributes(),
        uniforms,
        {},
        fragmentOnlyUniforms]);
};
OMEGA.Omega3D.Shaders.DownSample = function(){

    var vs = [
        OMEGA.Omega3D.Shaders.Components.Basic_Includes(),
        "varying vec2 vTexCoord;",
        "void main(void){",
        "mat4 mvp       = uProjectionMatrix * uViewMatrix * uModelMatrix;",
        "vec4 pos       = vec4(aVertexPos.xyz, 1.0);",
        "gl_Position    = pos;",
        OMEGA.Omega3D.Shaders.Components.Basic_TextureCoords("vTexCoord"),
        "}"
    ].join("\n");

    var fs = [
        OMEGA.Omega3D.Shaders.Components.Fragment_Precision_Mediump_Float(),
        OMEGA.Omega3D.Shaders.Components.Basic_Fragment_Includes(),
        "uniform sampler2D uSampler;",
        "varying vec2 vTexCoord;",
        "void main(void){",


        "vec4 color = texture2D(uSampler, vec2(1.0, 1.0) - vTexCoord );",

        OMEGA.Omega3D.Shaders.Components.Basic_Fragment_Logic("vec4(color.rgb, color.a)"),
        "}"
    ].join("\n");



    var uniforms = OMEGA.Omega3D.Shaders.Components.StandardUniforms();

    var fragmentOnlyUniforms = {};
    fragmentOnlyUniforms["uSampler"] = { id:"uSampler", type: "sampler2D", glsl: "", value: null };
    return new OMEGA.Omega3D.Shader(vs,fs,  [OMEGA.Omega3D.Shaders.Components.StandardAttributes(),
        uniforms,
        {},
        fragmentOnlyUniforms]);
};
OMEGA.Omega3D.Shaders.DirectionalBlur = function( horinzontal, resolution ){
    var vs = [
        OMEGA.Omega3D.Shaders.Components.Basic_Includes(),
        "varying vec2 vTexCoord;",
        "void main(void){",
        "mat4 mvp       = uProjectionMatrix * uViewMatrix * uModelMatrix;",
        "vec4 pos       = vec4(aVertexPos.xyz, 1.0);",
        "gl_Position    = pos;",
        OMEGA.Omega3D.Shaders.Components.Basic_TextureCoords("vTexCoord"),
        "}"
    ].join("\n");

    var fs = [
        OMEGA.Omega3D.Shaders.Components.Fragment_Precision_Mediump_Float(),
        OMEGA.Omega3D.Shaders.Components.Basic_Fragment_Includes(),
        "uniform sampler2D uSampler;",
        "varying vec2 vTexCoord;",

        "float weight[5];",

        "void main(void){",
        "weight[0] = 0.227027;",
        "weight[1] = 0.1945946;",
        "weight[2] = 0.1216216;",
        "weight[3] = 0.054054;",
        "weight[4] = 0.016216;",

        "vec4 color      = vec4(vec3(0), 1.0);",
        "vec2 tex_offset = vec2(1.0 / "+resolution.toFixed(1).toString()+");",
        "vec3 result     = texture2D( uSampler, vTexCoord).rgb * weight[0];",
        "float t = 1.0;",
        "if("+horinzontal+" == 1){",
        "for(int i = 1; i < 5; ++i){",
        "result += texture2D(uSampler, vTexCoord + vec2( tex_offset.x * t, 0.0)).rgb * weight[i];",
        "result += texture2D(uSampler, vTexCoord - vec2( tex_offset.x * t, 0.0)).rgb * weight[i];",
        "t += 1.0;",
        "}",
        "}else{",
        "for(int i = 1; i < 5; ++i){",
        "result += texture2D(uSampler, vTexCoord + vec2( 0.0, tex_offset.y * t)).rgb * weight[i];",
        "result += texture2D(uSampler, vTexCoord - vec2( 0.0, tex_offset.y * t)).rgb * weight[i];",
        "t += 1.0;",
        "}",
        "}",
        "color = vec4(result, 1.0);",
        OMEGA.Omega3D.Shaders.Components.Basic_Fragment_Logic("vec4(color.rgb, color.a)"),
        "}"
    ].join("\n");



    var uniforms = OMEGA.Omega3D.Shaders.Components.StandardUniforms();

    var fragmentOnlyUniforms = {};
    fragmentOnlyUniforms["uSampler"] = { id:"uSampler", type: "sampler2D", glsl: "", value: null };
    return new OMEGA.Omega3D.Shader(vs,fs,  [OMEGA.Omega3D.Shaders.Components.StandardAttributes(),
        uniforms,
        {},
        fragmentOnlyUniforms]);
}




//NEEDS TO BE REVISED.
OMEGA.Omega3D.Shaders.PP_ColorMultiply = function(val){
    //OMEGA.Omega3D.Log(" >> ShaderBuilder: Creating 'Basic PostProcessing' shader");
    var texture, fragmentColor;

    texture = "varying vec2 vTexCoord;";
    var vertex_source = OMEGA.Omega3D.Shaders.Components.Basic_Includes();
    vertex_source = vertex_source + texture;
    vertex_source = vertex_source + "void main(void){";
    vertex_source = vertex_source + OMEGA.Omega3D.Shaders.Components.Vertex_ScreenSpace_Conversion_V3("gl_Position", "aVertexPos");
    vertex_source = vertex_source + OMEGA.Omega3D.Shaders.Components.Basic_TextureCoords("vTexCoord");
    vertex_source = vertex_source + "}";



    texture = "uniform sampler2D uSampler;"+
        "varying vec2 vTexCoord;";


    var fragment_source = OMEGA.Omega3D.Shaders.Components.Fragment_Precision_Mediump_Float();
    fragment_source = fragment_source + OMEGA.Omega3D.Shaders.Components.Basic_Fragment_Includes();
    fragment_source = fragment_source + texture;
    fragment_source = fragment_source + "void main(void){";
    fragment_source = fragment_source + "vec4 color = texture2D(uSampler, vec2(vTexCoord.s, vTexCoord.t));";
    fragment_source = fragment_source + OMEGA.Omega3D.Shaders.Components.Basic_Fragment_Logic("vec4(color.rgb*"+val+", color.a)");
    fragment_source = fragment_source + "}";


    var uniforms = OMEGA.Omega3D.Shaders.Components.StandardUniforms();

    var fragmentOnlyUniforms = {};
    fragmentOnlyUniforms["uSampler"] = { id:"uSampler", type: "sampler2D", glsl: "", value: null };
    return new OMEGA.Omega3D.Shader(vertex_source,fragment_source,  [OMEGA.Omega3D.Shaders.Components.StandardAttributes(),
        uniforms,
        {},
        fragmentOnlyUniforms]);
};
OMEGA.Omega3D.Shaders.PP_Blur = function(){
    // OMEGA.Omega3D.Log(" >> ShaderBuilder: Creating 'Basic PostProcessing' shader");

    var vs = [
        OMEGA.Omega3D.Shaders.Components.Basic_Includes(),
        "varying vec2 vTexCoord;",

        "void main(void){",
        "mat4 mvp       = uProjectionMatrix * uViewMatrix * uModelMatrix;",
        "vec4 pos       = vec4(aVertexPos.xyz, 1.0);",
        "gl_Position    = pos;",
        "vTexCoord      = vec2(1.0, 1.0) - aTextureCoord;",

        "}"
    ].join("\n");


    var fs = [
        OMEGA.Omega3D.Shaders.Components.Fragment_Precision_Mediump_Float(),
        OMEGA.Omega3D.Shaders.Components.Basic_Fragment_Includes(),
        "uniform sampler2D uSampler;",
        "varying vec2 vTexCoord;",



        "void main(void){",
        "vec4 color = vec4(0);",
        "if ( Orientation == 0 ){",
        // Blur horizontal
        "for (int i = 0; i &lt; 10; ++i){",
        "if ( i &gt;= BlurAmount ) break;",
        "float offset = float(i) - halfBlur;",
        "color += texture2D(Sample0, vUv + vec2(offset * TexelSize.x, 0.0))", /* Gaussian(offset, deviation)*/
        "}",
        "}else{",
        // Blur vertical
        "for (int i = 0; i &lt; 10; ++i){",
        "if ( i &gt;= BlurAmount )break;",
        "float offset = float(i) - halfBlur;",
        "color += texture2D(Sample0, vUv + vec2(0.0, offset * TexelSize.y));", /* Gaussian(offset, deviation)*/
        "}",
        "}",

        // Calculate average
        "color = color / float(BlurAmount);",



        OMEGA.Omega3D.Shaders.Components.Basic_Fragment_Logic("vec4(color.rgb, color.a)"),
        "}"
    ].join("\n");



    var uniforms = OMEGA.Omega3D.Shaders.Components.StandardUniforms();

    var fragmentOnlyUniforms = {};
    fragmentOnlyUniforms["uSampler"] = { id:"uSampler", type: "sampler2D", glsl: "", value: null };
    return new OMEGA.Omega3D.Shader(vs,fs,  [OMEGA.Omega3D.Shaders.Components.StandardAttributes(),
        uniforms,
        {},
        fragmentOnlyUniforms]);
};
OMEGA.Omega3D.Shaders.Gausian_Blur = function(resolution){
    // OMEGA.Omega3D.Log(" >> ShaderBuilder: Creating 'Basic PostProcessing' shader");

    var vs = [
        OMEGA.Omega3D.Shaders.Components.Basic_Includes(),

        "void main(void){",
        "mat4 mvp       = uProjectionMatrix * uViewMatrix * uModelMatrix;",
        "vec4 pos       = vec4(aVertexPos.xyz, 1.0);",
        "gl_Position    = pos;",
        "}"
    ].join("\n");





    var fs = [
        OMEGA.Omega3D.Shaders.Components.Fragment_Precision_Mediump_Float(),
        OMEGA.Omega3D.Shaders.Components.Basic_Fragment_Includes(),
        "uniform sampler2D uSampler;",

        "float normpdf(in float x, in float sigma){",
        "return 0.39894*exp(-0.5*x*x/(sigma*sigma))/sigma;",
        "}",





        "void main(void){",
        "vec4 color = vec4(0);",
        //declare stuff
        "const int mSize = 8;",
        "const int kSize = (mSize-1)/2;",
        "float kernel[mSize];",
        "vec3 final_color = vec3(0.0);",

        //create the 1-D kernel
        "float sigma = 8.0;",
        "float Z = 0.0;",
        "for (int j = 0; j <= kSize; ++j){",
        "kernel[kSize+j] = kernel[kSize-j] = normpdf(float(j), sigma);",
        "}",

        //get the normalization factor (as the gaussian has been clamped)
        "for (int j = 0; j < mSize; ++j){",
        "Z += kernel[j];",
        "}",

        //read out the texels
        "vec2 uv =vec2(gl_FragCoord.xy);",
        "for (int i=-kSize; i <= kSize; ++i){",
        "for (int j=-kSize; j <= kSize; ++j){",
        "vec2 newUV = (uv.xy+vec2(float(i),float(j))) / vec2( "+resolution.toFixed(1).toString()+", "+resolution.toFixed(1).toString()+");",
        "final_color += kernel[kSize+j]*kernel[kSize+i]*texture2D(uSampler, newUV).rgb;",

        "}",
        "}",

        "color = vec4(final_color/(Z*Z), 1.0);",

        OMEGA.Omega3D.Shaders.Components.Basic_Fragment_Logic("vec4(color.rgb, color.a)"),
        "}"
    ].join("\n");



    var uniforms = OMEGA.Omega3D.Shaders.Components.StandardUniforms();

    var fragmentOnlyUniforms = {};
    fragmentOnlyUniforms["uSampler"] = { id:"uSampler", type: "sampler2D", glsl: "", value: null };
    return new OMEGA.Omega3D.Shader(vs,fs,  [OMEGA.Omega3D.Shaders.Components.StandardAttributes(),
        uniforms,
        {},
        fragmentOnlyUniforms]);
};
OMEGA.Omega3D.Shaders.PP_Bloom = function(amount){
    //OMEGA.Omega3D.Log(" >> ShaderBuilder: Creating 'Basic PostProcessing' shader");
    var texture, fragmentColor;

    texture = "varying vec2 vTexCoord;";
    var vertex_source = OMEGA.Omega3D.Shaders.Components.Basic_Includes();
    vertex_source = vertex_source + texture;
    vertex_source = vertex_source + "void main(void){";
    vertex_source = vertex_source + OMEGA.Omega3D.Shaders.Components.Vertex_ScreenSpace_Conversion_V3("gl_Position", "aVertexPos");
    vertex_source = vertex_source + OMEGA.Omega3D.Shaders.Components.Basic_TextureCoords("vTexCoord");
    vertex_source = vertex_source + "}";



    texture = "uniform sampler2D uSampler;"+
        "varying vec2 vTexCoord;";


    amount = amount == null ? "3" : amount;
    var fragment_source = OMEGA.Omega3D.Shaders.Components.Fragment_Precision_Mediump_Float();
    fragment_source = fragment_source + OMEGA.Omega3D.Shaders.Components.Basic_Fragment_Includes();
    fragment_source = fragment_source + texture;
    fragment_source = fragment_source + "void main(void){";
    fragment_source = fragment_source + "vec4 sum = vec4(0.0);";
    fragment_source = fragment_source + "vec2 tc = vec2(1.0, 1.0) - vTexCoord;";
    fragment_source = fragment_source + "int amount = 5;";
    fragment_source = fragment_source + "for( int i= -"+amount+" ;i < "+amount+"; i++){";
    fragment_source = fragment_source + "for ( int j = -"+amount+"; j < "+amount+"; j++){";
    fragment_source = fragment_source + "sum += texture2D(uSampler, tc + vec2(j, i)*0.004) * 0.25;";
    fragment_source = fragment_source + "}";
    fragment_source = fragment_source + "}";
    fragment_source = fragment_source + "if (texture2D(uSampler, tc).r < 0.3){";
    fragment_source = fragment_source + "gl_FragColor = sum*sum*0.012 + texture2D(uSampler, tc);";
    fragment_source = fragment_source + "}else{";

    fragment_source = fragment_source + "if (texture2D(uSampler, tc).r < 0.5){";

    fragment_source = fragment_source + "gl_FragColor = sum*sum*0.009 + texture2D(uSampler, tc);";
    fragment_source = fragment_source + "}else{";
    fragment_source = fragment_source + "gl_FragColor = sum*sum*0.0075 + texture2D(uSampler, tc);";
    fragment_source = fragment_source + "}}}";



    var uniforms = OMEGA.Omega3D.Shaders.Components.StandardUniforms();

    var fragmentOnlyUniforms = {};
    fragmentOnlyUniforms["uSampler"] = { id:"uSampler", type: "sampler2D", glsl: "", value: null };
    return new OMEGA.Omega3D.Shader(vertex_source,fragment_source,  [OMEGA.Omega3D.Shaders.Components.StandardAttributes(),
        uniforms,
        {},
        fragmentOnlyUniforms]);
};
















//Deprecated
//TODO: static lights
OMEGA.Omega3D.Shaders.Diffuse = function( isTextured ){
    var texture, fragmentColor;
    isTextured = isTextured || false;
    if(isTextured) texture = "varying vec2 vTexCoord;";
    else texture = "";
    var vertex_source = OMEGA.Omega3D.Shaders.Components.Basic_Includes();
    if(OMEGA.Omega3D.LIGHTS.length > 0 ) vertex_source = vertex_source + OMEGA.Omega3D.Shaders.Components.Basic_Light_Varying();
    vertex_source = vertex_source + texture;
    vertex_source = vertex_source + "void main(void){";
    vertex_source = vertex_source + OMEGA.Omega3D.Shaders.Components.Vertex_World_Conversion_V3("gl_Position", "aVertexPos");
    if(isTextured                      ) vertex_source = vertex_source + OMEGA.Omega3D.Shaders.Components.Basic_TextureCoords("vTexCoord");
    if(OMEGA.Omega3D.LIGHTS.length > 0 ) vertex_source = vertex_source + OMEGA.Omega3D.Shaders.Components.Basic_Diffuse_Vertex_Logic();
    vertex_source = vertex_source + "}";


    if(isTextured){
        texture = "uniform sampler2D uSampler;"+
            "varying vec2 vTexCoord;";
        fragmentColor = "texture2D(uSampler, vec2(vTexCoord.s, vTexCoord.t));";
    }
    else{
        fragmentColor = "vec4(0.8, 0.4, 0.0, 1.0);";
    }
    var fragment_source = OMEGA.Omega3D.Shaders.Components.Fragment_Precision_Mediump_Float();
    if(OMEGA.Omega3D.LIGHTS.length > 0 ) fragment_source = fragment_source + OMEGA.Omega3D.Shaders.Components.Basic_Light_Includes() + OMEGA.Omega3D.Shaders.Components.Basic_Light_Varying();
    if( isTextured                     ){
        fragment_source = fragment_source + OMEGA.Omega3D.Shaders.Components.Basic_Fragment_Includes();
        fragment_source = fragment_source + texture;
    }
    fragment_source = fragment_source + "void main(void){";
    if(OMEGA.Omega3D.LIGHTS.length > 0 ) fragment_source = fragment_source + OMEGA.Omega3D.Shaders.Components.Basic_Diffuse_Fragment_Logic(fragmentColor);
    else                                 fragment_source = fragment_source + OMEGA.Omega3D.Shaders.Components.Basic_Fragment_Logic(fragmentColor);
    fragment_source = fragment_source + "}";

    var fragmentOnlyUniforms = {};
    if(isTextured) fragmentOnlyUniforms["uSampler"] = { id:"uSampler", type: "sampler2D", glsl: "", value: null };
    return new OMEGA.Omega3D.Shader(vertex_source,fragment_source,  [OMEGA.Omega3D.Shaders.Components.StandardAttributes(),
        OMEGA.Omega3D.Shaders.Components.StandardUniforms(),
        OMEGA.Omega3D.Shaders.Components.StandardLightUniforms(),
        fragmentOnlyUniforms]);
};
//Deprecated
//TODO: static lights

OMEGA.Omega3D.Shaders.Phong = function( isTextured ){
    var texture, fragmentColor;
    isTextured = isTextured || false;
    if(isTextured) texture = "varying vec2 vTexCoord;";
    else texture = "";
    var vertex_source = OMEGA.Omega3D.Shaders.Components.Basic_Includes();
    if(OMEGA.Omega3D.LIGHTS.length > 0 ) vertex_source = vertex_source + OMEGA.Omega3D.Shaders.Components.Basic_Light_Varying();
    vertex_source = vertex_source + texture;
    vertex_source = vertex_source + "void main(void){";
    vertex_source = vertex_source + OMEGA.Omega3D.Shaders.Components.Vertex_World_Conversion_V3("gl_Position", "aVertexPos");
    if(isTextured                      ) vertex_source = vertex_source + OMEGA.Omega3D.Shaders.Components.Basic_TextureCoords("vTexCoord");
    if(OMEGA.Omega3D.LIGHTS.length > 0 ) vertex_source = vertex_source + OMEGA.Omega3D.Shaders.Components.Basic_Diffuse_Vertex_Logic();
    vertex_source = vertex_source + "}";


    if(isTextured){
        texture = "uniform sampler2D uSampler;"+
            "varying vec2 vTexCoord;";
        fragmentColor = "texture2D(uSampler, vec2(vTexCoord.s, vTexCoord.t));";
    }
    else{
        fragmentColor = "vec4(0.8, 0.4, 0.0, 1.0);";
    }
    var fragment_source = OMEGA.Omega3D.Shaders.Components.Fragment_Precision_Mediump_Float();
    if(OMEGA.Omega3D.LIGHTS.length > 0 ) fragment_source = fragment_source + OMEGA.Omega3D.Shaders.Components.Basic_Light_Includes() + OMEGA.Omega3D.Shaders.Components.Basic_Light_Varying();
    if( isTextured                     ){
        fragment_source = fragment_source + OMEGA.Omega3D.Shaders.Components.Basic_Fragment_Includes();
        fragment_source = fragment_source + texture;
    }
    fragment_source = fragment_source + "void main(void){";
    if(OMEGA.Omega3D.LIGHTS.length > 0 ) fragment_source = fragment_source + OMEGA.Omega3D.Shaders.Components.Basic_Phong_Fragment_Logic(fragmentColor);
    else                                 fragment_source = fragment_source + OMEGA.Omega3D.Shaders.Components.Basic_Fragment_Logic(fragmentColor);
    fragment_source = fragment_source + "}";


    var fragmentOnlyUniforms = {};
    if(isTextured) fragmentOnlyUniforms["uSampler"] = { id:"uSampler", type: "sampler2D", glsl: "", value: null };
    return new OMEGA.Omega3D.Shader(vertex_source,fragment_source,  [OMEGA.Omega3D.Shaders.Components.StandardAttributes(),
        OMEGA.Omega3D.Shaders.Components.StandardUniforms(),
        OMEGA.Omega3D.Shaders.Components.StandardLightUniforms(),
        fragmentOnlyUniforms]);
};


/**
 *  Cubemap Shader
 *
 * @returns {{vs_src: string, fs_src: string}}
 */
OMEGA.Omega3D.Shaders.CubeMap = function(scene){
    var vertex_source =
        OMEGA.Omega3D.Shaders.Components.Basic_Includes() +
        "varying vec3 vTexCoord;"+
        "void main(void){"+
        OMEGA.Omega3D.Shaders.Components.Vertex_View_Conversion_V3("gl_Position", "aVertexPos") +
        "vTexCoord = (vec4(aVertexPos, 0.0)).xyz;"+
        "}";


    var fragment_source =
        OMEGA.Omega3D.Shaders.Components.Fragment_Precision_Mediump_Float()+
        OMEGA.Omega3D.Shaders.Components.Basic_Fragment_Includes() +
        "uniform samplerCube uSampler;"+
        "varying vec3 vTexCoord;"+
        "void main(void){"+
        "gl_FragColor = textureCube(uSampler, vTexCoord);"+
        "}";


    var uniforms = OMEGA.Omega3D.Shaders.Components.StandardUniforms();
    //var injected = OMEGA.Omega3D.ShaderUtil.InjectGLSLForLights(vertex_source, fragment_source, uniforms, scene);
    //uniforms = injected.uniforms;
    //vertex_source =injected.vs;
    //fragment_source =injected.fs;


    var fragmentOnlyUniforms = {};
    fragmentOnlyUniforms["uSampler"] = { id:"uSampler", type: "sampler2D", glsl: "", value: null };
    return new OMEGA.Omega3D.Shader(vertex_source,fragment_source,  [OMEGA.Omega3D.Shaders.Components.StandardAttributes(),
        uniforms,
        {},
        fragmentOnlyUniforms]);
};

OMEGA.Omega3D.Shaders.SkyBox = function(scene){

    var vs = [
        OMEGA.Omega3D.Shaders.Components.Basic_Includes(),
        "varying vec3 vTexCoord;",
        "void main(void){",

        "mat4 viewMatrix = mat4(mat3(uViewMatrix));",
        "gl_Position = uProjectionMatrix * viewMatrix * uModelMatrix * vec4(aVertexPos, 1.0);",
        "vTexCoord   = (vec4(aVertexPos, 0.0)).xyz;",
        "}",
    ].join("\n");

    var fs = [
        OMEGA.Omega3D.Shaders.Components.Fragment_Precision_Mediump_Float(),
        OMEGA.Omega3D.Shaders.Components.Basic_Fragment_Includes(),
        "uniform samplerCube uSampler;",
        "varying vec3 vTexCoord;",
        "void main(void){",
        "gl_FragColor = textureCube(uSampler, vTexCoord);",
        "}",
    ].join("\n");



    var uniforms = OMEGA.Omega3D.Shaders.Components.StandardUniforms();
    var fragmentOnlyUniforms = {};
    fragmentOnlyUniforms["uSampler"] = { id:"uSampler", type: "sampler2D", glsl: "", value: null };
    return new OMEGA.Omega3D.Shader(vs,fs,  [OMEGA.Omega3D.Shaders.Components.StandardAttributes(),
        uniforms,
        {},
        fragmentOnlyUniforms]);
};






OMEGA.Omega3D.Shaders.Morph = function( ){
    var vertex_source =

        OMEGA.Omega3D.Shaders.Components.Basic_Includes_Post_Processing() +
        OMEGA.Omega3D.Shaders.Components.Noise3D();
    if(OMEGA.Omega3D.LIGHTS.length > 0 ) vertex_source = vertex_source + OMEGA.Omega3D.Shaders.Components.Basic_Light_Varying();
    vertex_source = vertex_source +
        "varying float noise;" +
        "float turbulence( vec3 p ) {" +
        "float t = -0.5;" +
        "for (float f = 1.0 ; f <= 10.0 ; f++){" +
        "float power = pow( 2.0, f );" +
        "t += abs( pnoise( vec3( power * p ), vec3( 10.0, 10.0, 10.0 ) ) / power );" +
        "}" +
        " return t;" +
        "}" +




        "varying float vTime;" +
        "varying vec2 vTexCoord;" +
        "varying vec3 vVertexNormal;" +
        "void main(void){"+
        "vec4 pos = uProjectionMatrix * uViewMatrix * uModelMatrix * vec4(aVertexPos.xy, 0.0, 1.0);" +
        "noise = 10.0 *  -.10 * turbulence(.5*aVertexNormal + uTime * 0.025);"+ //
        "float b = 5.0 * pnoise(0.05 * aVertexPos, vec3(100.0) );" +
        "float displacement = - 1.5 * noise + b;" +
        "vec3 newPos = aVertexPos + aVertexNormal * displacement;" +
        "vVertexNormal = abs(aVertexNormal * displacement);"+

        "gl_Position = uProjectionMatrix * uViewMatrix * uModelMatrix *vec4(newPos, 1.0);" +
        "vTexCoord = aTextureCoord;" +
        "vTime = uTime;";
    if(OMEGA.Omega3D.LIGHTS.length > 0 ) vertex_source = vertex_source + OMEGA.Omega3D.Shaders.Components.Basic_Diffuse_Vertex_Logic();
    vertex_source = vertex_source + "}";


    var fragment_source =
        OMEGA.Omega3D.Shaders.Components.Fragment_Precision_Mediump_Float() +
        OMEGA.Omega3D.Shaders.Components.Basic_Fragment_Includes();
    if(OMEGA.Omega3D.LIGHTS.length > 0 ) fragment_source = fragment_source + OMEGA.Omega3D.Shaders.Components.Basic_Light_Includes() + OMEGA.Omega3D.Shaders.Components.Basic_Light_Varying();
    fragment_source = fragment_source +
        "uniform sampler2D uSampler;"+
        "varying float vTime;" +
        "varying vec2 vTexCoord;"+
        "varying float noise;" +
        "void main(void){" +
        "float n = noise;" +
        "if(noise < 0.05)n = 0.05;" +
        "if(noise > 0.4)n = 0.4;" +
        "float ypos = 1.0-1.3*n;" +
        "if(ypos < 0.0) ypos = 0.0;" +
        "vec2 tPos = vec2(0, ypos*1.7);";
    var fragmentColor = "texture2D(uSampler, tPos);";
    if(OMEGA.Omega3D.LIGHTS.length > 0 ) fragment_source = fragment_source + OMEGA.Omega3D.Shaders.Components.Basic_Phong_Fragment_Logic(fragmentColor);
    else                                 fragment_source = fragment_source + OMEGA.Omega3D.Shaders.Components.Basic_Fragment_Logic(fragmentColor);
    fragment_source = fragment_source + "}";
    var fragmentOnlyUniforms = {};
    fragmentOnlyUniforms["uSampler"] = { id:"uSampler", type: "sampler2D", glsl: "", value: null };
    return new OMEGA.Omega3D.Shader(vertex_source,fragment_source,  [OMEGA.Omega3D.Shaders.Components.StandardAttributes(),
        OMEGA.Omega3D.Shaders.Components.StandardUniforms(),
        {},
        fragmentOnlyUniforms]);
};

OMEGA.Omega3D.Shaders.CubemapReflection = function(scene ) {
    var vertex_source =
        "attribute vec2 aTextureCoord;"+
        "attribute vec3 aVertexPos;"+
        "attribute vec3 aVertexNormal;"+

        "uniform mat4 uModelMatrix;"+
        "uniform mat4 uProjectionMatrix;"+
        "uniform mat4 uViewMatrix;"+
        "uniform mat4 uInvViewMatrix;"+
        "uniform mat3 uNormalMatrix;"+

        "varying vec3 vTexCoord;" +
        "void main(void){"+
        "   vec3 vEyeNormal = uNormalMatrix * aVertexNormal;"+
        "   vec4 vVert4 = uViewMatrix * uModelMatrix * vec4(aVertexPos, 1.0);"+
        "   vec3 vEyeVertex = normalize(vVert4.xyz/vVert4.w);"+
        "   vec4 vCoords = vec4(reflect(vVert4.xyz, vEyeNormal.xyz), 1.0);"+
        "   vCoords = uInvViewMatrix * vCoords;"+
        "   vTexCoord.xyz = normalize(vCoords.xyz);"+
        "   gl_Position = uProjectionMatrix * uViewMatrix * uModelMatrix *vec4(aVertexPos, 1.0);" +
        "}";
    var fragment_source =
        OMEGA.Omega3D.Shaders.Components.Fragment_Precision_Mediump_Float() +
        OMEGA.Omega3D.Shaders.Components.Basic_Fragment_Includes() +
        "varying vec3 vTexCoord;" +
        "uniform samplerCube uSampler;"+
        "void main(void){"+
        "vec4 color = textureCube(uSampler, vTexCoord);"+

        OMEGA.Omega3D.Shaders.Components.Basic_Fragment_Logic("vec4(color.rgb, color.a)") +
        "}";

    var uniforms = OMEGA.Omega3D.Shaders.Components.StandardUniforms();
    if(scene != null){
        var injected = OMEGA.Omega3D.ShaderUtil.InjectGLSLForLights(vertex_source, fragment_source, uniforms, scene);
        uniforms = injected.uniforms;
        vertex_source =injected.vs;
        fragment_source =injected.fs;

        //fog
        if(scene.hasFog){
            injected = OMEGA.Omega3D.ShaderUtil.InjectGLSLForFog(vertex_source, fragment_source, uniforms,scene);
            uniforms = injected.uniforms;
            vertex_source =injected.vs;
            fragment_source =injected.fs;
        }
    }

    var fragmentOnlyUniforms = {};
    fragmentOnlyUniforms["uSampler"] = { id:"uSampler", type: "sampler2D", glsl: "", value: null };
    return new OMEGA.Omega3D.Shader(vertex_source,fragment_source,  [OMEGA.Omega3D.Shaders.Components.StandardAttributes(),
        uniforms,
        {},
        fragmentOnlyUniforms]);
}


/**
 *  Post Processing Kernel Shader.
 *
 * @returns {OMEGA.Omega3D.Shader}
 * @constructor
 */
OMEGA.Omega3D.Shaders.PostProcessingKernel = function( kernel ){
    var vertex_source =
        OMEGA.Omega3D.Shaders.Components.Basic_Includes_Post_Processing() +
        "varying vec2 vTexCoord;" +
        "void main(void){"+
        "vec4 pos = uProjectionMatrix * uViewMatrix * uModelMatrix * vec4(aVertexPos.xy, 0.0, 1.0);" +
        "gl_Position = pos;" +
        "vTexCoord = aTextureCoord;" +
        "}";


    var fragment_source =
        OMEGA.Omega3D.Shaders.Components.Fragment_Precision_Mediump_Float()+
        OMEGA.Omega3D.Shaders.Components.Basic_Fragment_Includes() +
        "uniform float uKernel[9];" +
        "uniform sampler2D uSampler;"+
        "varying vec2 vTexCoord;"+
        "void main(void){"+
        "vec4 color = texture2D(uSampler, vec2(vTexCoord.s, vTexCoord.t));" +
        "vec2 onePixel = vec2(1.0, 1.0) / vec2(512, 512);" +
        "vec4 colorSum =" +
        "texture2D(uSampler, vTexCoord + onePixel * vec2(-1, -1)) * uKernel[1] + " +
        "texture2D(uSampler, vTexCoord + onePixel * vec2( 0, -1)) * uKernel[1] + " +
        "texture2D(uSampler, vTexCoord + onePixel * vec2( 1, -1)) * uKernel[2] + " +
        "texture2D(uSampler, vTexCoord + onePixel * vec2(-1,  0)) * uKernel[3] + " +
        "texture2D(uSampler, vTexCoord + onePixel * vec2( 0,  0)) * uKernel[4] + " +
        "texture2D(uSampler, vTexCoord + onePixel * vec2( 1,  0)) * uKernel[5] + " +
        "texture2D(uSampler, vTexCoord + onePixel * vec2(-1,  1)) * uKernel[6] + " +
        "texture2D(uSampler, vTexCoord + onePixel * vec2( 0,  1)) * uKernel[7] + " +
        "texture2D(uSampler, vTexCoord + onePixel * vec2( 1,  1)) * uKernel[8];" ;



    var str = "float kernelWeight = uKernel[0] + uKernel[1] + uKernel[2] + uKernel[3] + uKernel[4] + uKernel[5] + uKernel[6] +uKernel[7] +uKernel[8];" +
        "if(kernelWeight <= 0.0){"+
        "kernelWeight = 1.0;"+
        "}";
    fragment_source = fragment_source + str;


    fragment_source = fragment_source  +
        "gl_FragColor = vec4((colorSum / kernelWeight).rgb, 1);" +
        "}";
    return new OMEGA.Omega3D.PostProcessingKernelShader(vertex_source,fragment_source, kernel);
};
OMEGA.Omega3D.Shaders.PostProcessingKernel.Kernels = {
    normal: [
        0, 0, 0,
        0, 1, 0,
        0, 0, 0
    ],
    gaussianBlur: [
        0.045, 0.122, 0.045,
        0.122, 0.332, 0.122,
        0.045, 0.122, 0.045
    ],
    gaussianBlur2: [
        1, 2, 1,
        2, 4, 2,
        1, 2, 1
    ],
    gaussianBlur3: [
        0, 1, 0,
        1, 1, 1,
        0, 1, 0
    ],
    gaussianBlur4: [
        0, 16, 0,
        16, 0, 16,
        0, 16, 0
    ],
    unsharpen: [
        -1, -1, -1,
        -1,  9, -1,
        -1, -1, -1
    ],
    sharpness: [
        0,-1, 0,
        -1, 5,-1,
        0,-1, 0
    ],
    sharpen: [
        0, -1, 0,
        -1, 16, -1,
        0, -1, 0
    ],
    edgeDetect: [
        -0.125, -0.125, -0.125,
        -0.125,  1,     -0.125,
        -0.125, -0.125, -0.125
    ],
    edgeDetect2: [
        -1, -1, -1,
        -1,  8, -1,
        -1, -1, -1
    ],
    edgeDetect3: [
        -5, 0, 0,
        0, 0, 0,
        0, 0, 5
    ],
    edgeDetect4: [
        -1, -1, -1,
        0,  0,  0,
        1,  1,  1
    ],
    edgeDetect5: [
        -1, -1, -1,
        2,  2,  2,
        -1, -1, -1
    ],
    edgeDetect6: [
        -5, -5, -5,
        -5, 39, -5,
        -5, -5, -5
    ],
    sobelHorizontal: [
        1,  2,  1,
        0,  0,  0,
        -1, -2, -1
    ],
    sobelVertical: [
        1,  0, -1,
        2,  0, -2,
        1,  0, -1
    ],
    previtHorizontal: [
        1,  1,  1,
        0,  0,  0,
        -1, -1, -1
    ],
    previtVertical: [
        1,  0, -1,
        1,  0, -1,
        1,  0, -1
    ],
    boxBlur: [
        0.111, 0.111, 0.111,
        0.111, 0.111, 0.111,
        0.111, 0.111, 0.111
    ],
    robbieBlur: [
        0, 0, 0,
        0, 12, 0,
        0, 0, 0
    ],
    triangleBlur: [
        0.0625, 0.125, 0.0625,
        0.125,  0.25,  0.125,
        0.0625, 0.125, 0.0625
    ],
    emboss: [
        -2, -1,  0,
        -1,  1,  1,
        0,  1,  2
    ]
};

function AtlasTexture( img, ID ){
    Texture.apply( this, [img, true, ID ] );

    this.handleTextureLoaded = function(image, texture ){
        if(!this.needsUpdate) {
            this.gl_context.bindTexture(this.gl_context.TEXTURE_2D, texture);
            this.gl_context.pixelStorei(this.gl_context.UNPACK_FLIP_Y_WEBGL, false);
            if(image)this.gl_context.texImage2D(this.gl_context.TEXTURE_2D,  this.ID, this.gl_context.RGBA, this.gl_context.RGBA, this.gl_context.UNSIGNED_BYTE, image);
            this.gl_context.texParameteri(this.gl_context.TEXTURE_2D, this.gl_context.TEXTURE_MIN_FILTER, this.gl_context.NEAREST);
            this.gl_context.texParameteri(this.gl_context.TEXTURE_2D, this.gl_context.TEXTURE_MAG_FILTER, this.gl_context.NEAREST);
            if(image && this.IsPowerOf2(image.width) && this.IsPowerOf2(image.height) ){
                this.gl_context.generateMipmap(this.gl_context.TEXTURE_2D);
                this.gl_context.texParameteri(this.gl_context.TEXTURE_2D, this.gl_context.TEXTURE_MIN_FILTER, this.gl_context.NEAREST_MIPMAP_LINEAR );
            }else{
                if(image) this.gl_context.generateMipmap(this.gl_context.TEXTURE_2D);
                this.gl_context.texParameteri(this.gl_context.TEXTURE_2D, this.gl_context.TEXTURE_MIN_FILTER, this.gl_context.LINEAR);
                this.gl_context.texParameteri(this.gl_context.TEXTURE_2D, this.gl_context.TEXTURE_MAG_FILTER, this.gl_context.LINEAR);
                this.gl_context.texParameteri(this.gl_context.TEXTURE_2D, this.gl_context.TEXTURE_WRAP_S, this.gl_context.CLAMP_TO_EDGE);
                this.gl_context.texParameteri(this.gl_context.TEXTURE_2D, this.gl_context.TEXTURE_WRAP_T, this.gl_context.CLAMP_TO_EDGE);
            }
            this.gl_context.bindTexture(this.gl_context.TEXTURE_2D, null);
        }else {
            this.gl_context.bindTexture(this.gl_context.TEXTURE_2D, texture);
            this.gl_context.texParameteri(this.gl_context.TEXTURE_2D, this.gl_context.TEXTURE_MAG_FILTER, this.gl_context.LINEAR);
            this.gl_context.texParameteri(this.gl_context.TEXTURE_2D, this.gl_context.TEXTURE_MIN_FILTER, this.gl_context.LINEAR);
            this.gl_context.texParameteri(this.gl_context.TEXTURE_2D, this.gl_context.TEXTURE_WRAP_S, this.gl_context.CLAMP_TO_EDGE);
            this.gl_context.texParameteri(this.gl_context.TEXTURE_2D, this.gl_context.TEXTURE_WRAP_T, this.gl_context.CLAMP_TO_EDGE);
        };
    };
    this.Enable = function( shader ){

        this.gl_context.activeTexture( this.gl_context.TEXTURE0+this.ID );
        this.gl_context.bindTexture( this.gl_context.TEXTURE_2D,this.tex);
        if(shader && this.img){
            if(shader.GetSamplerLocation(this.tex_id) > -1 ) this.gl_context.uniform1i(shader.GetSamplerLocation(this.tex_id), this.ID );
        }

    };
    this.Disable = function(){
        this.gl_context.bindTexture(this.gl_context.TEXTURE_2D, null);
    };
    this.handleTextureLoaded(this.img, this.tex);
};
AtlasTexture.prototype = new Texture();
OMEGA.Omega3D.BasicTexture = AtlasTexture;
function BasicTexture( img, needsUpdate, ID ){
    Texture.apply( this, arguments );

    this.handleTextureLoaded = function(image, texture ){
        if(!this.needsUpdate) {
            this.gl_context.bindTexture(this.gl_context.TEXTURE_2D, texture);
            this.gl_context.pixelStorei(this.gl_context.UNPACK_FLIP_Y_WEBGL, false);
            if(image)this.gl_context.texImage2D(this.gl_context.TEXTURE_2D,  this.ID, this.gl_context.RGBA, this.gl_context.RGBA, this.gl_context.UNSIGNED_BYTE, image);
            this.gl_context.texParameteri(this.gl_context.TEXTURE_2D, this.gl_context.TEXTURE_MIN_FILTER, this.gl_context.NEAREST);
            this.gl_context.texParameteri(this.gl_context.TEXTURE_2D, this.gl_context.TEXTURE_MAG_FILTER, this.gl_context.NEAREST);
            if(image && this.IsPowerOf2(image.width) && this.IsPowerOf2(image.height) ){
                this.gl_context.generateMipmap(this.gl_context.TEXTURE_2D);
                this.gl_context.texParameteri(this.gl_context.TEXTURE_2D, this.gl_context.TEXTURE_MIN_FILTER, this.gl_context.NEAREST_MIPMAP_LINEAR );
            }else{
                if(image&& this.IsPowerOf2(image.width) && this.IsPowerOf2(image.height)) this.gl_context.generateMipmap(this.gl_context.TEXTURE_2D);
                this.gl_context.texParameteri(this.gl_context.TEXTURE_2D, this.gl_context.TEXTURE_MIN_FILTER, this.gl_context.LINEAR);
                this.gl_context.texParameteri(this.gl_context.TEXTURE_2D, this.gl_context.TEXTURE_MAG_FILTER, this.gl_context.LINEAR);
                this.gl_context.texParameteri(this.gl_context.TEXTURE_2D, this.gl_context.TEXTURE_WRAP_S, this.gl_context.CLAMP_TO_EDGE);
                this.gl_context.texParameteri(this.gl_context.TEXTURE_2D, this.gl_context.TEXTURE_WRAP_T, this.gl_context.CLAMP_TO_EDGE);

            }
            this.gl_context.bindTexture(this.gl_context.TEXTURE_2D, null);
        }else {
            this.gl_context.bindTexture(this.gl_context.TEXTURE_2D, texture);
            this.gl_context.texParameteri(this.gl_context.TEXTURE_2D, this.gl_context.TEXTURE_MAG_FILTER, this.gl_context.LINEAR);
            this.gl_context.texParameteri(this.gl_context.TEXTURE_2D, this.gl_context.TEXTURE_MIN_FILTER, this.gl_context.LINEAR);
            this.gl_context.texParameteri(this.gl_context.TEXTURE_2D, this.gl_context.TEXTURE_WRAP_S, this.gl_context.CLAMP_TO_EDGE);
            this.gl_context.texParameteri(this.gl_context.TEXTURE_2D, this.gl_context.TEXTURE_WRAP_T, this.gl_context.CLAMP_TO_EDGE);
        };
    };
    this.Enable = function( shader ){
        //this.gl_context.activeTexture( this.gl_context.TEXTURE0+this.ID );
        //this.gl_context.bindTexture( this.gl_context.TEXTURE_2D,this.tex);
        //if(shader && this.img){
        //    console.log( "ENABLE : " + this.ID + " -> "+ this.tex_id  + ": " + shader.GetSamplerLocation(this.tex_id));
        //    if(shader.GetSamplerLocation(this.tex_id) > -1 ) this.gl_context.uniform1i(shader.GetSamplerLocation(this.tex_id), this.ID );
        //}
        this.gl_context.activeTexture( this.gl_context.TEXTURE0+this.ID);
        this.gl_context.bindTexture(this.gl_context.TEXTURE_2D,  this.tex);
        if(shader && this.img){
            // console.log( "ENABLE : " + this.ID + " -> "+ this.tex_id  + ": " + shader.GetSamplerLocation(this.tex_id));
            var samplerLocation = shader.GetSamplerLocation(this.tex_id);
            this.gl_context.uniform1i(samplerLocation , this.ID );
        }

    };
    this.Disable = function(){
        this.gl_context.bindTexture(this.gl_context.TEXTURE_2D, null);
    };
    this.handleTextureLoaded(this.img, this.tex);
};
BasicTexture.prototype = new Texture();
OMEGA.Omega3D.BasicTexture = BasicTexture;
function CubemapTexture( imgs, needsUpdate, ID ){
    Texture.apply( this, arguments );
    this.handleTextureLoaded = function(image, texture ){
        this.gl_context.bindTexture(this.gl_context.TEXTURE_CUBE_MAP, texture);
        this.gl_context.texParameteri(this.gl_context.TEXTURE_CUBE_MAP, this.gl_context.TEXTURE_WRAP_S, this.gl_context.CLAMP_TO_EDGE);
        this.gl_context.texParameteri(this.gl_context.TEXTURE_CUBE_MAP, this.gl_context.TEXTURE_WRAP_T, this.gl_context.CLAMP_TO_EDGE);
        this.gl_context.texParameteri(this.gl_context.TEXTURE_CUBE_MAP, this.gl_context.TEXTURE_MIN_FILTER, this.gl_context.LINEAR);
        this.gl_context.texParameteri(this.gl_context.TEXTURE_CUBE_MAP, this.gl_context.TEXTURE_MAG_FILTER, this.gl_context.LINEAR);

        var faces = [ this.gl_context.TEXTURE_CUBE_MAP_POSITIVE_X,
            this.gl_context.TEXTURE_CUBE_MAP_NEGATIVE_X,
            this.gl_context.TEXTURE_CUBE_MAP_NEGATIVE_Y,
            this.gl_context.TEXTURE_CUBE_MAP_POSITIVE_Y,
            this.gl_context.TEXTURE_CUBE_MAP_POSITIVE_Z,
            this.gl_context.TEXTURE_CUBE_MAP_NEGATIVE_Z ];


        for (var i = 0; i < faces.length; i++) {
            this.gl_context.pixelStorei( this.gl_context.UNPACK_FLIP_Y_WEBGL, true);
            this.gl_context.bindTexture( this.gl_context.TEXTURE_CUBE_MAP, texture);
            this.gl_context.texImage2D(faces[i], 0,  this.gl_context.RGBA,  this.gl_context.RGBA,  this.gl_context.UNSIGNED_BYTE, image[i]);
            this.gl_context.bindTexture( this.gl_context.TEXTURE_CUBE_MAP, null);

        }
    };
    this.Enable = function( shader ){
        this.gl_context.activeTexture( this.gl_context.TEXTURE0+this.ID );
        this.gl_context.bindTexture( this.gl_context.TEXTURE_CUBE_MAP, this.tex);
        if(shader){
            this.gl_context.uniform1i(shader.GetSamplerLocation() , this.ID );
        }
    };
    this.Disable = function(){
        this.gl_context.bindTexture(this.gl_context.TEXTURE_CUBE_MAP, null)
    };

    this.handleTextureLoaded(this.img, this.tex);
};
CubemapTexture.prototype = new Texture();
OMEGA.Omega3D.CubemapTexture = CubemapTexture;
function FrameBufferTexture(w, h, ID){
    Texture.apply( this, [null, true, ID] );
    var frameBuffer, depthBuffer;

    //frame buffer
    frameBuffer =  this.gl_context.createFramebuffer();
    this.gl_context.bindFramebuffer(  this.gl_context.FRAMEBUFFER, frameBuffer);
    frameBuffer.width = w || 1024;
    frameBuffer.height = h || 1024;

    //texture
    this.tex =  this.gl_context.createTexture();
    this.gl_context.bindTexture( this.gl_context.TEXTURE_2D,  this.tex);
    this.gl_context.pixelStorei(this.gl_context.UNPACK_FLIP_Y_WEBGL, true);
    this.gl_context.texParameteri(this.gl_context.TEXTURE_2D, this.gl_context.TEXTURE_WRAP_S, this.gl_context.CLAMP_TO_EDGE);
    this.gl_context.texParameteri(this.gl_context.TEXTURE_2D, this.gl_context.TEXTURE_WRAP_T, this.gl_context.CLAMP_TO_EDGE);
    this.gl_context.texParameteri( this.gl_context.TEXTURE_2D,  this.gl_context.TEXTURE_MAG_FILTER,  this.gl_context.LINEAR );
    this.gl_context.texParameteri( this.gl_context.TEXTURE_2D,  this.gl_context.TEXTURE_MIN_FILTER,  this.gl_context.LINEAR);
    this.gl_context.texImage2D( this.gl_context.TEXTURE_2D, 0,  this.gl_context.RGBA, frameBuffer.width, frameBuffer.height, 0,  this.gl_context.RGBA,  this.gl_context.FLOAT, null);
    // this.gl_context.generateMipmap( this.gl_context.TEXTURE_2D);

    //depth buffer
    var depthBuffer = this.gl_context.createRenderbuffer();
    this.gl_context.bindRenderbuffer( this.gl_context.RENDERBUFFER, depthBuffer );
    this.gl_context.renderbufferStorage(this.gl_context.RENDERBUFFER, this.gl_context.DEPTH_COMPONENT16, frameBuffer.width, frameBuffer.height);

    this.gl_context.framebufferTexture2D(this.gl_context.FRAMEBUFFER, this.gl_context.COLOR_ATTACHMENT0, this.gl_context.TEXTURE_2D, this.tex, 0);
    this.gl_context.framebufferRenderbuffer(this.gl_context.FRAMEBUFFER, this.gl_context.DEPTH_ATTACHMENT, this.gl_context.RENDERBUFFER, depthBuffer);

    if(!this.gl_context.checkFramebufferStatus(this.gl_context.FRAMEBUFFER) === this.gl_context.FRAMEBUFFER_COMPLETE) {
        console.error("Framebuffer incomplete!");
    }

    this.gl_context.bindTexture(this.gl_context.TEXTURE_2D, null);
    this.gl_context.bindRenderbuffer(this.gl_context.RENDERBUFFER, null);
    this.gl_context.bindFramebuffer(this.gl_context.FRAMEBUFFER, null);


    this.Enable = function( shader ){
        //this.gl_context.bindRenderbuffer( this.gl_context.RENDERBUFFER, depthBuffer );
        this.gl_context.activeTexture( this.gl_context.TEXTURE0+this.ID);
        this.gl_context.bindTexture(this.gl_context.TEXTURE_2D,  this.tex);
        if(shader){
            console.log( "ENABLE : " + this.ID + " -> "+ this.tex_id  + ": " + shader.GetSamplerLocation(this.tex_id));
            var samplerLocation = shader.GetSamplerLocation(this.tex_id);
            this.gl_context.uniform1i(samplerLocation , this.ID );
        }
    };
    this.GetFrameBuffer = function(){
        return frameBuffer;
    };
    this.Disable = function(){
        this.gl_context.bindTexture(this.gl_context.TEXTURE_2D, null);
        this.gl_context.bindRenderbuffer( this.gl_context.RENDERBUFFER, null );
        this.gl_context.bindFramebuffer(this.gl_context.FRAMEBUFFER, null);
    };

    this.Update = function() {
        if(!this.needsUpdate ) if(!this.isDirty ) return;
        this.isDirty    = false;
    };

};
FrameBufferTexture.prototype = new Texture();
OMEGA.Omega3D.FrameBufferTexture = FrameBufferTexture;function ShadowMapTexture(w, h, ID){
    ID = ID || 0;
    Texture.apply( this, [null, true, ID]);
    // this.tex_id = "uShadowMap";
    var depthTextureExt = this.gl_context.getExtension("WEBKIT_WEBGL_depth_texture") ||
        this.gl_context.getExtension("MOZ_OES_depth_texture") ||
        this.gl_context.getExtension("WEBKIT_OES_depth_texture") ||
        this.gl_context.getExtension("WEBGL_depth_texture") ||
        this.gl_context.getExtension("OES_depth_texture") ||
        this.gl_context.getExtension( "MOZ_WEBGL_depth_texture" );
    if(!depthTextureExt) { alert("Depthtextures not available in this browser."); }


    //frame buffer
    var  frameBuffer =  this.gl_context.createFramebuffer();
    this.gl_context.bindFramebuffer( this.gl_context.FRAMEBUFFER, frameBuffer);
    frameBuffer.width  = w || 1024;
    frameBuffer.height = h || 1024;


    // Create a color texture
    var colorTexture = this.gl_context.createTexture();
    this.gl_context.bindTexture(this.gl_context.TEXTURE_2D, colorTexture);
    this.gl_context.texParameteri(this.gl_context.TEXTURE_2D, this.gl_context.TEXTURE_MAG_FILTER, this.gl_context.NEAREST);
    this.gl_context.texParameteri(this.gl_context.TEXTURE_2D, this.gl_context.TEXTURE_MIN_FILTER, this.gl_context.NEAREST);
    this.gl_context.texParameteri(this.gl_context.TEXTURE_2D, this.gl_context.TEXTURE_WRAP_S, this.gl_context.CLAMP_TO_EDGE);
    this.gl_context.texParameteri(this.gl_context.TEXTURE_2D, this.gl_context.TEXTURE_WRAP_T, this.gl_context.CLAMP_TO_EDGE);
    this.gl_context.texImage2D(this.gl_context.TEXTURE_2D, 0, this.gl_context.RGBA, frameBuffer.width, frameBuffer.height, 0, this.gl_context.RGBA, this.gl_context.FLOAT, null);


    //depth texture
    this.tex =  this.gl_context.createTexture();
    this.gl_context.bindTexture( this.gl_context.TEXTURE_2D,  this.tex);
    this.gl_context.texParameteri(this.gl_context.TEXTURE_2D, this.gl_context.TEXTURE_WRAP_S, this.gl_context.CLAMP_TO_EDGE);
    this.gl_context.texParameteri(this.gl_context.TEXTURE_2D, this.gl_context.TEXTURE_WRAP_T, this.gl_context.CLAMP_TO_EDGE);
    this.gl_context.texParameteri( this.gl_context.TEXTURE_2D,  this.gl_context.TEXTURE_MAG_FILTER,  this.gl_context.NEAREST );
    this.gl_context.texParameteri( this.gl_context.TEXTURE_2D,  this.gl_context.TEXTURE_MIN_FILTER,  this.gl_context.NEAREST);
    this.gl_context.texImage2D( this.gl_context.TEXTURE_2D, 0,  this.gl_context.DEPTH_COMPONENT, frameBuffer.width, frameBuffer.height, 0,  this.gl_context.DEPTH_COMPONENT,  this.gl_context.UNSIGNED_SHORT, null);


    this.gl_context.framebufferTexture2D( this.gl_context.FRAMEBUFFER,  this.gl_context.COLOR_ATTACHMENT0,  this.gl_context.TEXTURE_2D, colorTexture, 0);
    this.gl_context.framebufferTexture2D( this.gl_context.FRAMEBUFFER, this.gl_context.DEPTH_ATTACHMENT, this.gl_context.TEXTURE_2D, this.tex, 0);



    // while (this.gl_context.checkFramebufferStatus(this.gl_context.FRAMEBUFFER) != this.gl_context.FRAMEBUFFER_COMPLETE) {}
    // console.log(this.gl_context.checkFramebufferStatus(this.gl_context.FRAMEBUFFER));

    this.gl_context.bindTexture(this.gl_context.TEXTURE_2D, null);
    this.gl_context.bindFramebuffer(this.gl_context.FRAMEBUFFER, null);


    this.GetFrameBuffer = function(){
        return frameBuffer;
    };


    this.Enable = function( shader ){
        this.gl_context.activeTexture( this.gl_context.TEXTURE0+this.ID );
        this.gl_context.bindTexture( this.gl_context.TEXTURE_2D,this.tex);
        if(shader){
            this.gl_context.uniform1i(shader.GetSamplerLocation(this.tex_id), this.ID );

        }

    };

    if(this.gl_context.checkFramebufferStatus(this.gl_context.FRAMEBUFFER) != this.gl_context.FRAMEBUFFER_COMPLETE) {
        console.error("[ShadowMapTexture] Framebuffer incomplete!");
        switch(this.gl_context.checkFramebufferStatus(this.gl_context.FRAMEBUFFER) ){
            case this.gl_context.FRAMEBUFFER_INCOMPLETE_ATTACHMENT:
                console.error("[ShadowMapTexture] FRAMEBUFFER_INCOMPLETE_ATTACHMENT");
                break;
            case this.gl_context.GL_FRAMEBUFFER_INCOMPLETE_DIMENSIONS:
                console.error("[ShadowMapTexture] GL_FRAMEBUFFER_INCOMPLETE_DIMENSIONS");
                break;
            case this.gl_context.GL_FRAMEBUFFER_INCOMPLETE_MISSING_ATTACHMENT:
                console.error("[ShadowMapTexture] GL_FRAMEBUFFER_INCOMPLETE_MISSING_ATTACHMENT");
                break;
            case this.gl_context.GL_FRAMEBUFFER_UNSUPPORTED:
                console.error("[ShadowMapTexture] GL_FRAMEBUFFER_UNSUPPORTED");
                break;
            default:
                console.error("[ShadowMapTexture] " + this.gl_context.checkFramebufferStatus(this.gl_context.FRAMEBUFFER));
                break;
        }

    }
};
ShadowMapTexture.prototype = new Texture();
OMEGA.Omega3D.ShadowMapTexture = ShadowMapTexture;function CustomMesh( geom ){
    Mesh.apply(this, [geom]);

    this.CreateBuffers = function(){
        /*VERTEX BUFFER*/
        if( this.geometry.GetVertices().length > 0) {
            if(!this.vertexBuffer) this.vertexBuffer = this.gl.createBuffer();
            this.gl.bindBuffer(this.gl.ARRAY_BUFFER, this.vertexBuffer);
            this.gl.bufferData(this.gl.ARRAY_BUFFER, new Float32Array(this.geometry.GetVertices()), this.gl.STATIC_DRAW);
            this.vertexBuffer.itemSize = 3;
            this.vertexBuffer.numItems = this.geometry.GetVertices().length / this.vertexBuffer.itemSize;
        }

        /*NORMAL BUFFER*/
        if( this.geometry.GetNormals().length > 0) {
            if(!this.normalBuffer) this.normalBuffer = this.gl.createBuffer();
            this.gl.bindBuffer(this.gl.ARRAY_BUFFER, this.normalBuffer);
            this.gl.bufferData(this.gl.ARRAY_BUFFER, new Float32Array(this.geometry.GetNormals()), this.gl.STATIC_DRAW);
            this.normalBuffer.itemSize = 3;
            this.normalBuffer.numItems = this.geometry.GetNormals().length / 3;
        }


        /*INDEX BUFFER*/
        if( this.geometry.GetIndexes().length > 0) {
            if(!this.indexBuffer) this.indexBuffer = this.gl.createBuffer();
            this.gl.bindBuffer(this.gl.ELEMENT_ARRAY_BUFFER, this.indexBuffer);
            this.gl.bufferData(this.gl.ELEMENT_ARRAY_BUFFER, new Uint16Array(this.geometry.GetIndexes()), this.gl.STATIC_DRAW);
            this.indexBuffer.itemSize = 1;
            this.indexBuffer.numItems = this.geometry.GetIndexes().length;
        }


        /*UV BUFFER*/
        if( this.geometry.GetUVS().length > 0) {
            if(!this.uvBuffer) this.uvBuffer = this.gl.createBuffer();
            this.gl.bindBuffer(this.gl.ARRAY_BUFFER, this.uvBuffer);
            this.gl.bufferData(this.gl.ARRAY_BUFFER, new Float32Array(this.geometry.GetUVS()), this.gl.STATIC_DRAW);
            this.uvBuffer.itemSize = 2;
            this.uvBuffer.numItems = this.geometry.GetUVS().length / this.uvBuffer.itemSize;
        }

        /*TANGENT BUFFER*/
        if( this.geometry.GetTangents().length > 0) {
            if(!this.tangentBuffer) this.tangentBuffer = this.gl.createBuffer();
            this.gl.bindBuffer(this.gl.ARRAY_BUFFER, this.tangentBuffer);
            this.gl.bufferData(this.gl.ARRAY_BUFFER, new Float32Array(this.geometry.GetTangents()), this.gl.STATIC_DRAW);
            this.tangentBuffer.itemSize = 3;
            this.tangentBuffer.numItems = this.geometry.GetTangents().length / this.tangentBuffer.itemSize;
        }

        /*BITANGENT BUFFER*/
        if( this.geometry.GetBitangents().length > 0) {
            if(!this.bitangentBuffer) this.bitangentBuffer = this.gl.createBuffer();
            this.gl.bindBuffer(this.gl.ARRAY_BUFFER, this.bitangentBuffer);
            this.gl.bufferData(this.gl.ARRAY_BUFFER, new Float32Array(this.geometry.GetBitangents()), this.gl.STATIC_DRAW);
            this.bitangentBuffer.itemSize = 3;
            this.bitangentBuffer.numItems = this.geometry.GetBitangents().length / this.bitangentBuffer.itemSize;
        }
    };

};
CustomMesh.prototype = new Mesh();
OMEGA.Omega3D.CustomMesh = CustomMesh;
function Mesh( geom ){
    this.combinedBuffer;
    this.vertexBuffer;
    this.normalBuffer;
    this.indexBuffer;
    this.uvBuffer;
    this.tangentBuffer;
    this.bitangentBuffer;
    this.colorPickingBuffer;
    this.geometry = geom;
    this.gl = OMEGA.Omega3D.GL;
    this.GetCombinedBuffer = function(){ return this.combinedBuffer;};
    this.GetVertexBuffer = function(){ return this.vertexBuffer;};
    this.GetUVBuffer     = function(){ return this.uvBuffer;    };
    this.GetIndexBuffer  = function(){ return this.indexBuffer; };
    this.GetNormalBuffer = function(){ return this.normalBuffer;};
    this.GetTangentBuffer = function(){ return this.tangentBuffer;};
    this.GetBitangentBuffer = function(){ return this.bitangentBuffer;};
    this.GetColorPickingBuffer = function(){ return this.colorPickingBuffer;};
    this.GetIndexCount   = function(){ return this.geometry.GetIndexes() / 3;};
    this.GetGeometry     = function(){ return this.geometry; };

    this.ApplyGeometry   = function(geom){
        this.geometry = geom;
        if(this.geometry.isDirty)  this.CreateBuffers();
    };


    this.CreateBuffers = function(){
        var combined = new Array();
        var vertices = this.geometry.GetVertices();
        var normals  = this.geometry.GetNormals();
        var uvs      = this.geometry.GetUVS();
        var tangents  = this.geometry.tangents;


        var bitangents = this.geometry.bitangents;

        //Geometry.ComputeTangentBasis(vertices, uvs, normals, tangents, bitangents);
        var uvCounter = 0; //because uv = 2 component vector.
        var baricentricX = 0, baricentricY = 1, baricentricZ = 0;
        for( var i = 0; i < vertices.length; i+=3){
            combined.push( vertices[i], vertices[i+1], vertices[i+2],     // 0, 1, 2
                normals[i] , normals[i+1] , normals[i+2],      // 3, 4, 5
                uvs[uvCounter], uvs[uvCounter+1],              // 6, 7
                tangents[i],tangents[i+1],tangents[i+2],       // 8, 9, 10
                bitangents[i],bitangents[i+1],bitangents[i+2], // 11, 12, 13
                baricentricX, baricentricY,baricentricZ );
            if( baricentricY == 1 ){
                baricentricX = 0;baricentricY = 0;baricentricZ = 1;
            }else if( baricentricZ == 1 ){
                baricentricX = 1;baricentricY = 0;baricentricZ = 0;
            }else if( baricentricX == 1 ){
                baricentricX = 0;baricentricY = 1;baricentricZ = 0;
            }

            uvCounter+=2;
        }
        this.combinedBuffer = null;
        this.combinedBuffer = this.gl.createBuffer();
        this.gl.bindBuffer( this.gl.ARRAY_BUFFER, this.combinedBuffer );
        this.gl.bufferData( this.gl.ARRAY_BUFFER, new Float32Array( combined ), this.gl.STATIC_DRAW );



        /*INDEX BUFFER*/
        if( this.geometry.GetIndexes().length > 0) {
            if(!this.indexBuffer) this.indexBuffer = this.gl.createBuffer();
            this.gl.bindBuffer(this.gl.ELEMENT_ARRAY_BUFFER, this.indexBuffer);
            this.gl.bufferData(this.gl.ELEMENT_ARRAY_BUFFER, new Uint16Array(this.geometry.GetIndexes()), this.gl.STATIC_DRAW);
            this.indexBuffer.itemSize = 1;
            this.indexBuffer.numItems = this.geometry.GetIndexes().length;
        }
        this.geometry.isDirty = false;

    };
    this.CreateColorPickingBuffer = function( objectID ){
        if( parseFloat(objectID) > -1) {
            var colorData = new Array();
            var step = 1.0 / 255.0;
            var fID = parseFloat(objectID);
            var size = 255.0, r255 = 0, g255 = 0, b255 = 0;
            r255 = fID <= size ? fID : size;
            if(fID > (size * 1.0 )) g255 = fID - r255;        // bigger than: 255
            if(fID > (size * 2.0 )) b255 = fID - (r255+g255); // bigger than: 255 + 255


            for(var i = 0; i < this.geometry.GetVertices().length; i++){
                colorData.push(step * r255,step * g255, step * b255 );
            }
            if(!this.colorPickingBuffer) this.colorPickingBuffer = this.gl.createBuffer();
            this.gl.bindBuffer(this.gl.ARRAY_BUFFER, this.colorPickingBuffer);
            this.gl.bufferData(this.gl.ARRAY_BUFFER, new Float32Array(colorData), this.gl.STATIC_DRAW);
            this.colorPickingBuffer.itemSize = 3;
            this.colorPickingBuffer.numItems = colorData.length / this.colorPickingBuffer.itemSize;
        }
    };

    this.Update = function(){
        if(this.geometry.isDirty){
            this.CreateBuffers();
        }
    }
};
OMEGA.Omega3D.Mesh = Mesh;

function CubeGeometry(width, height, depth){
    Geometry.apply( this );
    var halfScaleW = width/2  || this.scale;
    var halfScaleH = height/2 || halfScaleW || this.scale;
    var halfScaleD = depth/2  || halfScaleW || this.scale;
    this.vertices = [
        /* Front face*/
        -1.0*halfScaleW, -1.0*halfScaleH,  1.0*halfScaleD,
        1.0*halfScaleW, -1.0*halfScaleH,  1.0*halfScaleD,
        1.0*halfScaleW,  1.0*halfScaleH,  1.0*halfScaleD,
        -1.0*halfScaleW,  1.0*halfScaleH,  1.0*halfScaleD,

        /* Back face*/
        -1.0*halfScaleW, -1.0*halfScaleH, -1.0*halfScaleD,
        -1.0*halfScaleW,  1.0*halfScaleH, -1.0*halfScaleD,
        1.0*halfScaleW,  1.0*halfScaleH, -1.0*halfScaleD,
        1.0*halfScaleW, -1.0*halfScaleH, -1.0*halfScaleD,

        /* Top face*/
        -1.0*halfScaleW,  1.0*halfScaleH, -1.0*halfScaleD,
        -1.0*halfScaleW,  1.0*halfScaleH,  1.0*halfScaleD,
        1.0*halfScaleW,  1.0*halfScaleH,  1.0*halfScaleD,
        1.0*halfScaleW,  1.0*halfScaleH, -1.0*halfScaleD,

        /* Bottom face*/
        -1.0*halfScaleW, -1.0*halfScaleH, -1.0*halfScaleD,
        1.0*halfScaleW, -1.0*halfScaleH, -1.0*halfScaleD,
        1.0*halfScaleW, -1.0*halfScaleH,  1.0*halfScaleD,
        -1.0*halfScaleW, -1.0*halfScaleH,  1.0*halfScaleD,

        /* Right face*/
        1.0*halfScaleW, -1.0*halfScaleH, -1.0*halfScaleD,
        1.0*halfScaleW,  1.0*halfScaleH, -1.0*halfScaleD,
        1.0*halfScaleW,  1.0*halfScaleH,  1.0*halfScaleD,
        1.0*halfScaleW, -1.0*halfScaleH,  1.0*halfScaleD,

        /* Left face*/
        -1.0*halfScaleW, -1.0*halfScaleH, -1.0*halfScaleD,
        -1.0*halfScaleW, -1.0*halfScaleH,  1.0*halfScaleD,
        -1.0*halfScaleW,  1.0*halfScaleH,  1.0*halfScaleD,
        -1.0*halfScaleW,  1.0*halfScaleH, -1.0*halfScaleD
    ];

    this.normals = [
        0, 0, -1,
        0, 0, -1,
        0, 0, -1,
        0, 0, -1,

        0, 0, 1,
        0, 0, 1,
        0, 0, 1,
        0, 0, 1,

        0, -1,  0,
        0, -1,  0,
        0, -1,  0,
        0, -1,  0,

        0, 1,  0,
        0, 1,  0,
        0, 1,  0,
        0, 1,  0,

        -1,0,  0,
        -1,0,  0,
        -1,0,  0,
        -1,0,  0,

        1,0,  0,
        1,0,  0,
        1,0,  0,
        1,0,  0
    ];


    this.indexes = [
        0, 1, 2,      0, 2, 3,    /* Front face */
        4, 5, 6,      4, 6, 7,    /* Back face */
        8, 9, 10,     8, 10, 11,  /* Top face */
        12, 13, 14,   12, 14, 15, /* Bottom face */
        16, 17, 18,   16, 18, 19, /* Right face */
        20, 21, 22,   20, 22, 23  /* Left face */
    ];

    this.uvs = [
        /* Front face */
        1.0, 0.0,
        0.0, 0.0,
        0.0, 1.0,
        1.0, 1.0,

        /* Back face */
        0.0, 0.0,                 //   0.0 -- 1.0
        0.0, 1.0,                 //    |  \    |
        1.0, 1.0,                 //    |    \  |
        1.0, 0.0,                 //   0.1 --- 1.1

        /* Bottom face */
        1.0, 1.0,
        1.0, 0.0,
        0.0, 0.0,
        0.0, 1.0,

        /* Top face */
        0.0, 1.0,
        1.0, 1.0,
        1.0, 0.0,
        0.0, 0.0,

        /* Right face */
        0.0, 0.0,
        0.0, 1.0,
        1.0, 1.0,
        1.0, 0.0,

        /* Left face */
        1.0, 0.0,
        0.0, 0.0,
        0.0, 1.0,
        1.0, 1.0
    ];
    /*TANGENTS, BITANGENTS */
    // OMEGA.Omega3D.VBOUtil.ComputeTangentBasis( this.vertices, this.uvs, this.normals, this.tangents, this.bitangents);
    OMEGA.Omega3D.Log("GEOMETRY : cube created");

};
CubeGeometry.prototype = new Geometry();
OMEGA.Omega3D.Geometry.CubeGeometry = CubeGeometry;

function CylinderGeometry(length, radius, segW, segH){
    Geometry.apply( this );
    var l = radius || this.scale* 0.25;
    var r = length || this.scale;

    var segW     = segW || 15;
    var segH     = segH || 10;
    var xPos, yPos, zPos;
    var angle,angle2;

    var temp_verts, temp_uvs, temp_normals, temp_tan, temp_bitan;
    temp_verts = new Array();temp_uvs = new Array();temp_normals = new Array();
    temp_tan = new Array();temp_bitan = new Array();
    for (var i = 0; i < segH; i++) {
        for (var j = 0; j < segW; j++) {

            angle  = (Math.PI * 2 / (segW - 1) * j);
            angle2 = (Math.PI * i / (segH - 1) - Math.PI / 2);


            xPos = Math.cos(angle) * l;
            yPos = i * (r/segH) - (r/2);
            zPos = Math.sin(angle) * l;



            var vec    = new Vector3( xPos, yPos, zPos );
            var uv     = new Vector2(j / (segW - 1), i / (segH - 1) );
            var normal = new Vector3( Math.cos( angle ),0,Math.sin( angle));
            temp_verts.push( vec );
            temp_normals.push( normal );
            temp_uvs.push( uv );
        }
    }


    /*TANGENTS, BITANGENTS */
    OMEGA.Omega3D.VBOUtil.ComputeTangentBasis( temp_verts, temp_uvs, temp_normals,  //in
        temp_tan, temp_bitan);               //out

    OMEGA.Omega3D.VBOUtil.IndexTB( temp_verts, temp_uvs, temp_normals, temp_tan, temp_bitan,                                      //in
        this.vertices_raw, this.uvs_raw, this.normals_raw,this.tangents_raw, this.bitangents_raw  );   //out

    OMEGA.Omega3D.VBOUtil.FlattenMeshDataTB( temp_verts, temp_uvs, temp_normals, temp_tan,temp_bitan,                //in
        this.vertices,this.uvs, this.normals, this.tangents, this.bitangents ); //out

    /*INDEXES.*/
    OMEGA.Omega3D.VBOUtil.ComputeIndices( segW, segH, this.indexes );


    /* FACES */
    Geometry.ComputeFaces( this.vertices, this.indexes, this.faces );

    OMEGA.Omega3D.Log("GEOMETRY : cillinder created");
};
CylinderGeometry.prototype = new Geometry();
OMEGA.Omega3D.Geometry.CylinderGeometry = CylinderGeometry;
function Geometry(){
    this.vertices  = new Array();
    this.normals   = new Array();
    this.uvs       = new Array();
    this.tangents   = new Array();
    this.bitangents = new Array();
    this.vertices_raw  = new Array();
    this.normals_raw    = new Array();
    this.uvs_raw        = new Array();
    this.tangents_raw    = new Array();
    this.bitangents_raw  = new Array();
    this.indexes   = new Array();
    this.faces     = new Array();

    this.scale    = 1.0;
    this.isDirty = true;

    this.SetVertices = function(value){ this.vertices=value; };
    this.SetNormals  = function(value){ this.normals=value;  };
    this.SetUVS      = function(value){ this.uvs=value;      };
    this.SetIndexes  = function(value){ this.indexes=value;  };
    this.SetFaces    = function(value){ this.faces=value;    };
    this.SetTangents   = function(value){ this.tangents =value; };
    this.SetBitangents = function(value){this.bitangents = value; };


    this.GetVertices   = function(){ return this.vertices; };
    this.GetNormals    = function(){ return this.normals;  };
    this.GetUVS        = function(){ return this.uvs;      };
    this.GetTangents   = function(){ return this.tangents; };
    this.GetBitangents = function(){ return this.bitangents; };
    this.GetIndexes    = function(){ return this.indexes;  };
    this.GetFaces      = function(){ return this.faces;    };

    this.GetLengthBetweenPoints = function( a, b ){
        var dx = b.x - a.x;
        var dy = b.y - a.y;
        var dz = b.z - a.z;
        var len = dx*dx+dy*dy+dz*dz;
        return Math.sqrt(len);
    };

    this.UpdateAtlasUV = function( rect, segH, segW ){
        var newUVS = new Array();
        var stepX = (rect.w) / segW;
        var stepY = (rect.h) / segH;
        for( var i = 0; i < segH; i++) {
            for (var j = 0; j < segW; j++) {
                newUVS.push( rect.x + (stepX * j));
                newUVS.push( rect.y + (stepY * i));
            }
        }
        this.uvs = newUVS;
        this.isDirty = true;
    }
};

Geometry.ComputeIndices = function(segmentsW, segmentsH,  out_indices ){
    for ( var i = 0; i < segmentsH; i++) {
        for ( var j = 0; j < segmentsW; j++) {
            if ( i < segmentsH -1 && j < segmentsW-1 ) {
                out_indices.push( i * segmentsW + j    , i * segmentsW + j + 1     , (i + 1) * segmentsW + j );
                out_indices.push( i * segmentsW + j + 1, (i + 1) *segmentsW + j + 1, (i + 1) * segmentsW + j );
            }
        }
    }
};
//Geometry.IndexVBO = function( in_vertices, in_uvs, in_normals, in_tangents, in_bitangents,out_indices, out_vertices, out_uvs, out_normals, out_tangents, out_bitangents){
//    for( var i = 0; i < in_vertices.length; i++){
//        var index;
//        var found = Geometry.GetSimularVertexIndex(in_vertices[i], in_uvs[i], in_normals[i], out_vertices, out_uvs, out_normals, index );
//        if(found){
//            out_indices.push(index);
//            out_tangents[index].add( in_tangents[i] );
//            out_bitangents[index].add( in_bitangents[i] );
//        }else{
//            out_vertices.push(in_vertices[i] );
//            out_uvs.push(in_uvs[i] );
//            out_normals.push(in_normals[i] );
//            out_tangents.push(in_tangents[i] );
//            out_bitangents.push(in_bitangents[i] );
//            out_indices.push( out_vertices.length - 1);
//        }
//    }
//};
//Geometry.GetSimularVertexIndex = function( in_vertex)
Geometry.ComputeTangentBasis = function( vertices, uvs, normals, tangents_out, bitangents_out){
    var totalAmountPerVerticeStep = 9;
    for( var i = 0; i < vertices.length; i+=totalAmountPerVerticeStep){
        var pointA = new Vector3( vertices[i+0],  vertices[i+1],  vertices[i+2]);
        var pointB = new Vector3( vertices[i+3],  vertices[i+4],  vertices[i+5]);
        var pointC = new Vector3( vertices[i+6],  vertices[i+7],  vertices[i+8]);

        var uvA = new Vector2(uvs[i+0],uvs[i+1],uvs[i+2] );
        var uvB = new Vector2(uvs[i+3],uvs[i+4],uvs[i+5] );
        var uvC = new Vector2(uvs[i+6],uvs[i+7],uvs[i+8] );

        var deltaPos1 = Vector3.Subtract(pointB, pointA);
        var deltaPos2 = Vector3.Subtract(pointC, pointA);

        var deltaUV1 = Vector2.Subtract(uvB, uvA);
        var deltaUV2 = Vector2.Subtract(uvC, uvA);

        var r = 1.0 / (deltaUV1.x * deltaUV2.y - deltaUV1.y * deltaUV2.x);
        var tangent = Vector3.MultiplyByValue(
            Vector2.Subtract(
                Vector3.MultiplyByValue(deltaPos1,deltaUV2.y),
                Vector3.MultiplyByValue(deltaPos2,deltaUV1.y)
            ),r);

        var bitangent = Vector3.MultiplyByValue(
            Vector3.Subtract(
                Vector3.MultiplyByValue(deltaPos2,deltaUV1.x),
                Vector3.MultiplyByValue(deltaPos1,deltaUV2.x)
            ),r);

        tangents_out.push( tangent );
        tangents_out.push( tangent );
        tangents_out.push( tangent );


        bitangents_out.push( bitangent);
        bitangents_out.push( bitangent);
        bitangents_out.push( bitangent);
    }
};
Geometry.ComputeFaces = function( vertices, indices, faces_out){
    for(var i=0;i< indices.length;i+=9){
        var v1 = { x: vertices[indices[i]], y: vertices[indices[i+1]], z: vertices[indices[i+2]] };
        var v2 = { x: vertices[indices[i + 3]], y: vertices[indices[i+4]], z: vertices[indices[i+5]] };
        var v3 = { x: vertices[indices[i + 6]], y: vertices[indices[i+7]], z: vertices[indices[i+8]] };
        faces_out.push( { a:v1, b:v2, c:v3 } );
    }
};
OMEGA.Omega3D.Geometry = Geometry;function IcosaGeometry(w, h ){
    Geometry.apply(this);

    this.vertices.push( -w, 0.0, h );
    this.vertices.push(  w, 0.0, h );
    this.vertices.push( -w, 0.0,-h );
    this.vertices.push(  w, 0.0,-h );

    this.vertices.push(  0.0, h, w );
    this.vertices.push(  0.0, h,-w );
    this.vertices.push(  0.0,-h, w );
    this.vertices.push(  0.0,-h,-w );

    this.vertices.push(  h, w, 0.0 );
    this.vertices.push( -h, w, 0.0 );
    this.vertices.push(  h,-w, 0.0 );
    this.vertices.push( -h,-w, 0.0 );


    this.indexes.push( 1, 4, 0);
    this.indexes.push( 4, 9, 0);
    this.indexes.push( 4, 5, 9);
    this.indexes.push( 8, 5, 4);
    this.indexes.push( 1, 8, 4);

    this.indexes.push( 1, 10, 8);
    this.indexes.push(10,  3, 8);
    this.indexes.push( 8,  3, 5);
    this.indexes.push( 3,  2, 5);
    this.indexes.push( 3,  7, 2);

    this.indexes.push( 3,  10, 7);
    this.indexes.push( 10,  6, 7);
    this.indexes.push( 6,  11, 7);
    this.indexes.push( 6,  0, 11);
    this.indexes.push( 6,  1,  0);

    this.indexes.push( 10,  1,  6);
    this.indexes.push( 11,  0,  9);
    this.indexes.push(  2, 11,  9);
    this.indexes.push(  5,  2,  9);
    this.indexes.push( 11,  2,  7);


    for(var i = 0; i < this.vertices.length; i++){
        this.normals.push(this.vertices[i]);
    }

    OMEGA.Omega3D.Log("GEOMETRY : icosahedron created");

}
IcosaGeometry.prototype = new Geometry();
OMEGA.Omega3D.Geometry.IcosaGeometry = IcosaGeometry;function SphereGeometry( radius, segW, segH){
    Geometry.apply( this);
    if(radius) this.scale = radius;
    var radius = radius || this.scale;

    segH = segH || 15;
    segW = segW || 15;
    var angle = 0;
    var angle2 = 0;

    var temp_verts, temp_uvs, temp_normals, temp_tan, temp_bitan;
    temp_verts = new Array();temp_uvs = new Array();temp_normals = new Array();
    temp_tan = new Array();temp_bitan = new Array();
    for (var i = 0; i < segW; i++) {
        for ( var j = 0; j < segH; j++){
            angle  = Math.PI * 2 / (segW -1) * j;
            angle2 = Math.PI * i / (segH -1) - Math.PI / 2;

            var xpos = Math.cos( angle  ) * radius * Math.cos( angle2 );
            var ypos = Math.sin( angle2 ) * radius;
            var zpos = Math.sin( angle  ) * radius * Math.cos( angle2 );

            var vec    = new Vector3( xpos, ypos, zpos );
            var uv     = new Vector2(j / (segW - 1), 1- i / (segH - 1) );
            var normal = new Vector3( Math.cos( angle ) * Math.cos( angle2 ),
                Math.sin( angle2 ),
                Math.sin( angle) * Math.cos( angle2) );

            //temp_verts.push( vec );
            //temp_normals.push( normal );
            //temp_uvs.push( uv );

            this.vertices.push(xpos);
            this.vertices.push(ypos );
            this.vertices.push(zpos);

            this.normals.push( -normal.x );
            this.normals.push(  -normal.y );
            this.normals.push(  -normal.z );

            this.uvs.push( j / (segH - 1));
            this.uvs.push( i / (segW - 1));

            if (i < segW-1 && j < segH-1) {

                this.indexes.push(i * segH + j);
                this.indexes.push(i * segH + j + 1);
                this.indexes.push((i+1) * segH + j);

                this.indexes.push(i * segH + j + 1);
                this.indexes.push((i+1) * segH + j + 1);
                this.indexes.push((i+1) * segH + j);
            }
        }
    }

    /*TANGENTS, BITANGENTS */
    OMEGA.Omega3D.VBOUtil.ComputeTangentBasis( temp_verts, temp_uvs, temp_normals,  //in
        temp_tan, temp_bitan);               //out
    //
    OMEGA.Omega3D.VBOUtil.IndexTB( temp_verts, temp_uvs, temp_normals, temp_tan, temp_bitan,                                      //in
        this.vertices_raw, this.uvs_raw, this.normals_raw,this.tangents_raw, this.bitangents_raw  );   //out

    OMEGA.Omega3D.VBOUtil.FlattenMeshDataTB( temp_verts, temp_uvs, temp_normals, temp_tan,temp_bitan,                //in
        this.vertices,this.uvs, this.normals, this.tangents, this.bitangents ); //out

    /*INDEXES.*/
    OMEGA.Omega3D.VBOUtil.ComputeIndices( segH, segW, this.indexes );


    /* FACES */
    Geometry.ComputeFaces( this.vertices, this.indexes, this.faces );

    OMEGA.Omega3D.Log("GEOMETRY : sphere created");
};
SphereGeometry.prototype = new Geometry();
OMEGA.Omega3D.Geometry.SphereGeometry = SphereGeometry;
function SquareGeometry(width, height,segW, segH){
    Geometry.apply( this );
    var halfScaleW = width || 1.0 ;
    var halfScaleH = height || 1.0 ;
    var segW = segW || 150;
    var segH = segH || 150;
    var stepW = width*2 / (segW-1);
    var stepH = height*2 / (segH-1)  ;

    var temp_verts, temp_uvs, temp_normals, temp_tan, temp_bitan;
    temp_verts = new Array();temp_uvs = new Array();temp_normals = new Array();
    temp_tan = new Array();temp_bitan = new Array();
    for( var i = 0; i < segH; i++){
        for( var j = 0; j < segW; j++){
            var xpos =  -halfScaleW + (j * stepW);
            var ypos = -halfScaleH + (i * stepH);
            var zpos =  0.0;

            var vec    = new Vector3( xpos, ypos, zpos );
            var uv     = new Vector2(1.0-j / (segW - 1),1.0- i / (segH - 1) );
            var normal = new Vector3( 0,0,1 );

            temp_verts.push( vec );
            temp_normals.push( normal );
            temp_uvs.push( uv );
        }
    }
    /*TANGENTS, BITANGENTS */
    OMEGA.Omega3D.VBOUtil.ComputeTangentBasis( temp_verts, temp_uvs, temp_normals,  //in
        temp_tan, temp_bitan);               //out



    OMEGA.Omega3D.VBOUtil.IndexTB( temp_verts, temp_uvs, temp_normals, temp_tan, temp_bitan,                                      //in
        this.vertices_raw, this.uvs_raw, this.normals_raw,this.tangents_raw, this.bitangents_raw  );   //out


    OMEGA.Omega3D.VBOUtil.FlattenMeshDataTB( temp_verts, temp_uvs, temp_normals, temp_tan,temp_bitan,                //in
        this.vertices,this.uvs, this.normals, this.tangents, this.bitangents ); //out

    /*INDEXES.*/
    OMEGA.Omega3D.VBOUtil.ComputeIndices( segW, segH, this.indexes );


    /* FACES */
    Geometry.ComputeFaces( this.vertices, this.indexes, this.faces );

    OMEGA.Omega3D.Log("GEOMETRY : square created");
};
SquareGeometry.prototype = new Geometry();
OMEGA.Omega3D.Geometry.SquareGeometry = SquareGeometry;

function TorusGeometry( radius, tube_radius, segW, segH ){
    Geometry.apply( this );

    var segW     = segW || 40;
    var segH     = segH || 50;
    var t_radius  = radius || this.scale;
    var c_radius  = tube_radius || t_radius/2;
    var xPos, yPos, zPos, xNorm, yNorm, zNorm;
    var outerAngle = 0;
    var innerAngle = 0;
    for (var i = 0; i < segW; i++) {
        outerAngle = (((Math.PI *2) / (segW-1)) * i);
        for (var j = 0; j < segH; j++) {
            innerAngle =  (((Math.PI *2) / (segH-1)) * j);

            xPos = (Math.cos(outerAngle) * (Math.cos(innerAngle) * c_radius - t_radius));
            yPos = (Math.sin(outerAngle) * (Math.cos(innerAngle) * c_radius- t_radius));
            zPos = (Math.sin(innerAngle) *  c_radius);


            xNorm = (Math.cos(outerAngle ) * Math.cos(innerAngle ));
            yNorm = (Math.sin(outerAngle ) * Math.cos(innerAngle));
            zNorm = (Math.sin(innerAngle  ));

            this.vertices.push(-xPos );
            this.vertices.push(-yPos );
            this.vertices.push(-zPos);

            this.normals.push( xNorm );
            this.normals.push( yNorm );
            this.normals.push( zNorm );

            this.uvs.push( j / (segH - 1));
            this.uvs.push( i / (segW - 1));

            if (i < segW-1 && j < segH-1) {

                this.indexes.push(i * segH + j);
                this.indexes.push(i * segH + j + 1);
                this.indexes.push((i+1) * segH + j);

                this.indexes.push(i * segH + j + 1);
                this.indexes.push((i+1) * segH + j + 1);
                this.indexes.push((i+1) * segH + j);
            }

        }
    }

    /*TANGENTS, BITANGENTS */
    // Geometry.ComputeTangentBasis( this.vertices, this.uvs, this.normals, this.tangents, this.bitangents);


    for(var i=0;i< this.indexes.length;i+=9){
        var v1 = { x: this.vertices[this.indexes[i]], y: this.vertices[this.indexes[i+1]], z: this.vertices[this.indexes[i+2]] };
        var v2 = { x: this.vertices[this.indexes[i + 3]], y: this.vertices[this.indexes[i+4]], z: this.vertices[this.indexes[i+5]] };
        var v3 = { x: this.vertices[this.indexes[i + 6]], y: this.vertices[this.indexes[i+7]], z: this.vertices[this.indexes[i+8]] };
        this.faces.push( { a:v1, b:v2, c:v3 } );

    }
    OMEGA.Omega3D.Log("GEOMETRY : torus created");
};
TorusGeometry.prototype = new Geometry();
OMEGA.Omega3D.Geometry.TorusGeometry = TorusGeometry;function TriangleGeometry(scale){
    Geometry.apply( this, arguments );
    this.vertices = [
        0.0,  1.0* this.scale,  0.0,
        -1.0* this.scale, -1.0* this.scale,  0.0,
        1.0* this.scale, -1.0* this.scale,  0.0
    ];
    this.uvs = [
        0.98, 0.98,
        0.60, 0.0,
        0.0, 0.60
    ];
    this.normals = [
        0, 0, 1,
        0, 0, 1,
        0, 0, 1
    ];
    this.indexes = [ 0, 1, 2 ];

    /*TANGENTS, BITANGENTS */
    Geometry.ComputeTangentBasis( this.vertices, this.uvs, this.normals, this.tangents, this.bitangents);

    OMEGA.Omega3D.Log("GEOMETRY : triangle created");
};
TriangleGeometry.prototype = new Geometry();
OMEGA.Omega3D.Geometry.TriangleGeometry = TriangleGeometry;

function OBJParser( debug ){
    var LINE_FEED = String.fromCharCode(10);
    var SPACE     = String.fromCharCode(32);
    var SLASH    = "/";
    var VERTEX    = "v";
    var NORMAL    = "vn";
    var UV        = "vt";
    var INDEX_DATA = "f";
    var vertexDataIsZXY = false;
    var scale = 1.0;
    var mirrorUv = true;
    var faceIndex = 0;
    var firstTimeG = true;
    this.debug = debug || false;


    var temp_vertices = new Array(), temp_normals = new Array(), temp_uvs = new Array(), temp_colors = new Array();
    var vertices = new Array(), normals = new Array(), uvs = new Array(), colors = new Array(), indexes = new Array();
    var objects = new Array();

    this.GetObjects = function() { return objects;  };
    this.GetVertices = function(){ return vertices; };
    this.GetNormals  = function(){ return normals;  };
    this.GetUVS      = function(){ return uvs;      };
    this.GetIndexes  = function(){ return indexes;  };

    this.ParseToObject3D = function( file_path, scale_factor, material ){
        objects  = new Array();
        if(this.debug)console.log("parsing: " + file_path);
        this.reset();
        this.parseFile(file_path, scale_factor);

        var root;
        if( this.GetObjects().length == 1){
            var obj = this.GetObjects()[0];
            var obj_geom = new Omega3D.Geometry();
            obj_geom.SetVertices( obj.vertices );
            obj_geom.SetNormals( obj.normals );
            obj_geom.SetUVS( obj.UVS );
            obj_geom.SetIndexes(obj.indexes );

            var obj_mesh = new Omega3D.Mesh( obj_geom )
            root = new Omega3D.Object3D( obj_mesh, material.Clone() );
            root.name = obj.name;
        }else{
            root = new Omega3D.Object3D();
            for(var i = 0; i < this.GetObjects().length; i++){
                var obj      = this.GetObjects()[i];
                var obj_geom = new Omega3D.Geometry();
                obj_geom.SetVertices( obj.vertices );
                obj_geom.SetNormals( obj.normals );
                obj_geom.SetUVS( obj.UVS );
                obj_geom.SetIndexes(obj.indexes );



                var obj_mesh = new Omega3D.Mesh( obj_geom )
                var object = new Omega3D.Object3D( obj_mesh, material.Clone()  );
                object.name = obj.name;

                object.SetParent(root);
            }
        }
        temp_vertices = new Array();
        temp_normals  = new Array();
        temp_uvs      = new Array();
        temp_colors   = new Array();
        this.reset();
        return root;
    };


    this.parseFile = function( file_path, scale_factor ){
        var self = this; var currentObject;
        var xmlhttp = new XMLHttpRequest() || new ActiveXObject('MSXML2.XMLHTTP');
        xmlhttp.open ("GET", file_path, false);
        xmlhttp.onreadystatechange = function(e){
            if(self.debug)console.log("[OBJParser] load ready. ");
            scale = scale_factor;
            var obj_content = xmlhttp.responseText;
            var lines       = obj_content.split(LINE_FEED);
            lines.push(null); //end of feed.

            var index = 0; var line;
            var sp = new OMEGA.Utils.StringParser();

            while((line= lines[index++]) != null){
                sp.Init(line);
                var cmd = sp.GetFirstWord();
                var space_index = line.indexOf(' ');
                var data;
                if(sp.words.length > 0 ) data = sp.words.slice(1);
                if ((data[0] == '') || (data[0] == ' ')) data = data.slice(1);

                switch(cmd){
                    case '#':
                        continue;
                    case 'mtllib':
                        //do mtl stuff!
                        //console.log("MTL: "+ data);
                        continue;
                    case 'o':
                    case 'g':
                        var str = data[0];
                        if(str.charCodeAt(str.length-1) == 13 ) str = str.substr(0, str.length-1);
                        if(str == 'default'|| firstTimeG == true){

                            if(currentObject != null && self.GetVertices().length >  0){
                                currentObject.vertices = self.GetVertices();
                                currentObject.normals  = self.GetNormals();
                                currentObject.UVS      = self.GetUVS();
                                currentObject.indexes  = self.GetIndexes();
                                if(self.debug)self.debugObject(currentObject);
                                self.reset();

                            }
                            var object  = {};
                            objects.push( object );
                            currentObject = object;
                            firstTimeG = false;
                            if(self.debug)console.log("[OBJParser] adding from G. ");
                            continue;



                        }else{
                            if(currentObject != null) currentObject.name = line.substr( space_index+1, line.length -(space_index+2) );
                            continue;
                        }
                        continue;
                    case 'v':
                        if(!currentObject &&  firstTimeG == true){
                            var object  = {};
                            objects.push( object );
                            currentObject = object;
                            firstTimeG = false;
                            if(self.debug)console.log("[OBJParser] adding from V. ");
                        }
                        self.parseVertex(data);
                        continue;
                    case 'vn':
                        self.parseNormal(data);
                        continue;
                    case 'vt':

                        if(data.length > 2) data.pop();
                        if(data.length > 2) console.log("vt is bigger than 2");
                        self.parseUV(data);
                        break;
                    case 'usemtl':
                        if(currentObject != null) currentObject.mtl = sp.words[1];
                        continue;
                    case 'f':
                        self.parseIndex(data);
                        continue;
                }
            }
            if(currentObject != null){
                currentObject.vertices = self.GetVertices();
                currentObject.normals  = self.GetNormals();
                currentObject.UVS      = self.GetUVS();
                currentObject.indexes  = self.GetIndexes();
                if(self.debug)self.debugObject(currentObject);
                self.reset();
                console.log("current objects length: " + self.GetObjects().length);
            }
        }
        xmlhttp.send (null);
    };

    this.parseVertex = function(data){
        if(vertexDataIsZXY){
            temp_vertices.push( parseFloat(data[1]) * scale );
            temp_vertices.push( parseFloat(data[2]) * scale );
            temp_vertices.push( parseFloat(data[0]) * scale );
        }else{
            var loop = data.length;
            if(loop>3)loop = 3;
            for(var i = 0; i<loop;++i){
                var element = data[i];
                temp_colors.push( Math.random());
                temp_vertices.push( parseFloat(element) * scale);
            }
            temp_colors.push(1.0);
        };
    };
    this.parseNormal = function(data) {
        var loop = data.length;
        if (loop > 3) loop = 3;
        for (var i= 0; i < loop;++i){
            var element = data[i];
            if (element != null) // handle 3dsmax extra spaces
                temp_normals.push(parseFloat(element));
        };
    };
    this.parseUV = function(data){
        var loop = data.length;
        if (loop > 2) loop = 2;
        for (var i = 0; i < loop; ++i){
            var element = data[i];
            temp_uvs.push(Number(element));
        };
    };
    this.parseIndex = function(data){
        var triplet;
        var subdata;
        var vertexIndex;
        var uvIndex;
        var normalIndex;
        var index;

        //
        for( var i = 0; i < data.length; i++){
            if(data[i] == "") data.splice( i, 1);
        }
        //console.log( "Data: "  + data );

        // console.log(temp_vertices.length);

        // Process elements.
        var i;
        var loop = data.length;
        var starthere = 0;
        while (data[starthere] == '' || data[starthere] == ' ')
            starthere++; // ignore blanks
        loop = starthere + 3;

        // loop through each element and grab values stored earlier
        // elements come as vertexIndex/uvIndex/normalIndex
        for(i = starthere; i < loop; ++i)
        {
            triplet = data[i];

            //console.log( triplet );
            subdata = triplet.split(SLASH);


            vertexIndex = parseInt(subdata[0]) - 1;
            uvIndex     = parseInt(subdata[1]) - 1;
            normalIndex = parseInt(subdata[2]) - 1;
            // sanity check
            if(vertexIndex < 0) vertexIndex = 0;
            if(uvIndex < 0) uvIndex = 0;
            if(normalIndex < 0) normalIndex = 0;

            // Extract from parse raw data to mesh raw data.
            // Vertex (x,y,z)
            if (temp_vertices.length){
                index = 3*vertexIndex;
                vertices.push(temp_vertices[index + 0], temp_vertices[index + 1], temp_vertices[index + 2]);
            }




            // Color (vertex r,g,b,a)
            // if (_randomVertexColors)
            colors.push(Math.random(), Math.random(), Math.random(), 1);
            //else
            //    _rawColorsBuffer.push(1, 1, 1, 1); // pure white
            // Normals (nx,ny,nz) - *if* included in the file
            if (temp_normals.length){
                index = 3*normalIndex;
                normals.push(-temp_normals[index + 0],-temp_normals[index + 1], -temp_normals[index + 2]);
            }
            // Texture coordinates (u,v)
            index = 2 * uvIndex;
            if (mirrorUv) uvs.push(temp_uvs[index+0], 1-temp_uvs[index+1]);
            else uvs.push(1-temp_uvs[index+0],1-temp_uvs[index+1]);
        }


        // Create index buffer - one entry for each polygon
        indexes.push(faceIndex, faceIndex+1, faceIndex+2);
        faceIndex += 3;
    };

    this.reset = function(){
        vertices = new Array();
        normals  = new Array();
        uvs      = new Array();
        colors   = new Array();
        indexes  = new Array();
        // objects  = new Array();
        faceIndex = 0;
        firstTimeG = true;
    };

    this.debugObject = function(object){
        console.log("OBJECT   : " + object.name  );
        console.log("VERTEXES : " + object.vertices.length / 3 );
        console.log("NORMALS  : " + object.normals.length / 3 );
        console.log("UV       : " + object.UVS.length / 2 );
        console.log("INDEXES  : " + object.indexes.length );
    }
};
OMEGA.Omega3D.parsers = OMEGA.Omega3D.parsers || {};
OMEGA.Omega3D.parsers.OBJParser = OBJParser;

function ColorPickPass(renderer, scene, camera ){
    RenderPass.apply(this, [renderer, scene, camera]);
    this.texture = new Omega3D.FrameBufferTexture();
    this.pickingShader = new OMEGA.Omega3D.Shaders.ColorPicking();
    this.buffer  = new Uint8Array(4);
    var mousePos = {x:0, y:0};
    var store = {};
    scene.selectedObject = null;
    this.render = function(){
        this.cam.update();
        this.scene.update();
        store["sceneColor"] = scene.getColor();
        scene.setColor( 1, 1, 1);
        scene.lightsON = false;
        this.swapShadersPicking(scene);

        var x = this.texture.GetFrameBuffer().width / canvas.width;
        var y = this.texture.GetFrameBuffer().height / canvas.height;



        this.gl.bindFramebuffer(this.gl.FRAMEBUFFER, this.texture.GetFrameBuffer());

        this.gl.enable( this.gl.DEPTH_TEST );
        this.renderer.viewPort( this.scene, 0, 0, this.texture.GetFrameBuffer().width ,this.texture.GetFrameBuffer().height );
        this.renderer.renderScene( this.scene, this.cam );
        this.gl.disable( this.gl.DEPTH_TEST );


        this.gl.readPixels(Math.floor(x*mousePos.x), this.texture.GetFrameBuffer().height - Math.floor(y*mousePos.y), 1, 1, this.gl.RGBA, this.gl.UNSIGNED_BYTE, this.buffer);
        var id = this.buffer[0]+this.buffer[1]+this.buffer[2];
        scene.selectedObject =  this.searchChild(scene, id)

        this.gl.bindFramebuffer(this.gl.FRAMEBUFFER, null);
        this.swapShadersNotPicking(scene);
        scene.lightsON = true;
        scene.setColor( store["sceneColor"].r, store["sceneColor"].g, store["sceneColor"].b);
        store = {};
    };

    this.searchChild = function( parent, id ){
        var child = null;
        for( var  i = 0 ; i < parent.children.length; i++){
            if(parseFloat(parent.children[i].id) == id ) child = parent.children[i];
            else if( parent.children[i].children.length > 0) child = this.searchChild(parent.children[i], id  );
            if(child!=null)break;
        }
        return child;
    }
    this.swapShadersPicking = function(parent){
        var child = null;
        for( var  i = 0 ; i < parent.children.length; i++){
            child = parent.children[i];
            if(child.GetMaterial() != null){
                if(child.GetMaterial().GetShader() != this.pickingShader)
                    store[child.id] = child.GetMaterial().GetShader();
                child.GetMaterial().SetShader(this.pickingShader);
            }
            if( parent.children[i].children.length > 0){
                //  if(parent instanceof OMEGA.Omega3D.Scene ) console.log( i + " -- > PARENT : " + typeof(parent) + " __ " + parent.children.length);
                this.swapShadersPicking(parent.children[i] );
            }
        }
    };
    this.swapShadersNotPicking = function(parent){
        var child = null;
        for( var  i = 0 ; i < parent.children.length; i++){
            child = parent.children[i];
            if(child.GetMaterial() != null && store[child.id] != null){
                child.GetMaterial().SetShader(store[child.id]);
            }
            if( parent.children[i].children.length > 0) this.swapShadersNotPicking(parent.children[i] );
        }
    };
    var canvas = document.getElementById("omega");
    document.onmousemove = function(evt){
        var rect = canvas.getBoundingClientRect();
        mousePos.x = evt.clientX - rect.left;
        mousePos.y = evt.clientY - rect.top;
    };
};
ColorPickPass.prototype = new Pass();
OMEGA.Omega3D.ColorPickPass = ColorPickPass;
function DepthPass(renderer,scene, camera, texture){
    RenderPass.apply(this, [renderer, scene, camera]);
    this.texture = texture;
    this.fogSave = false;
    this.render = function(){
        if(this.cam) this.cam.update();
        if(this.scene) {
            this.scene.update();
            this.fogSave = this.scene.hasFog;
            this.scene.hasFog = false;
        }


        this.gl.frontFace(this.gl.CW);
        this.gl.enable(this.gl.CULL_FACE);
        this.gl.cullFace(this.gl.FRONT);

        this.gl.bindFramebuffer(this.gl.FRAMEBUFFER, this.texture.GetFrameBuffer());


        this.gl.enable( this.gl.DEPTH_TEST );
        this.renderer.viewPort( this.scene, 0, 0, this.texture.GetFrameBuffer().width ,this.texture.GetFrameBuffer().height );
        this.renderer.renderScene( this.scene, this.cam );
        this.gl.disable( this.gl.DEPTH_TEST );


        this.gl.cullFace(this.gl.BACK);
        this.gl.disable(this.gl.CULL_FACE);

        //this.gl.enable( this.gl.DEPTH_TEST );
        //this.renderer.viewPort( this.scene, 0, 0, this.texture.GetFrameBuffer().width ,this.texture.GetFrameBuffer().height );
        //this.renderer.renderScene( this.scene, this.cam );
        //this.gl.disable( this.gl.DEPTH_TEST );
        //this.texture.GenerateMipmap();

        this.gl.bindFramebuffer(this.gl.FRAMEBUFFER, null);

        if(this.scene) {
            this.scene.hasFog = this.fogSave;
        }
    };
};
DepthPass.prototype = new RenderPass();
OMEGA.Omega3D.DepthPass = DepthPass;function FilterPass(renderer, shader, in_texture, out_texture){
    RenderPass.apply(this, [renderer, null, null]);

    this.in_texture = in_texture;
    this.out_texture = out_texture;
    this.shader = shader;

    this.render = function(){
        this.renderer.renderFilter( this.shader, this.in_texture, this.out_texture);
    };
};
PostProcessingPass.prototype = new RenderPass();
OMEGA.Omega3D.FilterPass = FilterPass;
function Pass(){
    this.next = null;this.prev = null;
    this.render = function(){};
};
OMEGA.Omega3D.Pass = Pass;function PostProcessingPass(renderer, screen_quad, in_shader, out_texture){
    RenderPass.apply(this, [renderer, screen_quad.parentScene, null]);

    //console.log(screen_quad.parentScene.GetGL());
    this.out_texture = out_texture;
    this.in_shader = in_shader;
    this.screen_quad = screen_quad;

    this.render = function(){
        this.screen_quad.parentScene.update();


        this.screen_quad.GetMaterial().SetShader( this.in_shader );
        this.gl.bindFramebuffer(this.gl.FRAMEBUFFER, this.out_texture.GetFrameBuffer());

        this.renderer.viewPort( this.scene, 0, 0, this.out_texture.GetFrameBuffer().width ,this.out_texture.GetFrameBuffer().height );
        this.renderer.renderScene( this.scene, null );

        this.gl.bindFramebuffer(this.gl.FRAMEBUFFER, null);
        this.out_texture.Disable();
        this.screen_quad.GetMaterial().SetTextures( [this.out_texture] );

    };
};
PostProcessingPass.prototype = new RenderPass();
OMEGA.Omega3D.PostProcessingPass = PostProcessingPass;
function ProxyPass(target, proxy, args ){
    Pass.apply(this);
    this.next = null;this.prev = null;
    this.target = target;
    this.proxy  = proxy;
    this.args   = args || null;
    this.render = function(){ this.proxy.apply(this.target,this.args);};
};
RenderPass.prototype = new Pass();
OMEGA.Omega3D.ProxyPass = ProxyPass;
function RenderChain(){
    var passes = [];
    var head = null, tail = null, current = null;

    this.AddRenderPass = function( pass ){
        passes.push( pass );
        if(!head){
            head = pass;
            tail = head;
        }else{
            pass.prev = tail;
            tail.next = pass;
            tail = pass;
        }
    };

    this.Render = function(){
        current = head;
        while(current){
            current.render();
            current = current.next;
        }
    };
};
OMEGA.Omega3D.RenderChain = RenderChain;
function RenderPass(renderer, scene, camera, alphaBlend ){
    Pass.apply(this);
    //if(!renderer||!scene||!camera){
    //    //if(!renderer) OMEGA.Omega3D.Log(" *** RENDERPASS WARNING: plz make sure you passed a renderer to the renderpass!");
    //   // if(!scene)    OMEGA.Omega3D.Log(" *** RENDERPASS WARNING: plz make sure you passed a scene to the renderpass!");
    //   // if(!camera)   OMEGA.Omega3D.Log(" *** RENDERPASS WARNING: plz make sure you passed a camera to the renderpass!");
    //    return;
    //}
    this.renderer = renderer;
    this.scene = scene;
    this.cam   = camera;
    this.gl    = scene!=null ? scene.getGL() : null;
    this.color = scene!=null ? scene.getColor():null;

    this.AlphaBlend = alphaBlend | false;

    this.render = function(){
        if(this.cam) this.cam.update();
        if(this.scene) this.scene.update();

        if(this.AlphaBlend){
            this.gl.disable( this.gl.DEPTH_TEST );
            this.gl.enable(this.gl.BLEND);
            this.gl.blendFunc( this.gl.SRC_ALPHA,  this.gl.ONE_MINUS_SRC_ALPHA);
        }else{
            this.gl.enable( this.gl.DEPTH_TEST );
        }

        //this.gl.cullFace(this.gl.BACK);
        this.renderer.viewPort(this.scene);
        this.renderer.renderScene( this.scene, this.cam );

        if(this.AlphaBlend){
            this.gl.disable(this.gl.BLEND);
            this.gl.enable( this.gl.DEPTH_TEST );
        }else{
            this.gl.disable( this.gl.DEPTH_TEST );
        }
        //
    };
};
RenderPass.prototype = new Pass();
OMEGA.Omega3D.RenderPass = RenderPass;function RenderToTexturePass(renderer,scene, camera, texture){
    RenderPass.apply(this, [renderer, scene, camera]);
    this.texture = texture;
    this.render = function(){
        if(this.cam!=null)this.cam.update();
        if(this.scene!=null) this.scene.update();

        this.gl.bindFramebuffer(this.gl.FRAMEBUFFER, this.texture.GetFrameBuffer());



        this.gl.enable( this.gl.DEPTH_TEST );



        this.renderer.viewPort( this.scene, 0, 0, this.texture.GetFrameBuffer().width ,this.texture.GetFrameBuffer().height );
        this.renderer.renderScene( this.scene, this.cam );
        this.gl.disable( this.gl.DEPTH_TEST );
        //this.texture.GenerateMipmap();

        this.gl.bindFramebuffer(this.gl.FRAMEBUFFER, null);
    };
};
RenderToTexturePass.prototype = new RenderPass();
OMEGA.Omega3D.RenderToTexturePass = RenderToTexturePass;
function ShadowMapPass(renderer,scene, camera, texture, target_scene){
    RenderPass.apply(this, [renderer, scene, camera]);
    this.texture = texture;
    this.target_scene = target_scene;


    var tex = new Omega3D.BasicTexture();
    var sha = new Omega3D.Shaders.Basic(false,scene);
    this.dummyMat =  new Omega3D.Material(sha, [tex]);

    var savedMaterials = new Array();
    this.swapMaterialsForBasic = function(){
        savedMaterials = new Array();
        var children = this.scene.children;
        for( var i = 0; i < children.length; i++){
            savedMaterials.push(children[i].GetMaterial() );
            children[i].SetMaterial(this.dummyMat);
        }
    };
    this.swapMaterialsForShadow = function(){
        var children = this.scene.children;
        for( var i = 0; i < children.length; i++){
            children[i].SetMaterial(savedMaterials[i]);
            if(children[i].parentScene != this.target_scene){
                this.target_scene.addChild( children[i] );
            }
        }
    };

    this.render = function(){
        this.cam.update();
        this.scene.update();
        this.swapMaterialsForBasic();
        this.gl.bindFramebuffer(this.gl.FRAMEBUFFER, this.texture.GetFrameBuffer());

        this.gl.colorMask(false, false, false, false);
        this.gl.enable( this.gl.DEPTH_TEST );
        this.gl.cullFace(this.gl.FRONT);
        this.renderer.viewPort( this.scene, 0, 0, this.texture.GetFrameBuffer().width ,this.texture.GetFrameBuffer().height );
        this.renderer.renderScene( this.scene, this.cam );
        this.gl.disable( this.gl.DEPTH_TEST );
        this.gl.cullFace(this.gl.BACK);
        //this.texture.GenerateMipmap();

        this.gl.bindFramebuffer(this.gl.FRAMEBUFFER, null);
        this.swapMaterialsForShadow();
        this.gl.colorMask(true, true, true, true);
    };
};
ShadowMapPass.prototype = new RenderPass();
OMEGA.Omega3D.ShadowMapPass = ShadowMapPass;function StencilPass(renderer, scene, camera, stencil_data  ){
    RenderPass.apply(this, [renderer, scene, camera]);
    var stencilData  = stencil_data.stencilData;
    var contentData = stencil_data.contentData;
    this.render = function(){
        this.cam.update();
        this.scene.update();

        this.gl.enable( this.gl.STENCIL_TEST );


        this.renderer.viewPort(this.scene);
        this.renderer.SetStencilParams(this.scene,
            stencilData.stencilFuncData,
            stencilData.stencilOpData,
            stencilData.stencilMask,
            stencilData.depthMask );

        this.renderer.renderScene( this.scene, this.cam );

        this.renderer.SetStencilParams(this.scene,
            contentData.stencilFuncData,
            contentData.stencilOpData,
            contentData.stencilMask,
            contentData.depthMask );
    };
};
StencilPass.prototype = new RenderPass();
OMEGA.Omega3D.StencilPass = StencilPass;
function WebGLRenderer( debugRender ){

    var projectionMatrix = null;
    var modelViewMatrix  = null;

    var rp_head = null;var rp_tail = null;
    var renderChain = null;
    this.passes    = new Array();
    this.materials = null;
    this.objects   = null;
    this.list      = null;
    var debug      = debugRender || false;

    this.autoClear = true;
    this.CLEAR_COLOR_BIT = true;
    this.CLEAR_DEPTH_BIT = true;
    this.CLEAR_STENCIL_BIT = true;


    __construct = function(){
        projectionMatrix = mat4.create();
        this.materials = new Array();
        this.objects   = new Array();
        this.list      = new Array();

        window.requestAnimFrame = (function(){
            return  (window.requestAnimationFrame       ||
            window.webkitRequestAnimationFrame ||
            window.mozRequestAnimationFrame    ||
            window.oRequestAnimationFrame      ||
            window.msRequestAnimationFrame     ||
            function(  callback ){
                window.setTimeout(callback, 1000 / 60);
            });
        })();
    }();

    this.clear = function(color){
        var flagC = this.CLEAR_COLOR_BIT   ? OMEGA.Omega3D.GL.COLOR_BUFFER_BIT   : 0;
        var flagD = this.CLEAR_DEPTH_BIT   ? OMEGA.Omega3D.GL.DEPTH_BUFFER_BIT   : 0;
        var flagS = this.CLEAR_STENCIL_BIT ? OMEGA.Omega3D.GL.STENCIL_BUFFER_BIT : 0;
        var color = color || {r:0,g:0,b:0, a:1.0};
        OMEGA.Omega3D.GL.clearColor( color.r, color.g, color.b, color.a );
        OMEGA.Omega3D.GL.clear(flagC | flagD | flagS );
    };

    this.viewPort = function(scene, x, y, w, h ){
        var gl = scene.getGL();
        gl.viewport( x || 0, y || 0, w || gl.viewPortWidth,h || gl.viewPortHeight);
    };

    this.SetStencilParams = function(scene, stencilFuncParams, stencilOpParams, stencilMask, depthMask){
        var gl = scene.getGL();
        if(stencilFuncParams) gl.stencilFunc(stencilFuncParams[0], stencilFuncParams[1], stencilFuncParams[2]); // Set any stencil to 1
        if(stencilOpParams) gl.stencilOp(stencilOpParams[0], stencilOpParams[1], stencilOpParams[2]);
        gl.stencilMask(stencilMask); // Write to stencil buffer
        gl.depthMask(depthMask); // Don't write to depth buffer
        //gl.clear(gl.STENCIL_BUFFER_BIT); // Clear stencil buffer (0 by default)
    };




    this.addRenderPass = function( pass ){
        this.passes.push( pass );
        if(!rp_head){
            rp_head = pass;
            rp_tail = rp_head;
        }else{
            pass.prev = rp_tail;
            rp_tail.next = pass;
            rp_tail = pass;
        }
    };
    this.SetRenderChain = function( chain ){
        renderChain = chain;
    }


    this.render = function(){
        if(OMEGA.Omega3D.AVAILABLE && renderChain!= undefined )renderChain.Render();
    };

    this.renderSceneToStencil = function(scene, camera){
        camera.update();
        this.materials = scene.materials;
        this.objects   = scene.objects;
        this.list      = scene.list;


        var gl = scene.getGL();
        gl.enable( gl.STENCIL_TEST );

        var color = scene.getColor();
        gl.disable( gl.STENCIL_TEST );
    };

    var filterScene = new Omega3D.Scene();
    var filterMesh  = new Omega3D.Mesh( new Omega3D.Geometry.SquareGeometry(1, 1));
    var filterMat   = new Omega3D.Material(null, []);
    var filterQuad  = new Omega3D.Object3D( filterMesh, filterMat );

    filterScene.addChild( filterQuad );
    this.renderFilter = function( shader, tex_in, tex_out){
        filterScene.update();
        filterQuad.GetMaterial().SetShader( shader );


        filterQuad.GetMaterial().SetTextures( [tex_in] );
        filterScene.getGL().bindFramebuffer(filterScene.getGL().FRAMEBUFFER, tex_out.GetFrameBuffer());
        this.viewPort( filterScene, 0, 0, tex_out.GetFrameBuffer().width, tex_out.GetFrameBuffer().height);
        this.renderScene( filterScene );
        filterScene.getGL().bindFramebuffer(filterScene.getGL().FRAMEBUFFER, null);
    }


    this.renderScene = function( scene, camera ){
        if(debug) OMEGA.Omega3D.Log("Renderer.Render START");
        var gl = scene.getGL();
        this.clear(scene.getColor());
        // gl.clearDepth(1.0);
        //  gl.colorMask(true, true, true, true);
        gl.clear(gl.COLOR_BUFFER_BIT | gl.DEPTH_BUFFER_BIT);


        var subject = null, program = null;
        for(var i = 0; i < scene.children.length; i++){
            subject = scene.children[i];
            subject.Update(scene.getGL(), camera );

            //If L.O.D, update.
            if(subject instanceof LODObject3D) subject.adjustLODLevel(camera);

            //render object.
            this.RenderObject(scene, camera, subject);

            subject.LateUpdate(scene.getGL(), camera );

        }
        if(debug) OMEGA.Omega3D.Log("Renderer.Render END");
    };

    this.RenderObject = function( scene, camera, subject  ){
        var gl = scene.getGL();
        if(subject.material!= null ){

            //Enable the material.
            subject.material.Enable();


            //-----------------------------------------------------------------
            //Update the material.
            // -----------------------------------------------------------------
            program = subject.material.GetShader().GetProgram();

            //Update textures.
            if( subject.material.GetTextures() != null ) {
                if(subject.material.hasAtlas){
                    if(subject.GetMesh().GetGeometry() != null ){
                        var l = Math.sqrt(subject.GetMesh().GetGeometry().vertices_raw.length);
                        subject.GetMesh().GetGeometry().UpdateAtlasUV( subject.material.GetCurrentAtlasFrame(), l, l);
                        subject.GetMesh().GetGeometry().isDirty = true;
                    }
                }

                var t = subject.material.GetTextures();
                var l = t.length;
                for (var j = 0; j < l; j++) if(t[j].needsUpdate)t[j].Update();
            }

            //Update mesh if needed.
            if(subject.GetMesh() != null ) subject.GetMesh().Update();


            //Set projection + view matrix.
            if(program.uProjectionMatrix && camera != null ) gl.uniformMatrix4fv( program.uProjectionMatrix, false, camera.GetProjectionMatrix() );
            if(program.uViewMatrix       && camera != null ) gl.uniformMatrix4fv( program.uViewMatrix   , false,  camera.GetMatrix() );
            if(program.uInvViewMatrix    && camera != null ) gl.uniformMatrix4fv( program.uInvViewMatrix, false, camera.GetInverseMatrix() );

            //Set standard uniforms in material.
            for(var key in subject.material.uniforms){
                gl.uniform3fv( program[key], subject.material.uniforms[key].value.apply(subject.material, null));
            }

            //Set custom uniforms in material.
            for(var key in subject.material.custom_uniforms){
                var u = subject.material.custom_uniforms[key];
                if(u.type == "int" ) gl.uniform1i(program[key], u.value );
                else if(u.type == "float" ) gl.uniform1f(program[key], u.value );
                else if(u.type == "mat4" ) gl.uniformMatrix4fv(program[key],false, u.value );
                else if(u.type == "mat3" ) gl.uniformMatrix3fv(program[key],false, u.value );
                else if(u.type == "vec4" ) gl.uniform4fv(program[key], u.value );
                else if(u.type == "vec3" ) {
                    gl.uniform3fv(program[key], u.value );
                    // console.log( key +  ": "+ u.value  + " : "+ program[key] );
                }
                else if(u.type == "vec2" ) gl.uniform2fv(program[key], u.value );
            }

            //custom attributes in material.
            for( var key in subject.material.custom_attribs){
                gl.enableVertexAttribArray(program[key]);
                gl.bindBuffer( gl.ARRAY_BUFFER, subject.material.custom_attribs[key].value);
                gl.vertexAttribPointer( program[key],subject.material.custom_attribs[key].value.itemSize, gl.FLOAT, false, 0, 0 );
            };


            //Render lights.
            if(scene.getLights().length > 0 && scene.lightsON && program != null ){
                var lights = scene.getLights();
                for( var index in lights){
                    for(var key in lights[index].uniforms){
                        if(lights[index].uniforms[key].type == "vec3"       ) gl.uniform3fv( program[key], lights[index].uniforms[key].value.apply( lights[index], null));
                        else if(lights[index].uniforms[key].type == "float" ) gl.uniform1f ( program[key], lights[index].uniforms[key].value.apply( lights[index], null));
                    }
                }
            }

            //Render fog.
            if(scene.hasFog){
                gl.uniform3fv(program["uFogColor"], [scene.getColor().r, scene.getColor().g, scene.getColor().b ] );
                gl.uniform2fv(program["uFogDist" ], [scene.fogStart, scene.fogEnd] );
            }


            //-----------------------------------------------------------------
            // Render the Object.
            // -----------------------------------------------------------------

            // model matrix
            gl.uniformMatrix4fv( program.uModelMatrix , false, subject.GetMatrix() );

            // normal matrix
            var normalMatrix4 = mat4.create();
            var normalMatrix3 = mat3.create();
            if(camera != null){
                mat4.multiply(normalMatrix4, camera.GetMatrix(), subject.GetMatrix() );
                mat3.fromMat4(normalMatrix3,normalMatrix4 );
                mat3.invert(normalMatrix3,normalMatrix3 );
                mat3.transpose(normalMatrix3,normalMatrix3);
            }
            gl.uniformMatrix3fv( program.uNormalMatrix, false, normalMatrix3 );





            //Bind combined buffer -> [ vertex, normal, uv,  barycentric ] = (3 + 3 + 2 + 3) * 4 bytes = 44 bytes
            //Bind combined buffer -> [ vertex, normal, uv, tangent, bitangent, barycentric ] = (3 + 3 + 2 + 3 + 3 + 3) * 4 bytes = 68 bytes
            var totalBytes = 68;
            if(subject.GetMesh() != null ) {
                gl.bindBuffer(gl.ARRAY_BUFFER, subject.GetMesh().GetCombinedBuffer());

                // vertices
                if(program.aVertexPos !=-1 ) {
                    gl.enableVertexAttribArray(program.aVertexPos);
                    gl.vertexAttribPointer(program.aVertexPos, 3, gl.FLOAT, false, totalBytes, 0);
                }

                // aVertexNormals
                if(program.aVertexNormal!=-1 ) {
                    gl.enableVertexAttribArray(program.aVertexNormal);
                    gl.vertexAttribPointer(program.aVertexNormal, 3, gl.FLOAT, false, totalBytes, 12); // offset = 12 = 4 * 3
                }

                // uvs
                if(program.aTextureCoord!=-1 ){
                    gl.enableVertexAttribArray(program.aTextureCoord);
                    gl.vertexAttribPointer( program.aTextureCoord,2, gl.FLOAT, false, totalBytes, 24 ); // offset = 24 = 4 * 6
                }

                // aVertexTangent
                if(program.aVertexTangent!=-1 ) {
                    gl.enableVertexAttribArray(program.aVertexTangent);
                    gl.vertexAttribPointer(program.aVertexTangent, 3, gl.FLOAT, false, totalBytes, 32); // offset = 32 = 4 * 8
                }

                // aVertexBitangent
                if(program.aVertexBitangent!=-1 ) {
                    gl.enableVertexAttribArray(program.aVertexBitangent);
                    gl.vertexAttribPointer(program.aVertexBitangent, 3, gl.FLOAT, false, totalBytes, 44); // offset = 44 = 4 * 11
                }

                // aBaricentric
                if(program.aBaricentric !=-1 ) {
                    gl.enableVertexAttribArray(program.aBaricentric);
                    gl.vertexAttribPointer(program.aBaricentric, 3, gl.FLOAT, false, totalBytes, 56);  // offset = 32 = 4 * 14
                }

                // aPickingColor
                if(program.aPickingColor !=-1 &&  subject.GetMesh().GetColorPickingBuffer() != undefined) {
                    gl.enableVertexAttribArray(program.aPickingColor);
                    gl.bindBuffer(gl.ARRAY_BUFFER, subject.GetMesh().GetColorPickingBuffer());
                    gl.vertexAttribPointer(program.aPickingColor, subject.GetMesh().GetColorPickingBuffer().itemSize, gl.FLOAT, false, 0, 0);
                }

                // indices
                gl.bindBuffer( gl.ELEMENT_ARRAY_BUFFER, subject.GetMesh().GetIndexBuffer() );

                // 4   DRAW OBJECT
                //gl.drawArrays( gl.TRIANGLES, 0, this.GetMesh().GetVertexBuffer().numItems);
                if( subject.drawType == OMEGA.Omega3D.Object3D.DEFAULT        ) gl.drawElements( gl.TRIANGLES , subject.GetMesh().GetIndexBuffer().numItems, gl.UNSIGNED_SHORT, subject.GetMesh().GetIndexBuffer());
                else if(subject.drawType == OMEGA.Omega3D.Object3D.WIREFRAME ) gl.drawElements( gl.LINES, subject.GetMesh().GetIndexBuffer().numItems, gl.UNSIGNED_SHORT,  subject.GetMesh().GetIndexBuffer());
                else if(subject.drawType == OMEGA.Omega3D.Object3D.POINTS    ) gl.drawArrays( gl.POINTS      , 0, subject.GetMesh().GetGeometry().GetVertices().length/3);
                else if(subject.drawType == OMEGA.Omega3D.Object3D.TRIANGLES ) gl.drawArrays( gl.TRIANGLES,0, subject.GetMesh().GetGeometry().GetVertices().length/3);



                //disable arrays.
                if(program.aVertexPos!=-1   ) gl.disableVertexAttribArray(program.aVertexPos   );
                if(program.aTextureCoord!=-1) gl.disableVertexAttribArray(program.aTextureCoord);
                if(program.aVertexNormal!=-1) gl.disableVertexAttribArray(program.aVertexNormal);
                if(program.aTangent!=-1     ) gl.disableVertexAttribArray(program.aBitangent);
                if(program.aBitangent!=-1   ) gl.disableVertexAttribArray(program.aBitangent);
            }

            //Disable the material.
            subject.material.Disable();

            //dispose gl.
            gl = null;
        }
        //Render children.
        for(var i = 0; i < subject.children.length; i++){
            this.RenderObject(scene, camera, subject.children[i] );
        }
    };

}
OMEGA.Omega3D.WebGLRenderer = WebGLRenderer;
