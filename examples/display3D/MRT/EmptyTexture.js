function EmptyTexture( ){
    Texture.apply( this, [null, false, 0] );

    this.handleTextureLoaded = function(image, texture ){ };
    this.Enable = function( shader ){
        this.gl_context.activeTexture( this.gl_context.TEXTURE0+this.ID);
        this.gl_context.bindTexture(this.gl_context.TEXTURE_2D, this.tex);
        if(shader){
            var samplerLocation = shader.GetSamplerLocation(this.tex_id);
            this.gl_context.uniform1i(samplerLocation , this.ID );
        }

    };
    this.Disable = function(){
        this.gl_context.bindTexture(this.gl_context.TEXTURE_2D, null);
    };
    this.handleTextureLoaded(this.img, this.tex);
};
EmptyTexture.prototype = new Texture();
OMEGA.Omega3D.EmptyTexture = EmptyTexture;
