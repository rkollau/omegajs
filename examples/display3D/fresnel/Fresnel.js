//1. Basic Textured Object.
BasicObject = function(main_scene, img ){
    //create.
    var object = new Omega3D.Object3D(
        new Omega3D.Mesh( new Omega3D.Geometry.SphereGeometry(1.0,25.0,25.0) ),
        new Omega3D.BasicMaterial([new Omega3D.BasicTexture(img)], main_scene));

    //set position.
    object.SetPosition( -4.5, -3, 0.0);

    //add to scene, with shadowcasting set to true.
    main_scene.addChild( object, true );

    this.render = function(){
        object.Rotate( 0.01, 0.02, 0.0);
    }
};

//2. Basic Textured Object with a fresnel rim.
FresnelRimObject = function(main_scene, img){
    //create.
    var texture = new Omega3D.BasicTexture(img);
    var shader  = new Omega3D.Shaders.FresnelRim(main_scene,{ scale:"10.0", power:"6.0", minimum:"0.1", color:"1.0, 1.0, 1.0"});
    var object  = new Omega3D.Object3D(
                    new Omega3D.Mesh( new Omega3D.Geometry.SphereGeometry(1.2, 50.0,50.0) ),
                    new Omega3D.Material(shader, [texture] )
                  );

    //set position.
    object.SetPosition( -1.5, -3, 0.0);

    //add to scene, with shadowcasting set to true.
    main_scene.addChild( object, true );

    this.render = function(){
        object.Rotate( 0.01, 0.02, 0.0);
    }
};

//3.  Reflective object (cubemapped).
CubeMapReflectionObject = function(main_scene, cubemap){
    //create.
    var texture  = new Omega3D.CubemapTexture(cubemap);
    var object   = new Omega3D.Object3D(
                    new Omega3D.Mesh( new Omega3D.Geometry.SphereGeometry(1.66, 50.0,50.0) ),
                    new Omega3D.Material( new Omega3D.Shaders.CubemapReflection(main_scene), [texture] )
                   );

    //set position.
    object.SetPosition( 2.0, -3, 0.0);

    //add to scene, with shadowcasting set to true.
    main_scene.addChild( object, true );


    this.render = function(){

    }
};

//4.  Fresnel Reflective object with basic texture.
FresnelObject = function(main_scene, img, cubemap){
    //create.
    var color      = new Omega3D.BasicTexture(img);
    var reflection = new Omega3D.CubemapTexture(cubemap);
    var geom       = new Omega3D.Mesh( new Omega3D.Geometry.SphereGeometry(2.0, 75.0,75.0) );
    var shader     = new Omega3D.Shaders.FresnelReflection( main_scene, { scale:"1.0", power:"3.0",minimum:"0.0",  color:"1.0, 1.0, 1.0"} );
    var mat        = new Omega3D.Material( shader, [reflection, color]);
    var fresnel    = new Omega3D.Object3D( geom, mat );

    //set position.
    fresnel.SetPosition(  7.0, -3, 0.0);

    //add to scene, with shadowcasting set to true.
    main_scene.addChild( fresnel, true );

    this.render = function(){
        fresnel.Rotate( 0.01, 0.02, 0.0);
    }
};

