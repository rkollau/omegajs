SkullsScene = function(renderer, camera, analyser ){
    this.camera = camera;
    this.ana = analyser;
    this.finalScene   = new Omega3D.Scene();

    var fbSize = 1024;
    this.sceneTexture = new Omega3D.FrameBufferTexture(fbSize,fbSize);
    this.textureFinal = new Omega3D.FrameBufferTexture(fbSize,fbSize);
    this.finalMaterial = new Omega3D.Material( new Omega3D.Shaders.PostProcessing( true,  this.finalScene ), [this.textureFinal]);

    var geometry       = new Omega3D.Geometry.SquareGeometry(1, 1);
    var mesh           = new Omega3D.Mesh( geometry );
    var quad            = new Omega3D.Object3D(mesh,  this.finalMaterial);
    this.finalScene .addChild( quad );



    this.scene = new Omega3D.Scene();
    this.scene.setColor(0, 0, 0);

    this.light01 = new Omega3D.Light( Omega3D.DIRECTIONAL_LIGHT );
    this.light01.SetDirection( 0, 1, 1);
    this.light01.SetAmbient( 0, 0, 0 );
    this.light01.SetDiffuse( 1.0, 1.0, 1.0 );
    this.light01.SetSpecular( 1.0, 1.0, 1.0 );
    this.scene.addLight(this.light01 );

    this.light02 = new Omega3D.Light( Omega3D.POINT_LIGHT );
    this.light02.SetPosition( 0, 0.5, 0);
    this.light02.SetAmbient( 0, 0, 0 );
    this.light02.SetDiffuse( 0.0, 1.0, 0.0 );
    this.light02.SetSpecular( 1.0, 1.0, 1.0 );
    this.scene.addLight(this.light02 );


    this.angle = 0;
    this.currAngle = 0;
    this.skullPattern = 1;
    this.skulls = new Array();
    this.kickCount = 0;
    var self = this;





    //objects.
    var material_skull = new Omega3D.BasicMaterial( [ ], this.scene, [ 1.0, 1.0, 0.0 ]);
    var oo             = new Omega3D.parsers.OBJParser(false);
    var skull_Mesh     = oo.ParseToObject3D("../../obj/skull_good.obj",1.0, material_skull).GetMesh();


    var skull_holder = new Omega3D.Object3D(null, null);
    this.scene.addChild( skull_holder );

    //render chain.
    //this.chain = new Omega3D.RenderChain();
    //this.chain.AddRenderPass( new RenderPass( renderer, this.scene, camera ));

    this.chain = new Omega3D.RenderChain();
    this.chain.AddRenderPass( new RenderToTexturePass( renderer, this.scene, this.camera, this.textureFinal ));
    // this.chain.AddRenderPass( new FilterPass( renderer, new Omega3D.Shaders.PP_Bloom( 2.0 ), this.sceneTexture, this.textureFinal ));
    // this.chain2.AddRenderPass( new FilterPass( renderer, new Omega3D.Shaders.PostProcessingKernel(Omega3D.Shaders.PostProcessingKernel.Kernels.boxBlur ), this.sceneTexture, this.textureFinal ));
    //this.chain2.AddRenderPass( new FilterPass( renderer, new Omega3D.Shaders.Gausian_Blur( "1024.0" ), this.sceneTexture, this.textureFinal ));
    this.chain.AddRenderPass( new RenderPass( renderer, this.finalScene, this.camera ));

    this.initialize = function(){
        var skull = new Omega3D.Object3D( skull_Mesh, material_skull);
        skull.SetRotation( Math.PI, 0, 0);
        skull.SetPosition( 0 , 0.1, 0);
        self.scene.addChild( skull );
    }


    this.start = function(){
        self.ana.threshold = 5;
        self.ana.decay     = 0.5;
        self.ana.bars      = 4;
        self.ana.startBar  = 1;

        self.kickCount = 0;
        self.camera.LookAt(0, 0, 0, [0, 0, -2]);
    };

    //onKick.
    this.onKick = function(){
        self.angle = Math.random() * Math.PI - Math.PI / 2;

        if( ++self.kickCount%4 == 0 ){
            var val = Math.random() * 4;
            val = Math.round( val );
            while(val == this.skullPattern){
                val = Math.random() * 4;
                val = Math.round( val );
            }
            self.skullPattern = val;
            self.SetSkullPattern();
        }
    };

    this.SetSkullPattern = function(){
        for( var  i = 0; i < self.skulls.length; i++){
            self.skulls[i].SetParent(null);
            self.skulls[i] = null;
        }

        self.skulls = new Array();
        var positions = new Array();
        switch( self.skullPattern ){
            default:
            case 1:
                positions.push( 0 , 0.1, 0 );
                break;
            case 2:
                positions.push( -0.17, 0.12, 0 );
                positions.push(  0.17, 0.12, 0 );
                break;
            case 3:
                positions.push( -0.2 , 0.2, 0 );
                positions.push(  0.2 , 0.2, 0 );
                positions.push(  0.0 , -0.05, 0 );
                break;
        }

        //add skulls.
        for( var  i = 0; i < positions.length; i+=3){
            var skull = new Omega3D.Object3D( skull_Mesh, material_skull);
            skull.SetRotation( Math.PI, 0, 0);
            skull.SetPosition( positions[i] , positions[i+1], positions[i+2]);

            var scale = Math.min( 1.0/(positions.length /3), 0.45 );
            skull.SetScale(scale,scale,scale);
            skull.SetParent(skull_holder);
            self.skulls.push( skull );
        }
    }

    //onRender.
    this.onRender = function(){
        if(this.currAngle != this.angle ){
            this.currAngle += ( this.angle - this.currAngle) * 0.1;
            skull_holder.SetRotation( 0,  -this.currAngle, 0);
        }
    }

    //this.initialize();
    this.SetSkullPattern();
}


