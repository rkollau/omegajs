Scene01 = function(renderer, camera, analyser ){
    this.camera = camera;
    this.ana = analyser;
    this.finalScene   = new Omega3D.Scene();

    var fbSize = 1024;
    this.sceneTexture = new Omega3D.FrameBufferTexture(fbSize,fbSize);
    this.textureFinal = new Omega3D.FrameBufferTexture(fbSize,fbSize);
    this.finalMaterial = new Omega3D.Material( new Omega3D.Shaders.PostProcessing( true,  this.finalScene ), [this.textureFinal]);
    var geometry       = new Omega3D.Geometry.SquareGeometry(1, 1);
    var mesh           = new Omega3D.Mesh( geometry );
    var quad            = new Omega3D.Object3D(mesh,  this.finalMaterial);
    this.finalScene .addChild( quad );


    this.scene = new Omega3D.Scene();
    this.scene.setColor(0, 0, 0);

    this.light01 = new Omega3D.Light( Omega3D.DIRECTIONAL_LIGHT );
    this.light01.SetDirection( 0, 1, 1);
    this.light01.SetAmbient( 0, 0, 0 );
    this.light01.SetDiffuse( 1.0, 1.0, 1.0 );
    this.light01.SetSpecular( 1.0, 1.0, 1.0 );
    this.scene.addLight(this.light01 );

    this.light02 = new Omega3D.Light( Omega3D.POINT_LIGHT );
    this.light02.SetPosition( 0, 0.5, 0);
    this.light02.SetAmbient( 0, 0, 0 );
    this.light02.SetDiffuse( 0.0, 1.0, 0.0 );
    this.light02.SetSpecular( 1.0, 1.0, 1.0 );
    this.scene.addLight(this.light02 );


    this.angle = 0;
    this.currAngle = 0;
    var self = this;

    var material_donut, mesh, donut;
    material_donut = new Omega3D.BasicMaterial( [], self.scene, [1.0, 1.0, 1.0]);
    mesh     = new Omega3D.Mesh( new Omega3D.Geometry.TorusGeometry(0.5, 0.15, 60, 40) );
    donut    = new Omega3D.Object3D( mesh, material_donut );
    this.scene.addChild( donut );



    //objects.
    var material_skull = new Omega3D.BasicMaterial( [ ], this.scene, [ 1.0, 1.0, 0.0 ]);
    var oo             = new Omega3D.parsers.OBJParser(false);
    var skull_Mesh     = oo.ParseToObject3D("../../obj/skull_good.obj",0.45, material_skull).GetMesh();
    var skull;

    //render chain.
    //this.chain = new Omega3D.RenderChain();
    //this.chain.AddRenderPass( new RenderPass( renderer, this.scene, camera ));

    this.chain = new Omega3D.RenderChain();
    this.chain.AddRenderPass( new RenderToTexturePass( renderer, this.scene, this.camera, this.textureFinal ));
   // this.chain.AddRenderPass( new FilterPass( renderer, new Omega3D.Shaders.PP_Bloom( 2.0 ), this.sceneTexture, this.textureFinal ));
    // this.chain2.AddRenderPass( new FilterPass( renderer, new Omega3D.Shaders.PostProcessingKernel(Omega3D.Shaders.PostProcessingKernel.Kernels.boxBlur ), this.sceneTexture, this.textureFinal ));
    //this.chain2.AddRenderPass( new FilterPass( renderer, new Omega3D.Shaders.Gausian_Blur( "1024.0" ), this.sceneTexture, this.textureFinal ));
    this.chain.AddRenderPass( new RenderPass( renderer, this.finalScene, camera ));

    this.initialize = function(){
        skull = new Omega3D.Object3D( skull_Mesh, material_skull);
        skull.SetRotation( Math.PI, 0, 0);
        skull.SetPosition( 0 , 0.1, 0);
        self.scene.addChild( skull );
    }


    this.start = function(){
        self.ana.threshold = 5;
        self.ana.decay     = 0.5;
        self.ana.bars      = 4;
        self.ana.startBar  = 1;

        self.kickCount = 0;
        self.camera.LookAt(0, 0, 0, [0, 0, -2]);
    };

    //onKick.
    this.onKick = function(){
        self.angle = Math.random() * Math.PI - Math.PI / 2;
    };


    //onRender.
    this.onRender = function(){
        self.camera.LookAt(0, 0, 0, [0, 0, -2]);
        if(this.currAngle != this.angle ){
           this.currAngle += ( this.angle - this.currAngle) * 0.1
            donut.SetRotation( this.currAngle, this.currAngle, this.currAngle );
        }
        skull.SetRotation(Math.PI, this.currAngle, 0);

    }
    this.initialize();
}


