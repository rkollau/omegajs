Scene02 = function(renderer, camera, analyser ){
    this.camera = camera;
    this.ana = analyser;
    this.renderer = renderer;
    var self = this;
    this.scene = new Omega3D.Scene();
    this.scene.setColor(0, 0, 0);

    this.finalScene   = new Omega3D.Scene();

    var fbSize = 1024;
    this.sceneTexture = new Omega3D.FrameBufferTexture(fbSize,fbSize);
    this.textureFinal = new Omega3D.FrameBufferTexture(fbSize,fbSize);

    this.light01 = new Omega3D.Light( Omega3D.DIRECTIONAL_LIGHT );
    this.light01.SetDirection( 0, 1, 0.2);
    this.light01.SetAmbient( 0, 0, 0 );
    this.light01.SetDiffuse( 0.5, 0.5, 0.5 );
    this.light01.SetSpecular( 1.0, 1.0, 1.0 );
    this.scene.addLight(this.light01 );


    this.light02 = new Omega3D.Light( Omega3D.POINT_LIGHT );
    this.light02.SetPosition( 0, 0.5, 0);
    this.light02.SetAmbient( 0, 0, 0 );
    this.light02.SetDiffuse( 1.0, 0.0, 0.0 );
   // this.light02.SetSpecular( 1.0, 1.0, 1.0 );
    this.light02.SetRadius( 3.0);
    this.scene.addLight(this.light02 );

    //objects.
    this.material_skulll = new Omega3D.BasicMaterial( [ ], this.scene, [ 1.0, 1.0, 0.0 ]);
    this.material_path   = new Omega3D.BasicMaterial( [ ], this.scene, [ 1.0, 0.0, 0.0 ]);
    this.finalMaterial   = new Omega3D.Material( new Omega3D.Shaders.PostProcessing( true,  this.finalScene ), [this.textureFinal]);

    var oo       = new Omega3D.parsers.OBJParser(false);
    this.skullmesh = new Omega3D.Mesh( oo.ParseToObject3D("../../obj/skull_good.obj",0.45,this.material_skulll).GetMesh().GetGeometry() );
    this.gnomemesh = new Omega3D.Mesh( oo.ParseToObject3D("../../obj/pistol.obj",0.15,this.material_skulll).GetMesh().GetGeometry() );
    this.cubemesh = new Omega3D.Mesh( oo.ParseToObject3D("../../obj/icosahedron.obj",0.1,this.material_skulll).GetMesh().GetGeometry() );





    //render chain.
    //this.chain = new Omega3D.RenderChain();
    //this.chain.AddRenderPass( new RenderPass( renderer, this.scene, this.camera));

    this.chain = new Omega3D.RenderChain();
    this.chain.AddRenderPass( new RenderToTexturePass( renderer, this.scene, this.camera, this.textureFinal ));
   //this.chain.AddRenderPass( new FilterPass( renderer, new Omega3D.Shaders.PP_Bloom( 2.0 ), this.sceneTexture, this.textureFinal ));
  // this.chain2.AddRenderPass( new FilterPass( renderer, new Omega3D.Shaders.PostProcessingKernel(Omega3D.Shaders.PostProcessingKernel.Kernels.boxBlur ), this.sceneTexture, this.textureFinal ));
    //this.chain2.AddRenderPass( new FilterPass( renderer, new Omega3D.Shaders.Gausian_Blur( "1024.0" ), this.sceneTexture, this.textureFinal ));
    this.chain.AddRenderPass( new RenderPass( renderer, this.finalScene, camera ));

    this.chainNum = true;

    this.switchChain = function(){
        //self.chainNum = !self.chainNum;
        //
        //if(self.chainNum == true) self.renderer.SetRenderChain( self.chain );
        //else self.renderer.SetRenderChain( self.chain2 );
    }

    this.start = function(){
        self.scene.children = new Array();
        self.currZ = 0;

        self.ana.threshold = 6;
        self.ana.decay     = 0.8;
        self.ana.bars      = 4;
        self.ana.startBar  = 1;
        self.camera.LookAt(0, 0, 0, [0, 0, -2]);
        self.light02.SetPosition(0, 0.5, 0);

        var skull     = new Omega3D.Object3D(self.skullmesh, self.material_skulll);
        skull.SetRotation( Math.PI, 0, 0);
        skull.SetPosition( 0 , 0.1, 0);
        self.scene.addChild( skull );

        var planeMesh = new Omega3D.Mesh( new Omega3D.Geometry.SquareGeometry( 1.0, 200.0, 20, 20 ));
        var plane = new Omega3D.Object3D( planeMesh, this.material_path );
        this.scene.addChild( plane );

        plane.SetRotation( -Math.PI/2, 0, 0);
        plane.SetPosition( 0, 0,-5);


       var geometry       = new Omega3D.Geometry.SquareGeometry(1, 1);
       var mesh           = new Omega3D.Mesh( geometry );
       var quad            = new Omega3D.Object3D(mesh,  self.finalMaterial);
        self.finalScene .addChild( quad );

        self.kickCount = 0;

        self.wormNum = !self.wormNum;
    };


    //onKick.
    this.angle = 0;
    this.currAngle = 0;
    this.currZ = -8;
    this.savedZ = -8;
    this.kickCount = 0;
    this.onKick = function(){
        self.angle = Math.random() * Math.PI - Math.PI / 2;
        self.addSkull();
       // self.addBlocks();
        self.kickCount += 1;
        if(self.kickCount % 4 == 0 ){
            self.switchChain();
            //self.switchWorm();
        }
    }

    var skullAnim;
    this.addSkull = function(){
        this.currZ += 2.0;
        var choose = 0.3;//Math.random();
        var skull = new Omega3D.Object3D(choose< 0.4 ? self.skullmesh : this.gnomemesh, self.material_skulll);
        skull.SetRotation( Math.PI, this.angle, 0);
        var scale = Math.random() + 1.5;

        skull.SetScale( scale, scale, scale);

        skull.SetPosition( 0 , -0.2, this.currZ);
        this.scene.addChild( skull );


        var a01 = Math.PI - 0.25 + Math.random() * (Math.PI/2)/2;
        var a02 = (Math.PI/1.39) -  Math.random() *  (Math.PI/2 ) ;
        skullAnim = TweenMax.to(self.camera, 0.5, {x:  skull.GetPosition()[0] + Math.cos(a01) * 4 * Math.cos(a02),
            y:  -scale,
            z:  skull.GetPosition()[2]+ Math.cos(a01) * 4 *  Math.sin(a02),
            lookX: skull.GetPosition()[0],
            lookY: skull.GetPosition()[1],
            lookZ: skull.GetPosition()[2] });

        self.light02.SetPosition(0, -0.1, this.currZ);
    };

    this.addBlocks = function(xValue, yValue, zValue){
        for( var i  =0; i < 1; ++i){
            var block     = new Omega3D.Object3D(self.cubemesh, self.material_skulll);
            block.SetPosition(i + xValue ,yValue ,zValue);
          //  block.SetRotation(yValue * 0.5, zValue * 0.1, 0);
          //  block.SetScale(5.0, 1.0, 1.0);
            self.scene.addChild( block );
        }
    }

    this.clear = function(){
        TweenMax.killAll();
        self.scene.children = new Array();
        self.currZ = -8;
        self.savedZ = -8;
        self.kickCount = 0;

    }

    this.wormNum = false;
    this.wormRad = 1.5;
    this.switchWorm = function(){
       // self.wormNum = !self.wormNum;
       // self.wormRad = Math.abs(Math.cos(self.savedZ * 0.5 ) * 2);
      //  if(self.wormRad < 1.5 ) self.wormRad = 1.5;
    }

    //onRender.
    this.blockAngle = 0;
    this.onRender = function(){
        //if(self.savedZ < self.scene.getTime()){
            if(self.wormNum == true ){
                self.addBlocks(-1.5, Math.cos(self.savedZ) * 0.5,self.savedZ);
                self.addBlocks(1.5, Math.cos(-self.savedZ) * 0.5,self.savedZ);
            }
            else self.addBlocks(Math.cos(self.savedZ) * self.wormRad, Math.sin(self.savedZ) * self.wormRad,self.savedZ);
            self.savedZ += 0.1;//(self.wormNum == true ? Math.min( 0.05, (self.scene.getTime() - self.savedZ) * 0.25)  : Math.min( 0.05, (self.scene.getTime() - self.savedZ) * 0.05));
       // }
        self.switchWorm();
        //skull.SetRotation( Math.PI, this.angle, 0);
       // self.light02.SetPosition( 0, Math.cos(self.angle), 0);

    }
}


