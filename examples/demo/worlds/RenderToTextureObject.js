RTTObject = function( renderer, main_chain, main_scene ){
    var PI2 = Math.PI * 2;

    var texture_scene = new Omega3D.Scene();
    texture_scene.setColor(0.5, 0.5, 0.0);

    var camera = new Omega3D.cameras.Camera();
    camera.LookAt( 0, 0, 0, [0, 0, -4]);

    var directional_light = new Omega3D.Light(Omega3D.DIRECTIONAL_LIGHT);
    directional_light.SetDirection( 0.2, -1.0, 0.1);
    directional_light.SetAmbient  ( 0.2, 0.2, 0.2);
    directional_light.SetDiffuse  ( 0.8, 0.8, 0.8);
    texture_scene.addLight( directional_light );

    var torus_mat = new Omega3D.BasicMaterial([], texture_scene, [1.0, 0.0, 0.0]);
    var torus     = new Omega3D.Object3D( new Omega3D.Mesh( new Omega3D.Geometry.TorusGeometry(1.0, 0.5, 30, 40)), torus_mat);
    texture_scene.addChild( torus );

    var size = 512;
    var fbo = new Omega3D.FrameBufferTexture(size, size);

    //main_chain.AddRenderPass( new RenderToTexturePass(renderer, texture_scene, camera, fbo));

    var rtt_mat = new Omega3D.BasicMaterial([fbo], main_scene);
    var cube    = new Omega3D.Object3D( new Omega3D.Mesh( new Omega3D.Geometry.CubeGeometry(4.0, 4.0, 4.0)), rtt_mat);
   // cube.SetRotation(Math.random() * PI2, Math.random() * PI2, Math.random() * PI2);
    cube.SetPosition( 10, -2, 2);
    main_scene.addChild( cube, true );

    this.scene = texture_scene;
    this.fbo = fbo;
    this.camera = camera;

    this.render = function(){
        torus.Rotate(0.01, 0.03, 0.02);
        cube.Rotate( 0, 0.01, 0);
    }
};
