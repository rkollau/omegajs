CubeMapReflectionObject = function(main_scene, cubemap, renderer){
    this.texture             = new Omega3D.DynamicCubemapTexture(cubemap, 1024, 1024);
    this.object = new Omega3D.Object3D(
        new Omega3D.Mesh( new Omega3D.Geometry.SphereGeometry(1.66, 50.0,50.0) ),
        new Omega3D.Material( new Omega3D.Shaders.CubemapReflection(main_scene), [this.texture]));

    this.object.SetPosition( 2.0, -3, 0.0);
    main_scene.addChild( this.object, true );
    this.renderer = renderer;
    this.scene = main_scene;



    this.render = function(){
        //set dynamic cubemap object to non renderable.
        this.object.mayRender = false;


        //update faces dynamic cubemap.
        // 1. From Object Position.
        // 2. Renderer.
        // 3. Scene To Render.
        this.texture.UpdateFaces(this.object.GetPosition(), this.renderer, this.scene );


        //set dynamic cubemap object to renderable.
        this.object.mayRender = true;
    }
};
