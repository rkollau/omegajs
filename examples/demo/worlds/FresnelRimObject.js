FresnelRimObject = function(main_scene, img, renderer){
    this.texture = new Omega3D.BasicTexture(img);
    this.object  = new Omega3D.Object3D(
        new Omega3D.Mesh( new Omega3D.Geometry.SphereGeometry(1.2, 50.0,50.0) ),
        new Omega3D.Material( new Omega3D.Shaders.FresnelRim(main_scene,{ scale:"1.0", power:"3.0", minimum:"0.0", color:"1.0, 1.0, 1.0"}), [this.texture] ));

    this.object.SetPosition( -1.5, -3, 0.0);
    main_scene.addChild( this.object, true );
    this.renderer = renderer;
    this.scene = main_scene;

    this.render = function(){
        this.object.Rotate( 0.01, 0.02, 0.0);
    }

};
