FresnelObject = function(main_scene, img, cubemap){
    var color      =  new Omega3D.BasicTexture(img);
    var reflection = new Omega3D.CubemapTexture(cubemap);
    var geom       =  new Omega3D.Mesh( new Omega3D.Geometry.SphereGeometry(2.0, 75.0,75.0) );
    var mat        = new Omega3D.Material( new Omega3D.Shaders.FresnelReflection( main_scene, { scale:"1.0", power:"2.0",minimum:"0.0",  color:"1.0, 1.0, 1.0"}), [reflection, color]);


    var fresnel = new Omega3D.Object3D( geom, mat );


    fresnel.SetPosition(  7.0, -3, 0.0);
    main_scene.addChild( fresnel, true );

    this.render = function(){
        fresnel.Rotate( 0.01, 0.02, 0.0);
    }

};
