BasicObject = function(main_scene, img ){
    this.object = new Omega3D.Object3D(
        new Omega3D.Mesh( new Omega3D.Geometry.SphereGeometry(1.0,25.0,25.0) ),
        new Omega3D.BasicMaterial([new Omega3D.BasicTexture(img)], main_scene));
    this.object.SetPosition( 0, -2, 0.0);
    main_scene.addChild(  this.object, true );

    this.render = function(){
        this.object.Rotate( 0.01, 0.02, 0.0);
    }
};