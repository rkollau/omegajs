AtlasObject = function(renderer, main_chain, main_scene, atlas_img){
    var atlas = new OMEGA.Omega3D.Atlas( new Rect(0, 0, 1024, 1024));
    for(var y = 0; y < 4; y++) {
        for(var x = 0; x < 7; x++) {
            if(x ==6 && y == 3) break;
            atlas.AddSegment((y * 5 + x).toString(), new Rect( (x * 130), ( y *150), 140, 160));
        }
    }

    var atlas_mat = new Omega3D.BasicMaterial([new Omega3D.BasicTexture(atlas_img)], null);
    atlas_mat.SetAtlas( atlas );

    //geometries.
    var g    = new Omega3D.Geometry.SquareGeometry(2.1, 3.0,10,10);
    var m    = new Omega3D.Mesh(g);

    //create a blendObject3d for transparent textures.
    var atlasObject = new Omega3D.BlendObject3D(m, atlas_mat);

    //position object.
    atlasObject.SetRotation(Math.PI,0,Math.PI);
    atlasObject.SetPosition( 0, -3, 24);

    //add to scene.
    main_scene.addChild( atlasObject );
};
