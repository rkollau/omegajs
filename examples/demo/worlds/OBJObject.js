OBJObject = function( main_scene, obj_img ){
    var astroboy_material = new Omega3D.BasicMaterial([new Omega3D.BasicTexture(obj_img)], main_scene);
    var oo       = new Omega3D.parsers.OBJParser(false);
    var astroboy =  oo.ParseToObject3D("../../obj/astroboy.obj",0.25, astroboy_material);
    astroboy.SetPosition(-8, 0, -10 );
    astroboy.SetRotation(0, -Math.PI, Math.PI);

    main_scene.addChild( astroboy, true );
}