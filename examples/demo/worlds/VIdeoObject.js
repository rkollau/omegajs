VideoObject = function(main_scene){
    var video = document.createElement( "video" );
    video.preload = "auto";
    video.oncanplay = startVideo;
    video.ended = videoDone;
    video.addEventListener("ended", videoDone, false);
    setVideo();
    video.autoPlay = true;
    video.controls = true;


    var video_texture = new Omega3D.BasicTexture(video, true);
    var cube        = new Omega3D.Object3D(
                         new Omega3D.Mesh(
                             new Omega3D.Geometry.CubeGeometry(4.0, 4.0, 4.0) ),
                             new Omega3D.BasicMaterial([video_texture], main_scene));
    cube.SetPosition( 10, -2, -10);
    main_scene.addChild( cube, true );


    function setVideo(){
        if (video.canPlayType('video/ogg').length > 0) {
            video.src   = "../../videos/sintel_trailer-480p.ogv";
        }else if(video.canPlayType('video/mp4').length > 0 ){
            video.src   = "../../videos/sintel_trailer-480p.mp4";
        }
    }
    function startVideo(){
        video.play();
    }
    function videoDone(e){
        setVideo();
    }

    this.render = function(){
       cube.Rotate( 0, 0.005, 0 );
    }
}
