ParticlesObject = function( main_scene, particle_img ){

   this.vs = [
       "uniform float uTime;",
        "varying vec2 vTexCoord;",
        "varying vec4 flag;",
        "void main(void){",
       "flag = vec4(1.0);",
        "vec4 newPos  = vec4(aVertexPos.xyz, 1.0);",
        "newPos.z  += uTime;",
       // "if(newPos.z  > -1.0);",
      // "newPos.z  -= 999.0;",
       "gl_PointSize = cos(uTime) * 30.0;",
       //"if( gl_PointSize < 10.0) flag = smoothstep(0.0, 0.4, gl_PointSize) * vec4(1.0, 0.0, 0.0, 1.0);",
        "vec4 pos  = uProjectionMatrix * uViewMatrix * uModelMatrix * vec4(newPos.xyz, 1.0);",


        "gl_Position = pos;",
        "vTexCoord = aTextureCoord;",
       // "flag = clamp(gl_PointSize, 0.0, 1.0);",
        "}"
    ].join("\n");


    this.fs = [
        "uniform sampler2D tex;",
        "varying vec2 vTexCoord;",
        "varying vec4 flag;",
        "void main(void){",
        "vec4 color = texture2D(tex, gl_PointCoord)*flag;",

        "gl_FragColor = color;",
        "}"
    ].join("\n");

    var particle_mat = new Omega3D.ShaderMaterial({
        uniforms:{
            tex:{
                type:"sampler2D",
                value:new Omega3D.BasicTexture(particle_img)
            },
            uTime:{
                type:"float",
                value:main_scene.getTime()
            }

        },
        vertex_src:this.vs,
        fragment_src:this.fs
    }, null);



    var emitter   = new Omega3D.Particles.ParticleEmitter(particle_mat);
    var particles = new Array();
    var spread = 100.0;
    var position = new Vector4(0, -spread/2, 0);
    for( var i = 0; i < 500; i++){
        var xpos = Math.random() * spread - (spread/2);
        var ypos = Math.random() * (spread/2);
        var zpos = Math.random() * spread - (spread/2);
        particles.push(position.x +  xpos,position.y + ypos, position.z + zpos);

    }
    emitter.AddParticles(particles);
    main_scene.addChild(emitter);

    this.render = function(){
        particle_mat.SetValue("uTime", main_scene.getTime());
    }
};