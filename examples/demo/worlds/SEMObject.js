SEMObject = function(main_scene, sem_img, finalScene){
    var torus = new Omega3D.Object3D(
        new Omega3D.Mesh( new Omega3D.Geometry.SphereGeometry(2.0, 50.0,50.0) ),
        new Omega3D.SEMMaterial([new Omega3D.BasicTexture(sem_img, false)], main_scene));
    torus.SetPosition( -8.0, -3, 0.0);
    main_scene.addChild( torus, true );

    this.render = function(){
        torus.Rotate( 0.01, 0.02, 0.005);
    }
};

