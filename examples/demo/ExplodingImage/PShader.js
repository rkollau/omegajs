OMEGA.Omega3D.Shaders.PShader = function(){
    var vs = [
        OMEGA.Omega3D.Shaders.Components.Basic_Includes(),
        "varying vec4 vColor;",

        "void main(void){",
        "gl_PointSize = 1.0;",
        OMEGA.Omega3D.Shaders.Components.Vertex_World_Conversion_V3("gl_Position", "aVertexPos"),

        "vColor  = vec4(aVertexColor, 1.0);", 
        "}"
    ].join("\n");

    var fs = [
        OMEGA.Omega3D.Shaders.Components.Fragment_Precision_Mediump_Float(),
        OMEGA.Omega3D.Shaders.Components.Basic_Fragment_Includes(),
        "varying vec4 vColor;",

        "void main(void){",
        "vec4 color = vColor;",
        OMEGA.Omega3D.Shaders.Components.Basic_Fragment_Logic("vec4(color.rgb, color.a)"),
        "}"
    ].join("\n");


    var uniforms = OMEGA.Omega3D.Shaders.Components.StandardUniforms();
    var fragmentOnlyUniforms = {};
    var custom_uniforms = {};
    return new OMEGA.Omega3D.Shader(vs,fs,  [OMEGA.Omega3D.Shaders.Components.StandardAttributes(),
        uniforms,
        custom_uniforms,
        fragmentOnlyUniforms]);

};
OMEGA.Omega3D.Shaders.PingPongShader = function(){
    var vs = [
        OMEGA.Omega3D.Shaders.Components.Basic_Includes(),
        "varying vec4 vVertexNormal;",
        "varying vec4 vVertexPosition;",
        "varying vec2 vTexCoord;",

        "void main(void){",
        OMEGA.Omega3D.Shaders.Components.Vertex_World_Conversion_V3("gl_Position", "aVertexPos"),
        "vVertexNormal   = uModelMatrix * vec4(aVertexNormal, 1.0);",
        "vVertexPosition =  uModelMatrix * vec4(aVertexPos, 1.0);",
        "vTexCoord       = aTextureCoord;",
        "}"
    ].join("\n");

    var fs = [
        "#extension GL_EXT_draw_buffers : require",
        "#extension GL_OES_standard_derivatives : enable",
        OMEGA.Omega3D.Shaders.Components.Fragment_Precision_Highp_Float(),
        OMEGA.Omega3D.Shaders.Components.Basic_Fragment_Includes(),
        "uniform sampler2D uSampler;",
        
        "varying vec2 vTexCoord;",
        "varying vec4 vVertexNormal;",
        "varying vec4 vVertexPosition;",

        "void main(void){",
            "float depth    = vVertexPosition.z / vVertexPosition.w * 0.1;",
            "gl_FragData[0] = vec4( vVertexNormal.xyz, 1.0);",
            "gl_FragData[1] = vec4( vVertexPosition.xyz, 1.0);",
            "gl_FragData[2] = texture2D(uSampler, vec2(vTexCoord.s, vTexCoord.t));",
            "gl_FragData[3] = vec4( depth,depth,depth, 1.0);",
        "}"
    ].join("\n");


    var uniforms = OMEGA.Omega3D.Shaders.Components.StandardUniforms();
    var fragmentOnlyUniforms = {};
    fragmentOnlyUniforms["uSampler"] = { id:"uSampler", type: "sampler2D", glsl: "", value: null };
    
    var custom_uniforms = {};
    return new OMEGA.Omega3D.Shader(vs,fs,  [OMEGA.Omega3D.Shaders.Components.StandardAttributes(),
        uniforms,
        custom_uniforms,
        fragmentOnlyUniforms]);

};