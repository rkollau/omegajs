function ParticleImage( image, width, height ){
    var m = new OMEGA.Omega3D.Mesh( new OMEGA.Omega3D.Geometry());
    var shader = new Omega3D.Shaders.PShader();
    var material = new Omega3D.Material(shader, []);
    var data = Omega3D.ReadPixels( image,0, 0, width, height );
    

    Object3D.apply(this, [m, material]);
    this.alphaBlend = true;
    this.drawType = OMEGA.Omega3D.Object3D.POINTS;
    this.sortParticles = true;
    this.particles = new Array();
    this.uvs       = new Array();
    this.normals   = new Array();
    this.colors    = new Array();
    
    this.tangents   = new Array();
    this.bitangents = new Array();

    var step   = 0.005;
    var xpos =-(step*(width/2)), ypos=-(step*(height/2)), zpos=0;
    var xCount = 0;
    var yCount = 0;

    for(var i = 0;i < data.length; i+=4 ){


        this.particles.push(xpos, ypos, zpos );
        this.uvs.push(xpos, ypos);
        this.normals.push( 0, 0, 1 );
        this.tangents.push( 0, 1, 0);
        this.bitangents.push(1, 0, 0 );
        this.colors.push( data[i]/255, data[i+1]/255, data[i+2]/255 );


        xpos += step*0.55;
        if(xCount == width-1){
            xpos = -(step*(width/2));
            xCount = 0;
            yCount++;
            ypos += step/1.5;
        }else{
            xCount++;
        }
        zpos = 0;

        //skip transparent pixels.
       // if( data[i+3] == 0)continue;

        

        

        
       
    }

    this.alphaBlend = false;
    this.GetMesh().GetGeometry().SetVertices( this.particles );
     this.GetMesh().GetGeometry().SetUVS    ( this.uvs     );
    this.GetMesh().GetGeometry().SetNormals ( this.normals );
    this.GetMesh().GetGeometry().SetColors  ( this.colors  );
    this.GetMesh().CreateBuffers();







    this.AddParticle = function( p, color ){
        this.particles.push(p.x, p.y, p.z);
        this.colors.push( color.x, color.y, color.z );
        this.GetMesh().GetGeometry().SetVertices( this.particles );
        this.GetMesh().GetGeometry().SetNormals( this.colors );
        this.GetMesh().CreateBuffers();
    };
    this.AddParticles = function( particles ){
        this.particles = particles;
        this.colors = colors;
        this.GetMesh().GetGeometry().SetVertices( this.particles );
        this.GetMesh().GetGeometry().SetNormals( this.colors );
        this.GetMesh().CreateBuffers();
    };

    this.Update = function(gl,camera){
        //for(var i = 0; i < this.GetMesh().combined.length ; i+=17){
        //    var r = (Math.random()) * 0.5;
        //    this.GetMesh().combined[i] += r;
        //    this.GetMesh().combined[i+1] += r;
        //    this.GetMesh().combined[i+2] += r;
        //
        //}
        //
        //gl.bindBuffer( gl.ARRAY_BUFFER, this.GetMesh().combinedBuffer );
        //gl.bufferData(gl.ARRAY_BUFFER, new Float32Array( this.GetMesh().combined ), gl.STATIC_DRAW );


        if(this.alphaBlend ){
            gl.disable(gl.DEPTH_TEST);
            gl.enable(gl.BLEND);
            gl.blendEquation( gl.FUNC_ADD  );
            gl.blendFunc(gl.ONE, gl.ONE);
        }
    };
    this.LateUpdate = function(gl,camera){
        if(this.alphaBlend){
            gl.enable(gl.DEPTH_TEST);
            gl.disable(gl.BLEND);
        }
    };


    this.sort = function(camera){
        this.particles.sort(function(a,b){
            return a[2] - b[2];
        })
    }

}
ParticleEmitter.prototype = new Object3D();
OMEGA.Omega3D.Particles = OMEGA.Omega3D.Particles || {};
OMEGA.Omega3D.Particles.ParticleImage = ParticleImage;

