OMEGA.Omega3D.Shaders.MRTShader = function(){
    var vs = [
        OMEGA.Omega3D.Shaders.Components.Basic_Includes(),
        "varying vec4 vVertexNormal;",
        "varying vec4 vVertexPosition;",
        "varying vec2 vTexCoord;",

        "void main(void){",
        OMEGA.Omega3D.Shaders.Components.Vertex_World_Conversion_V3("gl_Position", "aVertexPos"),
        "vVertexNormal = uModelMatrix * vec4(aVertexNormal, 1.0);",
        "vVertexPosition =  uModelMatrix * vec4(aVertexPos, 1.0);",
        "vTexCoord       = aTextureCoord;",
        "}"
    ].join("\n");

    var fs = [
        "#extension GL_EXT_draw_buffers : require",
        "#extension GL_OES_standard_derivatives : enable",

        OMEGA.Omega3D.Shaders.Components.Fragment_Precision_Highp_Float(),
        OMEGA.Omega3D.Shaders.Components.Basic_Fragment_Includes(),
        "varying vec2 vTexCoord;",
        "uniform sampler2D uSampler;",

        "varying vec4 vVertexNormal;",
        "varying vec4 vVertexPosition;",

        "void main(void){",
        "float depth = vVertexPosition.z / vVertexPosition.w * 0.1;",
        "gl_FragData[0] = vec4(vVertexNormal.xyz, 1.0);",
        "gl_FragData[1] = vec4( vVertexPosition.xyz, 1.0);",
        "gl_FragData[2] = texture2D(uSampler, vec2(vTexCoord.s, vTexCoord.t));",
        "gl_FragData[3] = vec4( depth,depth,depth, 1.0);",

        "}"
    ].join("\n");

    var fragmentOnlyUniforms = {};
    fragmentOnlyUniforms["uSampler"] = { id:"uSampler", type: "sampler2D", glsl: "", value: null };
    return new OMEGA.Omega3D.Shader(vs,fs,  [OMEGA.Omega3D.Shaders.Components.StandardAttributes(),
        OMEGA.Omega3D.Shaders.Components.StandardUniforms(),
        {},
        fragmentOnlyUniforms]);

};
OMEGA.Omega3D.Shaders.DeferredRenderingShader = function(){
    var vs = [
        OMEGA.Omega3D.Shaders.Components.Basic_Includes(),
        "varying vec4 vVertexNormal;",
        "varying vec4 vVertexPosition;",
        "varying vec2 vTexCoord;",
        "varying mat4 vViewMatrix;",

        "void main(void){",
        OMEGA.Omega3D.Shaders.Components.Vertex_World_Conversion_V3("gl_Position", "aVertexPos"),
        "vTexCoord  = aTextureCoord;",
        "vViewMatrix = uViewMatrix;",
        "}"
    ].join("\n");

    var fs = [
        OMEGA.Omega3D.Shaders.Components.Fragment_Precision_Highp_Float(),
        OMEGA.Omega3D.Shaders.Components.Basic_Fragment_Includes(),
        "varying vec2 vTexCoord;",
        "uniform sampler2D uSampler;",
        "uniform sampler2D uSampler_1;",
        "uniform sampler2D uSampler_2;",
        "uniform sampler2D uSampler_3;",
        "varying mat4 vViewMatrix;",


        "void main(void){",
            "vec4 normal    = texture2D(uSampler  , vTexCoord );",
            "vec4 position  = texture2D(uSampler_1, vTexCoord );",
            "vec4 image     = texture2D(uSampler_2, vTexCoord );",
            "vec4 depth     = texture2D(uSampler_3, vTexCoord );",

            "normal = normalize(normal);",

            "vec3 light    = (vViewMatrix * vec4(1,10,-1, 1.0)).xyz;",


            //diffuse.
            "vec3 lightVec          = vec3(position) - light;",
            "float lightLength      =  length(lightVec);",
            "vec3 lightDir          = normalize(lightVec/lightLength);",
            "lightLength            = lightLength * lightLength;",
            "float dirLightWeight   = max(dot(normal.xyz, lightDir), 0.0);",

            //specular.
            "vec3 eyeWorldPos       = vec3(vViewMatrix[3].xyz);",
            "vec3 viewVectorEye     = normalize(position.xyz - eyeWorldPos );",
            "vec3 halfVector        = normalize( viewVectorEye + lightDir );",
            "float specularWeight = min(1.0, pow(max(dot(normal.xyz, halfVector), 0.0), 25.0));",



            //elements.
            "vec3 diffuse  = (image.xyz *  dirLightWeight);",
            "vec3 ambient  = vec3(0, 0, 0);",
            "vec3 specular = vec3(1.0, 1.0, 1.0) * specularWeight * 2.0;",


//
            "vec4 color = vec4(vec3(ambient + diffuse + specular), 1.0);",


            OMEGA.Omega3D.Shaders.Components.Basic_Fragment_Logic("vec4(color.rgb, color.a)"),
        "}"
    ].join("\n");

    var fragmentOnlyUniforms = {};
    fragmentOnlyUniforms["uSampler"]   = { id:"uSampler", type: "sampler2D", glsl: "", value: null };
    fragmentOnlyUniforms["uSampler_1"] = { id:"uSampler_1", type: "sampler2D", glsl: "", value: null };
    fragmentOnlyUniforms["uSampler_2"] = { id:"uSampler_2", type: "sampler2D", glsl: "", value: null };
    fragmentOnlyUniforms["uSampler_3"] = { id:"uSampler_3", type: "sampler2D", glsl: "", value: null };
    return new OMEGA.Omega3D.Shader(vs,fs,  [OMEGA.Omega3D.Shaders.Components.StandardAttributes(),
        OMEGA.Omega3D.Shaders.Components.StandardUniforms(),
        {},
        fragmentOnlyUniforms]);

};