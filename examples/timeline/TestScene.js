TestScene = function( images, scene, camera )
{
    //create material and sphere
    var material    = new Omega3D.BasicMaterial( [new Omega3D.BasicTexture(images[6]) ], scene );
    var material2    = new Omega3D.BasicMaterial( [new Omega3D.BasicTexture(images[8]) ], scene );
   // var material    = new Omega3D.Material( new OMEGA.Omega3D.Shaders.BumpMapping(scene), [new Omega3D.BasicTexture(images[6]),new Omega3D.BasicTexture(images[7]) ] );
    var sphere = new Omega3D.Object3D(
                        new Omega3D.Mesh( new Omega3D.Geometry.TorusGeometry( 1.0, 0.5, 60, 60)),
                        material
                    );

    //var sphere2 = new Omega3D.Object3D(
    //                    new Omega3D.Mesh( new Omega3D.Geometry.SphereGeometry( 2.1, 60, 60)),
    //                    material2
    //                );
    //console.log( sphere.mesh["geometry"] );
    this.Awake = function()
    {
        if(sphere.parent == null )
            scene.addChild( sphere );

        //if(sphere2.parent == null )
        //    scene.addChild( sphere2 );
    };

    this.Start = function()
    {
        sphere.mayRender = true;
       // sphere2.mayRender = true;
    };

    this.Update = function( time )
    {
        sphere.Rotate( 0.0001, 0.001, 0 );
        //sphere2.Rotate( 0.0001, 0.0012, 0 );
    };

    this.End = function()
    {
        sphere.mayRender = false;
       // sphere2.mayRender = false;
    };

    this.Sleep = function()
    {
        scene.removeChild( sphere );
        //scene.removeChild( sphere2 );
    };
};
