OrbitCameraClip = function( camera, anchor, radius )
{

    var angleXY = 0;
    var angleXZ = 0;
    var pos = new Vector4(0, 0, 0);

    this.Awake = function()
    {
        camera.SetPosition( pos.x, pos.y, pos.z );
    };

    this.Start = function()
    {

    };

    this.Update = function( time )
    {
        angleXY += 0.001;
        angleXZ += 0.002;

        pos.x = Math.cos( angleXZ ) * radius * Math.cos( angleXY );
        pos.y = Math.sin( angleXZ ) * radius;
        pos.z = Math.cos( angleXZ ) * radius * Math.sin( angleXY );

       // camera.SetPosition( pos.x, pos.y, pos.z );
        camera.LookAt( anchor.x, anchor.y, anchor.z, [pos.x, pos.y, pos.z ])
    };

    this.End = function()
    {

    };

    this.Sleep = function()
    {

    };
};
